/*
 * Copyright (c) 2020 Nguyen Hoang Lam.
 * All rights reserved.
 */

package com.nguyenhoanglam.imagepicker.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.nguyenhoanglam.imagepicker.R
import com.nguyenhoanglam.imagepicker.model.Config

class DoneButtonView : RelativeLayout {

    private lateinit var doneText: TextView

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        View.inflate(context, R.layout.imagepicker_donebutton, this)
        doneText = findViewById(R.id.btn_done)
    }

    fun config(config: Config) {
        setBackgroundColor(config.getToolbarColor())
        doneText.text = config.doneTitle
        doneText.setTextColor(config.getToolbarTextColor())
        doneText.visibility = if (config.isAlwaysShowDoneButton) View.VISIBLE else View.GONE
    }

    fun setDoneText(text :String){
        doneText.text = text
    }


    fun showDoneButton(isShow: Boolean) {
        doneText.visibility = if (isShow) View.VISIBLE else View.GONE
    }

    fun setOnDoneClickListener(clickListener: OnClickListener) {
        doneText.setOnClickListener(clickListener)
    }
}