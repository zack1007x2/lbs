/*
 * Copyright (c) 2020 Nguyen Hoang Lam.
 * All rights reserved.
 */

package com.nguyenhoanglam.imagepicker.ui.imagepicker

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nguyenhoanglam.imagepicker.R
import com.nguyenhoanglam.imagepicker.helper.*
import com.nguyenhoanglam.imagepicker.listener.OnFolderClickListener
import com.nguyenhoanglam.imagepicker.listener.OnImageSelectListener
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Folder
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.ui.camera.DefaultCameraModule
import com.nguyenhoanglam.imagepicker.ui.camera.OnImageReadyListener
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.imagepicker_activity_imagepicker.*
import java.io.File

class ImagePickerActivity : AppCompatActivity(), OnFolderClickListener, OnImageSelectListener {

    private var config: Config? = null
    private lateinit var viewModel: ImagePickerViewModel
    private val cameraModule = DefaultCameraModule()
    private val logger = LogHelper.instance

    private val backClickListener = View.OnClickListener { onBackPressed() }
    private val cameraClickListener = View.OnClickListener { captureImageWithPermission() }
    private val doneClickListener = View.OnClickListener { onDone() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent == null) {
            finish()
            return
        }

        config = intent.getParcelableExtra(Config.EXTRA_CONFIG)
        setContentView(R.layout.imagepicker_activity_imagepicker)

        viewModel = ViewModelProvider(this, ImagePickerViewModelFactory(this.application)).get(ImagePickerViewModel::class.java)
        viewModel.setConfig(config!!)
        viewModel.selectedImages.observe(this, Observer {
//            toolbar.showDoneButton(config!!.isAlwaysShowDoneButton || it.isNotEmpty())
            donebar.showDoneButton(config!!.isAlwaysShowDoneButton || it.isNotEmpty())
        })

        setupViews()
    }

    override fun onResume() {
        super.onResume()
        fetchDataWithPermission()
    }

    private fun setupViews() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = config!!.getStatusBarColor()
        }

        toolbar.config(config!!)
        toolbar.setOnBackClickListener(backClickListener)
        toolbar.setOnCameraClickListener(cameraClickListener)
        donebar.config(config!!)
        donebar.setOnDoneClickListener(doneClickListener)
        //移動DONE位置
//        toolbar.setOnDoneClickListener(doneClickListener)

        val initialFragment = if (config!!.isFolderMode) FolderFragment.newInstance() else ImageFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, initialFragment)
            .commit()
    }


    private fun fetchDataWithPermission() {
        val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        PermissionHelper.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, object : PermissionHelper.PermissionAskListener {
            override fun onNeedPermission() {
                PermissionHelper.requestAllPermissions(this@ImagePickerActivity, permissions, Config.RC_WRITE_EXTERNAL_STORAGE_PERMISSION)
            }

            override fun onPermissionPreviouslyDenied() {
                PermissionHelper.requestAllPermissions(this@ImagePickerActivity, permissions, Config.RC_WRITE_EXTERNAL_STORAGE_PERMISSION)
            }

            override fun onPermissionDisabled() {
                snackbar.show(R.string.imagepicker_msg_no_write_external_storage_permission, View.OnClickListener {
                    PermissionHelper.openAppSettings(this@ImagePickerActivity)
                })
            }

            override fun onPermissionGranted() {
                fetchData()
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            Config.RC_WRITE_EXTERNAL_STORAGE_PERMISSION -> {
                if (PermissionHelper.hasGranted(grantResults)) {
                    logger?.d("Write External permission granted")
                    fetchData()
                } else {
                    logger?.e("Permission not granted: results len = " + grantResults.size)
                    logger?.e("Result code = " + if (grantResults.isNotEmpty()) grantResults[0] else "(empty)")
                    finish()
                }
            }
            Config.RC_CAMERA_PERMISSION -> {
                if (PermissionHelper.hasGranted(grantResults)) {
                    logger?.d("Camera permission granted")
                    captureImage()

                } else {
                    logger?.e("Permission not granted: results len = " + grantResults.size + " Result code = " + if (grantResults.isNotEmpty()) grantResults[0] else "(empty)")
                }
            }
            else -> {
                logger?.d("Got unexpected permission result: $requestCode")
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    private fun fetchData() {
        viewModel.fetchImages()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val fragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)
        if (fragment != null && fragment is FolderFragment) {
            toolbar.setTitle(config!!.folderTitle)
        }
    }

    private fun onDone() {
        val selectedImages = viewModel.selectedImages.value
        if (selectedImages != null && selectedImages.isNotEmpty()) {
            var i = 0
            while (i < selectedImages.size) {
                val (_, _, _, path) = selectedImages[i]
                val file = File(path)
                if (!file.exists()) {
                    selectedImages.removeAt(i)
                    i--
                }
                i++
            }
            finishPickImages(selectedImages)
        } else {
            finishPickImages(arrayListOf())
        }
    }


    private fun captureImageWithPermission() {
        val permissions = arrayOf(Manifest.permission.CAMERA)
        PermissionHelper.checkPermission(this, Manifest.permission.CAMERA, object : PermissionHelper.PermissionAskListener {
            override fun onNeedPermission() {
                PermissionHelper.requestAllPermissions(this@ImagePickerActivity, permissions, Config.RC_CAMERA_PERMISSION)
            }

            override fun onPermissionPreviouslyDenied() {
                PermissionHelper.requestAllPermissions(this@ImagePickerActivity, permissions, Config.RC_CAMERA_PERMISSION)
            }

            override fun onPermissionDisabled() {
                snackbar.show(R.string.imagepicker_msg_no_camera_permission, View.OnClickListener {
                    PermissionHelper.openAppSettings(this@ImagePickerActivity)
                })
            }

            override fun onPermissionGranted() {
                captureImage()
            }
        })
    }


    fun captureImage() {
        if (!CameraHelper.checkCameraAvailability(this)) {
            return
        }

        val intent = cameraModule.getCameraIntent(this, config!!)
        if (intent == null) {
            ToastHelper.show(this, getString(R.string.imagepicker_error_create_image_file))
            return
        }
        startActivityForResult(intent, Config.RC_CAPTURE_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Config.RC_CAPTURE_IMAGE->{
                if (resultCode == Activity.RESULT_OK) {
//                    finishCaptureImage() 關閉相簿中相機功能
                } else {
                    setResultCancelled()
                }
            }

            UCrop.REQUEST_CROP -> if (resultCode == RESULT_OK) {
                handleUCropResult(data)
            } else {
                setResultCancelled()
            }
            UCrop.RESULT_ERROR -> {
                val cropError = data?.let { UCrop.getError(it) }
                Log.e("Zack","Crop error: $cropError")
                setResultCancelled()
            }
            else -> setResultCancelled()

        }
    }

    private fun finishPickImages(images: ArrayList<Image>) {

        if(images.size==1){
            viewModel.tmpImage = images[0]
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                cropImage(images[0].uri)
            else
                cropImage(Uri.parse(images[0].path))
        }else{
            val data = Intent()
            data.putParcelableArrayListExtra(Config.EXTRA_IMAGES, images)
            setResult(Activity.RESULT_OK, data)
            finish()
        }

    }


    override fun onFolderClick(folder: Folder) {
        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, ImageFragment.newInstance(folder.bucketId))
            .addToBackStack(null)
            .commit()
        toolbar.setTitle(folder.name)
    }

    override fun onSelectedImagesChanged(selectedImages: ArrayList<Image>) {
        viewModel.selectedImages.value = selectedImages
        when(selectedImages.size){
            0,1->{
                donebar.setDoneText("確定")
            }
            else->{
                donebar.setDoneText("確定（"+selectedImages.size+"/"+config!!.maxSize+")")
            }
        }
    }

    override fun onSingleModeImageSelected(image: Image) {
        finishPickImages(ImageHelper.singleListFromImage(image))
    }

    private fun cropImage(sourceUri: Uri?) {
        Log.e("Zack","sourceUri: $sourceUri")
        var sourceUri_:Uri
        val path = File(externalCacheDir, "camera")
        if (!path.exists()) path.mkdirs()
        val destinationUri=Uri.fromFile(File(path, System.currentTimeMillis().toString() + ".jpg"))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            sourceUri_ = Uri.fromFile(File(ImageHelper.getRealPathFromUri(this, sourceUri)!!))
        }else {
            sourceUri_ = Uri.fromFile(File(sourceUri.toString()))
        }

        viewModel.tmpImage!!.uri = destinationUri
        viewModel.tmpImage!!.path = destinationUri.toString()

        Log.e("Zack","destinationUri: $destinationUri")

        val options = UCrop.Options()
        options.setCompressionQuality(80)

        // applying UI theme
        options.setToolbarColor(config!!.getToolbarColor())
        options.setStatusBarColor(config!!.getStatusBarColor())
        options.setActiveWidgetColor(config!!.getToolbarColor())
        options.withAspectRatio(1f, 1f)

        sourceUri_?.let {
            UCrop.of(it, destinationUri!!)
                .withOptions(options)
                .start(this)
        }
    }

    private fun handleUCropResult(data: Intent?) {
        if (data == null) {
            setResultCancelled()
            return
        }
        val resultUri = UCrop.getOutput(data)
        resultUri?.let { setResultOk(it) }
    }

    private fun setResultOk(imagePath: Uri) {
        val data = Intent()
        data.putParcelableArrayListExtra(Config.EXTRA_IMAGES, ImageHelper.singleListFromImage(viewModel.tmpImage!!))
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    private fun setResultCancelled() {
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }
}