/*
 * Copyright (c) 2020 Nguyen Hoang Lam.
 * All rights reserved.
 */

package com.nguyenhoanglam.imagepicker.ui.camera

import android.net.Uri
import com.nguyenhoanglam.imagepicker.model.Image

interface OnImageReadyListener {
    fun onImageReady(images: ArrayList<Image>, uri: Uri?, path:String?)
    fun onImageNotReady()
}