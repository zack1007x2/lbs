package com.app.lbs.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.app.lbs.R
import com.app.lbs.adapters.MissionListPagerAdapterOwner
import com.app.lbs.adapters.MissionListPagerAdapterTaker
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.Const.TOHISTORY
import com.astuetz.PagerSlidingTabStrip
import com.google.android.material.bottomsheet.BottomSheetBehavior

/**
 * @file MissionManagementFragment
 * @brief
 *       管理任務分頁功能
 *
 * @author Zack
 * */

class MissionManagementFragment : BaseFragment() {

    lateinit var mViewModel: MainViewModel
    private var viewPager: ViewPager? = null
    private var tabStrip: PagerSlidingTabStrip? = null
    private var root:View?=null
    private var lastPosition=0
    private var mFragmentStatePagerAdapter: FragmentStatePagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        root = inflater.inflate(R.layout.fragment_mission_management, container, false)

//        CustomBottomSheetDialogFeedBack().show(requireActivity())

        return root
    }

    private fun initView(root: View) {
        viewPager = root.findViewById(R.id.viewPager) as? ViewPager
        viewPager!!.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                lastPosition = position
//                mViewModel.initNewMissionObject()
            }

        })
        tabStrip = root.findViewById(R.id.pagerTabStrip)

    }

    companion object {
        const val FRAGMENT_ID = R.id.tab_mission_management
    }
    var obs = Observer<Const.USERSTATE> { state ->
        Log.d("state:::$state | "+ (mFragmentStatePagerAdapter==null)+" | "+(mFragmentStatePagerAdapter is MissionListPagerAdapterTaker))
        if(state != Const.USERSTATE.DEFAULT){
            if (state == Const.USERSTATE.OWNER) {
                tabStrip?.setIndicatorColorResource(R.color.owner_red)
                tabStrip?.setTextColorResource(R.color.owner_red)
                tabStrip?.textSize = resources.getDimensionPixelSize(R.dimen.text_size_ss)
                mFragmentStatePagerAdapter = MissionListPagerAdapterOwner(
                    activity as MainActivity,
                    childFragmentManager
                )
                if(lastPosition==0){
                    mViewModel.initNewMissionObject()
                }
            } else if (state == Const.USERSTATE.TAKER) {
                tabStrip?.setIndicatorColorResource(R.color.taker_blue)
                tabStrip?.setTextColorResource(R.color.taker_blue)
                tabStrip?.textSize = resources.getDimensionPixelSize(R.dimen.text_size_s)
                mFragmentStatePagerAdapter = MissionListPagerAdapterTaker(
                    activity as MainActivity,
                    childFragmentManager
                )
            }
            viewPager?.adapter = mFragmentStatePagerAdapter
            mFragmentStatePagerAdapter!!.notifyDataSetChanged()
            if(mViewModel.lastState != state){
                viewPager?.currentItem = 0
            }else{
                viewPager?.currentItem = lastPosition
            }
            var arg :Bundle? =null
            try{
                arg =requireArguments()
            }catch (e:Exception){
                Log.e(e)
            }

            arg?.let {
                if(it.getBoolean(TOHISTORY,false)){
                viewPager?.currentItem = mFragmentStatePagerAdapter!!.count-1
                }
            }
            try {
                tabStrip?.setViewPager(viewPager)
            }catch (e:IllegalStateException){
                Log.e(e)
            }
            tabStrip?.notifyDataSetChanged()
        }
    }
    override fun onResume() {
        super.onResume()
        setTitle(R.string.title_mission_management)
        mViewModel.updateOwnAndTakenJobList()
        initView(root!!)
        mViewModel.getUserState().observe(this.viewLifecycleOwner, obs)
    }

    override fun onPause() {
        super.onPause()
        mViewModel.getUserState().removeObserver(obs)
    }

    var bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(bottomSheet: View, slideOffset: Float) {
        }

        override fun onStateChanged(bottomSheet: View, newState: Int) {
        }

    }

    override fun refreshUI() {
        super.refreshUI()
        setTitle(R.string.title_mission_management)
    }
}


