package com.app.lbs.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.databinding.FragmentSettingListAccountAddBindingImpl
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.Utils

/**
 * @file SettingListAccountAddFragment
 * @brief
 *       銀行帳戶及信用卡範例頁面
 *
 * @author Zack
 * */

class SettingListAccountAddFragment: BaseFragment(), View.OnClickListener {
    private lateinit var mViewModel: MainViewModel
    var listItems = ArrayList<String>()
    var adapter: ArrayAdapter<String>? = null
    private lateinit var lv_acc_list:ListView
    private lateinit var btn_add_acc: Button
    private var isBank=false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        val binding: FragmentSettingListAccountAddBindingImpl = DataBindingUtil.inflate(
            inflater, R.layout.fragment_setting_list_account_add, container, false
        )
        val root = binding.root
        binding.mainViewModel = mViewModel
        val bundle = this.arguments
        if (bundle != null) {
            isBank = bundle.getBoolean(Const.ISBANK, false)
            Log.e("bundle: $isBank")
        }
        initView(root)
        return root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initView(v: View) {
        lv_acc_list = v.findViewById(R.id.lv_acc_list)
        btn_add_acc = v.findViewById(R.id.btn_add_acc)
        btn_add_acc.setOnClickListener(this)
        adapter = object:ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_list_item_1,
            listItems
        ){}
        lv_acc_list.adapter = adapter

    }

    override fun onPause() {
        super.onPause()
        listItems.clear()
    }
    companion object {
        const val FRAGMENT_ID = R.id.ll_title_bank_account
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_add_acc->{
                if(isBank)
                    listItems.add("bank account : "+ Utils.getRandomAcc("BK"))
                else
                    listItems.add("Credit : "+ Utils.getRandomAcc("CC"))
                adapter!!.notifyDataSetChanged()
            }
        }
    }
}

