package com.app.lbs.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.models.Mission
import com.app.lbs.ui.viewmodels.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.jetbrains.anko.backgroundResource


/**
 * @file CustomBottomSheetDialogCancel
 * @brief
 *       任務取消通知實作
 *
 * @param mMission:Mission相關資訊用於判斷是誰取消
 * @param isOwner:用於判斷背景色
 *
 * @author Zack
 * */
class CustomBottomSheetDialogCancel(val mMission:Mission, val isOwner: Boolean) : BottomSheetDialogFragment(), View.OnClickListener {
    private var behavior:BottomSheetBehavior<View>?=null
    private lateinit var mViewModel: MainViewModel
    private lateinit var btn_confirm_cancellation: Button
    private lateinit var btn_cancel:Button
    private lateinit var tv_mission_cancel_whom:TextView

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        val view = View.inflate(context, R.layout.custom_bottom_sheet_mission_cancel_request, null)
        dialog.setContentView(view)
        initView(view)
        (view.parent as View).setBackgroundColor(ContextCompat.getColor(requireContext(), android.R.color.transparent))
        behavior = BottomSheetBehavior.from(view.parent as View)
        return dialog
    }

    private fun initView(v: View) {
        btn_confirm_cancellation = v.findViewById(R.id.btn_confirm_cancellation)
        btn_confirm_cancellation.backgroundResource = if(isOwner) R.drawable.btn_owner_rect else R.drawable.btn_taker_rect
        btn_cancel = v.findViewById(R.id.btn_cancel)
        tv_mission_cancel_whom = v.findViewById(R.id.tv_mission_cancel_whom)
        btn_confirm_cancellation.setOnClickListener(this)
        btn_cancel.setOnClickListener(this)
        
        if(mMission.job_status<=0){
            if(mMission.modify_apply<0){
                v.findViewById<TextView>(R.id.dummy_0001).visibility = View.GONE
                v.findViewById<TextView>(R.id.dummy_0002).visibility = View.GONE
                tv_mission_cancel_whom.visibility = View.GONE
            }else{
                tv_mission_cancel_whom.setText("{ UID: ${mMission.modify_apply} }")
            }
        }else{
            if(mMission.job_owner == mViewModel.mUser?.userData?.userId){
                tv_mission_cancel_whom.setText(mMission.taker_firstname+" "+mMission.taker_lastname)
            }else{
                tv_mission_cancel_whom.setText(mMission.owner_firstname+" "+mMission.owner_lastname)
            }
        }

    }

    fun show(fragmentActivity: FragmentActivity) {
        show(fragmentActivity.supportFragmentManager, TAG)
    }

    companion object {
        private const val TAG = "MyBottomSheet"
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_confirm_cancellation->{

            }
            R.id.btn_cancel->{
                dismiss()
            }
        }
    }
}