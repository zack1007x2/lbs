package com.app.lbs.ui.fragments

import android.app.Dialog
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.adapters.MissionListAdapter
import com.app.lbs.adapters.MissionListAdapter.Companion.TYPE_MAIN_LIST
import com.app.lbs.interfaces.IFilterResults
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.Mission
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.CustomClusterRenderer
import com.app.lbs.utils.CustomDialogBuilder
import com.app.lbs.utils.Utils
import com.app.lbs.utils.view.CustomBottomSheetBehavior
import com.app.lbs.utils.view.MapListDrawer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import kotlinx.android.synthetic.main.custom_dialog_bottom_sheet_filter.*
import nl.bryanderidder.themedtogglebuttongroup.ThemedToggleButtonGroup
import kotlin.math.log2


/**
 * @file FindMissionByMapFragment
 * @brief
 *       以地圖形式顯示附近任務
 *
 * @author Zack
 * */


class FindMissionByMapFragment : BaseFragment(), IOnItemClickListener<Mission>,
    View.OnClickListener, CompoundButton.OnCheckedChangeListener, IFilterResults {

    private lateinit var mViewModel: MainViewModel
    private var googleMap: GoogleMap? = null
    var mMapView: MapView? = null
    private var mLocation: Location?=null
    private var mCircle: Circle? = null
    private var clusterManager: ClusterManager<Mission>? = null
    private var mClusterListAdapter: MissionListAdapter? = null
    private var hasInitView = false

    private lateinit var bottomBehavior: CustomBottomSheetBehavior<View>
    private lateinit var ll_bottom_sheet: LinearLayout
    private lateinit var rv_missionList: RecyclerView
    private var mFullMissionList: ArrayList<Mission>? = null
    private lateinit var mMapBottomListDrawer: MapListDrawer

    private var mAdapter: MissionListAdapter? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private lateinit var btngroup_filter: ThemedToggleButtonGroup
    private lateinit var rl_filter: RelativeLayout
    private lateinit var btn_filter_setting: Button
    private var mCustomFilterDialog: Dialog? = null
    private var mUserState:Const.USERSTATE = Const.USERSTATE.DEFAULT
    private var filter_type_array: Array<Boolean> = arrayOf(false, false, false, false, false, false, true, false, false)

    private lateinit var root: View


    private fun initView(root: View) {

        mMapBottomListDrawer = MapListDrawer(root, activity)
        ll_bottom_sheet = root.findViewById(R.id.bottom_list)
        bottomBehavior = CustomBottomSheetBehavior.from(ll_bottom_sheet)
        bottomBehavior.setBottomSheetCallback(bottomSheetCallBack)
        bottomBehavior.state = CustomBottomSheetBehavior.STATE_COLLAPSED
        bottomBehavior.setAnchorOffset(0.5f)

        rv_missionList = root.findViewById(R.id.rv_missionList)
        mLayoutManager = LinearLayoutManager(activity)
        rv_missionList.layoutManager = mLayoutManager
        mAdapter = MissionListAdapter(requireContext(), null, TYPE_MAIN_LIST)
        mAdapter!!.mPopularityVisable = true
        mAdapter!!.setOnItemClickListener(this)
        rv_missionList.adapter = mAdapter
        btn_filter_setting = root.findViewById(R.id.btn_filter_setting)
        btn_filter_setting.setOnClickListener(this)


        btngroup_filter = root.findViewById(R.id.btngroup_filter)
        rl_filter = root.findViewById(R.id.rl_filter)
        btngroup_filter.setOnSelectListener { btn ->
            when (btn.id) {
                R.id.btn_meal -> {
                    filter_type_array[1] = btn.isSelected
                }
                R.id.btn_within100 -> {
                    filter_type_array[2] = btn.isSelected
                }
                R.id.btn_inline -> {
                    filter_type_array[3] = btn.isSelected
                }
                R.id.btn_ticket -> {
                    filter_type_array[4] = btn.isSelected
                }
                R.id.btn_booking -> {
                    filter_type_array[5] = btn.isSelected
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_find_mission_by_map, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")


        root.tag = R.layout.fragment_find_mission_by_map
        mMapView = root.findViewById(R.id.mapView)
        mMapView!!.onCreate(savedInstanceState)
        mMapView!!.onResume() // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(requireActivity().applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return root
    }

    companion object {
        const val FRAGMENT_ID = R.id.tab_mission_list
    }


    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()

    }


    override fun onPause() {
        super.onPause()
        hasInitView = false
        mMapView!!.onPause()
    }

    override fun onStart() {
        super.onStart()
        Log.d("timerRunnable start")
        setTitle(R.string.title_mission_list)
        initView(root)
        mViewModel.getLocation().observe(viewLifecycleOwner, mLocationObserver)
        mViewModel.getUserState().observe(viewLifecycleOwner, userStateObserver)
        mMapView!!.onStart()
    }

    override fun onStop() {
        super.onStop()
        mViewModel.getLocation().removeObserver(mLocationObserver)
        mViewModel.getJobList().removeObserver(mJobListObserver)
        mViewModel.getUserState().removeObserver(userStateObserver)
        Log.d("timerRunnable stop")
        mMapView!!.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }

    private var mLocationObserver: Observer<Location> = Observer { t ->
        Log.v("location: $t  | $mFullMissionList")
        mLocation = t
        if (t != null) {
            try {
                mViewModel.updateJobList()//每10秒收到一次location更新, 並更新joblist
            }catch (e:Exception){
                Log.e(e)
            }
            updateMap(mFullMissionList, mUserState)
            mViewModel.getJobList().observe(viewLifecycleOwner, mJobListObserver)
        }
    }

    private var mJobListObserver: Observer<List<Mission>> =
        Observer { t ->
            mFullMissionList = t as ArrayList<Mission>
            Log.v("mJobListObserver: $mFullMissionList")
            mAdapter!!.setData(t)
            if (t.size > 0) {
                setBottomListEnable(true)
            } else {
                setBottomListEnable(false)
            }
            updateMap(mFullMissionList ,mUserState)
        }

    fun updateMap(missionList:ArrayList<Mission>?, state:Const.USERSTATE){
        Log.v("location: $mLocation  | $mFullMissionList")
        if(mLocation==null)
            return
        mMapView!!.getMapAsync { mMap ->
            mMap.uiSettings.isMapToolbarEnabled = false
            if (googleMap != null)
                googleMap!!.clear()
            googleMap = mMap
            //config
            val loc = LatLng(mLocation!!.latitude, mLocation!!.longitude)
            //draw user location
            mMap.addMarker(
                MarkerOptions().position(loc)
                    .icon(BitmapDescriptorFactory.fromResource(
                        if(mUserState==Const.USERSTATE.TAKER)
                        R.drawable.user_onmap_taker else R.drawable.user_onmap_owner))
            )

            val zoom =
                (log2(40000.0 / mViewModel.mUser?.userSetting?.missionDetectRange!!) - 0.5).toFloat()
            // For zooming automatically to the location of the marker
            val cameraPosition = CameraPosition.Builder()
                .target(loc)
                .zoom(zoom)
                .build()


            //init
            if (!hasInitView || clusterManager == null) {

                //locate to user location
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                //init cluster
                clusterManager = ClusterManager(this.requireContext(), mMap)
                (clusterManager as ClusterManager<*>).renderer =
                    CustomClusterRenderer(
                        this.requireContext(),
                        mMap,
                        (clusterManager as ClusterManager<ClusterItem>),mUserState
                    )
                clusterManager?.setOnClusterClickListener { cluster ->
                    mClusterListAdapter = MissionListAdapter(
                        requireActivity(),
                        cluster.items as ArrayList<Mission>?,
                        MissionListAdapter.TYPE_POP_CLUSTER_LIST
                    )

                    mClusterListAdapter!!.setOnItemClickListener(object :
                        IOnItemClickListener<Mission> {
                        override fun onItemClick(item_model: Mission, position: Int) {
                            if(mViewModel.getUserState().value==Const.USERSTATE.TAKER){
                                (activity as MainActivity).onMissionClick(item_model)
                            }
                        }

                        override fun onItemClick(view: View, position: Int) {
                            if(mViewModel.getUserState().value==Const.USERSTATE.TAKER){
                                (activity as MainActivity).onMissionClick(mClusterListAdapter!!.getSelectedMission(position))
                            }
                        }
                    })
                    mClusterListAdapter!!.setData((cluster.items as ArrayList<Mission>?)!!)
                    mMapBottomListDrawer.setAdapter(mClusterListAdapter!!)
                    mMapBottomListDrawer.switchDrawer(0)

                    //點擊cluster不改變上拉list item
//                        mAdapter!!.setData((cluster.items as ArrayList<Mission>?)!!)
//                        if(mAdapter!!.itemCount>0){
//                            setBottomListEnable(true)
//                        }else{
//                            setBottomListEnable(false)
//                        }


                    return@setOnClusterClickListener false
                }
                clusterManager?.setOnClusterItemClickListener { clusterItem ->
                    (activity as MainActivity).onMissionClick(clusterItem)
                    return@setOnClusterItemClickListener false
                }

                mMap.setOnMapClickListener {
                    if(mFullMissionList!=null)
                        mAdapter!!.setData(mFullMissionList as List<*>)
                    bottomBehavior.state = CustomBottomSheetBehavior.STATE_COLLAPSED
                    mMapBottomListDrawer.closeDrawerNow()
                }
                hasInitView = true
            }
            if (mCircle != null)
                mCircle!!.remove()
            mCircle = mMap.addCircle(
                CircleOptions()
                    .center(loc)
                    .radius((if (Const.DEBUG) 5 * 1000.0 else mViewModel.mUser!!.userSetting!!.missionDetectRange * 1000.0))
                    .strokeColor(ContextCompat.getColor(requireContext(),  R.color.colorMainBrightWhite))
                    .strokeWidth(5.0f)
                    .fillColor(
                    if(state==Const.USERSTATE.OWNER)
                        ContextCompat.getColor(requireContext(), R.color.map_fill_owner)
                    else
                        ContextCompat.getColor(requireContext(), R.color.map_fill_taker)
                    )
            )
            mCircle!!.isVisible = true

            mMap.setOnCameraIdleListener(clusterManager)
            /**
             * 目前採用workaround 避免頻繁更換markers導致crash
             * TODO 可能的解決方案: https://github.com/googlemaps/android-maps-utils/issues/63#issuecomment-173876521
             */
            if(missionList!=null){
                clusterManager?.clearItems()
                clusterManager?.cluster()
                mMapView?.postDelayed({
                    clusterManager?.addItems(missionList)
                    clusterManager?.cluster()
                }, 50)
            }
        }
    }

    val bottomSheetCallBack = object : CustomBottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(bottomSheet: View, slideOffset: Float) {
        }

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            Log.d("onStateChanged: $newState")
            when (newState) {
                CustomBottomSheetBehavior.STATE_ANCHOR -> {
                    mMapBottomListDrawer.closeDrawerNow()
                    rv_missionList.layoutParams.height =
                        resources.getDimension(R.dimen.size_255).toInt()
                    rv_missionList.requestLayout()
                }
                CustomBottomSheetBehavior.STATE_EXPANDED -> {
                    mMapBottomListDrawer.closeDrawerNow()
                    rv_missionList.layoutParams.height = WRAP_CONTENT
                    rv_missionList.requestLayout()
                }
            }
        }
    }

    override fun onItemClick(item_model: Mission, position: Int) {
    }

    override fun onItemClick(view: View, position: Int) {
        if(mViewModel.getUserState().value== Const.USERSTATE.TAKER){
            (activity as MainActivity).onMissionClick(mAdapter!!.getSelectedMission(position))
        }
    }

    private fun setBottomListEnable(enable: Boolean) {
        bottomBehavior.setEnable(enable)
        btn_filter_setting.isEnabled = enable
        Utils.recursiveEnableView(btngroup_filter, enable)
        if (!enable) {
            bottomBehavior.state = CustomBottomSheetBehavior.STATE_COLLAPSED
        }
    }


    override fun onClick(v: View?) {
        when (v?.id) {
//            R.id.btn_filter_agree -> {
//                Log.d("btn_filter_agree")
//
//            }
            R.id.rl_sort_distance->{
                var cb_sort_distance = mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_distance)
                cb_sort_distance?.isChecked = !cb_sort_distance?.isChecked!!
            }

            R.id.rl_sort_time->{
                var cb_sort_time = mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_time)
                cb_sort_time?.isChecked = !cb_sort_time?.isChecked!!
            }

            R.id.rl_sort_wage->{
                var cb_sort_wage = mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_wage)
                cb_sort_wage?.isChecked = !cb_sort_wage?.isChecked!!
            }

            R.id.btn_filter_setting -> {
                mCustomFilterDialog = CustomDialogBuilder(
                    requireContext(),
                    R.layout.custom_dialog_bottom_sheet_filter
                )
                    .setFilterState(filter_type_array)
                    .setIFilterResults(this)
                    .setCheckChangeListener(this)
                    .build()
                var rl_sort_distance = mCustomFilterDialog?.findViewById<RelativeLayout>(R.id.rl_sort_distance)!!
                var rl_sort_time = mCustomFilterDialog?.findViewById<RelativeLayout>(R.id.rl_sort_time)!!
                var rl_sort_wage = mCustomFilterDialog?.findViewById<RelativeLayout>(R.id.rl_sort_wage)!!
                rl_sort_distance.setOnClickListener(this)
                rl_sort_time.setOnClickListener(this)
                rl_sort_wage.setOnClickListener(this)
                mCustomFilterDialog?.show()
            }
        }

    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView?.id) {
            R.id.rb_mission_type_meal -> {
                filter_type_array[1] = isChecked
            }
            R.id.rb_mission_type_100m -> {
                filter_type_array[2] = isChecked
            }
            R.id.rb_mission_type_inline -> {
                filter_type_array[3] = isChecked
            }
            R.id.rb_mission_type_ticket -> {
                filter_type_array[4] = isChecked
            }
            R.id.rb_mission_type_booking -> {
                filter_type_array[5] = isChecked
            }
            R.id.cb_sort_distance -> {
                if (isChecked) {
                    mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_time)?.isChecked =
                        false
                    mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_wage)?.isChecked =
                        false
                }
                filter_type_array[6] = true
                filter_type_array[7] = false
                filter_type_array[8] = false
            }
            R.id.cb_sort_time -> {
                if (isChecked) {
                    mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_distance)?.isChecked =
                        false
                    mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_wage)?.isChecked =
                        false
                    filter_type_array[6] = false
                    filter_type_array[7] = true
                    filter_type_array[8] = false
                }
            }
            R.id.cb_sort_wage -> {
                if (isChecked) {
                    mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_time)?.isChecked =
                        false
                    mCustomFilterDialog?.findViewById<CheckBox>(R.id.cb_sort_distance)?.isChecked =
                        false
                    filter_type_array[6] = false
                    filter_type_array[7] = false
                    filter_type_array[8] = true
                }
            }
        }
    }

    private var userStateObserver: Observer<Const.USERSTATE> = Observer { state ->

        when (state) {
            Const.USERSTATE.DEFAULT -> {//common
            }

            Const.USERSTATE.OWNER -> {//owner
                filter_type_array[0]=true
                hasInitView = false
                btn_filter_setting.setBackgroundResource(R.drawable.btn_filter_setting_owner)
                btngroup_filter.buttons.forEach { it.selectedBgColor = ContextCompat.getColor(requireContext(),R.color.owner_red)}
                mUserState=Const.USERSTATE.OWNER
                updateMap(mFullMissionList, mUserState)
                mAdapter?.updateUserState(Const.USERSTATE.OWNER)
            }
            Const.USERSTATE.TAKER -> {//taker
                filter_type_array[0]=false
                hasInitView = false
                btn_filter_setting.setBackgroundResource(R.drawable.btn_filter_setting_taker)
                btngroup_filter.buttons.forEach { it.selectedBgColor = ContextCompat.getColor(requireContext(),R.color.taker_blue)}
                mUserState=Const.USERSTATE.TAKER
                updateMap(mFullMissionList, mUserState)
                mAdapter?.updateUserState(Const.USERSTATE.TAKER)
            }
        }

    }

    override fun ResultPublished(results: ArrayList<*>) {


    }

    override fun ResultPublished(results: Array<*>) {
        //0:owner, 1~5:filter_type_array,6~8:filter_sort_array
        for(i in 0..4){
            if(results[i+1] as Boolean xor btngroup_filter.buttons[i].isSelected){
                Log.d("item $i :"+results[i+1] as Boolean+" | "+btngroup_filter.buttons[i].isSelected)
                btngroup_filter.selectButtonWithAnimation(btngroup_filter.buttons[i])
            }
        }
        filter_type_array = results as Array<Boolean>
        //TODO 排序功能實作

        if(mCustomFilterDialog?.isShowing!!)
            mCustomFilterDialog?.dismiss()
    }

    override fun refreshUI() {
        super.refreshUI()
        setTitle(R.string.title_mission_list)
    }
}