package com.app.lbs.ui.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.Gravity
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.models.Mission
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.MyLog
import com.app.lbs.utils.Utils
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.math.BigDecimal
import java.util.*

/**
 * @file CustomBottomSheetDialogMissionModify
 * @brief
 *       任務修改
 *
 * @author Zack
 **/

class CustomBottomSheetDialogMissionModify(
    mission: Mission,
    state: Const.USERSTATE
) : BottomSheetDialogFragment(),View.OnClickListener {
    private lateinit var mViewModel: MainViewModel
    private var Log = MyLog.log()
    private lateinit var rl_modify_delivery_time:RelativeLayout
    private lateinit var rl_modify_delivery_location:RelativeLayout
    private lateinit var rl_modify_salary:RelativeLayout
    private lateinit var et_content_salary:EditText
    private lateinit var tv_content_delivery_time:TextView
    private lateinit var tv_content_delivery_location:TextView
    private lateinit var btn_confirm: Button
    private var mMission = mission
    private var mUserState = state
    private var retMission: Mission?=null
    private var dateTimePicker: SlideDateTimePicker? = null
    private var mDateTimePickerListener = object : SlideDateTimeListener() {

        override fun onDateTimeSet(date: Date) {
            tv_content_delivery_time.setText(Utils.getDate(date.time))
            tv_content_delivery_time.gravity = (Gravity.END + Gravity.CENTER_VERTICAL)
            retMission!!.finish_time = date.time.toString()
        }

        override fun onDateTimeCancel() {
            // Overriding onDateTimeCancel() is optional.
            tv_content_delivery_time.setText(mMission.finish_time)
            tv_content_delivery_time.gravity = Gravity.CENTER
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = View.inflate(context, R.layout.custom_bottom_sheet_mission_modify, null)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        dialog.setContentView(view)
        retMission = mMission
        (view.parent as View).setBackgroundColor(ContextCompat.getColor(requireContext(), android.R.color.transparent))
        initView(view)

        return dialog
    }

    private fun initView(v: View) {
        rl_modify_delivery_time = v.findViewById(R.id.rl_modify_delivery_time)
        rl_modify_delivery_location = v.findViewById(R.id.rl_modify_delivery_location)
        rl_modify_salary = v.findViewById(R.id.rl_modify_salary)
        et_content_salary = v.findViewById(R.id.et_content_salary)
        et_content_salary.hint = getSalaryString(mMission.salary.toString())
        et_content_salary.setOnClickListener(this)
        et_content_salary.onFocusChangeListener =
            View.OnFocusChangeListener { v, hasFocus ->
                var target = et_content_salary.text.toString()
                if(v?.id == R.id.et_content_salary){
                    if(hasFocus){
                        et_content_salary.isCursorVisible =true
                    }else{
                        et_content_salary.isCursorVisible =false
                        if(!target.endsWith("TWD")){
                            et_content_salary.setText(getSalaryString(target))
                        }
                    }
                }
            }

        et_content_salary.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                et_content_salary.postDelayed({
                    (activity as MainActivity).hideSoftKeyboard(et_content_salary)
                    et_content_salary.isCursorVisible =false
                }, 100)
                return@OnKeyListener true
            }
            false
        })
        tv_content_delivery_time = v.findViewById(R.id.tv_content_delivery_time)
        tv_content_delivery_location = v.findViewById(R.id.tv_content_delivery_location)

        btn_confirm = v.findViewById(R.id.btn_confirm)
        btn_confirm.setOnClickListener(this)
        if (mUserState ==Const.USERSTATE.TAKER) {
            btn_confirm.setBackgroundResource(R.drawable.btn_taker_rect)
        }else{
            btn_confirm.setBackgroundResource(R.drawable.btn_owner_rect)
        }


        rl_modify_delivery_time.setOnClickListener(this)
        rl_modify_delivery_location.setOnClickListener(this)
        rl_modify_salary.setOnClickListener(this)
    }

    fun show(fragmentActivity: FragmentActivity) {
        show(fragmentActivity.supportFragmentManager, TAG)
    }

    fun getSalaryString(salary: String):String {
        if(salary.trim().isEmpty()) {
            var ret = StringBuilder().append(retMission?.salary.toString()).append(" TWD").toString()
            return ret
        }else{
            return StringBuilder().append(salary).append(" TWD").toString()
        }
    }

    companion object {
        private const val TAG = "CustomBottomSheetDialogMissionModifyDialogFrag"
    }
    var obs = Observer<Mission> { mission ->
        try {
            tv_content_delivery_time.text = Utils.getDate(mission.finish_time!!.toLong())
        }catch (e: Exception){
            Log.e(e)
            try {
                tv_content_delivery_time.text = Utils.getDate(BigDecimal(mission.finish_time).toLong())
            }catch (e: Exception){
                tv_content_delivery_time.setText(R.string.tv_click_to_select)
            }
        }

        tv_content_delivery_location.text = mission.addr2coor
        et_content_salary.setText("${mission.salary} TWD")

    }
    override fun onResume() {
        super.onResume()
        Log.e("TIMMER - onResume")
        mViewModel.getObservTmpMission().observe(requireActivity(), obs)
    }

    override fun onPause() {
        super.onPause()
        Log.e("TIMMER - onPause")
        mViewModel.getObservTmpMission().removeObserver(obs)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.rl_modify_delivery_time -> {
                (activity as MainActivity).hideSoftKeyboard(et_content_salary)
                dateTimePicker =
                    SlideDateTimePicker.Builder(requireActivity().supportFragmentManager)
                        .setListener(mDateTimePickerListener)
                        .setMinDate(Date(System.currentTimeMillis()))
                        .setMinTime(Date(System.currentTimeMillis()))
                        .setInitialDate(Date(System.currentTimeMillis() + 30 * 60 * 1000))
                        .setIndicatorColor(ContextCompat.getColor(requireContext(), R.color.owner_red))
                        .build()
                dateTimePicker?.show()
            }
            R.id.rl_modify_delivery_location -> {
                (activity as MainActivity).hideSoftKeyboard(et_content_salary)
                mViewModel.tmp_mission.is_delivery_loc = true
                NewMissionSelectLocationDialogFragment().show(parentFragmentManager, null)
            }
            R.id.rl_modify_salary, R.id.et_content_salary -> {
                et_content_salary.postDelayed({
                    et_content_salary.requestFocus()
                    if(!et_content_salary.text.isEmpty() && !et_content_salary.isCursorVisible){
                        et_content_salary.setText(et_content_salary.text.split("TWD")[0])
                        et_content_salary.setSelection(et_content_salary.text.length -1)
                    }
                    (activity as MainActivity).showSoftKeyboard(et_content_salary)
                    et_content_salary.isCursorVisible =true
                }, 100)
            }

            R.id.btn_confirm -> {
                (activity as MainActivity).hideSoftKeyboard(et_content_salary)
            }
        }
    }


    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

        (activity as MainActivity).hideSoftKeyboard()
    }



}