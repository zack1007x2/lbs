package com.app.lbs.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.app.lbs.R
import com.app.lbs.adapters.HorizontalRecyclerViewAdapter
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.ImageModel
import com.app.lbs.models.Mission
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.Const.REQUEST_IMAGE
import com.app.lbs.utils.Const.REQUSET_CAMERA
import com.app.lbs.utils.CustomDialogBuilder
import com.app.lbs.utils.Utils
import com.app.lbs.utils.view.StateTextView
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker
import com.github.jjobes.slidedatetimepicker.SlideTimerPicker
import com.github.jjobes.slidedatetimepicker.SlideTimerSetListener
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * @file NewMissionStep1
 * @brief
 *       新建任務第一步（設定任務內容）任務名稱, 內容, 金額, 任務地點, 面交地點等等
 *
 * @author Zack
 * */


class NewMission : BaseFragment(), View.OnClickListener, ViewPager.OnPageChangeListener {

    private lateinit var mViewModel: MainViewModel
    private lateinit var et_content_mission_title: EditText

    private lateinit var rl_new_mission:RelativeLayout
    private lateinit var et_content_salary: EditText
    private lateinit var et_mission_content: EditText
    private lateinit var et_mission_demand_content:EditText
    private lateinit var btn_next: Button
    private lateinit var ll_salary: RelativeLayout
    private lateinit var sc_container: ScrollView
    private lateinit var btn_content_finish_location: StateTextView
    private lateinit var btn_content_mission_location: StateTextView
    private lateinit var ll_mission_location: RelativeLayout
    private lateinit var ll_finish_location: RelativeLayout
    private lateinit var tv_content_delivery_time: StateTextView
    private lateinit var tv_content_duration:StateTextView
    private lateinit var rl_image_list: RecyclerView
    private lateinit var ll_mission_title: RelativeLayout
    private lateinit var ll_tft_time: RelativeLayout
    private lateinit var ll_delete_panel: LinearLayout
    private lateinit var rg_mission_type:RadioGroup
    private lateinit var btn_gallery:Button
    private lateinit var btn_camera:Button
    private lateinit var img_money_sign:ImageView
    private lateinit var btn_clear_text:ImageView
    private lateinit var rb_taker_rate:RatingBar

    private var mDateTimePickerListener: SlideDateTimeListener? = null
    private var mSlideTimerSetListener: SlideTimerSetListener? = null
    private var dateTimePicker: SlideDateTimePicker? = null
    private var timerPicker: SlideTimerPicker? = null
    private var mImgListAdapter: HorizontalRecyclerViewAdapter? = null
    private var horizontalLayoutManager: LinearLayoutManager? = null
    private var dialog_pic_selector: Dialog?=null
    private var img_select_position: Int? = 0
    private var selectedImages = ArrayList<Image>()


    companion object {
        //TODO id待更新
        const val FRAGMENT_ID = 23141234
    }

    /**
     * AdapterDataObserver的 CallBack較完整且符合現在及未來需求故選擇之
     */
    var onImgDataChangeObsver = object: RecyclerView.AdapterDataObserver(){
        override fun onChanged() {
            var mList = mImgListAdapter!!.getList()
            //數量小於5時檢查是否要加回ADD選項
            if(mList.size<5){
                if(mList[0].getImagePath()!=Utils.getURLForResource(R.drawable.fab_add)){
                    mList.add(0, ImageModel(0, Utils.getURLForResource(R.drawable.fab_add)))
                }
            }
            //數量大於5時移除ADD選項
            if(mList.size>5){
                if(mList[0].getImagePath()==Utils.getURLForResource(R.drawable.fab_add))
                    mList.removeAt(0)
            }
            mImgListAdapter!!.notifyDataSetChanged()
            super.onChanged()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root =
            inflater.inflate(R.layout.fragment_new_mission, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        initView(root)
        return root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initView(v: View) {
        rl_new_mission = v.findViewById(R.id.rl_new_mission)
        sc_container = v.findViewById(R.id.sc_container)
        btn_next = v.findViewById(R.id.btn_next)
        btn_next.setOnClickListener(onNextClick)
        ll_salary = v.findViewById(R.id.ll_salary)
        ll_salary.visibility = View.GONE
        et_content_mission_title = v.findViewById(R.id.et_content_mission_title)
        et_mission_content = v.findViewById(R.id.et_mission_content)
        et_content_salary = v.findViewById(R.id.et_content_salary)
        tv_content_delivery_time = v.findViewById(R.id.tv_content_delivery_time)
        tv_content_duration = v.findViewById(R.id.tv_content_duration)
//        fab_to_top = v.findViewById(R.id.fab_to_top)
        rl_image_list = v.findViewById(R.id.rl_image_list)
        ll_mission_title = v.findViewById(R.id.ll_mission_title)
        ll_tft_time = v.findViewById(R.id.ll_delivery_time)
        ll_mission_location = v.findViewById(R.id.ll_mission_location)
        ll_finish_location = v.findViewById(R.id.ll_finish_location)
        btn_gallery = v.findViewById(R.id.btn_gallery)
        btn_camera = v.findViewById(R.id.btn_camera)
        img_money_sign = v.findViewById(R.id.img_money_sign)
        btn_clear_text = v.findViewById(R.id.btn_clear_text)
        rb_taker_rate = v.findViewById(R.id.rb_taker_rate)
        et_mission_demand_content = v.findViewById(R.id.et_mission_demand_content)

        btn_gallery.setOnClickListener(onGalleryClick)
        btn_camera.setOnClickListener(onCameraClick)
        ll_mission_title.setOnClickListener(this@NewMission)
        ll_salary.setOnClickListener(this@NewMission)
        ll_tft_time.setOnClickListener(this@NewMission)
        ll_mission_location.setOnClickListener(this@NewMission)
        ll_finish_location.setOnClickListener(this@NewMission)
        tv_content_delivery_time.setOnClickListener(this@NewMission)
        tv_content_duration.setOnClickListener(this@NewMission)
        btn_clear_text.setOnClickListener(this@NewMission)

        btn_content_mission_location = v.findViewById(R.id.btn_content_mission_location)
        btn_content_mission_location.setLocationSelect(false)
        btn_content_mission_location.setOnClickListener(this@NewMission)
        btn_content_finish_location = v.findViewById(R.id.btn_content_finish_location)
        btn_content_finish_location.setLocationSelect(false)
        btn_content_finish_location.setOnClickListener(this@NewMission)
        rb_taker_rate.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            if(fromUser)
                mViewModel.tmp_mission.yelp_rank = rating.toString()
        }
//        fab_to_top.setOnClickListener { v ->
//            sc_container.smoothScrollTo(0, 0)
//        }

        sc_container.addOnLayoutChangeListener(mOnLayoutChangeLsr)

        ll_mission_location = v.findViewById(R.id.ll_mission_location)
        ll_finish_location = v.findViewById(R.id.ll_finish_location)

        ll_mission_location.visibility = View.GONE
        ll_finish_location.visibility = View.GONE

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            sc_container.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
//                if (scrollY > 10 && !et_mission_content.isFocused) {
//                    fab_to_top.show()
//                } else {
//                    fab_to_top.hide()
//                }
//            }
//        }
        sc_container.setOnTouchListener { v, event ->
            if (event != null && event.action == MotionEvent.ACTION_MOVE) {
                val imm: InputMethodManager =
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                val isKeyboardUp: Boolean = imm.isAcceptingText

                if (isKeyboardUp) {
                    imm.hideSoftInputFromWindow(v!!.windowToken, 0)
                }
            }
            false
        }
        if(mViewModel.tmp_horizontalrecyclerviewAdapter!=null)
            mImgListAdapter = mViewModel.tmp_horizontalrecyclerviewAdapter
        else
            mImgListAdapter = HorizontalRecyclerViewAdapter(context, true)
//        mImgListAdapter!!.registerAdapterDataObserver(onImgDataChangeObsver)
        mImgListAdapter!!.setOnItemClickListener(mOnImageItemClickListener)

        rg_mission_type = v.findViewById(R.id.rg_mission_type)
        rg_mission_type.setOnCheckedChangeListener { group, checkedId ->
            checkNextEnable()
            when (checkedId) {
                R.id.rb_mission_type_meal -> {
                    mViewModel.tmp_mission.job_type = 1
                    img_money_sign.setImageResource(R.drawable.ico_money_meal)
                }
                R.id.rb_mission_type_100m -> {
                    mViewModel.tmp_mission.job_type = 2
                    img_money_sign.setImageResource(R.drawable.ico_money_100m)
                }
                R.id.rb_mission_type_inline -> {
                    mViewModel.tmp_mission.job_type = 3
                    img_money_sign.setImageResource(R.drawable.ico_money_inline)
                }
                R.id.rb_mission_type_ticket -> {
                    mViewModel.tmp_mission.job_type = 4
                    img_money_sign.setImageResource(R.drawable.ico_money_ticket)
                }
                R.id.rb_mission_type_booking -> {
                    mViewModel.tmp_mission.job_type = 5
                    img_money_sign.setImageResource(R.drawable.ico_money_booking)
                }
            }
        }
        horizontalLayoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        rl_image_list.layoutManager = horizontalLayoutManager
        rl_image_list.adapter = mImgListAdapter
        ll_salary.visibility = View.VISIBLE
        ll_mission_location.visibility = View.VISIBLE
        ll_finish_location.visibility = View.VISIBLE

        et_content_salary.setOnEditorActionListener(OEA)
        et_content_mission_title.setOnEditorActionListener(OEA)
        et_mission_content.setOnEditorActionListener(OEA)
        et_mission_demand_content.setOnEditorActionListener(OEA)

        et_content_mission_title.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                checkNextEnable()
            }

        })

        et_content_salary.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                checkNextEnable()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

        mSlideTimerSetListener = object: SlideTimerSetListener() {
            override fun onTimerSet(sec: Long?) {
                setDuration(sec!!.toInt())
                checkNextEnable()
            }

            override fun onTimerCancel() {
                requireActivity().runOnUiThread {
                    tv_content_duration.setText(R.string.tv_click_to_select)
                    tv_content_duration.gravity = Gravity.CENTER
                    tv_content_duration.setLocationSelect(false)
                    mViewModel.tmp_mission.duration = 0
                    checkNextEnable()
                }
            }
        }
    }

    private var obs = Observer<Mission> { mission ->
//        Log.w("Observer mission: $mission")
        setDuration(mission.duration)
        et_content_mission_title.setText(mission.title)
        et_mission_content.setText(mission.description)
        et_mission_demand_content.setText(mission.comment)
//            if (mission.job_type == 1) {
        et_content_salary.setText(mission.salary.toString())
//            }
        if (mission.devilery_time!!>0) {
            tv_content_delivery_time.text = Utils.getDate(mViewModel.tmp_mission.devilery_time!!, "MM月 dd日 EEEE               hh:mm:ss a     ")
            tv_content_delivery_time.setLocationSelect(true)
        }else {
            tv_content_delivery_time.setText(R.string.tv_click_to_select)
            tv_content_delivery_time.setLocationSelect(false)
        }
        if (mission.store_name.isNotEmpty()) {
            btn_content_mission_location.setLocationSelect(true)
            btn_content_mission_location.text = mission.store_addr
//                        mission.store_name + "\n" + mission.store_addr
        } else {
            btn_content_mission_location.setLocationSelect(false)
            btn_content_mission_location.setText(R.string.tv_click_to_select)
        }

        if (mission.addr2coor!!.isNotEmpty() || mission.comment!!.isNotEmpty()) {
            btn_content_finish_location.setLocationSelect(true)
            btn_content_finish_location.text = mission.addr2coor
//              mission.comment + "\n" + mission.addr2coor
        } else {
            btn_content_finish_location.setLocationSelect(false)
            btn_content_finish_location.setText(R.string.tv_click_to_select)
        }

        if(mission.yelp_rank.isNotEmpty()){
            try {
                rb_taker_rate.rating = mission.yelp_rank.toFloat()
            }catch (e:Exception){
                Log.e(e)
            }
        }
        if(mViewModel.tmp_mission.job_type>0){
            (rg_mission_type.getChildAt(mViewModel.tmp_mission.job_type-1) as RadioButton).isChecked = true
        }

        checkNextEnable()

    }

    private val OEA: TextView.OnEditorActionListener =
        TextView.OnEditorActionListener { v, actionId, event ->
            if(actionId== EditorInfo.IME_ACTION_DONE){
                //Clear focus here from edittext
                et_content_salary.clearFocus()
                et_content_mission_title.clearFocus()
                et_mission_content.clearFocus()
                et_mission_demand_content.clearFocus()
                (activity as MainActivity).hideSoftKeyboard()
            }
            false
        }
    override fun onResume() {
        super.onResume()
        if(mImgListAdapter==null)
            mImgListAdapter = mViewModel.tmp_horizontalrecyclerviewAdapter
        et_content_salary.setOnClickListener(this)
        et_content_mission_title.setOnClickListener(clearFocusListener)

        tv_content_delivery_time.setLocationSelect(false)
        btn_content_mission_location.setLocationSelect(false)
        btn_content_finish_location.setLocationSelect(false)
        tv_content_duration.setLocationSelect(false)
        Log.e("TIMMER - onResume")
        mViewModel.getObservTmpMission().observe(this, obs)

        mDateTimePickerListener = object : SlideDateTimeListener() {

            override fun onDateTimeSet(date: Date) {
                // Do something with the date. This Date object contains
                // the date and time that the user has selected.
                val formatter = SimpleDateFormat(
                    "MM月 dd日 EEEE               hh:mm:ss a     ",
                    Locale.TRADITIONAL_CHINESE
                )
                tv_content_delivery_time.setText(formatter.format(date))
                tv_content_delivery_time.gravity = (Gravity.END + Gravity.CENTER_VERTICAL)
                mViewModel.tmp_mission.devilery_time = date.time
                tv_content_delivery_time.setLocationSelect(true)
                checkNextEnable()

            }

            override fun onDateTimeCancel() {
                // Overriding onDateTimeCancel() is optional.
                tv_content_delivery_time.setText(R.string.tv_click_to_select)
                tv_content_delivery_time.gravity = Gravity.CENTER
                mViewModel.tmp_mission.devilery_time = 0L
                tv_content_delivery_time.setLocationSelect(false)
                checkNextEnable()
            }
        }

        checkNextEnable()
    }

    override fun onPause() {
        super.onPause()
        Log.i("onPause")
        mImgListAdapter=null
        Log.e("TIMMER - onPause")
        mViewModel.getObservTmpMission().removeObserver(obs)
    }


    //--------------Listener-----------------


    private fun checkNextEnable() {
        Log.d("enable: "+et_content_mission_title.text.isNotEmpty()+" | "+
                et_content_salary.text.isNotEmpty() +" | "+
                btn_content_mission_location.getLocationSelect() +" | "+
                btn_content_finish_location.getLocationSelect() +" | "+
                tv_content_duration.getLocationSelect() +" | "+
                tv_content_delivery_time.getLocationSelect() +" | "+
                (rg_mission_type.checkedRadioButtonId!=-1))
        btn_next.isEnabled = !(et_content_mission_title.text.isNullOrEmpty() ||
                et_content_salary.text.isNullOrEmpty() ||
                !btn_content_mission_location.getLocationSelect() ||
                !btn_content_finish_location.getLocationSelect() ||
                !tv_content_duration.getLocationSelect() ||
                !tv_content_delivery_time.getLocationSelect() ||
                rg_mission_type.checkedRadioButtonId==-1)
    }

    private val onNextClick = View.OnClickListener {
            mViewModel.tmp_mission.job_type = 1//TODO BD任務類型
            mViewModel.tmp_mission.title = et_content_mission_title.text.toString()
            val start: Long = System.currentTimeMillis()
            mViewModel.tmp_mission.duration = (mViewModel.tmp_mission.devilery_time!! - System.currentTimeMillis()).toInt()
            mViewModel.tmp_mission.start_time = Utils.getDate(start)
            mViewModel.tmp_mission.finish_time = Utils.getDate(mViewModel.tmp_mission.devilery_time!!, "MM月 dd日 EEEE               hh:mm:ss a     ")

            mViewModel.tmp_mission.description = et_mission_content.text.toString()
            mViewModel.tmp_mission.comment = et_mission_demand_content.text.toString()
            mViewModel.tmp_mission.job_owner = mViewModel.mUser!!.userData.userId

            if (et_content_salary.text.isNullOrEmpty())
                mViewModel.tmp_mission.salary = 0
            else
                mViewModel.tmp_mission.salary = et_content_salary.text.toString().toInt()

            mViewModel.updateTmpMission()
            mViewModel.serverCreateNewJob()
            (activity as MainActivity).showMissionPublishSuccess()
    }


    private var mOnLayoutChangeLsr =
        View.OnLayoutChangeListener { v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int,
                                      oldRight: Int, oldBottom: Int ->
            if (oldBottom in 1 until bottom) {
                //keyboard dismiss
                et_content_mission_title.clearFocus()
                et_mission_content.clearFocus()
                et_mission_demand_content.clearFocus()
//                et_content_salary.clearFocus()
                et_content_salary.setText(Utils.trimLeadingZeros(et_content_salary.text.toString()))
//                if (sc_container.scrollY > 10) {
//                    fab_to_top.show()
//                }
            }
        }


    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.btn_content_mission_location, R.id.ll_mission_location -> {
                Dexter.withActivity(activity)
                    .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                    .withListener(object : MultiplePermissionsListener {
                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                            if (report.areAllPermissionsGranted()) {
                                selectLoc(false)
                            } else {
                                // TODO - handle permission denied case
                                Toast.makeText(activity, "permission denied", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permissions: List<PermissionRequest>,
                            token: PermissionToken
                        ) {
                            token.continuePermissionRequest()
                        }
                    }).check()
            }
            R.id.btn_content_finish_location, R.id.ll_finish_location -> {
                Dexter.withActivity(activity)
                    .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                    .withListener(object : MultiplePermissionsListener {
                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                            if (report.grantedPermissionResponses.size > 0) {
                                selectLoc(true)
                            } else {
                                // TODO - handle permission denied case
                                Toast.makeText(activity, "permission denied", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permissions: List<PermissionRequest>,
                            token: PermissionToken
                        ) {
                            token.continuePermissionRequest()
                        }
                    }).check()
            }
            R.id.ll_duration, R.id.tv_content_duration ->{
                timerPicker = SlideTimerPicker.Builder(requireActivity().supportFragmentManager)
                    .setListener(mSlideTimerSetListener)
                    .setIndicatorColor(ContextCompat.getColor(requireContext(), R.color.owner_red))
                    .build()
                timerPicker?.show()
            }
            R.id.tv_content_delivery_time, R.id.ll_delivery_time -> {
                dateTimePicker =
                    SlideDateTimePicker.Builder(requireActivity().supportFragmentManager)
                        .setListener(mDateTimePickerListener)
                        .setMinDate(Date(System.currentTimeMillis()))
                        .setMinTime(Date(System.currentTimeMillis()))
                        .setInitialDate(Date(System.currentTimeMillis() + 30 * 60 * 1000))
                        .setIndicatorColor(ContextCompat.getColor(requireContext(), R.color.owner_red))
                        .build()
                dateTimePicker?.show()
            }

            R.id.ll_mission_title, R.id.et_content_mission_title -> {
                (activity as MainActivity).hideSoftKeyboard()
                et_content_mission_title.postDelayed({
                    et_content_mission_title.requestFocus()
                    (activity as MainActivity).showSoftKeyboard(et_content_mission_title)
                }, 500)
            }
            R.id.ll_salary, R.id.et_content_salary -> {
                (activity as MainActivity).hideSoftKeyboard()
                et_content_salary.postDelayed({
                    et_content_salary.requestFocus()
                    (activity as MainActivity).showSoftKeyboard(et_content_salary)
                }, 500)
            }

            R.id.btn_clear_text->{
                et_content_salary.setText("0")
            }

        }
    }

    private fun selectLoc(is_delivery_loc: Boolean) {
        Log.d("NewMission - selectLoc:$is_delivery_loc")
        mViewModel.tmp_mission.is_delivery_loc = is_delivery_loc
        cacheTmpMission()
        (activity as MainActivity).addFragmentWithAnim(NewMissionSelectLocation.FRAGMENT_ID
        ,R.anim.anim_bottom_top_enter, R.anim.anim_top_bottom_leave)
    }

    private fun cacheTmpMission() {
        mViewModel.tmp_horizontalrecyclerviewAdapter = mImgListAdapter
        mViewModel.tmp_mission.title = et_content_mission_title.text.toString()
        mViewModel.tmp_mission.finish_time = if (mViewModel.tmp_mission.devilery_time == 0L) {
            ""
        } else {
            Utils.getDate(
                mViewModel.tmp_mission.devilery_time!!, "MM月 dd日 EEEE               hh:mm:ss a     "
            )
        }
        mViewModel.tmp_mission.description = et_mission_content.text.toString()
        mViewModel.tmp_mission.comment = et_mission_demand_content.text.toString()
        if (et_content_salary.text.isNullOrEmpty())
            mViewModel.tmp_mission.salary = 0
        else
            mViewModel.tmp_mission.salary = et_content_salary.text.toString().toInt()

    }


    private var clearFocusListener: View.OnClickListener = View.OnClickListener {
        (activity as MainActivity).hideSoftKeyboard()
        et_content_mission_title.clearFocus()
        et_mission_content.clearFocus()
        et_content_salary.clearFocus()
        et_content_salary.setText(Utils.trimLeadingZeros(et_content_salary.text.toString()))
//        if (sc_container.scrollY > 10) {
//            fab_to_top.show()
//        }
    }

    private var mOnImageItemClickListener: IOnItemClickListener<ImageModel> =
        object : IOnItemClickListener<ImageModel> {
            override fun onItemClick(imageModel: ImageModel, position: Int) {

                Dexter.withActivity(activity)
                    .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                    .withListener(object : MultiplePermissionsListener {
                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                            if (report.areAllPermissionsGranted()) {
                                if (imageModel.getImagePath()
                                    == Utils.getURLForResource(R.drawable.fab_add)
                                ) {
                                    if(dialog_pic_selector!=null && dialog_pic_selector?.isShowing!!)
                                        return
                                    dialog_pic_selector =
                                        CustomDialogBuilder(requireContext(), R.layout.custom_dialog_camera_or_gallery)
                                            .setCameraListener(onCameraClick)
                                            .setGalleryListener(onSingleGalleryClick)
                                            .setCustomTitle(R.string.tv_img_upload)
                                            .build()
                                    dialog_pic_selector?.findViewById<LinearLayout>(R.id.ll_delete_panel)?.visibility =
                                        View.GONE

                                } else {
                                    if(dialog_pic_selector!=null && dialog_pic_selector?.isShowing!!)
                                        return
                                    dialog_pic_selector =
                                        CustomDialogBuilder(requireContext(), R.layout.custom_dialog_camera_or_gallery)
                                            .setCameraListener(onCameraClick)
                                            .setGalleryListener(onGalleryClick)
                                            .setCustomTitle(R.string.tv_img_upload)
                                            .build()
                                    dialog_pic_selector?.findViewById<LinearLayout>(R.id.ll_delete_panel)?.visibility =
                                        View.VISIBLE
                                    dialog_pic_selector?.findViewById<Button>(R.id.btnDelete)
                                        ?.setOnClickListener {
                                            var mList = mImgListAdapter!!.getList()
                                            mList.removeAt(position)
                                            mImgListAdapter!!.notifyDataSetChanged()
                                            onImgDataChangeObsver.onChanged()
                                            dialog_pic_selector?.dismiss()
                                        }
                                }
                                dialog_pic_selector?.show()


                                img_select_position = position
                            } else {
                                // TODO - handle permission denied case
                                Toast.makeText(activity, "permission denied", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permissions: List<PermissionRequest>,
                            token: PermissionToken
                        ) {
                            token.continuePermissionRequest()
                        }
                    }).check()
            }

            override fun onItemClick(view: View, position: Int) {
                //DO NOTHING
            }
        }

    private val onCameraClick = View.OnClickListener {
        launchCameraIntent()
    }

    private val onGalleryClick = View.OnClickListener {
        launchGalleryIntent()
    }
    private val onSingleGalleryClick = View.OnClickListener {
        launchGalleryIntent(false)
    }
    //--------------Listener-----------------


    @SuppressLint("ResourceType")
    private fun launchCameraIntent() {
        dialog_pic_selector?.dismiss()
        cacheTmpMission()
        ImagePicker.with(this)
            .setCameraOnly(true)
            .setMultipleMode(false)
            .setRequestCode(REQUSET_CAMERA)
            .setToolbarColor(getString(R.color.owner_red))
            .setIndicatorColor(getString(R.color.owner_red))
            .setBackgroundColor(getString(R.color.colorMainBrightWhite))
            .start()
    }

    @SuppressLint("ResourceType")
    private fun launchGalleryIntent(isMultipleMode: Boolean =true) {
        dialog_pic_selector?.dismiss()
        cacheTmpMission()
        ImagePicker.with(this)
            .setFolderMode(true)
            .setFolderTitle("Album")
            .setRootDirectoryName(Config.ROOT_DIR_DCIM)
            .setDirectoryName("Image Picker")
            .setMultipleMode(isMultipleMode)
            .setShowNumberIndicator(true)
            .setMaxSize(6-mImgListAdapter!!.getList().size)
            .setShowCamera(false)
            .setSelectedImages(selectedImages)
            .setRequestCode(REQUEST_IMAGE)
            .setToolbarColor(getString(R.color.owner_red))
            .setIndicatorColor(getString(R.color.owner_red))
            .setBackgroundColor(getString(R.color.colorMainBrightWhite))
            .start()

    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int, data: Intent?
    ) {
        Log.e("image.uri : RESULTOK")
        mImgListAdapter = mViewModel.tmp_horizontalrecyclerviewAdapter
        if (requestCode == REQUEST_IMAGE || requestCode == REQUSET_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                val images: ArrayList<Image> = ImagePicker.getImages(data)
                Log.e("image.uri : "+images.size)
                // Do stuff with image's path or id. For example:
                for (image in images) {
                    Log.d("image.uri : "+image.uri)
                    var item: ImageModel?
                    if (img_select_position == 0) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            item = ImageModel(image.id.toInt(), image.uri.toString())
                        } else {
                            item = ImageModel(image.id.toInt(), image.path)
                        }
                        mImgListAdapter!!.getList().add(item)
                    } else {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            item = ImageModel(image.id.toInt(), image.uri.toString())
                        } else {
                            item = ImageModel(image.id.toInt(), image.path)
                        }
                        Log.d("img_select_position: "+img_select_position +" | size: "+mImgListAdapter!!.getList().size)
                        mImgListAdapter!!.getList()[img_select_position!!]=item
                    }
                }
                mImgListAdapter!!.notifyDataSetChanged()
                onImgDataChangeObsver.onChanged()
            }
        }
    }


    private fun setDuration(sec :Int){
        var hour = sec?.div(60)?.div(60)
        var min= sec!! %3600
        min /= 60
        requireActivity().runOnUiThread {
            if(sec<=0) {
                tv_content_duration.text = resources.getString(R.string.tv_click_to_select)
                tv_content_duration.setLocationSelect(false)
            }else{
                tv_content_duration.text = "$hour 小時   :   $min 分鐘"
                tv_content_duration.setLocationSelect(true)
            }
        }

        mViewModel.tmp_mission.duration = sec
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        Log.e("onPageSelected: $position")
    }

    override fun onPageScrollStateChanged(state: Int) {

    }
}