package com.app.lbs.ui.viewmodels

import android.app.Application
import android.content.Context
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.lbs.R
import com.app.lbs.adapters.HorizontalRecyclerViewAdapter
import com.app.lbs.models.Mission
import com.app.lbs.models.Store
import com.app.lbs.models.UserAccount
import com.app.lbs.models.UserSetting
import com.app.lbs.repository.JobListRepository
import com.app.lbs.repository.SysRepository
import com.app.lbs.utils.*
import com.google.android.gms.maps.model.LatLng
import com.app.lbs.utils.Const.USERSTATE

/**
 * @file MainViewModel
 * @brief
 *       處理View, User Data相關資訊
 *       確保data不會隨短期生命週期轉換而遺失
 *
 *
 * @author Zack
 * */


open class MainViewModel(application: Application) : AndroidViewModel(application){

    //SingleLiveEvent : only sent event one time
    //MutableLiveData : might resent event when lifecycle changed

    private var Log = MyLog.log()
    private val mTitle = MutableLiveData<String>()
    private val mUserLiveData: MutableLiveData<UserAccount>
    var mUser: UserAccount?=null
    private val mSysRepo: SysRepository
    private var mJobListRepo: JobListRepository
    var tmp_mission: Mission //cache massion
    var tmp_mission_w: Mission //cache massion work arround
    var tmp_horizontalrecyclerviewAdapter: HorizontalRecyclerViewAdapter?=null
    private val mObservMission = MutableLiveData<Mission>()
    private val mObservTmpMission = MutableLiveData<Mission>()
    private val mUserState = MutableLiveData<USERSTATE>()//0:default 1:owner 2:taker 3:decide by previous
    private var mainBackground:Int=R.color.hyperlink_blue
    private var isOwner = false
    var lastState:USERSTATE?=null

    private val result_code = MutableLiveData<Int>()
    init {

        var userData:String?=application.getSharedPreferences(Const.APPNAME, Context.MODE_PRIVATE)
            .getString(Const.PREF_USER_KEY, null)
        Log.i("MainViewModel INIT : $userData")
//        if(userData==null){
//            userData="{\"accessToken\":\"ya29.a0AfH6SMCIZAjatEXIqjtcn2B-tCL7_LsuK2srqk4zVpIIRdkqQjWgs3dzS3c5fJ1ALU-SH5KwsSWkaA80CE2USU4vLIjhbS_OaayHZDqTEIt5N9GVH8C88--RYXAEVW0t1iVMf25VjLeuehHNqbnaTjzHHxBB\",\"apiToken\":\"a65abea8f21e4750afc86bcd20909b61\",\"email\":\"loststar1991@gapp.nthu.edu.tw\",\"familyName\":\"\",\"givenName\":\"Phantom\",\"id\":\"110354830222471366469\",\"locale\":\"zh-TW\",\"name\":\"Phantom\",\"picture\":\"https://lh4.googleusercontent.com/-1sVAcDtd340/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucnxGIZiLLrKFYQWZD0kAlPCkzEflw/s96-c/photo.jpg\",\"refreshToken\":\"1//0e_3rJsw2Gnp3CgYIARAAGA4SNwF-L9Irid-YxfLtpvQxFgjk9uNNovfpsPVBJfxj3bi2KJDCYGIa7BxvORNekhSND28CFViF9xE\",\"resultCode\":0,\"userData\":{\"business_district_index\":[],\"user_email\":\"loststar1991@gapp.nthu.edu.tw\",\"user_firstname\":\"xus\",\"user_id\":44,\"user_lastname\":\"za\",\"user_nickname\":\"xus za\",\"user_picture\":\"https://lh4.googleusercontent.com/-1sVAcDtd340/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucnxGIZiLLrKFYQWZD0kAlPCkzEflw/s96-c/photo.jpg\",\"verified_by_email\":false,\"verified_by_mobilephone\":false},\"userSetting\":{\"missionDetectRange\":5.0,\"priceIncreaseAlert\":false},\"verifiedEmail\":true}"
//        }
        mUser = gson.fromJson(userData, UserAccount::class.java)
        if(mUser?.userSetting==null)
            mUser?.userSetting = UserSetting()
        mJobListRepo = JobListRepository.getInstance(mUser!!)
        Log.v("user: $userData")
        mSysRepo = SysRepository.getInstance()
        mSysRepo.setUserData(mUser!!)
        mUserLiveData =  mSysRepo.getUserData()
        tmp_mission = mJobListRepo.tmp_mission
        tmp_mission_w = Mission()
    }

    fun getMainBG() :Int{
        return mainBackground
    }

    fun setMainBG(bg:Int){
        mainBackground = bg
    }
    fun getTitle(): LiveData<String> {
        return mTitle
    }

    fun setTitle(str: String) {
        mTitle.postValue(str)
    }

    fun getUserState(): LiveData<USERSTATE> {
        return mUserState
    }

    fun setUserState(state: USERSTATE) {
        when(state){
            USERSTATE.OWNER ->{isOwner=true
                mUserState.postValue(state)
                lastState = USERSTATE.OWNER
                mUser!!.isOwner=true
                saveUserData()
            }
            USERSTATE.TAKER ->{isOwner=false
                mUserState.postValue(state)
                lastState = USERSTATE.TAKER
                mUser!!.isOwner=false
                saveUserData()
            }
            USERSTATE.DECIED_BY_PREVIOUS ->{
                if(isOwner)mUserState.postValue(USERSTATE.OWNER)
                else mUserState.postValue(USERSTATE.TAKER)
            }
            else -> mUserState.postValue(state)
        }
    }

    fun getJobList(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getJobList()
    }

    fun getOwnJobListPre(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getOwnJobListPre()
    }
    fun getOwnJobListOnGoing(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getOwnJobListOnGoing()
    }
    fun getOwnJobFinList(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getOwnJobFinList()
    }

    fun getTakenJobListApply(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getTakenJobListApply()
    }
    fun getTakenJobListOnGoing(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getTakenJobListOnGoing()
    }

    fun getTakenJobFinList(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getTakenJobFinList()
    }

    fun getOwnerNotiList(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getOwnerNotiList()
    }

    fun getTakerNotiList(): MutableLiveData<List<Mission>> {
        return mJobListRepo.getTakerNotiList()
    }

    fun getLocation(): MutableLiveData<Location> {
        return mSysRepo.getLocation()
    }

    fun getStoreList():MutableLiveData<List<Store>>{
        return mJobListRepo.getStoreList()
    }

    fun getStoreId():MutableLiveData<Int>{
        return mJobListRepo.getStoreId()
    }

    fun saveUserData(){
        mUserLiveData.postValue(mUser)
    }

    fun getUserData():LiveData<UserAccount>{
        return mUserLiveData
    }

    fun getResultCode():LiveData<Int>{
        return result_code
    }

    fun getAlertMission(): MutableLiveData<SysRepository.AlertMission> {
        return mSysRepo.getAlertMission()
    }

    fun getCurMemorySize():MutableLiveData<String>{
        return mSysRepo.getCurMemorySize()
    }

    fun getObservMission():MutableLiveData<Mission>{
        Log.d("TIMMER - getObservMission : "+tmp_mission.duration)
        return mObservMission
    }
    fun getObservTmpMission():MutableLiveData<Mission>{
        Log.d("TIMMER - getObservTmpMission : "+tmp_mission.duration)
        return mObservTmpMission
    }

    fun updateTmpMission(){
        Log.d("TIMMER - updateTmpMission "+tmp_mission.duration)
        mObservTmpMission.setValue(tmp_mission)
    }
    fun updateMission(){
        Log.d("TIMMER - updateMission "+tmp_mission.duration)
        mObservMission.setValue(tmp_mission)
    }

    fun getIsRequestLocation():MutableLiveData<Boolean>{
        return mSysRepo.getIsRequestLocation()
    }
    fun getDebug():MutableLiveData<Boolean> {return mSysRepo.getIsDEBUG()}
    fun setDebug(debug:Boolean):MutableLiveData<Boolean>{
        mSysRepo.getIsDEBUG().postValue(debug)
        Const.DEBUG = debug
        return mSysRepo.getIsDEBUG()
    }

    /**
     * ------------server call------------ *
     */
    fun updateJobList() {
        val param = QueryBody()
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.userId = mUser?.userData?.userId.toString()
        param.apiToken = mUser?.apiToken
        val location: Location = mSysRepo.getLocation().value!!
        param.longi = location.longitude.toString()
        param.lati = location.latitude.toString()
        param.radius = mUser?.userSetting?.missionDetectRange.toString()
        param.mac = Utils.getMac("RenewSubDB", param.serializeToMap())
        mJobListRepo.serverRenewSubDB(param)
    }

    fun updateOwnAndTakenJobList() {
        val param = QueryBody()
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.userId = mUser?.userData?.userId.toString()
        param.apiToken = mUser?.apiToken
        param.mac = Utils.getMac("RetrieveJobList", param.serializeToMap())
        mJobListRepo.serverRetrieveJobList(param)
    }

    fun serverGetCountryData() {
        val param = QueryBody()
        param.apiToken = mUser?.apiToken
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.userId = mUser?.userData?.userId.toString()
        param.mac = Utils.getMac("GetCountryData", param.serializeToMap())
        mSysRepo.serverGetCountryData(param)
    }

    fun serverCreateNewStore(store:Store){
        val param = QueryBody()
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.userId = mUser?.userData?.userId.toString()
        param.apiToken = mUser?.apiToken
        param.store = store.store
        val loc = Utils.getLatLng(store.coor2addr)
        param.longi = loc.longitude.toString()
        param.lati = loc.latitude.toString()
        param.addr = store.addr
        param.mac = Utils.getMac("CreateNewStore", param.serializeToMap())
        mJobListRepo.serverCreateNewStore(param)
    }

    fun serverGetStoreList(lati:String, lng:String){
        val param = QueryBody()
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.userId = mUser?.userData?.userId.toString()
        param.apiToken = mUser?.apiToken
        param.longi = lng
        param.lati = lati
        param.mac = Utils.getMac("GetStoreList", param.serializeToMap())
        mJobListRepo.serverGetStoreList(param)
    }

    fun serverCreateNewJob(){
        val param = QueryBody()
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.apiToken = mUser?.apiToken
        param.userId = mUser?.userData?.userId.toString()
        param.title = tmp_mission.title
        param.jobType = tmp_mission.job_type
        param.description = tmp_mission.description
        param.startTime = param.timestamp
        param.duration = tmp_mission.duration
        param.storeId = tmp_mission.store_id
        val loc:LatLng =Utils.getLatLng(tmp_mission.coor2addr)
        param.longi = loc.longitude.toString()
        param.lati = loc.latitude.toString()
        if(tmp_mission.job_type==1){ //question=0, delivery=1
            param.salary = tmp_mission.salary
        }
        param.mac = Utils.getMac("CreateNewJob", param.serializeToMap())
        mJobListRepo.serverCreateNewJob(param)
    }

    fun serverTakeJob(){
        val param = QueryBody()
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.userId = mUser?.userData?.userId.toString()
        param.apiToken = mUser?.apiToken
        param.jobId = tmp_mission.job_id
        param.mac = Utils.getMac("TakeJob", param.serializeToMap())
        mJobListRepo.serverTakeJob(param)
    }

    fun serverFinishJob(){
        val param = QueryBody()
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.userId = mUser?.userData?.userId.toString()
        param.apiToken = mUser?.apiToken
        param.jobId = tmp_mission.job_id
        param.comment = "NULL"
        param.mac = Utils.getMac("FinishJob", param.serializeToMap())
        mJobListRepo.serverFinishJob(param)
    }

    fun serverCancelJob(){
        val param = QueryBody()
        param.timestamp = mSysRepo.getSysTime().value.toString()
        param.userId = mUser?.userData?.userId.toString()
        param.apiToken = mUser?.apiToken
        param.jobId = tmp_mission.job_id
        param.comment = "NULL"
        param.mac = Utils.getMac("CancelJob", param.serializeToMap())
        mJobListRepo.serverCancelJob(param)
    }

    fun initNewMissionObject(){
        Log.i("initNewMissionObject")
        tmp_mission = mJobListRepo.initNewMissionObject()
        mObservMission.setValue(tmp_mission)
        mObservTmpMission.setValue(tmp_mission)
    }
}
