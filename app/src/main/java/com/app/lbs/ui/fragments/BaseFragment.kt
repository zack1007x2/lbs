package com.app.lbs.ui.fragments

import android.view.KeyEvent
import android.view.View
import androidx.fragment.app.Fragment
import com.app.lbs.interfaces.IonBackPress
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.utils.MyLog


/**
 * @file BaseFragment
 * @brief
 *       統一管理fragment的共用功能
 *
 * @author Zack
 * */
abstract class BaseFragment : Fragment(), IonBackPress {

    open var Log = MyLog.log()
    open var FRAGMENT_ID_INT:Int?=-1

    fun setTitle(StringId: Int) {
        (activity as MainActivity).setTitle(resources.getString(StringId))
    }

    fun setTitle(str: String) {
        (activity as MainActivity).setTitle(str)
    }

    open fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return false
    }

    override fun onBackPressed():Boolean {
        return true
    }

    open fun refreshUI(){

    }
}
