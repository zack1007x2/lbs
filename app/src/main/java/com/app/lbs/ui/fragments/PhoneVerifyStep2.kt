package com.app.lbs.ui.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.ui.activities.LoginActivity
import com.app.lbs.ui.viewmodels.LoginViewModel
import com.app.lbs.utils.Const

class PhoneVerifyStep2 : BaseFragment(), View.OnClickListener {
    private lateinit var mViewModel: LoginViewModel
    private lateinit var btn_next: Button
    private lateinit var btn_resent: Button
    private lateinit var et_phone_veri:EditText
    private lateinit var tv_phone_vari_prefix:TextView
    lateinit var mPref: SharedPreferences
    companion object {
        const val FRAGMENT_ID = R.id.rv_taken_missionList
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.viewpage_phone_veri_step2, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[LoginViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        initView(root)
        mPref = requireActivity().getSharedPreferences(Const.APPNAME, Context.MODE_PRIVATE)
        return root
    }

    override fun onResume() {
        super.onResume()
        mViewModel.getPrefix().observe(viewLifecycleOwner, Observer { pf->
            tv_phone_vari_prefix.text = "$pf  -  "
        })
    }

    override fun onPause() {
        super.onPause()
        mViewModel.getPrefix().removeObservers(viewLifecycleOwner)
    }

    private fun initView(v: View) {
        btn_resent = v.findViewById(R.id.btn_resent)
        btn_next = v.findViewById(R.id.btn_next)
        tv_phone_vari_prefix = v.findViewById(R.id.tv_phone_vari_prefix)
        btn_resent.setOnClickListener(this)
        btn_next.setOnClickListener(this)
        et_phone_veri = v.findViewById(R.id.et_phone_veri)

        et_phone_veri.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(et_phone_veri.text.toString().length==6){
                    btn_next.isEnabled = true
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        btn_next.setTextColor(resources.getColor(R.color.colorMainBrightWhite,null))
                    }else{
                        btn_next.setTextColor(resources.getColor(R.color.colorMainBrightWhite))
                    }
                    btn_next.setBackgroundResource(R.drawable.custom_owner_btn_oval_pressed)

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        btn_resent.setTextColor(resources.getColor(R.color.black,null))
                    }else{
                        btn_resent.setTextColor(resources.getColor(R.color.black))
                    }
                    btn_resent.setBackgroundResource(R.drawable.custom_btn_oval_disable)
                }else{
                    btn_next.isEnabled = false
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        btn_next.setTextColor(resources.getColor(R.color.black,null))
                    }else{
                        btn_next.setTextColor(resources.getColor(R.color.black))
                    }
                    btn_next.setBackgroundResource(R.drawable.custom_btn_oval_disable)

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        btn_resent.setTextColor(resources.getColor(R.color.colorMainBrightWhite,null))
                    }else{
                        btn_resent.setTextColor(resources.getColor(R.color.colorMainBrightWhite))
                    }
                    btn_resent.setBackgroundResource(R.drawable.custom_owner_btn_oval_pressed)
                }

            }

        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_resent -> {
                //TODO 重新發送驗證碼
                et_phone_veri.setText("")
                tv_phone_vari_prefix.setText(R.string.phone_vari_init_string)
                mViewModel.serverSMSValidation(mViewModel.mMobile)
            }
            R.id.btn_next -> {
                //TODO 驗證驗證碼後處理
                if((parentFragment as PhoneVerifyFragment).isSettingMode()){
                    activity?.onBackPressed()
                }else{
                    mPref.edit().putBoolean(Const.PREF_TUTORIAL, true).apply()
                    (activity as LoginActivity).replaceFragment(IntroFragment())
                }

            }
        }
    }


}