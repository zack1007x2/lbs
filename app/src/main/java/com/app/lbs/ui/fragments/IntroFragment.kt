package com.app.lbs.ui.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.app.lbs.R
import com.app.lbs.adapters.PhoneVerifyPageAdapter
import com.app.lbs.databinding.FragmentIntroductionBinding
import com.app.lbs.ui.viewmodels.LoginViewModel
import com.app.lbs.utils.Const
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * @file IntroFragment
 * @brief
 *       App介紹頁面管理，依序顯示 -------- IntroTakerFragment
 *                                  |--- IntroOwnerFragment
 *                                  |--- IntroSelectFragment
 *
 * @author Zack
 **/

class IntroFragment : BaseFragment(), View.OnClickListener{
    lateinit var mLoginViewModel: LoginViewModel
    lateinit var binding: FragmentIntroductionBinding
    private lateinit var intro_viewpager: ViewPager2
    private lateinit var tabLayout: TabLayout
    lateinit var mPref: SharedPreferences
    lateinit var mPageAdapter: PhoneVerifyPageAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        Log.i("onCreateView")

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_introduction,
            container,
            false
        )
        mLoginViewModel = activity?.run {
            ViewModelProvider(this)[LoginViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        binding.loginViewModel = this.mLoginViewModel
        init(binding.root)
        mPref = requireActivity().getSharedPreferences(Const.APPNAME, Context.MODE_PRIVATE)
        return binding.root
    }

    fun init(v: View) {
        intro_viewpager = v.findViewById(R.id.intro_viewpager)
         tabLayout = v.findViewById(R.id.tabLayout)
        mPageAdapter = PhoneVerifyPageAdapter(childFragmentManager,lifecycle)
        mPageAdapter.setFragmentsList(
            arrayListOf(
                IntroTakerFragment(),IntroOwnerFragment(),IntroSelectFragment()
            )
        )
        intro_viewpager.adapter = mPageAdapter
        TabLayoutMediator(tabLayout, intro_viewpager) { tab, position ->
            //Some implementation
        }.attach()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
        }
    }


}