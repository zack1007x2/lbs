package com.app.lbs.ui.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.BuildConfig
import com.app.lbs.R
import com.app.lbs.adapters.NearByLocationAdapter
import com.app.lbs.adapters.SearchLocationAdapter
import com.app.lbs.interfaces.IDialogValueListener
import com.app.lbs.interfaces.IFilterResults
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.map_models.LocationModel
import com.app.lbs.repository.SysRepository
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.dialog.CustomBottomSheetDialogNearBy
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.CustomDialogBuilder
import com.app.lbs.utils.view.CustomBottomSheetBehavior
import com.app.lbs.utils.view.CustomBottomSheetBehavior.Companion.STATE_ANCHOR
import com.app.lbs.utils.view.CustomBottomSheetBehavior.Companion.STATE_COLLAPSED
import com.app.lbs.utils.view.CustomBottomSheetBehavior.Companion.STATE_EXPANDED
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.*
import com.google.gson.JsonElement
import com.app.lbs.models.map_models.LocationApiModule
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.math.log2
import android.util.Base64
import com.app.lbs.interfaces.IonBackPress
import com.app.lbs.utils.Utils
import org.jetbrains.anko.doAsync


/**
 * @file NewMissionSelectLocation
 * @brief
 *       用於新增任務的自訂新地點
 *
 * @author Zack
 * */
class NewMissionSelectLocation : BaseFragment(), View.OnClickListener, IOnItemClickListener<LocationModel>
    , CompoundButton.OnCheckedChangeListener,GoogleMap.OnCameraMoveListener,
    GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener {

    private lateinit var mViewModel: MainViewModel
    private var mMapView: MapView? = null
    private var googleMap:GoogleMap?=null
    lateinit var mLocation: Location
    private var mMapLocation:LatLng?=null
    private lateinit var bottomBehavior: CustomBottomSheetBehavior<View>
    private lateinit var ll_bottom_sheet: LinearLayout
    private lateinit var et_search_location_bar:EditText
    private lateinit var btn_text_clear:Button
    private lateinit var rv_location_list:RecyclerView
    private var mLocationAdapter:SearchLocationAdapter?=null
    private var mNearByAdapter: NearByLocationAdapter?=null
    private var favorite_loc_map = hashMapOf<String,LocationModel>()

    private var storeId: Int? = null
    private var isSubmitClick = false
    private var queryString: String? = null
    private lateinit var placesClient: PlacesClient
    private var token: AutocompleteSessionToken? = AutocompleteSessionToken.newInstance()

    private lateinit var rl_confirm_loc:RelativeLayout
    private lateinit var btn_custom_loc:Button
    private lateinit var btn_search_nearby:Button
    private lateinit var tv_location_name_content:TextView
    private lateinit var tv_location_addr_content:TextView
    private lateinit var search_bar_status:TextView
    private lateinit var img_custom_loc:ImageView
    private var mCustomBottomSheetDialogNearBy:CustomBottomSheetDialogNearBy?=null
    private var isOwner:Boolean?=false
    private var isNearBySearch=false
    private var addLocDialog:Dialog? =null

    override fun onCameraMove() {

    }


    companion object {
        const val FRAGMENT_ID = R.id.btn_content_finish_location
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root =
            inflater.inflate(R.layout.fragment_new_mission_select_location, container, false)

        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        mMapView = root.findViewById(R.id.mv_new_mission)
        mMapView!!.onCreate(savedInstanceState)
        mMapView!!.onResume() // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(requireActivity().applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if(!Places.isInitialized())
            Places.initialize(requireContext(), BuildConfig.API_KEY)

        placesClient = Places.createClient(requireContext())

        setHasOptionsMenu(true)
        isOwner=  (mViewModel.getUserState().value==Const.USERSTATE.OWNER)
        initView(root)

        return root
    }

    @SuppressLint("ClickableViewAccessibility")
    fun initView(v: View) {
        ll_bottom_sheet = v.findViewById(R.id.bottom_sheet_location_search)
        bottomBehavior = CustomBottomSheetBehavior.from(ll_bottom_sheet)
        bottomBehavior.setBottomSheetCallback(bottomSheetCallBack)
        bottomBehavior.setAnchorOffset(0f)
        bottomBehavior.state = STATE_COLLAPSED

        mCustomBottomSheetDialogNearBy = CustomBottomSheetDialogNearBy()
        btn_text_clear = v.findViewById(R.id.btn_text_clear)
        btn_text_clear.setOnClickListener(this)
        rv_location_list = v.findViewById(R.id.rv_location_list)
        rv_location_list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        search_bar_status = v.findViewById(R.id.search_bar_status)
        search_bar_status.setText(R.string.tv_loc_saved)
        mLocationAdapter = SearchLocationAdapter(requireContext(), null, true,isOwner!! , favorite_loc_map)
        mLocationAdapter?.setOnCheckChangeListener(this@NewMissionSelectLocation)
        mLocationAdapter?.setOnItemClickListener(this@NewMissionSelectLocation)
        mLocationAdapter?.setFavoriteList(favorite_loc_map)
        rv_location_list.adapter = mLocationAdapter
        rl_confirm_loc = v.findViewById(R.id.rl_confirm_loc)
        rl_confirm_loc.setOnClickListener(this)
        btn_custom_loc = v.findViewById(R.id.btn_custom_loc)
        btn_custom_loc.setOnClickListener(this)
        btn_search_nearby = v.findViewById(R.id.btn_search_nearby)
        btn_search_nearby.setOnClickListener(this)
        tv_location_name_content = v.findViewById(R.id.tv_location_name_content)
        tv_location_addr_content = v.findViewById(R.id.tv_location_addr_content)
        img_custom_loc = v.findViewById(R.id.img_custom_loc)


        et_search_location_bar = v.findViewById(R.id.et_search_location_bar)
        et_search_location_bar.setOnClickListener(this)
        et_search_location_bar.isFocusableInTouchMode = false
        et_search_location_bar.isFocusable=false
        et_search_location_bar.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(s!!.isNotEmpty()){
                    search_bar_status.setText(R.string.tv_loc_result)
                    queryString = s.toString()
                    isNearBySearch=false
                    getNearbyPlaceViaWeb(queryString, LatLng(mLocation.latitude, mLocation.longitude))
                }else{
                    Log.i("afterTextChanged: NULL" + (rv_location_list.adapter != null)+" | "+favorite_loc_map.toString())
                    queryString = ""
                    search_bar_status.setText(R.string.tv_loc_saved)
                    if(mLocationAdapter!=null) {
                        (rv_location_list.adapter as SearchLocationAdapter).setFavoriteList(favorite_loc_map)
                        rv_location_list.adapter!!.notifyDataSetChanged()
                        Log.i("af " + rv_location_list.adapter!!.itemCount)

                    }else{
                        rv_location_list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                        mLocationAdapter = SearchLocationAdapter(requireContext(), null, true,isOwner!! , favorite_loc_map)
                        mLocationAdapter?.setOnCheckChangeListener(this@NewMissionSelectLocation)
                        mLocationAdapter?.setOnItemClickListener(this@NewMissionSelectLocation)
                        mLocationAdapter?.setFavoriteList(favorite_loc_map)
                        rv_location_list.adapter = mLocationAdapter

                    }
                }
            }
        })
        mMapView

        placesClient = Places.createClient(requireContext())
    }

    override fun onResume() {
        super.onResume()
        setTitle(R.string.title_add_mission)
//        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        storeId = null
        isSubmitClick = false



        mMapView!!.onResume()
        mViewModel.getLocation().observe(viewLifecycleOwner, mLocationObserver)
        mLocation = SysRepository.getInstance().getLocation().value!!

    }

    override fun onPause() {
        super.onPause()
        mMapView!!.onPause()
        mViewModel.getLocation().removeObserver(mLocationObserver)
        token=null
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }

    private var mLocationObserver: Observer<Location> =
        Observer { t ->
            mLocation = t
            if(mMapLocation==null)
                mMapLocation = LatLng(mLocation.latitude, mLocation.longitude)
            mMapView!!.getMapAsync { mMap ->
                googleMap = mMap
                mMap.setOnCameraMoveStartedListener(this)
                mMap.setOnCameraIdleListener(this)
                mMap.uiSettings.isMapToolbarEnabled = false
                mMap.clear()
                mMap.addMarker(
                    MarkerOptions().position(LatLng(mLocation.latitude, mLocation.longitude))
                        .icon(BitmapDescriptorFactory.fromResource(
                            if(isOwner!!)
                                R.drawable.user_onmap_owner else R.drawable.user_onmap_taker))
                )

                val zoom =
                    (log2(40000.0 / 0.5) - 0.5).toFloat()
                val loc = LatLng(mLocation.latitude, mLocation.longitude)
                // For zooming automatically to the location of the marker
                val cameraPosition = CameraPosition.Builder()
                    .target(loc)
                    .zoom(zoom)
                    .build()
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                mMap.setOnMapClickListener {
                    (activity as MainActivity).hideSoftKeyboard()
                }

            }
        }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.et_search_location_bar->{
                if(bottomBehavior.state== STATE_COLLAPSED) {
                    bottomBehavior.state = STATE_EXPANDED
                }else{
                    et_search_location_bar.postDelayed({
                        et_search_location_bar.isFocusableInTouchMode = true
                        et_search_location_bar.isFocusable=true
                        if(!et_search_location_bar.isCursorVisible)
                            et_search_location_bar.isCursorVisible = true
                        et_search_location_bar.requestFocus()
                        (activity as MainActivity).showSoftKeyboard(et_search_location_bar)
                        }, 50)
                }
            }

            R.id.btn_text_clear->{
                et_search_location_bar.setText("")
            }

            R.id.btn_custom_loc->{
                //show custom loc
                val geocoder = Geocoder(requireContext(), Locale.getDefault())
                var address =""
                doAsync{
                    try {
                        val addresses: List<Address> = geocoder.getFromLocation(
                            mMapLocation!!.latitude,
                            mMapLocation!!.longitude,
                            1
                        )
                        address =
                            addresses[0].getAddressLine(0)
                        addLocDialog?.findViewById<EditText>(R.id.et_address_bar)?.setText(address)
                    }catch (e:Exception){
                        Log.e(e)
                        address = ""
                    }
                }
                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                addLocDialog = CustomDialogBuilder(requireContext(), R.layout.custom_bottom_sheet_add_custom_loc)
                    .setCustomMessage(address)
                    .setIFilterResults(object :IFilterResults{
                        override fun ResultPublished(results: ArrayList<*>) {
                            var tmp = LocationModel( Base64.encodeToString(System.currentTimeMillis().toString().toByteArray(), Base64.DEFAULT)
                                , results[0].toString(), results[1].toString(), mMapLocation!!.latitude, mMapLocation!!.longitude)
                            rl_confirm_loc.visibility = View.VISIBLE
                            btn_custom_loc.visibility = View.GONE
                            tv_location_name_content.text = tmp.name
                            tv_location_addr_content.text = tmp.addr

                            rl_confirm_loc.tag = tmp

                            if(results[2].toString().trim() == true.toString()){
                                Log.e("ttttt: "+ (results[2].toString().trim() == true.toString()))
                                favorite_loc_map[tmp.place_id!!] = tmp
                            }

                            if(addLocDialog?.isShowing!!){
                                addLocDialog?.dismiss()
                                (activity as MainActivity).hideSoftKeyboard()
                            }

                            if(isOwner!!)
                                img_custom_loc.setImageResource(R.drawable.marker_owner)
                            else
                                img_custom_loc.setImageResource(R.drawable.marker_taker)


                        }
                        override fun ResultPublished(results: Array<*>) {
                        }
                    })
                    .build()
                addLocDialog?.show()

//                val city = addresses[0].locality
//                val state = addresses[0].adminArea
//                val country = addresses[0].countryName
//                val postalCode = addresses[0].postalCode
//                val knownName = addresses[0].featureName // Only if available else return NULL
            }


            R.id.rl_confirm_loc->{
                //dismiss bottom sheet return to new mission or modify
                var loc = rl_confirm_loc.tag as LocationModel
                Log.e("rl_confirm_loc click: "+loc)
                if( mViewModel.tmp_mission.is_delivery_loc) {
//                    mViewModel.tmp_mission.comment = loc.name
                    mViewModel.tmp_mission.addr2coor = loc.addr
                    mViewModel.tmp_mission.coor2addr = Utils.getCoor2Addr(LatLng(loc.lat!!, loc.lng!!))
                }else{
                    mViewModel.tmp_mission.store_name = loc.name!!
                    mViewModel.tmp_mission.store_addr = loc.addr!!
                    mViewModel.tmp_mission.store_coor2addr = Utils.getCoor2Addr(LatLng(loc.lat!!, loc.lng!!))
                }

                mViewModel.updateTmpMission()
                activity?.onBackPressed()
            }
            R.id.btn_search_nearby->{
                if(mMapLocation!=null){
                    isNearBySearch=true
                    mCustomBottomSheetDialogNearBy?.show(requireActivity())
                    getNearbyPlaceViaWeb(null, mMapLocation!!)
    //                findCurrentPlaceWithPermissions()
               }
            }
        }
    }

    fun getLocationFromAddress(context: Context?, strAddress: String?): LatLng? {
        val coder = Geocoder(context)
        val address: List<Address>?
        var p1: LatLng? = null
        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null) {
                return null
            }
            val location = address[0]
            p1 = LatLng(location.latitude, location.longitude)
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return p1
    }

    val bottomSheetCallBack = object : CustomBottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(bottomSheet: View, slideOffset: Float) {
        }

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            Log.d("onStateChanged: $newState")
            when (newState) {
                STATE_COLLAPSED -> {
                    (activity as MainActivity).hideSoftKeyboard(et_search_location_bar)
                    et_search_location_bar.isFocusableInTouchMode = false
                    et_search_location_bar.isFocusable = false
                }
                STATE_EXPANDED, STATE_ANCHOR -> {
                    et_search_location_bar.postDelayed({
                        et_search_location_bar.isFocusableInTouchMode = true
                        et_search_location_bar.isFocusable = true
                        et_search_location_bar.requestFocus()
                        et_search_location_bar.setText("")
                    }, 50)
                }
            }
        }
    }


    //place api, data provide by android lib and web-service
    private fun findAutocompletePredictions(query:String) {
        if (token == null) {
            token = AutocompleteSessionToken.newInstance()
        }
        val requestBuilder = FindAutocompletePredictionsRequest.builder()
            .setQuery(query)
            .setCountries(Locale.getDefault().country)
            .setOrigin(LatLng(mLocation.latitude, mLocation.longitude))
            .setSessionToken(token)
        val task = placesClient.findAutocompletePredictions(requestBuilder.build())
        task.addOnSuccessListener { response: FindAutocompletePredictionsResponse? ->
            response?.let {
                Log.d("findAutocompletePredictions" + it.autocompletePredictions)
                mLocationAdapter?.addPredicList(it.autocompletePredictions)
            }
        }
        task.addOnFailureListener { exception: Exception ->
            Log.e(exception)
        }
    }

//    private fun findCurrentPlaceWithPermissions() {
//
//        val placeFields: List<Place.Field> = listOf(Place.Field.NAME, Place.Field.ID, Place.Field.ADDRESS, Place.Field.LAT_LNG)
//
//        // Use the builder to create a FindCurrentPlaceRequest.
//        val request: FindCurrentPlaceRequest = FindCurrentPlaceRequest.newInstance(placeFields)
//        // Call findCurrentPlace and handle the response (first check that the user has granted permission).
//        if (ContextCompat.checkSelfPermission(requireContext(), ACCESS_FINE_LOCATION) ==
//            PackageManager.PERMISSION_GRANTED) {
//
//            val placeResponse = placesClient.findCurrentPlace(request)
//            placeResponse.addOnCompleteListener { task ->
//                if (task.isSuccessful) {
//                    val response = task.result
//                    var list = arrayListOf<LocationModel>()
//                    for (placeLikelihood: PlaceLikelihood in response?.placeLikelihoods
//                        ?: emptyList()) {
//                        var tmp =LocationModel(placeLikelihood.place.id, placeLikelihood.place.name, placeLikelihood.place.address
//                        ,placeLikelihood.place.latLng?.latitude, placeLikelihood.place.latLng?.longitude)
//                        list.add(tmp)
//                    }
//                    mNearByAdapter = NearByLocationAdapter(requireContext(), list, isOwner!!, favorite_loc_map)
//                    mNearByAdapter?.setOnCheckChangeListener(this)
//                    mCustomBottomSheetDialogNearBy?.setAdapter(mNearByAdapter!!)
//                } else {
//
//                    val exception = task.exception
//                    if (exception is ApiException) { Log.e("Place not found: ${exception.statusCode}") }
//                }
//                mCustomBottomSheetDialogNearBy?.isLoading(false)
//            }
//        } else {
//            requestPermissions(arrayOf(ACCESS_FINE_LOCATION), 1)
//            mCustomBottomSheetDialogNearBy?.isLoading(false)
//        }
//    }


    private var mApiCallback: Callback<JsonElement> = object : Callback<JsonElement> {
        override fun onFailure(call: Call<JsonElement>, t: Throwable) {
            Log.e("onFailure: " + t.message)
            mCustomBottomSheetDialogNearBy?.isLoading(false)
            Toast.makeText(requireContext(), "網路異常", Toast.LENGTH_SHORT).show()
        }

        override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
//            Log.d("onResponse: call--$call\n uri--$response \nresponse--${response.body().toString()}")
            val jsonObject = JSONObject(response.body().toString())
            val resultArr = jsonObject.getJSONArray("results")
            var list = arrayListOf<LocationModel>()
            for (i in 0 until resultArr.length()) {
                var tmp = LocationModel(null, null, null, null, null)
                try {
                    var obj: JSONObject? = resultArr.getJSONObject(i)
                    val temp: Iterator<String> = obj!!.keys()
                    while (temp.hasNext()) {
                        val key = temp.next()
                        val value: Any = obj.get(key)
                        when(key){
                             "place_id"->{
                                 tmp.place_id = value as String
                             }
                            "name"->{
                                tmp.name = value as String
                            }
                            "vicinity"->{
                                tmp.addr=value as String
                            }
                            "geometry"->{
                                var loc = JSONObject(value.toString()).getJSONObject("location")
                                tmp.lat = loc.getDouble("lat")
                                tmp.lng = loc.getDouble("lng")
                            }
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
               list.add(tmp)
            }
            if(isNearBySearch){
                mNearByAdapter = NearByLocationAdapter(requireContext(), list, isOwner!!, favorite_loc_map)
                mNearByAdapter?.setOnCheckChangeListener(this@NewMissionSelectLocation)
                mNearByAdapter?.setOnItemClickListener(this@NewMissionSelectLocation)
                mCustomBottomSheetDialogNearBy?.setAdapter(mNearByAdapter!!)
                mCustomBottomSheetDialogNearBy?.isLoading(false)
            }else{
                mLocationAdapter?.setList(list)
                if(resultArr.length() <5 && queryString!!.isNotEmpty()){
                    findAutocompletePredictions(queryString!!)
                }
            }

        }
    }

    override fun onItemClick(item_model: LocationModel, position: Int) {
        if(mCustomBottomSheetDialogNearBy?.isShowing()!!)
            mCustomBottomSheetDialogNearBy?.dismiss()
        if(bottomBehavior.state== STATE_EXPANDED|| bottomBehavior.state== STATE_ANCHOR)
            bottomBehavior.state = STATE_COLLAPSED

        rl_confirm_loc.visibility = View.VISIBLE
        tv_location_name_content.text = item_model.name
        tv_location_addr_content.text = item_model.addr
        rl_confirm_loc.tag = item_model
        if(item_model.lat==null ||item_model.lng==null){
            getDetailsWithPlaceId(item_model, object:IDialogValueListener<LocationModel>{
                override fun onReturnValue(ret: LocationModel) {
                    moveMap(ret)
                }
            })
        }else{
            moveMap(item_model)
        }
    }

    private fun moveMap(item_model:LocationModel){
        mMapView?.getMapAsync{mMap->
            googleMap = mMap
            mMap.setOnCameraMoveStartedListener (this)
            mMap.setOnCameraIdleListener(this)

            mMap.uiSettings.isMapToolbarEnabled = false
            val zoom =
                (log2(40000.0 / 0.5) - 0.5).toFloat()
//
//            val metersPerPx =
//                156543.03392 * Math.cos(item_model.lat!! * Math.PI / 180) / Math.pow(2.0, zoom.toDouble())
//            val pixels: Int = (0.6 / 2 * Utils.getScreenHeight(requireActivity())).roundToInt()
//            val meters = metersPerPx * pixels
//            val newLat: Double = item_model.lat!! - meters * 0.000009
//            val camLoc = LatLng(newLat, item_model.lng!!)

            val loc = LatLng(item_model.lat!!, item_model.lng!!)

            mMap.clear()
            mMap.addMarker(
                MarkerOptions().position(LatLng(mLocation.latitude, mLocation.longitude))
                    .icon(BitmapDescriptorFactory.fromResource(
                        if(isOwner!!)
                            R.drawable.user_onmap_owner else R.drawable.user_onmap_taker))
            )
            mMap.addMarker(
                MarkerOptions().position(loc).icon(
                    BitmapDescriptorFactory.fromResource(
                        if(isOwner!!)
                            R.drawable.marker_owner else R.drawable.marker_taker))
            )
            val cameraPosition = CameraPosition.Builder()
                .target(loc)
                .zoom(zoom)
                .build()
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            mMap.setOnMapClickListener {
                (activity as MainActivity).hideSoftKeyboard()
            }
        }
    }

    override fun onItemClick(view: View, position: Int) {
    }


    private fun getDetailsWithPlaceId(location: LocationModel, iDialogValueListener: IDialogValueListener<LocationModel>) {
        // Specify the fields to return.
        val placeFields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)

        // Construct a request object, passing the place ID and fields array.
        val request = FetchPlaceRequest.newInstance(location.place_id!!, placeFields)
        placesClient.fetchPlace(request)
            .addOnSuccessListener { response: FetchPlaceResponse ->
                val place = response.place
                var tmp = LocationModel(place.id, place.name,place.address, place.latLng?.latitude, place.latLng?.longitude)
                iDialogValueListener.onReturnValue(tmp)
            }.addOnFailureListener { exception: Exception ->
                if (exception is ApiException) {
                    Log.e("Place not found: ${exception.message}")
                    val statusCode = exception.statusCode
                    iDialogValueListener.onReturnValue(location)
                }
            }

    }

    fun getNearbyPlaceViaWeb(query:String?, location:LatLng){
        LocationApiModule().provideApiService()
            .getLocations(query,  Locale.getDefault().toString(),
                location.latitude.toString() + ","+location.longitude.toString() , 5000).enqueue(mApiCallback)
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        val item = buttonView!!.tag as LocationModel
        if(isChecked){
            favorite_loc_map[item.place_id!!] = item
        }else{
            favorite_loc_map.remove(item.place_id!!)
        }
    }

    override fun onBackPressed(): Boolean {
        if(bottomBehavior.state== STATE_EXPANDED || bottomBehavior.state== STATE_ANCHOR) {
            bottomBehavior.state = STATE_COLLAPSED
            return false
        }else if(mCustomBottomSheetDialogNearBy?.isShowing()!!){
            mCustomBottomSheetDialogNearBy?.dismiss()
            return false
        }else
            return super.onBackPressed()
    }

    override fun onCameraMoveStarted(action: Int) {
        when(action){
            REASON_GESTURE->{
                rl_confirm_loc.visibility=View.GONE
                googleMap!!.clear()
                googleMap!!.addMarker(
                    MarkerOptions().position(LatLng(mLocation.latitude, mLocation.longitude))
                        .icon(BitmapDescriptorFactory.fromResource(
                            if(isOwner!!)
                                R.drawable.user_onmap_owner else R.drawable.user_onmap_taker))
                )

                btn_custom_loc.visibility = View.VISIBLE
                img_custom_loc.visibility = View.VISIBLE
                img_custom_loc.setImageResource(R.drawable.ico_custom_loc)

            }
            else->{
                img_custom_loc.visibility = View.GONE
                btn_custom_loc.visibility = View.GONE
            }
        }
    }

    override fun onCameraIdle() {
        mMapLocation = LatLng(googleMap!!.cameraPosition.target.latitude, googleMap!!.cameraPosition.target.longitude)
    }

}