package com.app.lbs.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.LoginViewModel
import com.app.lbs.utils.Const.IAMOWNER


class IntroSelectFragment : BaseFragment(), View.OnClickListener {
    private lateinit var mViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.vp_intro_select, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[LoginViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        val btn_iamtaker = root.findViewById<Button>(R.id.btn_iamtaker)
        val btn_iamowner = root.findViewById<Button>(R.id.btn_iamowner)
        btn_iamtaker.setOnClickListener(this)
        btn_iamowner.setOnClickListener(this)
        return root
    }

    override fun onClick(v: View?) {
        var startMainIntent = Intent(activity, MainActivity::class.java)
        when (v?.id) {
            R.id.btn_iamtaker -> {
                startMainIntent.putExtra(IAMOWNER, false)
            }

            R.id.btn_iamowner -> {
                startMainIntent.putExtra(IAMOWNER, true)
            }
        }

        this@IntroSelectFragment.startActivity(startMainIntent)
        this@IntroSelectFragment.requireActivity().finish()
    }
}