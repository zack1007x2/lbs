package com.app.lbs.ui.fragments

import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Utils
import com.github.clans.fab.FloatingActionButton
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.maps.android.ui.IconGenerator
import kotlin.math.log2


/**
 * @file PrevMapFragment
 * @brief
 *       聊天室中顯示詳細地圖功能
 *       目前未使用
 *
 * @author Zack
 * */

class PrevMapFragment : BaseFragment(), View.OnClickListener, OnMapReadyCallback {
    private lateinit var mViewModel: MainViewModel
    private var mMapView: MapView? = null
    private lateinit var mLocation: Location
    private var hasInitView = false
    private var mLine: Polyline? = null
    private lateinit var fb_navi_by_google: FloatingActionButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_find_mission_by_map, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        root.tag = R.layout.fragment_find_mission_by_map
        mMapView = root.findViewById(R.id.mapView)
        mMapView!!.onCreate(savedInstanceState)

        mMapView!!.onResume() // needed to get the map to display immediately


        try {
            MapsInitializer.initialize(requireActivity().applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        fb_navi_by_google = root.findViewById(R.id.fb_navi_by_google)
        fb_navi_by_google.setOnClickListener(this)
        fb_navi_by_google.visibility = View.VISIBLE
        return root
    }

    companion object {
        const val FRAGMENT_ID = R.id.ll_prev_map
    }


    override fun onResume() {
        super.onResume()
        setTitle(R.string.title_mission_list)
        mMapView!!.onResume()
        mViewModel.getLocation().observe(viewLifecycleOwner, mLocationObserver)
        mMapView!!.getMapAsync(this)

    }

    override fun onPause() {
        super.onPause()
        hasInitView = false
        mViewModel.getLocation().removeObserver(mLocationObserver)
        mMapView!!.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }

    private var mLocationObserver: Observer<Location> = Observer { t ->
        mLocation = t
        val loc = LatLng(mLocation.latitude, mLocation.longitude)
        mMapView!!.getMapAsync { mMap ->
            mMap.uiSettings.isMapToolbarEnabled = false
            mMap.addMarker(
                MarkerOptions().position(loc)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.user))
            )

            if (mLine != null)
                mLine!!.remove()

            try {
                mLine = mMap.addPolyline(
                    PolylineOptions()
                        .add(loc, Utils.getLatLng(mViewModel.tmp_mission.store_coor2addr))
                        .width(5f)
                        .color(Color.BLACK)
                )
                mLine!!.isVisible = true
            } catch (e: StringIndexOutOfBoundsException) {
                mLine?.isVisible = false
            }


            /**
             * 以手機寬度為準 縮放地圖到適當大小
             * 低緯度單位寬度較長因此取三點中緯度最低點
             * 取最東最西的差計算三點的最大寬度距離
             */
            var zoom =
                (log2(40000.0 / mViewModel.mUser?.userSetting?.missionDetectRange!!) - 0.5).toFloat()
            var midpoint: LatLngBounds? = null
            try {
                val loc_mission = Utils.getLatLng(mViewModel.tmp_mission.store_coor2addr)
                val loc_delivery = Utils.getLatLng(mViewModel.tmp_mission.coor2addr)
                val minLat = minOf(mLocation.latitude, loc_mission.latitude, loc_delivery.latitude)
                val minLng =
                    minOf(mLocation.longitude, loc_mission.longitude, loc_delivery.longitude)
                val maxLng =
                    maxOf(mLocation.longitude, loc_mission.longitude, loc_delivery.longitude)
                val result = FloatArray(3)
                Location.distanceBetween(minLat, minLng, minLat, maxLng, result)
                midpoint = LatLngBounds.builder().include(loc_mission).include(loc_delivery)
                    .include(LatLng(mLocation.latitude, mLocation.longitude)).build()
                zoom =
                    (log2(40000.0 / (result[0] / 1000.0)) - 0.5).toFloat()
                // For zooming automatically to the location of the marker

            } catch (e: Exception) {
                Log.e(e)
            }
            val cameraPosition = CameraPosition.Builder()
                .target(midpoint?.center ?: LatLng(mLocation.latitude, mLocation.longitude))
                .zoom(zoom)
                .build()


            //locate to user location
            if (!hasInitView) {
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                hasInitView = true
            }
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.fb_navi_by_google -> {
                try {
                    val loc_mission = Utils.getLatLng(mViewModel.tmp_mission.store_coor2addr)
                    val loc_delivery = Utils.getLatLng(mViewModel.tmp_mission.coor2addr)

                    val gmmIntentUri =
                        Uri.parse(
                            "https://www.google.com.tw/maps/dir/?api=1&origin=" + "&waypoints=" + loc_mission.latitude.toString() + "," + loc_mission.longitude.toString()
                                    + "&destination=" + loc_delivery.latitude.toString() + "," + loc_delivery.longitude.toString()
                        )
                    Log.e(gmmIntentUri)
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)

                    /**
                     * workaround
                     * problem : 初次啟動出現waypoint消失的問題
                     * solve : 先啟動地圖, 1.5秒後再發送導航資訊
                     */

                    val intent: Intent =
                        requireActivity().packageManager.getLaunchIntentForPackage("com.google.android.apps.maps")!!
                    intent.action = Intent.ACTION_VIEW
                    startActivity(intent)

                    fb_navi_by_google.postDelayed({
                        Log.d("trigger map navi - $gmmIntentUri")
                        startActivity(mapIntent)
                    }, 1500)
                } catch (e: Exception) {
                    Log.e(e)
                }

            }
        }
    }

    override fun onMapReady(mMap: GoogleMap?) {
        mMap?.uiSettings?.isMapToolbarEnabled = false
        val str_mission = requireActivity().resources.getString(R.string.map_mission)
        val str_delivery = requireActivity().resources.getString(R.string.map_delivery)
        var loc_mission: LatLng? = null
        var loc_delivery: LatLng? = null
        try {
            loc_mission = Utils.getLatLng(mViewModel.tmp_mission.store_coor2addr)
            mMap!!.addMarker(
                MarkerOptions().position(loc_mission)
                    .icon(
                        BitmapDescriptorFactory.fromBitmap(
                            IconGenerator(requireActivity()).makeIcon(
                                str_mission
                            )
                        )
                    )
            )
        } catch (e: Exception) {

        }
        try {
            loc_delivery = Utils.getLatLng(mViewModel.tmp_mission.coor2addr)
            mMap!!.addMarker(
                MarkerOptions().position(loc_delivery)
                    .icon(
                        BitmapDescriptorFactory.fromBitmap(
                            IconGenerator(requireActivity()).makeIcon(
                                str_delivery
                            )
                        )
                    )
            )
        } catch (e: Exception) {

        }


        if (loc_mission != null && loc_delivery != null)
            mMap!!.addPolyline(
                PolylineOptions()
                    .add(loc_mission, loc_delivery)
                    .width(5f)
                    .color(Color.BLACK)
            )
    }

}