package com.app.lbs.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.adapters.FaqAdapter
import com.app.lbs.ui.viewmodels.MainViewModel
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

/**
 * @file FeedbackFragment
 * @brief
 *       任務結束給予feedback
 *
 * @author Zack
 * */

class FaqFragmant:BaseFragment(), View.OnClickListener{

    companion object {
        const val FRAGMENT_ID = R.id.tab_faq
    }

    private var expandableLayout_owner:ExpandableLayout?=null
    private var expandableLayout_taker:ExpandableLayout?=null
    private var expandableLayout_fin:ExpandableLayout?=null
    private lateinit var expand_button_taker:RelativeLayout
    private lateinit var expand_button_owner:RelativeLayout
    private lateinit var expand_button_fin:RelativeLayout
    private lateinit var rv_imowner:RecyclerView
    private lateinit var rv_imtaker:RecyclerView
    private lateinit var rv_payment_flow:RecyclerView
    private lateinit var mViewModel: MainViewModel
    private var owner_qList:List<String>?= null
    private var owner_aList:List<String>?=null
    private var taker_qList:List<String>?=null
    private var taker_aList:List<String>?=null
    private var cash_qList:List<String>?=null
    private var cash_aList:List<String>?=null
    private var owner_ada:FaqAdapter?=null
    private var taker_ada:FaqAdapter?=null
    private var cash_ada:FaqAdapter?=null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_faq, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        initView(root)
        return root
    }

    private fun initView(v: View) {
        expandableLayout_taker = v.findViewById(R.id.expandableLayout_taker)
        expandableLayout_owner = v.findViewById(R.id.expandableLayout_owner)
        expandableLayout_fin = v.findViewById(R.id.expandableLayout_fin)

        expand_button_taker = v.findViewById(R.id.expand_button_taker)
        expand_button_owner = v.findViewById(R.id.expand_button_owner)
        expand_button_fin = v.findViewById(R.id.expand_button_fin)

        expand_button_taker.setOnClickListener(this)
        expand_button_owner.setOnClickListener(this)
        expand_button_fin.setOnClickListener(this)

        rv_imowner = v.findViewById(R.id.rv_imowner)
        rv_imtaker = v.findViewById(R.id.rv_imtaker)
        rv_payment_flow = v.findViewById(R.id.rv_payment_flow)
    }

    override fun onResume() {
        super.onResume()
        setTitle(R.string.title_faq)
        expandableLayout_taker?.let {
            if (it.isExpanded)
                it.collapse()
        }
        expandableLayout_owner?.let {
            if (it.isExpanded)
                it.collapse()
        }
        expandableLayout_fin?.let {
            if (it.isExpanded)
                it.collapse()
        }
        owner_qList = resources.getStringArray(R.array.faq_owner_qlist).toList()
        owner_aList = resources.getStringArray(R.array.faq_owner_alist).toList()
        taker_qList = resources.getStringArray(R.array.faq_taker_qlist).toList()
        taker_aList = resources.getStringArray(R.array.faq_taker_alist).toList()
        cash_qList = resources.getStringArray(R.array.faq_cash_qlist).toList()
        cash_aList = resources.getStringArray(R.array.faq_cash_alist).toList()
        owner_ada = FaqAdapter(context, owner_qList, owner_aList)
        taker_ada = FaqAdapter(context, taker_qList, taker_aList)
        cash_ada = FaqAdapter(context, cash_qList, cash_aList)
        Log.d("que:"+ owner_qList!![0])
        rv_imowner.adapter = owner_ada
        rv_imtaker.adapter = taker_ada
        rv_payment_flow.adapter = cash_ada
        rv_imowner.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_imtaker.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_payment_flow.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.expand_button_taker->{
                if(expandableLayout_taker!!.isExpanded)
                    collapseAll()
                else
                    expandableLayout_taker!!.expand()

            }
            R.id.expand_button_owner->{
                if(expandableLayout_owner!!.isExpanded)
                    collapseAll()
                else
                    expandableLayout_owner!!.expand()
            }
            R.id.expand_button_fin->{
                if(expandableLayout_fin!!.isExpanded)
                    collapseAll()
                else
                    expandableLayout_fin!!.expand()
            }
        }
    }

    private fun collapseAll(){
        expandableLayout_taker!!.collapse()
        expandableLayout_owner!!.collapse()
        expandableLayout_fin!!.collapse()
    }

    override fun onBackPressed() :Boolean{
        if(expandableLayout_taker!!.isExpanded || expandableLayout_owner!!.isExpanded || expandableLayout_fin!!.isExpanded){
            collapseAll()
            return false
        }else
            return true
    }


}