package com.app.lbs.ui.dialog

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.app.lbs.R
import com.app.lbs.models.ImageModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import java.util.*

/**
 * @file ZoomImageDialogFragment
 * @brief
 *       圖片放大滑動瀏覽功能
 *
 * @author Zack
 **/

class SlideshowDialogFragment : DialogFragment() {
    private val TAG = SlideshowDialogFragment::class.java.simpleName
    private var images: ArrayList<ImageModel>? = null
    private var viewPager: ViewPager? = null
    private var myViewPagerAdapter: SlideshowDialogFragment.MyViewPagerAdapter? = null
    private var lblCount: TextView? = null
    private var lblTitle: TextView? = null
    private var lblDate: TextView? = null
    private var selectedPosition = 0
    private val mContext: Context? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v: View = inflater.inflate(R.layout.fragment_image_slider, container, false)
        viewPager = v.findViewById<View>(R.id.viewpager) as ViewPager
        lblCount = v.findViewById<View>(R.id.lbl_count) as TextView
        images = getArguments()?.getSerializable("images") as ArrayList<ImageModel>
        selectedPosition = requireArguments().getInt("position")
        Log.e(TAG, "position: $selectedPosition")
        Log.e(TAG, "images size: " + images!!.size)
        myViewPagerAdapter = MyViewPagerAdapter()
        viewPager!!.adapter = myViewPagerAdapter
        viewPager!!.addOnPageChangeListener(viewPagerPageChangeListener)
        setCurrentItem(selectedPosition)
        return v
    }

    private fun setCurrentItem(position: Int) {
        viewPager!!.setCurrentItem(position, false)
        displayMetaInfo(selectedPosition)
    }

    //	page change listener
    var viewPagerPageChangeListener: OnPageChangeListener = object : OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            displayMetaInfo(position)
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}
        override fun onPageScrollStateChanged(arg0: Int) {}
    }

    private fun displayMetaInfo(position: Int) {
        lblCount!!.text = (position + 1).toString() + " of " + images!!.size
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
    }

    //	adapter
    inner class MyViewPagerAdapter : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            layoutInflater = requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view: View =
                layoutInflater!!.inflate(R.layout.image_fullscreen_preview, container, false)
            val imageViewPreview = view.findViewById<View>(R.id.imagepreview) as ImageView
            val image: ImageModel = images!![position]
//            Glide.with(requireContext()).load(image.getImagePath())
//                .thumbnail(0.5f)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imageViewPreview)
            Glide.with(requireActivity().applicationContext)
                .load(image.getImagePath())
                .transform(RoundedCorners(requireContext().resources.getDimensionPixelSize(R.dimen.size_10)))
                .into(object : CustomTarget<Drawable>() {
                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        imageViewPreview.setImageDrawable(resource)
                    }

                    override fun onLoadCleared(@Nullable placeholder: Drawable?) {}
                })
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return images!!.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj as View
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }
    }

    companion object {
        fun newInstance(): SlideshowDialogFragment {
            return SlideshowDialogFragment()
        }
    }
}