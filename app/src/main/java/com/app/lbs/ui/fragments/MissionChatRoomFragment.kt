package com.app.lbs.ui.fragments

//import com.skydoves.expandablelayout.ExpandableLayout
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.app.lbs.R
import com.app.lbs.models.Mission
import com.app.lbs.models.MyChatUser
import com.app.lbs.models.UserAccount
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.github.bassaer.chatmessageview.model.IChatUser
import com.github.bassaer.chatmessageview.model.Message
import com.github.bassaer.chatmessageview.util.IOnReplyCallback
import com.github.bassaer.chatmessageview.view.MessageView
import com.google.android.gms.maps.MapsInitializer
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * @file MissionChatRoomFragment
 * @brief
 *       任務聊天室根據不同任務狀態顯示介面
 *      TODO 可能須改成MutableLiveData監聽任務狀態 即時更改聊天室介面
 * @author Zack
 * */

class MissionChatRoomFragment : BaseFragment(), View.OnClickListener {
    private lateinit var mViewModel: MainViewModel

    private lateinit var message_view: MessageView
    private lateinit var et_message_input: EditText
    private lateinit var btn_send: Button
    private lateinit var btn_gallery: Button
    private lateinit var btn_camera: Button
    private lateinit var pullToRefresh: SwipeRefreshLayout
    private lateinit var ll_msg_ctrl_panel: RelativeLayout
    private lateinit var ll_btn_panel_done: LinearLayout
    private var imageFilePath: String? = null
    private var modified_mission = Mission()
    private var mChatUserMap = HashMap<String, IChatUser>()

    private lateinit var rl_chat_room:RelativeLayout

    companion object {
        const val FRAGMENT_ID = R.id.fab_chatroom
        const val ONREPLY="OnReply"
    }


    private val mHanlder = Handler(Looper.getMainLooper())

    private var mOnReplyCallback = object : IOnReplyCallback {
        override fun onReply(msg: Message) {
            et_message_input.hint = "請輸入回覆"
            (activity as MainActivity).showSoftKeyboard(et_message_input)
            et_message_input.tag=msg
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_chat_room, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        try {
            MapsInitializer.initialize(requireActivity().applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initView(root)
        return root
    }

    @SuppressLint("NewApi")
    private fun initView(v: View) {
        message_view = v.findViewById(R.id.message_view)
        pullToRefresh = v.findViewById(R.id.pullToRefresh)
        et_message_input = v.findViewById(R.id.et_message_input)
        ll_msg_ctrl_panel = v.findViewById(R.id.ll_msg_ctrl_panel)
        ll_btn_panel_done = v.findViewById(R.id.ll_btn_panel_done)

        btn_send = v.findViewById(R.id.btn_send)
        btn_gallery = v.findViewById(R.id.btn_gallery)
        btn_camera = v.findViewById(R.id.btn_camera)
        rl_chat_room = v.findViewById(R.id.rl_chat_room)

        btn_send.setOnClickListener(this)
        btn_gallery.setOnClickListener(onGalleryClick)
        btn_camera.setOnClickListener(onCameraClick)


        mChatUserMap.clear()
        mChatUserMap[mViewModel.mUser!!.id_admin] = MyChatUser(mViewModel.mUser!!)
        if(Const.DEBUG)
            fakeMessageInit()
        //TODO init with history
        val msgHistory: ArrayList<Message> = ArrayList()
        message_view.init(msgHistory)
        message_view.divider=null
        message_view.setReplyCallback(mOnReplyCallback)

        when (mViewModel.tmp_mission.job_status) {
            2, 3, 4, 5 -> {
                ll_btn_panel_done.visibility = View.VISIBLE
                ll_msg_ctrl_panel.visibility = View.GONE
                message_view.setMsgTouchEnable(false)
            }
            else->{
                ll_msg_ctrl_panel.visibility = View.VISIBLE
                message_view.setMsgTouchEnable(true)
            }
        }
    }//END OF initView()

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        Log.i("onResume")
//        mViewModel.updateTmpMission()
        mViewModel.getObservMission().observe(viewLifecycleOwner, tmpMissionObserver)
    }


    override fun onPause() {
        super.onPause()
        Log.i("onPause")
        mViewModel.getObservMission().removeObserver(tmpMissionObserver)
        mHanlder.removeCallbacksAndMessages(null)
    }

    override fun onClick(view: View) {
        when (view.id) {
            //傳送圖片訊息
//            R.id.btn_gallery -> {
//                //check permission
//
//                dialog_pic_selector =
//                    CustomDialogBuilder(requireContext(), R.layout.custom_dialog_camera_or_gallery)
//                        .setCameraListener(onCameraClick)
//                        .setGalleryListener(onGalleryClick)
//                        .setCustomTitle(R.string.tv_img_upload)
//                        .build()
//                dialog_pic_selector.show()
//
//            }

            //傳送訊息
            R.id.btn_send -> {
                if(et_message_input.tag!=null){
                    var mainMsg = et_message_input.tag as Message
                    val msg = Message.Builder()
                        .setUser(mChatUserMap[mViewModel.mUser!!.id_admin]!!)
                        .setText(et_message_input.text.toString())
                        .setType(Message.Type.REPLY)
                        .setRight(false)
                        .setIsWhiteBg(mainMsg.isWhiteBg)
                        .build()
                    Log.e("add msg is white: "+mainMsg.isWhiteBg)
                    mainMsg.replyList.add(msg)
                    message_view.smoothScrollToPosition(message_view.updateMessage(mainMsg))
                    et_message_input.text.clear()
                    et_message_input.tag=null
                    et_message_input.hint = "新增留言"
                }else{
                    if (et_message_input.text.toString().isNotEmpty()) {
                        val msg = Message.Builder()
                            .setUser(mChatUserMap[mViewModel.mUser!!.id_admin]!!)
                            .setText(et_message_input.text.toString())
                            .setType(Message.Type.TEXT)
                            .setRight(false)
                            .setIsWhiteBg(message_view.getStartPrivate())
                            .build()
                        message_view.setMessage(msg)
                        et_message_input.text.clear()
                        message_view.smoothScrollToPosition(message_view.updateMessage(msg))
                    }
                }

            }

        }
    }


    private val onCameraClick = View.OnClickListener {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                Const.MY_PERMISSIONS_REQUEST
            )
        }else{
            launchCameraIntent()
        }

    }

    private val onGalleryClick = View.OnClickListener {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                Const.MY_PERMISSIONS_REQUEST
            )
        }else{
            launchGalleryIntent(false)
        }
    }

    @SuppressLint("ResourceType")
    private fun launchCameraIntent() {
        ImagePicker.with(this)
            .setCameraOnly(true)
            .setMultipleMode(false)
            .setRequestCode(Const.REQUSET_CAMERA)
            .setToolbarColor(getString(R.color.hyperlink_blue))
            .setIndicatorColor(getString(R.color.hyperlink_blue))
            .setBackgroundColor(getString(R.color.colorMainBrightWhite))
            .start()
    }

    @SuppressLint("ResourceType")
    private fun launchGalleryIntent(isMultipleMode: Boolean =true) {
        ImagePicker.with(this)
            .setFolderMode(true)
            .setFolderTitle("Album")
            .setRootDirectoryName(Config.ROOT_DIR_DCIM)
            .setDirectoryName("Image Picker")
            .setMultipleMode(isMultipleMode)
            .setShowNumberIndicator(true)
            .setMaxSize(1)
            .setShowCamera(false)
            .setRequestCode(Const.REQUEST_IMAGE)
            .setToolbarColor(getString(R.color.hyperlink_blue))
            .setIndicatorColor(getString(R.color.hyperlink_blue))
            .setBackgroundColor(getString(R.color.colorMainBrightWhite))
            .start()

    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        Log.e("onActivityResult")
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Const.REQUEST_IMAGE || requestCode == Const.REQUSET_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                val images: ArrayList<Image> = ImagePicker.getImages(data)
                Log.e("image.uri : "+images.size)
                // Do stuff with image's path or id. For example:
                for (image in images) {
                    Log.d("image.uri : "+image.uri)
                    imageFilePath = image.uri.path
                    Glide.with(requireActivity().applicationContext).asBitmap().load(imageFilePath)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(object : CustomTarget<Bitmap>() {
                            override fun onResourceReady(
                                resource: Bitmap,
                                transition: Transition<in Bitmap>?
                            ) {
                                val msg = Message.Builder()
                                    .setUser(mChatUserMap[mViewModel.mUser!!.id_admin]!!)
                                    .setPicture(resource)
                                    .setType(Message.Type.PICTURE)
                                    .setText("test")
                                    .setRight(false)
                                    .build()
                                message_view.setMessage(msg)
                                et_message_input.text.clear()
                                message_view.scrollToEnd()
                            }

                            override fun onLoadCleared(@Nullable placeholder: Drawable?) {}
                        })
                }
            }
        }
    }

    private val tmpMissionObserver = Observer<Mission> { mission ->
        Log.v("onMission Update : $mission")
        setTitle(mission.owner_firstname+" "+mission.owner_lastname)
        /**
         * workaround
         * problem : kotlin 中synchronized wait notify依然會卡到mainthread
         *           因此採用背景延遲UI更新來達到synchronized效果
         *           避免連續刷新任務造成重複執行多個延遲UI的task
         * solve : 每次刷新任務先清除之前執行到一半以及待執行的所有tasks
         */
        mHanlder.removeCallbacksAndMessages(null)
    }

    private fun fakeMessageInit() {
        //fake user for mission
        val customUser1 = UserAccount()
        val customUser2 = UserAccount()
        customUser1.userData.userId = 15179845
        var mission = mViewModel.getJobList().value!![0]
        customUser1.name_admin = if (mission.owner_nickname.isNotEmpty()) {
            mission.owner_nickname
        } else {
            mission.owner_lastname + mission.owner_firstname
        }
        customUser1.picture = mission.owner_picture

        mission = mViewModel.getJobList().value!![1]
        customUser2.userData.userId = 42345
        customUser2.name_admin = if (mission.owner_nickname.isNotEmpty()) {
            mission.owner_nickname
        } else {
            mission.owner_lastname + mission.owner_firstname
        }
        customUser2.picture = mission.owner_picture

        mChatUserMap[customUser1.id_admin] = MyChatUser(customUser1)
        mChatUserMap[customUser2.id_admin] = MyChatUser(customUser2)

        if (Const.DEBUG) {
            for (x in 10 downTo 0) {
                var type:Message.Type=if(x%3==2){ if(x==8)Message.Type.MODIFY else if(x==5) Message.Type.SYSTEM_PRIVATE else Message.Type.SYSTEM} else Message.Type.TEXT
                if(type==Message.Type.SYSTEM_PRIVATE){
                    message_view.setPrivate(true)
                }
                val msg = Message.Builder()
                    .setUser(
                        when (x % 3) {
                            0 -> mChatUserMap[mViewModel.mUser!!.id_admin]!!
                            1 -> mChatUserMap[customUser1.id_admin]!!
                            else -> mChatUserMap[mViewModel.mUser!!.id_admin]!!
                        }
                    )
                    .setIsHost(
                        when (x % 3) {
                            0 -> true
                            1 -> false
                            else -> false
                        }
                    )
                    .setText("testtest  $x")
                    .setType(type)
                    .setRight(false)
                    .setIsWhiteBg(message_view.getStartPrivate())
                    .build()
                message_view.setMessage(msg)
            }
        }
        Thread {
            try {
                Thread.sleep(100)
                message_view.scrollToEnd()
            } catch (e: Exception) {
                Log.e(e)
            }

        }.start()


        var counter = 11

        pullToRefresh.setOnRefreshListener {
            val cal = Calendar.getInstance().clone() as Calendar
            for (x in counter + 9 downTo counter) {
                cal.time = Date(cal.time.time - (x * 10000L))
                val msg = Message.Builder()
                    .setUser(
                        when (x % 3) {
                            0 -> {
                                mChatUserMap[mViewModel.mUser!!.id_admin]!!
                            }
                            1 -> mChatUserMap[customUser1.id_admin]!!
                            else -> mChatUserMap[customUser2.id_admin]!!
                        }
                    )
                    .setIsHost(
                        when (x % 3) {
                            0 -> true
                            1 -> false
                            else -> false
                        }
                    )
                    .setSendTime(cal)
                    .setText("testtest  $x")
                    .setType(Message.Type.TEXT)
                    .setRight(false)
                    .setIsWhiteBg(!message_view.getStartPrivate())
                    .build()
                message_view.setMessage(msg)
                message_view.scrollTo(0, 0)
                pullToRefresh.isRefreshing = false
                counter++
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if(grantResults.isNotEmpty())
            Log.w("grantResults: " + grantResults[0] + " | " + PackageManager.PERMISSION_GRANTED)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Const.MY_PERMISSIONS_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    //若無權限
                }
            }
        }
    }

    override fun onBackPressed(): Boolean {
//        mViewModel.initNewMissionObject()
//        (activity as MainActivity).updateCurFragment(MissionManagementFragment.FRAGMENT_ID)
        mViewModel.setUserState(Const.USERSTATE.DECIED_BY_PREVIOUS)
        return super.onBackPressed()
    }
}