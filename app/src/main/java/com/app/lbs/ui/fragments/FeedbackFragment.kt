package com.app.lbs.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel

/**
 * @file FeedbackFragment
 * @brief
 *       任務結束給予feedback
 *
 * @author Zack
 * */

class FeedbackFragment:BaseFragment(), View.OnClickListener{

    companion object {
        const val FRAGMENT_ID = R.id.btn_taker_taken_complete
    }

    private lateinit var mViewModel: MainViewModel
    private lateinit var rbtn_good:RadioButton
    private lateinit var rbtn_poor:RadioButton


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_feedback, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        initView(root)
        return root
    }

    private fun initView(v: View) {
        v.findViewById<Button>(R.id.btn_submit).setOnClickListener(this)
        rbtn_good = v.findViewById(R.id.rbtn_good)
        rbtn_poor = v.findViewById(R.id.rbtn_poor)
    }

    override fun onResume() {
        super.onResume()
        setTitle(R.string.title_feedback)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_submit->{
                (activity as MainActivity).showMissionFeedBackSuccess()
            }
        }
    }

    override fun onBackPressed() :Boolean{
        return false
    }


}