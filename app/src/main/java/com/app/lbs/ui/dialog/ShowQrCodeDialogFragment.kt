package com.app.lbs.ui.dialog

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.models.Mission
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.MyLog
import com.app.lbs.utils.Utils.getLatLng
import com.app.lbs.utils.drawroutemap.DrawMarker
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds


/**
 * @file ShowQrCodeDialogFragment
 * @brief
 *       顯示QRCode
 * 未來
 *
 * @author Zack
 * */


class ShowQrCodeDialogFragment(val mission: Mission):DialogFragment(), View.OnClickListener,
    OnMapReadyCallback {

    private lateinit var mViewModel: MainViewModel
    private lateinit var btn_close: Button
    private lateinit var btn_finish:Button
    private var mMapView:MapView?=null
    private val Log = MyLog.log()
    private var fusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        try {
            MapsInitializer.initialize(requireActivity().applicationContext)
        } catch (e: Exception) {
            Log.e(e)
        }
        val root =
            inflater.inflate(R.layout.custom_dialog_show_qr, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        mMapView = root.findViewById(R.id.mp_show_qr)
        mMapView!!.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        initView(root)
        setHasOptionsMenu(true)
        return root
    }

    private fun initView(v : View){
        btn_close = v.findViewById(R.id.btn_close)
        btn_close.setOnClickListener(this)
        btn_finish = v.findViewById(R.id.btn_finish)
        btn_finish.setOnClickListener(this)

    }

    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
        mMapView!!.getMapAsync(this)
    }

    override fun onPause() {
        super.onPause()
        mMapView!!.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_close->{
                dismiss()
            }
            R.id.btn_finish->{
                dismiss()
            }
        }
    }
    @SuppressLint("MissingPermission")
    override fun onMapReady(mMap: GoogleMap?) {
        mMap!!.uiSettings.isMapToolbarEnabled = false
        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.uiSettings.setAllGesturesEnabled(false)

        fusedLocationClient!!.lastLocation.addOnSuccessListener { location: Location? ->
            if (location != null) {
                mMap.uiSettings.isMyLocationButtonEnabled = false
                val loc_user =
                    LatLng(
                        location.latitude,
                        location.longitude
                    )
                val loc_delivery =
                    getLatLng(mission.coor2addr)
                DrawMarker.getInstance(context)
                    .draw(mMap, loc_user, R.drawable.user_onmap_taker, "")
                DrawMarker.getInstance(context)
                    .draw(mMap, loc_delivery, R.drawable.ico_map_mission, "")
                val builder = LatLngBounds.Builder()
                builder.include(loc_user)
                builder.include(loc_delivery)
                val bounds = builder.build()
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
            }
        }
    }
}