package com.app.lbs.ui.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.models.ImageModel
import com.app.lbs.ui.viewmodels.MainViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition


/**
 * @file ZoomImageDialogFragment
 * @brief
 *       圖片放大功能
 * @see SlideshowDialogFragment 已廢棄由SlideshowDialogFragment取代
 * @author Zack
 * */
class ZoomImageDialogFragment(val img:ImageModel):DialogFragment() {

    private lateinit var mViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root =
            inflater.inflate(R.layout.fragment_zoom_image, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        Glide.with(requireActivity().applicationContext)
            .load(img.getImagePath())
            .transform(RoundedCorners(requireContext().resources.getDimensionPixelSize(R.dimen.size_10)))
            .into(object : CustomTarget<Drawable>() {
            override fun onResourceReady(
                resource: Drawable,
                transition: Transition<in Drawable>?
            ) {
                root.findViewById<ImageView>(R.id.img_zoom).setImageDrawable(resource)
            }

            override fun onLoadCleared(@Nullable placeholder: Drawable?) {}
        })
        root.findViewById<ImageView>(R.id.img_zoom)
        setHasOptionsMenu(true)
        return root
    }
}