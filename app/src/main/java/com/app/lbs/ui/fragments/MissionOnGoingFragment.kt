package com.app.lbs.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.adapters.MissionListAdapter
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.Mission
import com.app.lbs.repository.JobListRepository
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const

/**
 * @file MissionOwnFragment
 * @brief
 *       任務管理中 自己開且已結束的任務清單
 *
 * @author Zack
 * */

class MissionOnGoingFragment() : BaseFragment(), IOnItemClickListener<Mission> {

    private lateinit var mViewModel: MainViewModel
    private lateinit var rv_missionList: RecyclerView
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var mAdapter: MissionListAdapter? = null

    companion object {
        const val FRAGMENT_ID = R.id.rv_own_missionList
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_mission_own_list, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        initView(root)
        return root
    }

    private fun initView(v: View) {
        rv_missionList = v.findViewById(R.id.rv_own_missionList)
        mLayoutManager = LinearLayoutManager(activity)
        rv_missionList.layoutManager = mLayoutManager
        mAdapter = MissionListAdapter(requireContext(), null)
        mAdapter!!.setOnItemClickListener(this)
        rv_missionList.adapter = mAdapter
    }

    override fun onResume() {
        super.onResume()
        JobListRepository.instance.refreshJobListsImmediately()
        mViewModel.getUserState().observe(this, Observer { state ->
            if(state==Const.USERSTATE.OWNER){
                mViewModel.getOwnJobListOnGoing().observe(this, Observer{ t->
                    mAdapter!!.setData(t)
                    mAdapter!!.updateUserState(state)
                })
            }else if(state==Const.USERSTATE.TAKER){
                mViewModel.getTakenJobListOnGoing().observe(this, Observer{ t->
                    mAdapter!!.setData(t)
                    mAdapter!!.updateUserState(state)
                })
            }
        })


    }

    override fun onItemClick(view: View, position: Int) {
        (activity as MainActivity).onMissionClick(mAdapter!!.getSelectedMission(position))
    }

    override fun onItemClick(item_model: Mission, position: Int) {
        //DO NOTHING
    }

}