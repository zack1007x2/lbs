package com.app.lbs.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.app.lbs.repository.SysRepository
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ui.IconGenerator
import kotlin.math.log2

/**
 * @file PageMapFragment
 * @brief
 *       用於分頁地圖 根據傳進來的經緯度, 標籤名顯示在各頁面顯示對應地點與圖標
 *
 * @author Zack
 * */


class PageMapFragment(private val latlngs:List<LatLng>, val position: Int, private val tabArr:Array<String>) : SupportMapFragment(), OnMapReadyCallback {
    private var needsInit = false

    private var mSysRepo:SysRepository = SysRepository.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) {
            needsInit = true
        }

    }

    override fun onResume() {
        super.onResume()
        getMapAsync(this)
        mSysRepo.getLocation().observe(viewLifecycleOwner, Observer {
            loc ->
            ArrayList<LatLng>(latlngs)[position] = LatLng(loc.latitude, loc.longitude)
            getMapAsync(this)
        })
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(map: GoogleMap) {
        map.uiSettings.isMapToolbarEnabled = false
        if (needsInit) {
//            val center = CameraUpdateFactory.newLatLng(latlng)
//            val zoom = CameraUpdateFactory.zoomTo(15f)
            val zoom =
                (log2(40000.0 / 1) - 0.5).toFloat()
            // For zooming automatically to the location of the marker
            val cameraPosition = CameraPosition.Builder()
                .target(latlngs[position])
                .zoom(zoom)
                .build()

            map.isMyLocationEnabled = false
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            for(i in latlngs.indices){
                map.addMarker(
                    MarkerOptions().position(latlngs[i])
                        .icon(
                            BitmapDescriptorFactory.fromBitmap(
                                IconGenerator(requireActivity()).makeIcon(
                                    tabArr[i]
                                )
                            )
                        )
                )
            }

        }
    }
}