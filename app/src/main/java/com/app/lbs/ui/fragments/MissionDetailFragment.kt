package com.app.lbs.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.adapters.HorizontalRecyclerViewAdapter
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.interfaces.OnStateChangedListener
import com.app.lbs.models.ImageModel
import com.app.lbs.models.Mission
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.dialog.*
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.CustomDialogBuilder
import com.app.lbs.utils.Utils
import com.app.lbs.utils.drawroutemap.DrawMarker
import com.app.lbs.utils.drawroutemap.DrawRouteMaps
import com.app.lbs.utils.view.CircularCounter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.material.bottomsheet.BottomSheetBehavior
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


/**
 * @file MissionChatRoomFragment
 * @brief
 *       任務聊天室根據不同任務狀態顯示介面
 *      TODO 可能須改成MutableLiveData監聽任務狀態 即時更改聊天室介面
 * @author Zack
 * */

class MissionDetailFragment : BaseFragment(), View.OnClickListener, OnStateChangedListener {
    private lateinit var mViewModel: MainViewModel

    private lateinit var mMapView: MapView

    private lateinit var tv_mission_title_:Any
    private lateinit var tv_mission_owner:TextView
    private lateinit var tv_content_salary: TextView
    private lateinit var tv_content_delivery_time: TextView
    private lateinit var tv_content_mission_content: TextView
    private lateinit var tv_content_store_location: TextView
    private lateinit var tv_content_fin_location: TextView
    private lateinit var tv_content_mission_demand:TextView
    private lateinit var tv_photo_num:TextView
    private lateinit var counter_timer:CircularCounter
    private lateinit var tv_content_mission_loc_map_dir:TextView
    private lateinit var tv_content_abort_loc_map_dir:TextView
    private lateinit var tv_travel_time_to_waypoint:TextView
    private lateinit var tv_travel_time_to_fin:TextView
    private lateinit var btn_navi_to:TextView

    var timer :CountDownTimer?=null

    private lateinit var btn_accept: Button
    private lateinit var btn_taker_taken_complete: Button
    private lateinit var btn_owner_init_modify: Button
    private lateinit var btn_owner_init_abort: Button
    private lateinit var btn_owner_init_taken_abort:Button
    private lateinit var btn_owner_taken_certification: Button
    private lateinit var btn_owner_pending_abort: Button
    private lateinit var btn_confirm_request: Button
    private lateinit var btn_owner_buffering_certification: Button
    private lateinit var btn_taker_taken_confirm_modify: Button
    private lateinit var btn_owner_taken_modify_order: Button
    private lateinit var btn_taker_buffering_complete: Button
    private lateinit var btn_taker_taken_modify_order: Button
    private lateinit var btn_owner_taken_confirm_modify: Button
    private lateinit var btn_owner_taken_abort:Button
    private lateinit var btn_taker_taken_abort:Button
    private lateinit var btn_taker_init_unaccept:Button
    private lateinit var btn_please_fill_reason:Button
    private lateinit var fab_chatroom: Button
    private lateinit var sc_container:ScrollView

    private lateinit var ll_btn_panel_other_owner_init: LinearLayout
    private lateinit var ll_btn_panel_owner_init: LinearLayout
    private lateinit var ll_btn_panel_owner_taken: LinearLayout
    private lateinit var ll_btn_panel_owner_pending: LinearLayout
    private lateinit var ll_btn_panel_owner_buffering: LinearLayout
    private lateinit var ll_btn_panel_taker_taken_job: LinearLayout
    private lateinit var ll_btn_panel_done: LinearLayout
    private lateinit var ll_btn_panel_taker_init:LinearLayout
    private lateinit var ll_btn_panel_owner_init_taken:LinearLayout
    private lateinit var ll_btn_panel_fill_reason:LinearLayout

    private lateinit var ll_prev_map: RelativeLayout
    private var hasInitMap = false

    private val btnPanelArray = ArrayList<LinearLayout>()


    private lateinit var rv_hori_img_preview: RecyclerView
    private lateinit var img_adapter: HorizontalRecyclerViewAdapter
    private var horizontalLayoutManager: LinearLayoutManager? = null

    private lateinit var dialog_pic_selector: Dialog
    private var imageFilePath: String? = null
    private var modified_mission = Mission()

    private lateinit var abort_dialog:Dialog
    private lateinit var dialog_owner_taken:Dialog

    private var qrDialog:DialogFragment? =null
    private var fusedLocationClient: FusedLocationProviderClient? = null

    companion object {
        const val FRAGMENT_ID = R.id.rl_mission_detail
    }

    private val mHanlder = Handler(Looper.getMainLooper())

    private fun initArray() {
        btnPanelArray.clear()
        btnPanelArray.add(ll_btn_panel_other_owner_init)
        btnPanelArray.add(ll_btn_panel_owner_init)
        btnPanelArray.add(ll_btn_panel_owner_taken)
        btnPanelArray.add(ll_btn_panel_owner_pending)
        btnPanelArray.add(ll_btn_panel_owner_buffering)
        btnPanelArray.add(ll_btn_panel_taker_taken_job)
        btnPanelArray.add(ll_btn_panel_done)
        btnPanelArray.add(ll_btn_panel_taker_init)
        btnPanelArray.add(ll_btn_panel_owner_init_taken)
        btnPanelArray.add(ll_btn_panel_fill_reason)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_mission_detail, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")


        try {
            MapsInitializer.initialize(requireActivity().applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mMapView = root.findViewById(R.id.mv_preview_map)
        mMapView.onCreate(savedInstanceState)
        sc_container = root.findViewById(R.id.sc_container)
        mMapView.setOnTouchListener { v, event ->
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN -> {
                    // Disallow ScrollView to intercept touch events.
                    sc_container.requestDisallowInterceptTouchEvent(true)
                    // Disable touch on transparent view
                    false
                }
                MotionEvent.ACTION_UP -> {
                    // Allow ScrollView to intercept touch events.
                    sc_container.requestDisallowInterceptTouchEvent(false)
                    true
                }
                MotionEvent.ACTION_MOVE -> {
                    sc_container.requestDisallowInterceptTouchEvent(true)
                    false
                }
                else -> true
            }
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity());
        initView(root)
        return root
    }

    @SuppressLint("NewApi")
    private fun initView(v: View) {
        setTitle(R.string.tv_mission_detail)
        dialog_owner_taken = CustomDialogBuilder(
            requireContext(),
            R.layout.custom_dialog_message_2_btn
        )
            .setCustomMessage( "請在三分鐘內按下確定，\n" +
                    "超過時間之後\n" +
                    "工作將重新PO出")
            .setCustomTitle("已經有人接案!")
            .build()
        tv_mission_title_ = v.findViewById(R.id.tv_mission_title_)
        tv_content_salary = v.findViewById(R.id.tv_content_salary)
        tv_mission_owner  = v.findViewById(R.id.tv_mission_owner)
        tv_photo_num = v.findViewById(R.id.tv_photo_num)
        tv_content_mission_content = v.findViewById(R.id.tv_content_mission_content)
        tv_content_delivery_time = v.findViewById(R.id.tv_content_delivery_time)
        tv_content_store_location = v.findViewById(R.id.tv_content_store_location)
        tv_content_fin_location = v.findViewById(R.id.tv_content_fin_location)
        tv_content_mission_loc_map_dir = v.findViewById(R.id.tv_content_mission_loc_map_dir)
        tv_content_abort_loc_map_dir = v.findViewById(R.id.tv_content_abort_loc_map_dir)
        tv_travel_time_to_waypoint = v.findViewById(R.id.tv_travel_time_to_waypoint)
        tv_travel_time_to_fin = v.findViewById(R.id.tv_travel_time_to_fin)
        btn_navi_to = v.findViewById(R.id.btn_navi_to)

        ll_btn_panel_other_owner_init = v.findViewById(R.id.ll_btn_panel_other_owner_init)
        ll_btn_panel_owner_taken = v.findViewById(R.id.ll_btn_panel_owner_taken)
        ll_btn_panel_owner_pending = v.findViewById(R.id.ll_btn_panel_owner_pending)
        ll_prev_map = v.findViewById(R.id.ll_prev_map)
        rv_hori_img_preview = v.findViewById(R.id.rv_hori_img_preview)
        ll_btn_panel_owner_buffering = v.findViewById(R.id.ll_btn_panel_owner_buffering)
        ll_btn_panel_owner_init = v.findViewById(R.id.ll_btn_panel_owner_init)
        ll_btn_panel_taker_taken_job = v.findViewById(R.id.ll_btn_panel_taker_taken_job)
        ll_btn_panel_done = v.findViewById(R.id.ll_btn_panel_done)
        ll_btn_panel_taker_init = v.findViewById(R.id.ll_btn_panel_taker_init)
        ll_btn_panel_owner_init_taken = v.findViewById(R.id.ll_btn_panel_owner_init_taken)
        ll_btn_panel_fill_reason = v.findViewById(R.id.ll_btn_panel_fill_reason)

        btn_accept = v.findViewById(R.id.btn_accept)
        btn_taker_taken_complete = v.findViewById(R.id.btn_taker_taken_complete)
        btn_owner_init_modify = v.findViewById(R.id.btn_owner_init_modify)
        btn_owner_init_abort = v.findViewById(R.id.btn_owner_init_abort)
        btn_owner_init_taken_abort = v.findViewById(R.id.btn_owner_init_taken_abort)
        btn_owner_taken_certification = v.findViewById(R.id.btn_owner_taken_certification)
        btn_owner_pending_abort = v.findViewById(R.id.btn_owner_pending_abort)
        btn_confirm_request = v.findViewById(R.id.btn_confirm_request)
        btn_owner_buffering_certification = v.findViewById(R.id.btn_owner_buffering_certification)
        btn_taker_taken_confirm_modify = v.findViewById(R.id.btn_taker_taken_confirm_modify)
        btn_owner_taken_modify_order = v.findViewById(R.id.btn_owner_taken_modify_order)
        btn_taker_buffering_complete = v.findViewById(R.id.btn_taker_buffering_complete)
        btn_taker_taken_modify_order = v.findViewById(R.id.btn_taker_taken_modify_order)
        btn_owner_taken_confirm_modify = v.findViewById(R.id.btn_owner_taken_confirm_modify)
        btn_owner_taken_abort = v.findViewById(R.id.btn_owner_taken_abort)
        btn_taker_taken_abort = v.findViewById(R.id.btn_taker_taken_abort)
        btn_taker_init_unaccept = v.findViewById(R.id.btn_taker_init_unaccept)
        btn_please_fill_reason = v.findViewById(R.id.btn_please_fill_reason)
        fab_chatroom = v.findViewById(R.id.fab_chatroom)
        tv_content_mission_demand = v.findViewById(R.id.tv_content_mission_demand)


        ll_prev_map.setOnClickListener(this)

        btn_accept.setOnClickListener(this)
        btn_taker_taken_complete.setOnClickListener(this)
        btn_owner_init_modify.setOnClickListener(this)
        btn_owner_init_abort.setOnClickListener(this)
        btn_owner_init_taken_abort.setOnClickListener(this)
        btn_owner_taken_certification.setOnClickListener(this)
        btn_owner_pending_abort.setOnClickListener(this)
        btn_confirm_request.setOnClickListener(this)
        btn_owner_buffering_certification.setOnClickListener(this)
        btn_taker_taken_confirm_modify.setOnClickListener(this)
        btn_owner_taken_modify_order.setOnClickListener(this)
        btn_taker_buffering_complete.setOnClickListener(this)
        btn_taker_taken_modify_order.setOnClickListener(this)
        btn_owner_taken_confirm_modify.setOnClickListener(this)
        btn_owner_taken_abort.setOnClickListener(this)
        btn_taker_taken_abort.setOnClickListener(this)
        fab_chatroom.setOnClickListener(this)
        btn_navi_to.setOnClickListener(this)
        btn_please_fill_reason.setOnClickListener(this)

        img_adapter = HorizontalRecyclerViewAdapter(requireActivity(), false)
        val list = ArrayList<ImageModel>()
        if (Const.DEBUG) {
            list.add(ImageModel(0, Utils.getURLForResource(R.drawable.demo1)))
            list.add(ImageModel(1, Utils.getURLForResource(R.drawable.demo2)))
            list.add(ImageModel(2, Utils.getURLForResource(R.drawable.demo3)))
            list.add(ImageModel(3, Utils.getURLForResource(R.drawable.demo4)))
        }
        img_adapter.setList(list)
        tv_photo_num.setText(img_adapter.itemCount.toString())

        horizontalLayoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        rv_hori_img_preview.layoutManager = horizontalLayoutManager
        rv_hori_img_preview.adapter = img_adapter

        img_adapter.setOnItemClickListener(object : IOnItemClickListener<ImageModel> {
            override fun onItemClick(item_model: ImageModel, position: Int) {
                //放大單圖
//                val dialog = ZoomImageDialogFragment(item_model)
//                dialog.isCancelable = true
//                dialog.show(requireActivity().supportFragmentManager, "")

                val bundle = Bundle()
                bundle.putSerializable("images", list)
                bundle.putInt("position", position)

                val ft: FragmentTransaction = parentFragmentManager.beginTransaction()
                val newFragment = SlideshowDialogFragment.newInstance()
                newFragment.arguments = bundle
                newFragment.show(ft, "slideshow")
            }

            override fun onItemClick(view: View, position: Int) {
            }

        })

        counter_timer = v.findViewById(R.id.counter_timer)

        val isOwner = (mViewModel.getUserState().value==Const.USERSTATE.OWNER)
        Log.e("setFirstWidth + OWNER: "+ isOwner)
        counter_timer.setFirstWidth(resources.getDimension(R.dimen.size_2))
        counter_timer.setTextColor(
            if(isOwner){
                ContextCompat.getColor(requireContext(), R.color.owner_red)
            }else {
                ContextCompat.getColor(requireContext(), R.color.taker_blue)
            }
        )
        counter_timer.setFirstColor(if(isOwner){
            ContextCompat.getColor(requireContext(), R.color.owner_red)
        }else {
            ContextCompat.getColor(requireContext(), R.color.taker_blue)
        })

        counter_timer.setSecondColor(ContextCompat.getColor(requireContext(), R.color.transparent))
        counter_timer.setThirdColor(ContextCompat.getColor(requireContext(), R.color.transparent))

        counter_timer.setBackgroundColor(
            ContextCompat.getColor(requireContext(), R.color.colorMainBrightWhite)
        )

//        bottom_sheet_modify = v.findViewById(R.id.bottom_sheet_modify)
//        bottomBehavior_modify = BottomSheetBehavior.from(bottom_sheet_modify!!)
//        bottomBehavior_modify!!.addBottomSheetCallback(bottomSheetCallBack)

    }//END OF initView()

    @SuppressLint("SetTextI18n", "MissingPermission")
    override fun onResume() {
        super.onResume()
        Log.i("onResume: "+mViewModel.tmp_mission_w.duration)
        if (btnPanelArray.isEmpty()) {
            initArray()
        }
        mViewModel.tmp_mission = mViewModel.tmp_mission_w
        mViewModel.getObservMission().observe(viewLifecycleOwner, missionObserver)
        mViewModel.updateMission()
        mMapView.onResume()

        mMapView.getMapAsync { mMap ->
            mMap.isMyLocationEnabled=true
            mMap.uiSettings.isMapToolbarEnabled = false
            mMap.uiSettings.setAllGesturesEnabled(false)

            val str_delivery=requireContext().resources.getString(R.string.map_delivery)

            val loc_mission = Utils.getLatLng(mViewModel.tmp_mission.store_coor2addr)
            val loc_delivery = Utils.getLatLng(mViewModel.tmp_mission.coor2addr)

            fusedLocationClient!!.lastLocation
                .addOnSuccessListener(requireActivity()
                ) { location ->
                    if (location != null) {
                        mMap.uiSettings.isMyLocationButtonEnabled =false
                        val str_user=""
                        var loc_user = LatLng(location.latitude, location.longitude)
                        DrawRouteMaps.getInstance(context)
                            .draw(loc_user, loc_mission, mMap)
                        DrawMarker.getInstance(context).draw(mMap, loc_user, R.drawable.ico_map_user, "")
                        DrawMarker.getInstance(context).draw(mMap, loc_mission, R.drawable.ico_map_mission, "")

                        mMap.setOnMarkerClickListener { true }
                        val builder = LatLngBounds.Builder()
                        builder.include(loc_user)
                        builder.include(loc_mission)
                        builder.include(loc_delivery)
                        val bounds = builder.build()
                        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
                        tv_travel_time_to_waypoint.setText("走路："+Utils.getMapTravelTime(loc_user, loc_mission))
                        tv_travel_time_to_fin.setText("走路："+Utils.getMapTravelTime(loc_mission, loc_delivery))
                    }

                }
            DrawRouteMaps.getInstance(context).draw(loc_mission, loc_delivery, mMap)
            DrawMarker.getInstance(context).draw(mMap, loc_mission, R.drawable.ico_map_mission, "")
            DrawMarker.getInstance(context).draw(mMap, loc_delivery, R.drawable.ico_map_final, str_delivery)
            hasInitMap = true



        }
    }


    override fun onPause() {
        super.onPause()
        Log.i("onPause")
        mViewModel.getObservMission().removeObserver(missionObserver)
        mHanlder.removeCallbacksAndMessages(null)
        hasInitMap = false
    }

    override fun onClick(view: View) {
        when (view.id) {
            //聊天室
            R.id.fab_chatroom->{
                (activity as MainActivity).addFullScreenFragment(MissionChatRoomFragment.FRAGMENT_ID)
            }

            //團片訊息
            R.id.btn_gallery -> {
                //check permission
                if (ContextCompat.checkSelfPermission(
                        requireActivity(),
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(
                        requireActivity(),
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        Const.MY_PERMISSIONS_REQUEST
                    )
                }
                dialog_pic_selector =
                    CustomDialogBuilder(requireContext(), R.layout.custom_dialog_camera_or_gallery)
                        .setCameraListener(onCameraClick)
                        .setGalleryListener(onGalleryClick)
                        .setCustomTitle(R.string.tv_img_upload)
                        .build()
                dialog_pic_selector.show()

            }
            //接受任務
            R.id.btn_accept -> {
                mViewModel.serverTakeJob()
            }

            //工作完成
            R.id.btn_taker_taken_complete -> {
                qrDialog = if(mViewModel.tmp_mission.job_owner==mViewModel.mUser!!.userData.userId){
                    (activity as MainActivity).QrCheckPermissions(this)
                    ReadQRCodeDialogFragment(mViewModel.tmp_mission)
                }else{
                    ShowQrCodeDialogFragment(mViewModel.tmp_mission)
                }
                qrDialog!!.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
                qrDialog!!.show(requireActivity().supportFragmentManager, "")

            }
            //終止/收回
            R.id.btn_owner_init_abort, R.id.btn_owner_pending_abort, R.id.btn_owner_init_taken_abort -> {
//                mViewModel.serverCancelJob()
//                activity?.onBackPressed()
                CustomBottomSheetDialogCancel(mViewModel.tmp_mission ,true).show(activity as FragmentActivity)
            }
            R.id.btn_owner_taken_abort->{
                CustomBottomSheetDialogCancel(mViewModel.tmp_mission ,true).show(activity as FragmentActivity)
            }
            R.id.btn_taker_taken_abort->{
                //TODO 跳到 (接案人沒到): 退錢給發案人 & 接案人有跑單紀錄+1 (不需要評價)
//                mViewModel.serverCancelJob()
//                activity?.onBackPressed()
                CustomBottomSheetDialogCancel(mViewModel.tmp_mission ,false).show(activity as FragmentActivity)
            }

            //確認執行請求
            R.id.btn_confirm_request -> {
                dialog_owner_taken.show()
            }
            //執行者領收
            R.id.btn_owner_taken_certification, R.id.btn_owner_buffering_certification -> {
                qrDialog = if(mViewModel.tmp_mission.job_owner==mViewModel.mUser!!.userData.userId){
                    (activity as MainActivity).QrCheckPermissions(this)
                    ReadQRCodeDialogFragment(mViewModel.tmp_mission)
                }else{
                    ShowQrCodeDialogFragment(mViewModel.tmp_mission)
                }
                qrDialog!!.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
                qrDialog!!.show(requireActivity().supportFragmentManager, "")

            }

            R.id.btn_owner_init_modify, R.id.btn_owner_taken_modify_order, R.id.btn_taker_taken_modify_order -> {
                mViewModel.getObservTmpMission().setValue(mViewModel.tmp_mission)
                CustomBottomSheetDialogMissionModify(mViewModel.tmp_mission, mViewModel.getUserState().value!!).show(requireActivity())
            }

            R.id.btn_taker_taken_confirm_modify, R.id.btn_owner_taken_confirm_modify -> {
                mViewModel.getObservTmpMission().setValue(mViewModel.tmp_mission)
                CustomBottomSheetDialogMissionModify(mViewModel.tmp_mission, mViewModel.getUserState().value!!).show(requireActivity())
            }
            R.id.btn_please_fill_reason->{
                CustomBottomSheetDialogFail().show(requireActivity())
            }

            //導航
            R.id.btn_navi_to->{
                try {
                    val loc_mission = Utils.getLatLng(mViewModel.tmp_mission.store_coor2addr)
                    val loc_delivery = Utils.getLatLng(mViewModel.tmp_mission.coor2addr)

                    val gmmIntentUri =
                        Uri.parse(
                            "https://www.google.com.tw/maps/dir/?api=1&origin=" + "&waypoints=" + loc_mission.latitude.toString() + "," + loc_mission.longitude.toString()
                                    + "&destination=" + loc_delivery.latitude.toString() + "," + loc_delivery.longitude.toString()
                        )
                    Log.e(gmmIntentUri)
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)

                    /**
                     * workaround
                     * problem : 初次啟動出現waypoint消失的問題
                     * solve : 先啟動地圖, 1.5秒後再發送導航資訊
                     */

                    val intent: Intent =
                        requireActivity().packageManager.getLaunchIntentForPackage("com.google.android.apps.maps")!!
                    intent.action = Intent.ACTION_VIEW
                    startActivity(intent)

                    btn_navi_to.postDelayed({
                        Log.d("trigger map navi - $gmmIntentUri")
                        startActivity(mapIntent)
                    }, 1500)
                } catch (e: Exception) {
                    Log.e(e)
                }
            }

        }
    }


    private val onCameraClick = View.OnClickListener {
        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (pictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            //Create a file to store the image
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    requireContext(),
                    requireContext().packageName + ".provider",
                    photoFile
                )
                pictureIntent.putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    photoURI
                )
                startActivityForResult(
                    pictureIntent,
                    Const.MY_PERMISSION_TAKE_PIC
                )
            }
        }
    }

    private val onGalleryClick = View.OnClickListener {
        val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, Const.MY_PERMISSION_GALLERY)
    }

    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.getDefault()
        ).format(Date())
        val imageFileName: String = "IMG_" + timeStamp + "_"
        val storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",         /* suffix */
            storageDir      /* directory */
        )

        imageFilePath = image.absolutePath
        return image
    }

    private val missionObserver = Observer<Mission> { mission ->
        Log.w("Observer mission: $mission")
        /**
         * workaround
         * problem : kotlin 中synchronized wait notify依然會卡到mainthread
         *           因此採用背景延遲UI更新來達到synchronized效果
         *           避免連續刷新任務造成重複執行多個延遲UI的task
         * solve : 每次刷新任務先清除之前執行到一半以及待執行的所有tasks
         */
        mHanlder.removeCallbacksAndMessages(null)
        for (ll in btnPanelArray)
            ll.visibility = View.GONE
        mHanlder.postDelayed({
            if (mission.job_status == 0) {
                fab_chatroom.setBackgroundResource(R.drawable.chatroom_public_btn_normal)
            } else {
                fab_chatroom.setBackgroundResource(R.drawable.chatroom_private_btn_normal)
            }

            if (mission.job_owner > 0) {
                Log.d(
                    "owner: " + mission.job_owner.toString() + " | " +
                            "taker: " + mission.job_taker.toString() + " | " +
                            "user: " + mViewModel.mUser!!.userData.userId + " | " +
                            "job_status: " + mission.job_status
                )

                //choose botton group by usage
                for (ll in btnPanelArray) {
                    ll.visibility = View.GONE
                }

//                tv_mission_status.text =
//                    resources.getStringArray(R.array.mission_status)[mission.job_status]

                //owner 自己, Taker別人
                if (mission.job_owner == mViewModel.mUser!!.userData.userId
                    && mission.job_taker != mViewModel.mUser!!.userData.userId
                ) {
                    when (mission.job_status) {
                        0 -> {
                            if(mission.modify_apply>0){
                                ll_btn_panel_owner_init_taken.visibility = View.VISIBLE
                            }else{
                                ll_btn_panel_owner_init.visibility = View.VISIBLE
                            }

                        }
                        1 -> ll_btn_panel_owner_taken.visibility = View.VISIBLE
                        6 -> {
                            if ((activity as MainActivity).mCurFragment is MissionDetailFragment) {
                                ll_btn_panel_owner_pending.visibility = View.VISIBLE
                                val timer=object:CountDownTimer(180000, 1000){
                                    override fun onFinish() {
                                    }

                                    override fun onTick(millisUntilFinished: Long) {
                                        val secs =
                                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)

                                        val formatted = "(${(secs / 60).toString().padStart(2, '0')} : ${(secs % 60).toString().padStart(2, '0')})"
                                        try{
                                            btn_confirm_request.text = resources.getString(R.string.btn_confirm_request)+formatted
                                            btn_owner_pending_abort.text = resources.getString(R.string.btn_abort)+formatted
                                        }catch (e:Exception){
                                            cancel()
                                        }
                                    }

                                }
                                timer.start()

                                dialog_owner_taken.show()
                            }
                        }
                        7 -> {
                                ll_btn_panel_owner_buffering.visibility = View.VISIBLE
                            CustomDialogBuilder(requireContext(), R.layout.custom_dialog_message_only)
                                .setCustomMessage("時間到了！請儘速完成領收")
                                .setCustomTitle("Notification").build().show()
                            }
                        2->{
                            counter_timer.visibility = View.GONE
                            ll_btn_panel_done.visibility = View.VISIBLE
                        }
                        3,4,5->{
                            counter_timer.visibility = View.GONE
                            ll_btn_panel_fill_reason.visibility = View.VISIBLE
                        }
                    }
                    //owner 別人, Taker 自己
                } else if (mission.job_owner != mViewModel.mUser!!.userData.userId
                    && (mission.job_taker == mViewModel.mUser!!.userData.userId)
                ) {
                    when (mission.job_status) {
                        0-> {
                            ll_btn_panel_taker_init.visibility = View.VISIBLE
                            btn_taker_init_unaccept.setText(mission.owner_firstname + " " + mission.owner_lastname + " "
                                    +resources.getString(R.string.btn_taker_init_unaccept))
                        }
                        1 -> ll_btn_panel_taker_taken_job.visibility = View.VISIBLE
//                        7 -> {
//                            ll_btn_panel_done.visibility = View.VISIBLE
//                            CustomDialogBuilder(requireContext(), R.layout.custom_dialog_message_only)
//                                .setCustomMessage("時間到了！請儘速完成領收")
//                                .setCustomTitle(R.string.title_notification).build().show()
//                        }
                        2->{
                            counter_timer.visibility = View.GONE
                            ll_btn_panel_done.visibility = View.VISIBLE
                        }
                        3,4,5-> {
                            counter_timer.visibility = View.GONE
                            ll_btn_panel_fill_reason.visibility = View.VISIBLE
                        }
                        6->{
                            val dialog=CustomDialogBuilder(requireContext(), R.layout.custom_dialog_message_1_btn)
                                .setCustomMessage("您已經接到案子\n"+
                                        "正跟發案人確認中\n" +
                                        "剩下3:00秒，如對方未確認，\n" +
                                        "工作將重新PO出")
                                .setCustomTitle("您已經接到案子")
                                .setCancelable(true)
                                .build()
                            dialog.show()
                            val tvContent = dialog.findViewById<TextView>(R.id.tvContent)

                            val timer=object:CountDownTimer(180000, 1000){
                                override fun onFinish() {
                                }

                                override fun onTick(millisUntilFinished: Long) {
                                    val secs =
                                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1

                                    val formatted = "${(secs / 60).toString().padStart(2, '0')} : ${(secs % 60).toString().padStart(2, '0')}"
                                    try {
                                        tvContent.text = "正跟發案人確認中\n" +
                                                "剩下"+formatted+"秒\n，如對方未確認，\n" +
                                                "工作將重新PO出"
                                    }catch (e:Exception){
                                        cancel()
                                    }


                                }

                            }
                            timer.start()
                        }
                    }
                    //owner 別人, Taker 別人
                } else if (mission.job_owner != mViewModel.mUser!!.userData.userId
                    && mission.job_taker != mViewModel.mUser!!.userData.userId
                ) {
                    //強制把“”或-1.0轉為-1
                    if(mission.job_taker.toString().isNullOrBlank())
                        mission.job_taker = mission.job_taker as? Int?: -1
                    else
                        mission.job_taker = mission.job_taker.toString().toDouble().toInt()

                    Log.d("job_taker: "+ mission.job_taker)
                    //TODO chatroom 按鈕移除

                    when (mission.job_status) {
                        0 -> ll_btn_panel_other_owner_init.visibility = View.VISIBLE
                        2->{
                            counter_timer.visibility = View.GONE
                            ll_btn_panel_done.visibility = View.VISIBLE
                        }
                        3, 4, 5 -> {
                            counter_timer.visibility = View.GONE
                            ll_btn_panel_fill_reason.visibility = View.VISIBLE
                        }
                        1, 7 -> {
                        }
                        6->{
                            CustomDialogBuilder(requireContext(), R.layout.custom_dialog_message_only)
                                .setCustomMessage("抱歉您沒接到，\n" +
                                        "已經有人比您快接案囉")
                                .setCustomTitle(R.string.title_notification)
                                .build().show()
                        }
                    }
                }

                if (mission.job_type == -1) {
                    mission.job_type = 1
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    (tv_mission_title_ as TextView).text = mission.title
                }else{
                    (tv_mission_title_ as AppCompatTextView).text = mission.title
                }

                tv_mission_owner.setText(mission.owner_firstname+" "+mission.owner_lastname)
                tv_content_store_location.setText(mission.store_addr)
                tv_content_fin_location.setText(mission.addr2coor)
                tv_content_salary.setText(mission.salary.toString())
                val duration = mission.duration
                Log.d("duration: $duration")
                tv_content_delivery_time.text =
                    Utils.getDate(System.currentTimeMillis() + duration * 1000)
                tv_content_mission_content.text = mission.description
                tv_content_mission_demand.text = mission.comment
                tv_content_mission_loc_map_dir.setText(mission.store_addr)
                tv_content_abort_loc_map_dir.setText(mission.addr2coor)


            }
            if(counter_timer.visibility == View.VISIBLE){
                try {
                    var last = (mission.finish_time!!.toBigDecimal()
                        .toLong() - System.currentTimeMillis())
                    Log.d("TIMMER: $last | ${mission.duration}")
                    counter_timer.setRange(if(mission.duration<=0) last.toInt()*20 else mission.duration*20)
                    if (timer != null) {
                        timer!!.cancel()
                    }
                    timer = object:CountDownTimer(last, 1000){
                        override fun onFinish() {
                            counter_timer.setValues(0, 0, 0)
                            mission.finish_time=""
                        }

                        override fun onTick(millisUntilFinished: Long) {
                            counter_timer.setValues(millisUntilFinished.toInt(), 0, 0)
                        }

                    }.start()

                } catch (e: NumberFormatException) {
                    Log.e("TIMMER: " + mission.finish_time + " | " + e)
                    //時間格式不統一 造成資料轉換錯誤 且server資料無法更改
                }
            }

        }, 5)

    }


    fun onQRVarify() {
        //TODO 驗證成功
        Toast.makeText(context, "驗證成功",Toast.LENGTH_SHORT).show()
        if(qrDialog?.showsDialog!!){
            qrDialog!!.dismiss()
            qrDialog!!.showsDialog=false
            requireActivity().onBackPressed()
        }
//        qrDialog?.dismiss()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if(grantResults.isNotEmpty())
            Log.w("grantResults: " + grantResults[0] + " | " + PackageManager.PERMISSION_GRANTED)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Const.MY_PERMISSIONS_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    dialog_pic_selector.dismiss()
                }
            }
        }
    }

    override fun onStateChanged(state: Int) {
        if(state == PackageManager.PERMISSION_GRANTED){
            qrDialog = if(mViewModel.tmp_mission.job_owner==mViewModel.mUser!!.userData.userId){
                ReadQRCodeDialogFragment(mViewModel.tmp_mission)}else{ ShowQrCodeDialogFragment(mViewModel.tmp_mission)}
            qrDialog!!.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
            qrDialog!!.show(requireActivity().supportFragmentManager, "")
            qrDialog!!.showsDialog=true
        }
    }

    var bottomSheetCallBack = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(bottomSheet: View, slideOffset: Float) {

        }

        override fun onStateChanged(bottomSheet: View, newState: Int) {

        }

    }

    override fun refreshUI() {
        super.refreshUI()
        setTitle(R.string.tv_mission_detail)
    }
}