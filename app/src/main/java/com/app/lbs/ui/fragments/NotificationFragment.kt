package com.app.lbs.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.adapters.MissionListAdapter
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.Mission
import com.app.lbs.repository.JobListRepository
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.Utils
import java.util.*
import kotlin.collections.ArrayList

/**
 * @file NotificationFragment
 * @brief
 *       主頁面顯示通知
 *
 * @author Zack
 * */

class NotificationFragment : BaseFragment(), IOnItemClickListener<Mission>, LifecycleOwner {


    private lateinit var mViewModel: MainViewModel
    private lateinit var rv_notifylist: RecyclerView
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var mAdapter: MissionListAdapter? = null
    private var mOwnerList:List<Mission>?=null
    private var mTakerList:List<Mission>?=null

    companion object {
        const val FRAGMENT_ID = R.id.tab_notify
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_notification, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        initView(root)
        return root
    }

    private fun initView(v: View) {

        rv_notifylist = v.findViewById(R.id.rv_notifylist)
        mLayoutManager = LinearLayoutManager(activity)
        rv_notifylist.layoutManager = mLayoutManager
        mAdapter = MissionListAdapter(requireContext(), null, MissionListAdapter.TYPE_NOTIFICATION, mViewModel.mUser?.userData)
        mAdapter!!.setOnItemClickListener(this)
        rv_notifylist.adapter = mAdapter
    }

    override fun onResume() {
        super.onResume()
        setTitle(R.string.title_notification)
        JobListRepository.instance.refreshJobListsImmediately()
        mViewModel.getOwnerNotiList().observe(this, Observer{ t->
            mOwnerList = t
            updateList()
        })

        mViewModel.getTakerNotiList().observe(this, Observer { t->
            mTakerList = t
            updateList()
        })
    }

    private fun updateList(){
        val mTempList: ArrayList<Mission> = arrayListOf()
        if(mOwnerList!=null)
            mTempList.addAll(mOwnerList!!)
        if(mTakerList!=null)
            mTempList.addAll(mTakerList!!)
        Log.d("updateNotiList owner : ${mOwnerList?.size}")
        Log.d("updateNotiList taker : ${mTakerList?.size}")
        mTempList.sortWith(Utils.MissionDateComparator)
        mAdapter!!.setData(mTempList)
    }

    override fun onItemClick(view: View, position: Int) {
        var mission =mAdapter!!.getSelectedMission(position)
        if(mission.job_owner == mViewModel.mUser?.userData?.userId)
            (activity as MainActivity).onMissionClick(mission, Const.USERSTATE.OWNER)
        else
            (activity as MainActivity).onMissionClick(mission, Const.USERSTATE.TAKER)
    }

    override fun onItemClick(item_model: Mission, position: Int) {
        //DO NOTHING
    }

    override fun refreshUI() {
        super.refreshUI()
        setTitle(R.string.title_notification)
    }
}