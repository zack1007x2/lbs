package com.app.lbs.ui.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.util.SparseArray
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.Nullable
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.app.lbs.R
import com.app.lbs.databinding.ActivityMainBinding
import com.app.lbs.interfaces.OnStateChangedListener
import com.app.lbs.models.Mission
import com.app.lbs.models.UserAccount
import com.app.lbs.repository.JobListRepository
import com.app.lbs.repository.SysRepository
import com.app.lbs.service.MyService
import com.app.lbs.ui.dialog.CustomBottomSheetDialogFeedBack
import com.app.lbs.ui.dialog.ReadQRCodeDialogFragment
import com.app.lbs.ui.fragments.*
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.*
import com.app.lbs.utils.Const.IAMOWNER
import com.app.lbs.utils.Const.MY_PERMISSION_CAMERA_ETC
import com.app.lbs.utils.Const.TOHISTORY
import com.app.lbs.utils.Const.USERSTATE
import com.google.firebase.analytics.FirebaseAnalytics
import com.kyleduo.switchbutton.SwitchButton
import kotlinx.android.synthetic.main.fragment_phone_verification.*
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig
import java.util.*


/**
 * @file MainActivity
 * @brief
 *       管理fragment及一些共用功能
 *
 * @author Zack
 * */

class MainActivity : FragmentActivity(), View.OnClickListener, ReadQRCodeDialogFragment.OnListener {

    lateinit var mFirebaseAnalytics: FirebaseAnalytics


    private var fragmentManager: FragmentManager? = null
    lateinit var mCurFragment: Fragment
    private var fragmentTransaction: FragmentTransaction? = null
    private val navigateMap = SparseArray<BaseFragment>()
    private val Log = MyLog.log()
    private lateinit var mViewModel: MainViewModel
    lateinit var binding: ActivityMainBinding

    private lateinit var title_bar_switch: SwitchButton
    private lateinit var title_bar_icon:Button
    private lateinit var bottom_nav_bar: View
    private lateinit var CL_title_bar: androidx.constraintlayout.widget.ConstraintLayout
    private lateinit var rl_mission_publish_success:RelativeLayout
    private lateinit var rl_mission_feedback_success:RelativeLayout
    private lateinit var btn_access_to_mission:Button
    private lateinit var btn_access_to_history:Button
    private lateinit var tab_mission_management:RadioButton
    private lateinit var tab_mission_list:RadioButton
    private var fragIdStack = Stack<Int>()

    val alertMissionDialogBuider = CustomDialogBuilder(this, R.layout.custom_dialog_message_1_btn)
    lateinit var mPref: SharedPreferences

    // Used in checking for runtime permissions.

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private val myReceiver = MyReceiver()

    // A reference to the service used to get location updates.
    private var mService: MyService? = null

    // Tracks the bound state of the service.
    private var mBound = false

    private var needLocationUpdate = false

    private var isOwner = false
    private var isNeedGuide = false

    fun getNeedGuide():Boolean{
        return isNeedGuide
    }

    private val mOnCheckChangedListener = { compoundButton: CompoundButton, b: Boolean ->
        mViewModel.setUserState(if(b)USERSTATE.OWNER else USERSTATE.TAKER)
    }

    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder: MyService.LocalBinder = service as MyService.LocalBinder
            mService = binder.service
            mBound = true
            if (needLocationUpdate)
                mService!!.requestLocationUpdates()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBound = false
        }
    }

    /**
     * Receiver for broadcasts sent by [LocationUpdatesService].
     */
    private class MyReceiver : BroadcastReceiver() {
        override fun onReceive(
            context: Context,
            intent: Intent
        ) {
            val location =
                intent.getParcelableExtra<Location>(MyService.EXTRA_LOCATION)
            if (location != null) {
//                Toast.makeText(
//                    context, Utils.getLocationText(location),
//                    Toast.LENGTH_SHORT
//                ).show()
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("onCreate")
        mPref = getSharedPreferences(Const.APPNAME, Context.MODE_PRIVATE)

        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        mViewModel =
            ViewModelProvider(this@MainActivity).get(MainViewModel(application)::class.java)
        binding.mainViewModel = mViewModel
        binding.lifecycleOwner = this
        if(intent.extras!=null)
            Log.d("intent:"+intent.extras!!.getBoolean(IAMOWNER,false))
        else
            Log.d("intent:"+intent.extras)
        if(intent.extras!=null) {
            isNeedGuide = true
            isOwner = intent.extras!!.getBoolean(IAMOWNER, false)
        } else {
            isOwner = mViewModel.mUser!!.isOwner!!
        }
        if(isOwner){
            mViewModel.setUserState(USERSTATE.OWNER)
        }else{
            mViewModel.setUserState(USERSTATE.TAKER)
        }

        initView()

        initFragment()

        // Check that the user hasn't revoked permissions by going to Settings.
        if (mViewModel.getIsRequestLocation().value ?: false) {
            requestLocationPermission()
        }
    }

    private fun initView() {
        bottom_nav_bar = findViewById(R.id.bottom_nav_bar)
        title_bar_switch = findViewById(R.id.title_bar_switch)
        if(isOwner)
            title_bar_switch.toggleImmediatelyNoEvent()
        title_bar_icon = findViewById(R.id.title_bar_icon)
        title_bar_switch.setOnCheckedChangeListener(mOnCheckChangedListener)
        title_bar_icon.setOnClickListener(this)
        CL_title_bar = findViewById(R.id.CL_title_bar)
        rl_mission_feedback_success = findViewById(R.id.rl_mission_feedback_success)
        rl_mission_feedback_success.setOnClickListener(this)
        rl_mission_publish_success = findViewById(R.id.rl_mission_publish_success)
        rl_mission_publish_success.setOnClickListener(this)
        btn_access_to_history = findViewById(R.id.btn_access_to_history)
        btn_access_to_history.setOnClickListener(this)
        btn_access_to_mission = findViewById(R.id.btn_access_to_mission)
        btn_access_to_mission.setOnClickListener(this)

        tab_mission_management = findViewById(R.id.tab_mission_management)
        tab_mission_list = findViewById(R.id.tab_mission_list)

        if(isNeedGuide){
            val config = ShowcaseConfig()
            config.delay = 500 // half second between each showcase view
            val sequence = MaterialShowcaseSequence(this, IAMOWNER)


            sequence.setConfig(config)
            sequence.addSequenceItem(
                title_bar_switch,
                "可切換使用者身份", "GOT IT"
            )

            sequence.addSequenceItem(
                findViewById(R.id.rg_mission_type),
                "任務：可進行任務管理\n附近：顯示附近任務\n如有其他疑問可至FAQ分頁查詢", "GOT IT"
            )
            sequence.start()
//            sequence.setOnItemDismissedListener { itemView, position ->
//                itemView.resetSingleUse()
//            }
        }

    }

    //init fragment map
    private fun initFragment() {

        fragmentManager = supportFragmentManager
        navigateMap.clear()
        mapNaviToFragment(
            FaqFragmant.FRAGMENT_ID,
            FaqFragmant()
        )

        mapNaviToFragment(
            NotificationFragment.FRAGMENT_ID,
            NotificationFragment()
        )
        mapNaviToFragment(
            MissionManagementFragment.FRAGMENT_ID,
            MissionManagementFragment()
        )
        mapNaviToFragment(
            SettingFragment.FRAGMENT_ID,
            SettingFragment()
        )
        mapNaviToFragment(
            FindMissionByMapFragment.FRAGMENT_ID,
            FindMissionByMapFragment()
        )
        mapNaviToFragment(
            NewMission.FRAGMENT_ID,
            NewMission(),
            false
        )
        mapNaviToFragment(
            MissionChatRoomFragment.FRAGMENT_ID,
            MissionChatRoomFragment(),
            false
        )
        mapNaviToFragment(
            FeedbackFragment.FRAGMENT_ID,
            FeedbackFragment(),
            false
        )
        mapNaviToFragment(
            NewMissionSelectLocation.FRAGMENT_ID,
            NewMissionSelectLocation(),
            false
        )
        mapNaviToFragment(
            PrevMapFragment.FRAGMENT_ID,
            PrevMapFragment(),
            false
        )
        mapNaviToFragment(
            MissionDetailFragment.FRAGMENT_ID,
            MissionDetailFragment(),
            false
        )

        mapNaviToFragment(
            PhoneVerifyFragment.FRAGMENT_ID,
            PhoneVerifyFragment(),
            false
        )

        mapNaviToFragment(
            SettingUserNameFragment.FRAGMENT_ID,
            SettingUserNameFragment(),
            false
        )

        mapNaviToFragment(
            SettingListAccountAddFragment.FRAGMENT_ID,
            SettingListAccountAddFragment(),
            false
        )

        replaceNavFragment(FindMissionByMapFragment.FRAGMENT_ID)
        tab_mission_list.isChecked=true
    }


    @Throws(IllegalArgumentException::class)
    private fun mapNaviToFragment(
        id: Int,
        fragment: BaseFragment,
        setListenerByVid: Boolean = true
    ) {
        fragment.FRAGMENT_ID_INT = id
        val view = findViewById<View>(id)
        if (setListenerByVid)
            view.setOnClickListener(this)
        if (navigateMap.get(id) == null) {
            navigateMap.put(id, fragment)
        } else
            throw IllegalArgumentException("Key Already Exist")
    }

    fun replaceFragment(viewid: Int) {
        val tag = viewid.toString()
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.replace(R.id.contentframe, navigateMap.get(viewid), tag)
        fragmentTransaction!!.commit()

        mCurFragment = navigateMap.get(viewid)
        if (viewid == MissionManagementFragment.FRAGMENT_ID || viewid == FindMissionByMapFragment.FRAGMENT_ID) {
            mViewModel.setUserState(USERSTATE.DECIED_BY_PREVIOUS)
        } else {
            mViewModel.setUserState(USERSTATE.DEFAULT)
        }
        hideSoftKeyboard()
    }

    fun replaceNavFragment(viewid: Int, bundle:Bundle?=null) {
        val tag = viewid.toString()
        if(viewid==MissionManagementFragment.FRAGMENT_ID){
            navigateMap.get(viewid).arguments=bundle
        }
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.replace(R.id.contentframe, navigateMap.get(viewid), tag)
        fragmentTransaction!!.commit()

        mCurFragment = navigateMap.get(viewid)
        title_bar_icon.visibility = View.GONE
        if (viewid == MissionManagementFragment.FRAGMENT_ID || viewid == FindMissionByMapFragment.FRAGMENT_ID) {
            title_bar_switch.visibility = View.VISIBLE
            mViewModel.setUserState(USERSTATE.DECIED_BY_PREVIOUS)
        } else {//setting, notify, faq
            title_bar_switch.visibility = View.GONE
            mViewModel.setUserState(USERSTATE.DEFAULT)
        }
        hideSoftKeyboard()
        fragIdStack = Stack<Int>()
    }

    fun addFullScreenFragment(viewid: Int, bundle: Bundle?=null) {
        if (viewid.toString() == mCurFragment.tag)
            return
        if(viewid==PhoneVerifyFragment.FRAGMENT_ID){
            navigateMap.get(viewid).arguments=bundle
        }
        fragIdStack.push(mCurFragment.tag!!.toInt())
        val tag = viewid.toString()
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.add(R.id.contentframe, navigateMap.get(viewid), tag)
        fragmentTransaction!!.addToBackStack(null)
        fragmentTransaction!!.commit()

        mCurFragment = navigateMap.get(viewid)
        hideSoftKeyboard()
        bottom_nav_bar.visibility = View.GONE
        title_bar_icon.visibility = View.VISIBLE
        if(viewid == MissionChatRoomFragment.FRAGMENT_ID){
            mViewModel.setUserState(USERSTATE.DEFAULT)
        }
    }

    fun addFragmentWithAnim(viewid: Int, bundle: Bundle?=null) {
        if (viewid.toString() == mCurFragment.tag)
            return
        if(viewid==PhoneVerifyFragment.FRAGMENT_ID){
            navigateMap.get(viewid).arguments=bundle
        }

        val tag = viewid.toString()
        fragIdStack.push(mCurFragment.tag!!.toInt())
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.setCustomAnimations(
            R.anim.anim_bottom_top_enter,
            R.anim.fade_out,
            R.anim.fade_in,
            R.anim.anim_top_bottom_leave
        )
        fragmentTransaction!!.add(R.id.contentframe, navigateMap.get(viewid), tag)
        fragmentTransaction!!.addToBackStack(null)
        fragmentTransaction!!.commit()
        bottom_nav_bar.visibility = View.GONE
        title_bar_icon.visibility = View.VISIBLE
        hideSoftKeyboard()
        mCurFragment = navigateMap.get(viewid)
    }

    fun addFragmentWithAnim(vararg param: Int, bundle: Bundle?=null) {
        val tag = param[0].toString()
        if (tag == mCurFragment.tag)
            return

        if(param[0]==PhoneVerifyFragment.FRAGMENT_ID
            || param[0]==SettingListAccountAddFragment.FRAGMENT_ID){
            navigateMap.get(param[0]).arguments=bundle
        }

        fragIdStack.push(mCurFragment.tag!!.toInt())
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.setCustomAnimations(
            if (param.size > 1) param[1] else R.anim.fade_in,
            if (param.size > 2) param[2] else R.anim.anim_top_bottom_leave,
            if (param.size > 1) param[1] else R.anim.fade_in,
            if (param.size > 2) param[2] else R.anim.anim_top_bottom_leave
        )
        fragmentTransaction!!.add(R.id.contentframe, navigateMap.get(param[0]), tag)
        fragmentTransaction!!.addToBackStack(null)
        fragmentTransaction!!.commit()
        title_bar_switch.visibility = View.GONE
        bottom_nav_bar.visibility = View.GONE
        title_bar_icon.visibility = View.VISIBLE
        hideSoftKeyboard()
        mCurFragment = navigateMap.get(param[0])
    }

    /**
     *
     * @param param [0]ID, [1]ANIM_IN, [2]ANIM_OUT
     */
    fun replaceFragmentWithAnim(vararg param: Int) {
        val tag = param[0].toString()
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.setCustomAnimations(
            if (param.size > 1) param[1] else R.anim.fade_in,
            if (param.size > 2) param[2] else R.anim.anim_top_bottom_leave,
            if (param.size > 1) param[1] else R.anim.fade_in,
            if (param.size > 2) param[2] else R.anim.anim_top_bottom_leave
        )
        fragmentTransaction!!.replace(com.app.lbs.R.id.contentframe, navigateMap.get(param[0]), tag)
        fragmentTransaction!!.commit()
        mCurFragment = navigateMap.get(param[0])
    }

    fun finishFragment(viewid: Int) {
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.remove(navigateMap.get(viewid))
        fragmentTransaction!!.commit()
    }


    fun setTitle(title: String) {
        mViewModel.setTitle(title)
    }

    override fun onClick(v: View?) {
        v?.requestFocus()
        hideSoftKeyboard()
        when (v?.id) {
            MissionManagementFragment.FRAGMENT_ID,
            FindMissionByMapFragment.FRAGMENT_ID->{
                replaceNavFragment(v.id)
            }
            SettingFragment.FRAGMENT_ID, FaqFragmant.FRAGMENT_ID, NotificationFragment.FRAGMENT_ID -> {
                replaceNavFragment(v.id)
            }
            //TODO add mission換位
//            R.id.float_button -> {
//                title_bar_switch.visibility = View.GONE
//                mViewModel.setUserState(0)
//                mCurFragment.onPause()
//                last_frag_id = try {
//                    mCurFragment.tag?.toInt()
//                } catch (e: Exception) {
//                    MissionListFragment.FRAGMENT_ID
//                }
//
//                addFullScreenFragment(NewMissionStep1.FRAGMENT_ID)
//                mViewModel.initNewMissionObject()
//            }

            //////////////////////////////////////////

            //btn for find mission by map
//            R.id.title_bar_map -> {
//                if (mCurFragment == navigateMap.get(MissionListFragment.FRAGMENT_ID))
//                    replaceFragment(FindMissionByMapFragment.FRAGMENT_ID)
//                else if (mCurFragment == navigateMap.get(FindMissionByMapFragment.FRAGMENT_ID)) {
//                    replaceFragment(MissionListFragment.FRAGMENT_ID)
//                }
//            }

            R.id.btn_access_to_mission->{
                rl_mission_publish_success.visibility = View.GONE
                Log.e("TIMMER - btn_access_to_mission")
                onMissionClick(mViewModel.getObservTmpMission().value!!)
//                mViewModel.initNewMissionObject()
            }
            R.id.rl_mission_publish_success->{
                rl_mission_publish_success.visibility = View.GONE
                mViewModel.initNewMissionObject()
            }

            R.id.btn_access_to_history->{
                rl_mission_feedback_success.visibility = View.GONE
                var bun = Bundle()
                bun.putBoolean(TOHISTORY, true)
                replaceNavFragment(MissionManagementFragment.FRAGMENT_ID, bun)
            }
            R.id.rl_mission_feedback_success->{
                rl_mission_feedback_success.visibility = View.GONE
            }

            R.id.title_bar_icon->{
                onBackPressed()
            }
        }
    }

    @SuppressLint("ShowToast")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            MY_PERMISSION_CAMERA_ETC -> {
                Log.e("onRequestPermissionsResult" + grantResults[0])
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // PERMISSION GRANTED then show qr read frag
                    if (permissionGrantedListener != null)
                        permissionGrantedListener.onStateChanged(grantResults[0])
                }
                return
            }

            REQUEST_PERMISSIONS_REQUEST_CODE -> {
                if (grantResults.isEmpty()) {
                    // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i("User interaction was cancelled.")
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted.
                    if (mService != null)
                        mService!!.requestLocationUpdates()
                    else
                        needLocationUpdate = true
                } else {
                    // Permission denied.
                    Toast.makeText(this, R.string.permission_deny, Toast.LENGTH_SHORT)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        bindService(
            Intent(this, MyService::class.java), mServiceConnection,
            Context.BIND_AUTO_CREATE
        )

        requestLocationPermission()

        mViewModel.getIsRequestLocation().observe(this, locationRequestObserver)
        mViewModel.getUserState().observe(this , userStateObserver)

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
    }

    override fun onResume() {
        super.onResume()
        Log.i("onResume")
        initFakeData()
        mViewModel.getUserData().observe(this, mAccountObserver)
        mViewModel.getResultCode().observe(this, apiResultObserver)
        mViewModel.getAlertMission().observe(this, alertMissionObserver)
        LocalBroadcastManager.getInstance(this).registerReceiver(
            myReceiver,
            IntentFilter(MyService.ACTION_BROADCAST)
        )
    }

    override fun onPause() {
        super.onPause()
        mViewModel.getUserData().removeObserver(mAccountObserver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver!!)
    }

    override fun onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection)
            mBound = false
        }
        mViewModel.getIsRequestLocation().removeObserver(locationRequestObserver)
        mViewModel.getUserState().removeObserver(userStateObserver)
        super.onStop()
    }


    override fun onBackPressed() {
        if (mCurFragment is BaseFragment && !((mCurFragment as BaseFragment).onBackPressed())) {
            return
        }
        Log.i("onBackPressed Before - " + supportFragmentManager.backStackEntryCount + " tag : " + mCurFragment::class.java)
        if(mCurFragment is MissionDetailFragment)
            mViewModel.initNewMissionObject()

        if (supportFragmentManager.backStackEntryCount >= 1) {
            mCurFragment = navigateMap.get(fragIdStack.pop())
        }
        Log.i("onBackPressed After - " + supportFragmentManager.backStackEntryCount + " tag : " + mCurFragment::class.java)

        (mCurFragment as? BaseFragment)?.refreshUI()

        if (supportFragmentManager.backStackEntryCount == 1) {
            bottom_nav_bar.visibility = View.VISIBLE
            title_bar_icon.visibility = View.GONE
            if(mCurFragment is MissionManagementFragment || mCurFragment is FindMissionByMapFragment)
                title_bar_switch.visibility = View.VISIBLE


            if(mCurFragment is NotificationFragment){
                mViewModel.setUserState(USERSTATE.DEFAULT)
            }

        }
        super.onBackPressed()
    }

    fun clearAllBackStackFragments(
        backToIndex: Int? = 0,
        @Nullable frag_id: Int? = FindMissionByMapFragment.FRAGMENT_ID
    ) {
        while (supportFragmentManager.backStackEntryCount > backToIndex!!) {
            supportFragmentManager.popBackStackImmediate()
        }
        if (backToIndex == 0) {
            title_bar_switch.visibility = View.VISIBLE
            mViewModel.setUserState(USERSTATE.DECIED_BY_PREVIOUS)
            bottom_nav_bar.visibility = View.VISIBLE
            mCurFragment = navigateMap.get(FindMissionByMapFragment.FRAGMENT_ID)
            setTitle(R.string.title_mission_list)
        } else {
            when (frag_id) {
                MissionChatRoomFragment.FRAGMENT_ID -> {
                    title_bar_switch.visibility = View.GONE
                    mViewModel.setUserState(USERSTATE.DECIED_BY_PREVIOUS)
                    bottom_nav_bar.visibility = View.GONE
                    mCurFragment = navigateMap.get(MissionChatRoomFragment.FRAGMENT_ID)
                    setTitle(R.string.title_mission_management)
                }
            }
        }
    }

    //監聽user資訊變化
    private var mAccountObserver: Observer<UserAccount> = Observer { user ->
        if (user != null) {
            Log.v("USER DATA CHANGE: " + gson.toJson(user).toString())
            //save data to file
            mPref.edit().putString(Const.PREF_USER_KEY, gson.toJson(user)).commit()
//            mViewModel.setDebug(user.userSetting!!.priceIncreaseAlert)
        } else {
            mPref.edit().clear().commit()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    //api result code and show detail dialog
    private var apiResultObserver: Observer<Int> = Observer { result_code ->
        val buider = CustomDialogBuilder(this, R.layout.custom_dialog_message_only)
        buider.setCustomTitle(R.string.msg_error)
        when (result_code) {
            111 -> buider.setCustomMessage(R.string.msg_mission_not_found).build().show()
            112 -> buider.setCustomMessage(R.string.msg_someone_taken).build().show()
            113 -> buider.setCustomMessage(R.string.msg_own_job).build().show()
            114 -> buider.setCustomMessage(R.string.msg_no_owner).build().show()
        }
        buider.build().show()
    }

    private var alertMissionObserver: Observer<SysRepository.AlertMission> = Observer { mission ->
        //TODO 任務時間提醒（尚未測試）
        val msg: String = "任務:" + mission.mission.title + "剩餘" + mission.left + "分鐘"
        val dialog = alertMissionDialogBuider
            .setCustomTitle(R.string.msg_default_time)
            .setCustomMessage(msg).build()
        dialog.show()
    }

    /**
     *  @brief 隱藏鍵盤
     */
    fun hideSoftKeyboard(view: View?=null) {
        val inputMethodManager: InputMethodManager =
            getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        if(view!=null) {
            view.postDelayed({
                inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                view.clearFocus()
                if(view is EditText)
                    view.isCursorVisible = false
                (view.parent as View).requestFocus()
            },50)
        }else{
            if(currentFocus==null) {
                title_bar_icon.requestFocus()
                title_bar_icon.postDelayed({
                    inputMethodManager.hideSoftInputFromWindow(title_bar_icon.windowToken, 0)
                },50)
            }else {
                currentFocus?.postDelayed({
                    inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
                }, 50)
            }
        }
    }

    /**
     *  @brief 開啟鍵盤
     */
    fun showSoftKeyboard(view: View) {
        val inputMethodManager: InputMethodManager =
            getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        view.requestFocus()
        inputMethodManager.showSoftInput(view, 0)
    }

    /**
     *  @brief 隱藏鍵盤
     */
    fun onMissionClick(mission: Mission, state:USERSTATE?=USERSTATE.DECIED_BY_PREVIOUS) {
        mViewModel.initNewMissionObject()
        mViewModel.tmp_mission = mission
        mViewModel.tmp_mission_w = mission

        title_bar_switch.visibility = View.GONE
        mViewModel.setUserState(state!!)
        Log.d("TIMMER: duration: " + mission.duration)
        mViewModel.updateMission()
        addFullScreenFragment(MissionDetailFragment.FRAGMENT_ID)
    }

    override fun ongetNewSN(qrCode: String?) {
        Log.i("Read QR $qrCode")
        //TODO 收到QR後處理
        Toast.makeText(this, qrCode, Toast.LENGTH_SHORT).show()
        if (mCurFragment is MissionDetailFragment) {
            (mCurFragment as MissionDetailFragment).onQRVarify()
            CustomBottomSheetDialogFeedBack().show(this)
        }
//        addFullScreenFragment(FeedbackFragment.FRAGMENT_ID)
    }

    //Check permission for qrcode
    private lateinit var permissionGrantedListener: OnStateChangedListener
    fun QrCheckPermissions(listener: OnStateChangedListener): Boolean {
        permissionGrantedListener = listener
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_DENIED
        ) {
            var result: Int
            val listPermissionsNeeded = ArrayList<String>()
            for (p in Const.permissions) {
                result = ContextCompat.checkSelfPermission(this, p)
                if (result != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(p)
                }
            }
            if (listPermissionsNeeded.isNotEmpty()) {
                ActivityCompat.requestPermissions(
                    this,
                    listPermissionsNeeded.toTypedArray(),
                    MY_PERMISSION_CAMERA_ETC
                )
                return false
            }
        } else {
            return true
        }
        return true
    }

    //load fake data to repository(讀資料需要context 但repository不應和context有任何掛鉤)
    private fun initFakeData() {
        JobListRepository.instance.jsonJobList = Utils.loadJSONFromAsset(this, "fake_job_list.json")
        JobListRepository.instance.jsonOwnJobList =
            Utils.loadJSONFromAsset(this, "fake_own_job_list.json")
        JobListRepository.instance.jsonOwnFinJobList =
            Utils.loadJSONFromAsset(this, "fake_own_fin_job_list.json")
        JobListRepository.instance.jsonTakenJobList =
            Utils.loadJSONFromAsset(this, "fake_taken_job_list.json")
        JobListRepository.instance.jsonTakenFinJobList =
            Utils.loadJSONFromAsset(this, "fake_taken_fin_job_list.json")
    }


    fun requestLocationPermission() {
        needLocationUpdate = false
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
        // The ACCESS_FINE_LOCATION is denied, then I request it and manage the result in
        // onRequestPermissionsResult() using the constant MY_PERMISSION_ACCESS_FINE_LOCATION
        else if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        } else {
            if (mService != null)
                mService!!.requestLocationUpdates()
            else
                needLocationUpdate = true
        }
    }

    /**
     * Returns the current state of the permissions needed.
     */
//    private fun checkPermissions(): Boolean {
//        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
//            this,
//            Manifest.permission.ACCESS_FINE_LOCATION
//        )
//    }
//
////    private fun requestPermissions() {
////            Log.i("Requesting permission")
////            // Request permission. It's possible this can be auto answered if device policy
////            // sets the permission in a given state or the user denied the permission
////            // previously and checked "Never ask again".
////            ActivityCompat.requestPermissions(
////                this@MainActivity,
////                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
////                REQUEST_PERMISSIONS_REQUEST_CODE
////            )
////    }

    private var locationRequestObserver: Observer<Boolean> = Observer { isRequestLocation ->

    }
    private var userStateObserver: Observer<USERSTATE> = Observer { state ->
        Log.d("state: "+state+ " | frag: "+mCurFragment::class.java)

        when(state){

            USERSTATE.DEFAULT -> {//default
                CL_title_bar.setBackgroundResource(R.color.hyperlink_blue)
                title_bar_switch.visibility = View.GONE
            }

            USERSTATE.OWNER ->{//owner
                mViewModel.setMainBG(R.color.owner_red)
                CL_title_bar.setBackgroundResource(R.color.owner_red)
                if(mCurFragment is MissionDetailFragment)
                    title_bar_switch.visibility = View.GONE
                else
                    title_bar_switch.visibility = View.VISIBLE

                title_bar_switch.setBackDrawableRes(R.drawable.switch_track_owner)
                tab_mission_management.setCompoundDrawablesWithIntrinsicBounds(null,ContextCompat.getDrawable(this,R.drawable.bottom_nav_img_selector_mission_owner), null, null)
                tab_mission_list.setCompoundDrawablesWithIntrinsicBounds(null,ContextCompat.getDrawable(this,R.drawable.bottom_nav_img_selector_mission_owner), null, null)
                tab_mission_management.setTextColor(ContextCompat.getColorStateList(this, R.color.custom_owner_text_black_selector))
                tab_mission_list.setTextColor(ContextCompat.getColorStateList(this, R.color.custom_owner_text_black_selector))
                if(mCurFragment is FindMissionByMapFragment)
                    setTitle(R.string.title_mission_list)
                if(mCurFragment is MissionManagementFragment)
                    setTitle(R.string.title_mission_management)
            }

            USERSTATE.TAKER ->{//taker
                mViewModel.setMainBG(R.color.taker_blue)
                CL_title_bar.setBackgroundResource(R.color.taker_blue)
                if(mCurFragment is MissionDetailFragment)
                    title_bar_switch.visibility = View.GONE
                else
                    title_bar_switch.visibility = View.VISIBLE
                title_bar_switch.setBackDrawableRes(R.drawable.switch_track_taker)
                tab_mission_management.setTextColor(ContextCompat.getColorStateList(this, R.color.custom_taker_text_black_selector))
                tab_mission_list.setTextColor(ContextCompat.getColorStateList(this, R.color.custom_taker_text_black_selector))
                tab_mission_management.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.bottom_nav_img_selector_mission_taker, 0, 0)
                tab_mission_list.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.bottom_nav_img_selector_mission_taker, 0, 0)

                if(mCurFragment is FindMissionByMapFragment)
                    setTitle(R.string.title_mission_list)
                if(mCurFragment is MissionManagementFragment)
                    setTitle(R.string.title_mission_management)
            }

        }
    }

    fun showMissionPublishSuccess(){
        rl_mission_publish_success.visibility = View.VISIBLE
    }

    fun showMissionFeedBackSuccess(){
        rl_mission_feedback_success.visibility = View.VISIBLE
    }

    /**
     * 使用addfragment 返回時要更新curFargment
     */
    fun updateCurFragment(id:Int){
        mCurFragment = navigateMap.get(id)
    }
}
