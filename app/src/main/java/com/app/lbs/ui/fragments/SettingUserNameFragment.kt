package com.app.lbs.ui.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.databinding.FragmentSettingBinding
import com.app.lbs.databinding.FragmentSettingUsernameBindingImpl
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const.ISSETTING
import com.app.lbs.utils.Const.PAYMENTS_ENVIRONMENT
import com.app.lbs.utils.PaymentsUtil
import com.app.lbs.utils.Utils
import com.google.android.gms.common.api.Api
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.wallet.IsReadyToPayRequest
import com.google.android.gms.wallet.PaymentsClient
import com.google.android.gms.wallet.Wallet
import com.google.android.gms.wallet.Wallet.WalletOptions
import com.google.android.gms.wallet.WalletConstants
import com.kyleduo.switchbutton.SwitchButton
import org.json.JSONObject


/**
 * @file SettingUserNameFragment
 * @brief
 *       使用者名稱設定頁面
 *
 * @author Zack
 * */

class SettingUserNameFragment : BaseFragment(), View.OnClickListener{
    private lateinit var mViewModel: MainViewModel
    private lateinit var et_content_user_name:EditText
    private lateinit var btn_clear_text:ImageButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        val binding: FragmentSettingUsernameBindingImpl = DataBindingUtil.inflate(
            inflater, R.layout.fragment_setting_username, container, false
        )
        val root = binding.root
        binding.mainViewModel = mViewModel
        initView(root)
        return root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initView(v: View) {
        et_content_user_name = v.findViewById(R.id.et_content_user_name)
        et_content_user_name.setText(mViewModel.mUser?.name_admin)
        btn_clear_text = v.findViewById(R.id.btn_clear_text)
        btn_clear_text.setOnClickListener(this)
    }

    companion object {
        const val FRAGMENT_ID = R.id.btn_username
    }

    override fun onResume() {
        super.onResume()
        setTitle(R.string.tv_title_account_name)
        et_content_user_name.setText(mViewModel.mUser?.name_admin)
    }

    override fun onBackPressed(): Boolean {
        if(et_content_user_name.text.isNotEmpty()){
            mViewModel.mUser?.name_admin = et_content_user_name.text.toString()
            mViewModel.saveUserData()
        }
        return super.onBackPressed()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_clear_text->{
                et_content_user_name.setText("")
            }
        }
    }

}