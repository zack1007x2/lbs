package com.app.lbs.ui.fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.databinding.FragmentLoginBinding
import com.app.lbs.models.UserAccount
import com.app.lbs.ui.activities.LoginActivity
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.LoginViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.gson
import com.google.android.gms.common.SignInButton
import org.json.JSONException

/**
 * @file LoginFragment
 * @brief
 *       登入頁面
 *
 * @author Zack
 * */

class LoginFragment : BaseFragment(), View.OnClickListener {

    lateinit var mPref: SharedPreferences
    lateinit var mLoginViewModel: LoginViewModel
    lateinit var binding: FragmentLoginBinding
    lateinit var googleLoginBtn: Button
    lateinit var fbLoginBtn: Button
    lateinit var webDialog: AlertDialog

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.sign_in_button -> mLoginViewModel.onClickGoogleLogin()
            R.id.login_button -> mLoginViewModel.onClickFacebookLogin()
//            R.id.sign_in_button->(activity as LoginActivity).replaceFragment(PhoneVerifyFragment())
//            R.id.login_button -> (activity as LoginActivity).replaceFragment(PhoneVerifyFragment())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        Log.i("onCreateView")
        mPref = requireActivity().getSharedPreferences(Const.APPNAME, MODE_PRIVATE)
        //確認如果登入過直接跳到main

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        mLoginViewModel = activity?.run {
            ViewModelProvider(this)[LoginViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        binding.loginViewModel = this.mLoginViewModel
        if (!mPref.getString(Const.PREF_USER_KEY, null).isNullOrEmpty()) {
            this@LoginFragment.startActivity(Intent(activity, MainActivity::class.java))
            this@LoginFragment.requireActivity().finish()
        }
        init(binding.root)

        return binding.root
    }

    private fun init(v: View) {

        googleLoginBtn = v.findViewById(R.id.sign_in_button)
        fbLoginBtn = v.findViewById(R.id.login_button)

        googleLoginBtn.setOnClickListener(this)
        fbLoginBtn.setOnClickListener(this)

        mLoginViewModel.getLoginUrl().observe(viewLifecycleOwner, Observer { t ->
            Log.v("onChanged:: $t")
            showLoginDialog(t.toString())
        })
    }

    @JavascriptInterface
    @Throws(JSONException::class)
    fun getJSdata(data: String) {
        Log.v("data: $data")
        webDialog.dismiss()
        //登入google收到資料後存到sp並跳轉到電話驗證
        mLoginViewModel.mUser = gson.fromJson(data, UserAccount::class.java)
        if (mPref.edit().putString(Const.PREF_USER_KEY, data).commit()) {
                if(mLoginViewModel.mUser?.userData?.verifiedByMobilephone==false)
                    (activity as LoginActivity).replaceFragment(PhoneVerifyFragment())
                else{
                    mPref.edit().putBoolean(Const.PREF_TUTORIAL, true).apply()
                    (activity as LoginActivity).replaceFragment(IntroFragment())
                }

        }
    }

    override fun onResume() {
        super.onResume()
        mLoginViewModel.setTitle(R.string.title_login)
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun showLoginDialog(mUri:String){
        val mWebViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {
                webView.loadUrl(url)
                Log.d("shouldOverrideUrlLoading() URL : $url")
                return true
            }
        }

        val alert: AlertDialog.Builder = AlertDialog.Builder(this@LoginFragment.context)
        val webView: WebView = object : WebView(this@LoginFragment.context) {
            override fun onCheckIsTextEditor(): Boolean {
                return true
            }
        }
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        webView.setInitialScale(30)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            webView.settings.userAgentString = "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36"
        else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
            webView.settings.userAgentString = "Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30"
        else
            webView.settings.userAgentString = "Mozilla/5.0 (Linux; Android 4.4; Nexus 5 Build/_BuildID_) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36"
        if(mUri.contains("accounts.google"))
            webView.settings.userAgentString = "here"

        webView.addJavascriptInterface(this@LoginFragment, "Android")
        webView.loadUrl(mUri)
        webView.webViewClient = mWebViewClient
        alert.setView(webView)
        webDialog = alert.create()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        webDialog.window!!.setLayout((width * 0.95).toInt(), (height * 0.85).toInt())
        webDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        webDialog.show()
    }


}