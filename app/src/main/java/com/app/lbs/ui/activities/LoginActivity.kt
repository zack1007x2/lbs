package com.app.lbs.ui.activities

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.ui.fragments.BaseFragment
import com.app.lbs.ui.fragments.LoginFragment
import com.app.lbs.ui.viewmodels.LoginViewModel
import com.app.lbs.utils.MyLog


/**
 * @file LoginActivity
 * @brief
 *       管理login頁面
 *
 * @author Zack
 * */

class LoginActivity : FragmentActivity() {

    private var fragmentManager: FragmentManager? = null
    private var fragmentTransaction: FragmentTransaction? = null
    private val Log: MyLog = MyLog.log()

    private var mViewModel: LoginViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        fragmentManager = supportFragmentManager
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.replace(R.id.login_content_frame, LoginFragment(), null)
        fragmentTransaction!!.commit()

        mViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
    }

    fun getViewModel(): LoginViewModel? {
        return mViewModel
    }

    fun replaceFragment(freg:BaseFragment){
        Log.e("replaceFragment")
        fragmentManager = supportFragmentManager
        fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction!!.replace(R.id.login_content_frame, freg, null)
        fragmentTransaction!!.commit()
    }

}