package com.app.lbs.ui.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.app.lbs.R
import com.app.lbs.adapters.PhoneVerifyPageAdapter
import com.app.lbs.databinding.FragmentPhoneVerificationBinding
import com.app.lbs.ui.activities.LoginActivity
import com.app.lbs.ui.viewmodels.LoginViewModel
import com.app.lbs.utils.Const
import com.app.lbs.utils.Const.ISSETTING
import com.app.lbs.utils.Const.PREF_TUTORIAL

/**
 * @file PhoneVerifyFragment
 * @brief
 *       電話驗證頁面管理，依序顯示 -------- PhoneVerifyStep1
 *                                  |--- PhoneVerifyStep2
 *
 *
 * @author Zack
 **/


class PhoneVerifyFragment : BaseFragment(), View.OnClickListener{
    lateinit var mLoginViewModel: LoginViewModel
    lateinit var binding: FragmentPhoneVerificationBinding
    private lateinit var view_pager: ViewPager2
    lateinit var mPhoneVerifyPageAdapter: PhoneVerifyPageAdapter
    private lateinit var btn_back: Button
    private lateinit var btn_skip: Button
    lateinit var mPref: SharedPreferences
    private var isSetting:Boolean=false

    companion object {
        const val FRAGMENT_ID = R.id.tv_content_bind_phone
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        Log.i("onCreateView")

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_phone_verification,
            container,
            false
        )
        val bundle = this.arguments
        if (bundle != null) {
            isSetting = bundle.getBoolean(ISSETTING, false)
        }
        mLoginViewModel = activity?.run {
            ViewModelProvider(this)[LoginViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        binding.loginViewModel = this.mLoginViewModel
        init(binding.root)
        mPref = requireActivity().getSharedPreferences(Const.APPNAME, Context.MODE_PRIVATE)
        return binding.root
    }

    fun init(v: View) {
        view_pager = v.findViewById(R.id.view_pager)
        mPhoneVerifyPageAdapter = PhoneVerifyPageAdapter(childFragmentManager, lifecycle)
        mPhoneVerifyPageAdapter.setFragmentsList(
            arrayListOf(
                PhoneVerifyStep1(), PhoneVerifyStep2()
            )
        )
        view_pager.adapter = mPhoneVerifyPageAdapter
        view_pager.isUserInputEnabled = false
        view_pager.isSaveEnabled = false

        btn_back = v.findViewById(R.id.btn_back)
        btn_skip = v.findViewById(R.id.btn_skip)
        if(isSetting){
            btn_back.visibility = View.GONE
            btn_skip.visibility = View.GONE
        }else{
            btn_back.setOnClickListener(this)
            btn_skip.setOnClickListener(this)
        }

    }

    fun gotoStep(i: Int) {
        view_pager.setCurrentItem(i - 1, true)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> {
                if(isSetting){

                }else{
                    mPref.edit().putString(Const.PREF_USER_KEY, null).commit()
                    (activity as LoginActivity).replaceFragment(LoginFragment())
                }

            }
            R.id.btn_skip -> {
                if(isSetting){
                    activity?.onBackPressed()
                }else{
                    mPref.edit().putBoolean(PREF_TUTORIAL, true).apply()
                    (activity as LoginActivity).replaceFragment(IntroFragment())
                }

            }
        }
    }

    fun isSettingMode():Boolean{
        return isSetting
    }

}