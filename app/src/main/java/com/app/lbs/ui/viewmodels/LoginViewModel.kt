package com.app.lbs.ui.viewmodels

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.lbs.interfaces.ApiInterface
import com.app.lbs.models.UserAccount
import com.app.lbs.repository.SysRepository
import com.app.lbs.utils.*

/**
 * @file LoginViewModel
 * @brief
 *       只處理與login相關的資訊
 *
 * @author Zack
 * */

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    //SingleLiveEvent : only sent event one time
    //MutableLiveData : might resent event when lifecycle changed

    private val mTitle = MutableLiveData<String>()
    var mSysRepo:SysRepository = SysRepository()
    var mUser: UserAccount?=null
    var Log = MyLog.log()
    var mMobile:String=""
    init {
        var userData:String?=application.getSharedPreferences(Const.APPNAME, Context.MODE_PRIVATE)
            .getString(Const.PREF_USER_KEY, null)
        Log.i("LoginViewModel INIT : $userData")
        mUser = gson.fromJson(userData, UserAccount::class.java)
    }

    fun getLoginUrl(): MutableLiveData<String> {
        return mSysRepo.getLoginUrl()
    }

    fun getPrefix(): MutableLiveData<String> {
        return mSysRepo.getPrefix()
    }

    fun getTitle(): LiveData<String> {
        return mTitle
    }

    fun setTitle(resId: Int) {
        mTitle.postValue(getApplication<Application>().getString(resId))
    }


    fun onClickGoogleLogin() {
        Log.d("onClickGoogleLogin")
//        serverOAuth2Login(ApiInterface.TYPE_GOO)
        serverSecureOAuth2Login(ApiInterface.TYPE_GOO)
    }

    fun onClickFacebookLogin() {
//        serverOAuth2Login(ApiInterface.TYPE_FB)
        serverSecureOAuth2Login(ApiInterface.TYPE_FB)
    }

    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(
            className: ComponentName,
            service: IBinder
        ) {
            Log.d("Service Bind")
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            Log.d("Service unBind")
        }
    }

    private fun serverOAuth2Login(type: String) {
        val param = QueryBody()
        if (Const.IGNORE == 1) {
            param.ignore = "1"
        } else {
            param.timestamp = Utils.timeStamp
            param.mac = Utils.getMac("GetOAuth2LoginUrl", param.serializeToMap())
        }
        mSysRepo.serverOAuth2Login(type, param)
    }

    fun serverSMSValidation(mobile:String) {
        val param = QueryBody()
        param.userId = mUser?.id_admin
        param.apiToken = mUser?.apiToken
        param.mobile = mobile
        if(Const.IGNORE==1)
            param.ignore = "1"
        Log.e("param: $param")
        mSysRepo.serverSMSValidation(param)
    }

    private fun serverSecureOAuth2Login(type: String) {
        mSysRepo.serverSecureOAuth2Login(type)
    }

    fun loginByOAuth2Token(type: String, param: QueryBody) {
        mSysRepo.loginByOAuth2Token(type, param)
    }

    fun getServiceConnection(): ServiceConnection {
        return mServiceConnection
    }
}
