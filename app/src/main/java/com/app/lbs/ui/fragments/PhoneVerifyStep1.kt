package com.app.lbs.ui.fragments

import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.ui.viewmodels.LoginViewModel



class PhoneVerifyStep1:BaseFragment() {
    private lateinit var mViewModel: LoginViewModel
    private lateinit var btn_next:Button
    private lateinit var et_phone_veri:EditText
    companion object {
        const val FRAGMENT_ID = R.id.rv_taken_missionList
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.viewpage_phone_veri_step1, container, false)
        mViewModel = activity?.run {
            ViewModelProvider(this)[LoginViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        initView(root)
        return root
    }

    private fun initView(root: View) {
        btn_next = root.findViewById(R.id.btn_next)
        btn_next.setOnClickListener {
            mViewModel.mMobile = et_phone_veri.text.trim().toString()
            mViewModel.serverSMSValidation(mViewModel.mMobile)
            (parentFragment as? PhoneVerifyFragment)?.gotoStep(2)
        }

        et_phone_veri = root.findViewById(R.id.et_phone_veri)
        if((parentFragment as? PhoneVerifyFragment)?.isSettingMode()!!){
            et_phone_veri.hint = "請輸入欲更改的手機號碼"
        }
        et_phone_veri.addTextChangedListener(PhoneNumberFormattingTextWatcher("TW"))
        et_phone_veri.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(et_phone_veri.getText().toString().length==12)     //size is your limit
                {
                    btn_next.isEnabled = true
                    btn_next.text = "發送驗證碼"
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        btn_next.setTextColor(resources.getColor(R.color.colorMainBrightWhite,null))
                    }else{
                        btn_next.setTextColor(resources.getColor(R.color.colorMainBrightWhite))
                    }
                    btn_next.setBackgroundResource(R.drawable.custom_owner_btn_oval_pressed)
                }else{
                    btn_next.isEnabled = false
                    btn_next.text = "下一步"
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        btn_next.setTextColor(resources.getColor(R.color.black,null))
                    }else{
                        btn_next.setTextColor(resources.getColor(R.color.black))
                    }
                    btn_next.setBackgroundResource(R.drawable.custom_btn_oval_disable)
                }
            }

        })
    }


}