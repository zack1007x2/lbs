package com.app.lbs.ui.dialog

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.app.lbs.R
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.utils.MyLog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlin.math.roundToInt

/**
 * @file CustomBottomSheetDialogFail
 * @brief
 *       任務失敗原因選單
 *
 * @author Zack
 **/

class CustomBottomSheetDialogFail : BottomSheetDialogFragment(), CompoundButton.OnCheckedChangeListener {
    var behavior:BottomSheetBehavior<View>?=null
    private lateinit var cb_fail_unverify:CheckBox
    private lateinit var cb_fail_bailon:CheckBox
    private lateinit var cb_fail_other:CheckBox
    private lateinit var et_fail_other_reason:EditText
    private lateinit var btn_fail_reason_submit:Button
    private var Log = MyLog.log()



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = View.inflate(context, R.layout.custom_bottom_sheet_fail, null)
        dialog.setContentView(view)
        initView(view)

        return dialog
    }

    private fun initView(view: View) {
        cb_fail_unverify = view.findViewById(R.id.cb_fail_unverify)
        cb_fail_unverify.setOnCheckedChangeListener(this)
        cb_fail_bailon = view.findViewById(R.id.cb_fail_bailon)
        cb_fail_bailon.setOnCheckedChangeListener(this)
        cb_fail_other = view.findViewById(R.id.cb_fail_other)
        cb_fail_other.setOnCheckedChangeListener(this)
        et_fail_other_reason = view.findViewById(R.id.et_fail_other_reason)
        et_fail_other_reason.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if(s!!.isNotEmpty()){
                    cb_fail_other.isChecked=true
                }
            }

        })

        btn_fail_reason_submit = view.findViewById(R.id.btn_fail_reason_submit)
        (view.parent as View).setBackgroundColor(ContextCompat.getColor(requireContext(), android.R.color.transparent))
        behavior = BottomSheetBehavior.from(view.parent as View)
        behavior?.peekHeight = resources.getDimension(R.dimen.size_550).roundToInt()
    }


    fun show(fragmentActivity: FragmentActivity) {
        show(fragmentActivity.supportFragmentManager, TAG)
    }

    companion object {
        private const val TAG = "CustomBottomSheetDialogFail"
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when(buttonView?.id){
            R.id.cb_fail_unverify->{
                if(isChecked){
                    cb_fail_bailon.isChecked=false
                    cb_fail_other.isChecked=false
                    et_fail_other_reason.text=null
                    et_fail_other_reason.clearFocus()
                    (activity as MainActivity).hideSoftKeyboard(et_fail_other_reason)
                }

            }
            R.id.cb_fail_bailon->{
                if(isChecked){
                    cb_fail_unverify.isChecked=false
                    cb_fail_other.isChecked=false
                    et_fail_other_reason.text=null
                    et_fail_other_reason.clearFocus()
                    (activity as MainActivity).hideSoftKeyboard(et_fail_other_reason)
                }
            }
            R.id.cb_fail_other->{
                if(isChecked){
                    cb_fail_bailon.isChecked=false
                    cb_fail_unverify.isChecked=false
                    et_fail_other_reason.requestFocus()
                    (activity as MainActivity).showSoftKeyboard(et_fail_other_reason)
                }else{
                    et_fail_other_reason.text=null
                    et_fail_other_reason.clearFocus()
                    (activity as MainActivity).hideSoftKeyboard(et_fail_other_reason)
                }
            }
        }
    }
}