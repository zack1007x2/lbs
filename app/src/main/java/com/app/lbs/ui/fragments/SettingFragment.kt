package com.app.lbs.ui.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.lbs.R
import com.app.lbs.databinding.FragmentSettingBinding
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.ui.viewmodels.MainViewModel
import com.app.lbs.utils.Const.DEBUG
import com.app.lbs.utils.Const.ISBANK
import com.app.lbs.utils.Const.ISSETTING
import com.app.lbs.utils.Const.PAYMENTS_ENVIRONMENT
import com.app.lbs.utils.PaymentsUtil
import com.app.lbs.utils.Utils
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.wallet.IsReadyToPayRequest
import com.google.android.gms.wallet.PaymentsClient
import com.google.android.gms.wallet.Wallet
import com.google.android.gms.wallet.Wallet.WalletOptions
import com.kyleduo.switchbutton.SwitchButton


/**
 * @file SettingFragment
 * @brief
 *       使用者設定頁面
 *
 * @author Zack
 * */

class SettingFragment : BaseFragment(), View.OnClickListener {
    private lateinit var mViewModel: MainViewModel

    private lateinit var tv_content_user_name: TextView
    private lateinit var tv_gpay_abailablity:TextView
    private lateinit var sw_notification: SwitchButton
    private lateinit var sv_outer: LinearLayout
    private lateinit var btn_logout: LinearLayout
    private lateinit var btn_phone_veri:LinearLayout
    private lateinit var btn_username:LinearLayout
    private lateinit var tv_content_bind_phone:TextView

    private lateinit var btn_report:Button
    private lateinit var btn_clearcache:Button
    private lateinit var ll_debug_panel:LinearLayout
    private lateinit var ll_title_new_pay_way:LinearLayout
    private lateinit var ll_title_bank_account:LinearLayout
    private lateinit var tv_content_new_pay_way:TextView
    private lateinit var tv_content_bank_account:TextView

    private var mPaymentsClient: PaymentsClient? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mViewModel = activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        val binding: FragmentSettingBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_setting, container, false
        )
        val root = binding.root
        binding.mainViewModel = mViewModel
        Log.d("onCreateView")
        initView(root)
        return root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initView(v: View) {
        tv_content_user_name = v.findViewById(R.id.tv_content_user_name)
        tv_gpay_abailablity = v.findViewById(R.id.tv_gpay_abailablity)
        tv_content_bind_phone = v.findViewById(R.id.tv_content_bind_phone)

        btn_report = v.findViewById(R.id.btn_report)
        btn_clearcache = v.findViewById(R.id.btn_clearcache)
        ll_debug_panel = v.findViewById(R.id.ll_debug_panel)
        ll_debug_panel.visibility = if(DEBUG)View.VISIBLE else View.GONE
        btn_report.setOnClickListener(this)
        btn_clearcache.setOnClickListener(this)

        sw_notification = v.findViewById(R.id.sw_notification)
        sv_outer = v.findViewById(R.id.sv_outer)
        sv_outer.requestFocus()
        btn_logout = v.findViewById(R.id.btn_logout)
        btn_username = v.findViewById(R.id.btn_username)
        btn_phone_veri = v.findViewById(R.id.btn_phone_veri)
        ll_title_new_pay_way = v.findViewById(R.id.ll_title_new_pay_way)
        ll_title_bank_account = v.findViewById(R.id.ll_title_bank_account)


        btn_logout.setOnClickListener(this)
        btn_phone_veri.setOnClickListener(this)
        btn_username.setOnClickListener(this)
        ll_title_new_pay_way.setOnClickListener(this)
        ll_title_bank_account.setOnClickListener(this)


        sw_notification.isChecked = mViewModel.mUser?.userSetting!!.priceIncreaseAlert
        sw_notification.setOnCheckedChangeListener { buttonView, isChecked ->
            mViewModel.mUser?.userSetting?.priceIncreaseAlert = isChecked
            mViewModel.saveUserData()
        }


        mPaymentsClient = createPaymentsClient(requireActivity())
        possiblyShowGooglePayButton()
    }

    companion object {
        const val FRAGMENT_ID = R.id.tab_setting
    }

    override fun onResume() {
        super.onResume()
        Log.d("onResume")
        mViewModel.getUserData().observe(viewLifecycleOwner, Observer { acc ->
            var phone = acc.userData.userMobilephone.toString()
            tv_content_bind_phone.text = if(phone.isEmpty() || phone=="null") "尚未綁定" else phone
        })
        refreshUI()
    }

    override fun onPause() {
        super.onPause()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ll_title_new_pay_way -> {
                val bun = Bundle()
                bun.putBoolean(ISBANK, false)
                (activity as MainActivity).addFragmentWithAnim(
                    SettingListAccountAddFragment.FRAGMENT_ID, R.anim.anim_right_left_enter
                    , R.anim.anim_left_right_leave, bundle=bun)
            }
            R.id.ll_title_bank_account->{
                val bun = Bundle()
                bun.putBoolean(ISBANK, true)
                (activity as MainActivity).addFragmentWithAnim(
                    SettingListAccountAddFragment.FRAGMENT_ID, R.anim.anim_right_left_enter
                    , R.anim.anim_left_right_leave, bundle=bun)
            }

            R.id.btn_logout -> {
                mViewModel.mUser = null
                mViewModel.saveUserData()
                requireActivity().viewModelStore.clear()
                Utils.clearCache(requireActivity(), 2)
            }

            R.id.btn_username -> {
                (activity as MainActivity).addFragmentWithAnim(SettingUserNameFragment.FRAGMENT_ID, R.anim.anim_right_left_enter, R.anim.anim_left_right_leave)
            }

            R.id.btn_phone_veri -> {
                val bun = Bundle()
                bun.putBoolean(ISSETTING, true)
                (activity as MainActivity).addFragmentWithAnim(
                    PhoneVerifyFragment.FRAGMENT_ID, R.anim.anim_right_left_enter
                    , R.anim.anim_left_right_leave, bundle=bun)
            }

            R.id.btn_report->{
                var intent =Utils.mailCrashLogIntent(requireContext())
                if(intent.action!=null)
                    startActivity(intent)
            }

            R.id.btn_clearcache->{
                Utils.clearCache(requireContext(),2)
            }
        }
    }


    fun createPaymentsClient(activity: Activity?): PaymentsClient? {

        //TODO: 測試模式：ENVIRONMENT_TEST / 正式：ENVIRONMENT_PRODUCTION
        val walletOptions =
            WalletOptions.Builder().setEnvironment(PAYMENTS_ENVIRONMENT).build()
        return Wallet.getPaymentsClient(activity, walletOptions)
    }

    private fun possiblyShowGooglePayButton() {

        val isReadyToPayJson = PaymentsUtil.isReadyToPayRequest() ?: return
        val request = IsReadyToPayRequest.fromJson(isReadyToPayJson.toString()) ?: return
        // The call to isReadyToPay is asynchronous and returns a Task. We need to provide an
        // OnCompleteListener to be triggered when the result of the call is known.
        val task = mPaymentsClient!!.isReadyToPay(request)
        task.addOnCompleteListener { completedTask ->
            try {
                completedTask.getResult(ApiException::class.java)?.let(::setGooglePayAvailable)
            } catch (exception: ApiException) {
                // Process error
                Log.w("isReadyToPay failed: $exception")
            }
        }
    }

    private fun setGooglePayAvailable(available: Boolean) {
        if (available) {
            tv_gpay_abailablity.setText(R.string.gpay_available)
        } else {
            tv_gpay_abailablity.setText(R.string.gpay_unavailable)
        }
    }

    override fun refreshUI() {
        super.refreshUI()
        setTitle(R.string.title_setting)
        var phone = mViewModel.mUser?.userData?.userMobilephone.toString()
        tv_content_user_name.text = mViewModel.mUser?.name_admin
        tv_content_bind_phone.text = if(phone.isEmpty() || phone=="null") "尚未綁定" else phone
    }
}