package com.app.lbs.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.app.lbs.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * @file CustomBottomSheetDialogFeedBack
 * @brief
 *       任務評分回饋
 *
 * @author Zack
 * */

class CustomBottomSheetDialogFeedBack : BottomSheetDialogFragment(), View.OnClickListener {
    private var behavior:BottomSheetBehavior<View>?=null
    private lateinit var btn_send_feedback: Button

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = View.inflate(context, R.layout.custom_bottom_sheet_feedback, null)
        dialog.setContentView(view)
        (view.parent as View).setBackgroundColor(ContextCompat.getColor(requireContext(), android.R.color.transparent))
        behavior = BottomSheetBehavior.from(view.parent as View)
        initView(view)
        return dialog
    }

    private fun initView(v: View) {
        btn_send_feedback = v.findViewById(R.id.btn_send_feedback)
        btn_send_feedback.setOnClickListener(this)
    }


    fun show(fragmentActivity: FragmentActivity) {
        show(fragmentActivity.getSupportFragmentManager(), TAG)
    }

    companion object {
        private const val TAG = "MyBottomSheet"
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_send_feedback->{
                //TODO FEEDBACK SENT
                dismiss()
            }
        }
    }
}