package com.app.lbs.ui.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.adapters.NearByLocationAdapter
import com.app.lbs.utils.MyLog
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlin.math.roundToInt

/**
 * @file CustomBottomSheetDialogNearBy
 * @brief
 *       附近地點下方選單
 *
 * @author Zack
 **/

class CustomBottomSheetDialogNearBy : BottomSheetDialogFragment() {
    var behavior:BottomSheetBehavior<View>?=null
    private var Log = MyLog.log()
    private var rv_nearby_list:RecyclerView?=null
    private var loading_flame:RelativeLayout?=null
    private var img_loading:ImageView?=null
    private var isShowing = false

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = View.inflate(context, R.layout.custom_bottom_sheet_nearby_places, null)
        dialog.setContentView(view)
        initView(view)
        isLoading(true)
        return dialog
    }

    private fun initView(view: View) {
        (view.parent as View).setBackgroundColor(ContextCompat.getColor(requireContext(), android.R.color.transparent))
        rv_nearby_list = view.findViewById(R.id.rv_nearby_list)
        rv_nearby_list?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        behavior = BottomSheetBehavior.from(view.parent as View)
        behavior?.peekHeight = resources.getDimension(R.dimen.size_700).roundToInt()

        loading_flame = view.findViewById(R.id.loading_flame)
        img_loading = view.findViewById(R.id.img_loading)
    }

    fun show(fragmentActivity: FragmentActivity) {
        if (isShowing) return
        show(fragmentActivity.supportFragmentManager, TAG)
        isShowing = true
    }

    override fun onDismiss(dialog: DialogInterface) {
        isShowing=false
        super.onDismiss(dialog)
    }

    fun isShowing():Boolean{
        return isShowing
    }

    companion object {
        private const val TAG = "CustomBottomSheetDialogNearBy"
    }

    fun setAdapter(ada:NearByLocationAdapter){
        rv_nearby_list!!.adapter = ada
        rv_nearby_list!!.adapter?.notifyDataSetChanged()
    }
    fun isLoading(isloading:Boolean){
        /*load from internet*/
        if(isloading){
            loading_flame?.visibility = View.VISIBLE
            Glide.with(requireContext()).load("https://www.intogif.com/resource/image/loading/spin.gif").into(img_loading!!)
        }else{
            loading_flame?.visibility = View.GONE
        }
    }

}