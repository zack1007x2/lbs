package com.app.lbs.repository

import android.location.Location
import androidx.annotation.BoolRes
import androidx.lifecycle.MutableLiveData
import com.app.lbs.interfaces.ApiInterface
import com.app.lbs.models.Mission
import com.app.lbs.models.UserAccount
import com.app.lbs.utils.Const
import com.app.lbs.utils.MyLog
import com.app.lbs.utils.QueryBody
import com.app.lbs.utils.Utils
import com.google.gson.JsonElement
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import javax.net.ssl.*

/**
 * @file SysRepository
 * @brief
 *       管理 OAuth相關API接口,System Time , Location相關資訊
 *
 * @author Zack
 * */

class SysRepository {

    private var retrofit: Retrofit
    private var apiService: ApiInterface
    private var Log = MyLog.log()
    private val mLocation = MutableLiveData<Location>()
    private val mSysTime = MutableLiveData<Long>()
    private val alertMission: MutableLiveData<AlertMission> = MutableLiveData()
    private val result_code = MutableLiveData<Int>()
    private val curMemorySize = MutableLiveData<String>()
    private val mUserLiveData = MutableLiveData<UserAccount>()
    private val isRequestLocation = MutableLiveData<Boolean>()
    private val DEBUG = MutableLiveData<Boolean>()
    val mUrl = MutableLiveData<String>()
    val mPrefix = MutableLiveData<String>()

    init {
        retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create()) // 使用 Gson 解析
            .baseUrl(ApiInterface.BASE_SECURE)
//            .client(getUnsafeOkHttpClient())
            .build()
        apiService = retrofit.create(ApiInterface::class.java)
        mSysTime.postValue(Utils.timeStamp.toLong())
        curMemorySize.postValue("0 Bytes")
        isRequestLocation.postValue(false)
        DEBUG.postValue(Const.DEBUG)
    }

    companion object {
        lateinit var INSTANCE: SysRepository
        private val initialized = AtomicBoolean()
        fun getInstance(): SysRepository {

            if (!initialized.getAndSet(true)) {
                INSTANCE = SysRepository()
            }
            return INSTANCE
        }
    }

    fun getLoginUrl():MutableLiveData<String> = mUrl
    fun getPrefix():MutableLiveData<String> = mPrefix

    fun serverOAuth2Login(type: String, param: QueryBody) {
        val call = apiService.getOAuth2LoginUrl(type, param)
        call.enqueue(mApiCallback)
    }

    fun serverSecureOAuth2Login(type: String) {
        val call = apiService.getSecureOAuth2LoginUrl(type)
        call.enqueue(mApiCallback)
    }
    fun serverSMSValidation(param: QueryBody) {
        val call = apiService.getSMSValidation(param)
        call.enqueue(mApiCallback)
    }




    fun loginByOAuth2Token(type: String, param: QueryBody) {
        val apiService = retrofit.create(ApiInterface::class.java)
        val call = apiService.loginByOAuth2Token(type, param)
        call.enqueue(mApiCallback)
    }

    fun serverGetCountryData(param: QueryBody) {
        val call = apiService.getCountryData(param)
        call.enqueue(mApiCallback)
    }

    fun setSysTime(utime: Long) {
        mSysTime.postValue(utime)
    }
    fun setLocation(location: Location) {
        Log.i("setLocation: $location")
        mLocation.postValue(location)
    }

    fun setUserData(mUser:UserAccount){
        mUserLiveData.setValue(mUser)
    }

    fun getSysTime(): MutableLiveData<Long> = mSysTime
    fun getLocation(): MutableLiveData<Location> = mLocation
    fun getCurMemorySize(): MutableLiveData<String> = curMemorySize
    fun getUserData(): MutableLiveData<UserAccount> = mUserLiveData
    fun getIsRequestLocation():MutableLiveData<Boolean> = isRequestLocation
    fun getIsDEBUG():MutableLiveData<Boolean> = DEBUG


    fun setNotiTimer(mission: Mission) {
        Timer().schedule(notiTask(AlertMission(mission, 10)), 600000)//10 min
        Timer().schedule(notiTask(AlertMission(mission, 30)), 1800000)//30min
    }

    private inner class notiTask(private val mission: AlertMission) : TimerTask() {
        override fun run() {
            alertMission.postValue(mission)
        }
    }

    open class AlertMission(mission: Mission, left: Int) {
        var mission: Mission = mission
        var left: Int = left
    }

    fun getAlertMission(): MutableLiveData<AlertMission> = alertMission

    //------------callback------------//
    private var mApiCallback: Callback<JsonElement> = object : Callback<JsonElement> {
        override fun onFailure(call: Call<JsonElement>, t: Throwable) {
            Log.e("onFailure: " + t.message)
        }

        override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
            val func_name = (call.request() as Request).url.encodedPathSegments[1]
            Log.e("$func_name - response body: " + response.body().toString())
            if(response.code()==404 || response.body().toString().isNullOrEmpty() || response.body().toString()=="null")//not found
                return
            val jsonObject = JSONObject(response.body().toString())
            setSysTime(jsonObject.getLong("timestamp"))
            val res_code = jsonObject.getInt("result_code")
            if (res_code != 0) {
                //error handle
                result_code.postValue(res_code)
            } else {
                when (func_name) {
                    "GetCountryData" -> Log.d("GetCountryData")
                    "GetOAuth2LoginUrl"-> mUrl.postValue(response.body()!!.asJsonObject.get("login_url").asString)
                    "login-url"->mUrl.postValue(response.body()!!.asJsonObject.get("login_url").asString)
                    "sms_validation"->mPrefix.postValue(response.body()!!.asJsonObject.get("prefix").asString)
                }
            }
        }
    }

    private fun getUnsafeOkHttpClient(): OkHttpClient {
        // Create a trust manager that does not validate certificate chains
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
            }

            override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
            }

            override fun getAcceptedIssuers() = arrayOf<X509Certificate>()
        })

        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory = sslContext.socketFactory

        return OkHttpClient.Builder()
            .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            .hostnameVerifier(HostnameVerifier { _, _ -> true }).build()
    }

}