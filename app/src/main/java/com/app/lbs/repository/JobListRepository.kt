package com.app.lbs.repository

import androidx.lifecycle.MutableLiveData
import com.app.lbs.interfaces.ApiInterface
import com.app.lbs.models.Mission
import com.app.lbs.models.Store
import com.app.lbs.models.UserAccount
import com.app.lbs.utils.*
import com.app.lbs.utils.Const.FIREBASE_JOBLIST_KEY
import com.app.lbs.utils.Const.FIREBASE_OWNED_JOBLIST_KEY
import com.app.lbs.utils.Const.FIREBASE_TAKEN_JOBLIST_KEY
import com.google.firebase.database.*
import com.google.gson.JsonElement
import okhttp3.Request
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.atomic.AtomicBoolean

/**
 * @file JobListRepository
 * @brief
 *       管理 Jobs, Stores相關API接口以及資訊保存
 *
 * @author Zack
 * */

class JobListRepository(client: UserAccount) {

    private val mSysRepo: SysRepository = SysRepository.getInstance()
    private var retrofit: Retrofit
    private var apiService: ApiInterface
    private var Log = MyLog.log()
    private val mJobList: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mOwnJobListPre: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mOwnJobListOnGoing: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mOwnJobFinList: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mTakenJobListApply: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mTakenJobListOnGoing: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mTakenJobFinList: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mOwnerNotiList: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mTakerNotiList: MutableLiveData<List<Mission>> = MutableLiveData()
    private val mStoreList: MutableLiveData<List<Store>> = MutableLiveData()
    private val mStoreId = MutableLiveData<Int>()
    private val result_code = MutableLiveData<Int>()
    var tmp_mission:Mission
    var userId:Int
    var jsonJobList:String?=null
    var jsonOwnJobList:String?=null
    var jsonOwnFinJobList:String?=null
    var jsonTakenJobList:String?=null
    var jsonTakenFinJobList:String?=null

    private var database = FirebaseDatabase.getInstance()
    private var rootDatabaseRef: DatabaseReference =
        database.reference.child(client?.userData?.userId.toString())


    init {
        retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create()) // 使用 Gson 解析
            .baseUrl(ApiInterface.BASE)
            .build()

        apiService = retrofit.create(ApiInterface::class.java)
        tmp_mission = Mission()
        userId= client!!.userData.userId
    }

    fun initNewMissionObject():Mission{
        tmp_mission = Mission()
        return tmp_mission
    }


    private val mJobListListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {

        }

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val mList = ArrayList<Mission>()
            if (dataSnapshot.exists()) {
                for (item in dataSnapshot.children) {
                    val job:Mission = item.getValue(Mission::class.java)!!
                    mList.add(job)
                }
            }
            if(Const.DEBUG)
                mJobList.postValue(Utils.getFakeJobList(jsonJobList!!))
            else
                mJobList.postValue(mList)
        }
    }

    private val mOwnJobListListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {

        }

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            Log.e("mOwnJobListListener onDataChange ")
            val mListPre = ArrayList<Mission>()
            val mListOngoing = ArrayList<Mission>()
            val mFinList = ArrayList<Mission>()
            val mNotifyList = ArrayList<Mission>()
            if(Const.DEBUG){

                var list = Utils.getFakeJobList(jsonOwnJobList!!)
                for(mission in list){
                    mission.job_owner = client.userData.userId
                    mission.owner_picture = client.userData.userPicture
                    mission.owner_firstname = client.userData.userFirstname
                    mission.owner_lastname = client.userData.userLastname
                    mission.owner_nickname = client.userData.userNickname

                    if(mission.job_status==0){
                        mListPre.add(mission)
                    }else if(mission.job_status==1){
                        mListOngoing.add(mission)
                    }

                    if(mission.job_status==0 && mission.modify_apply!!>=0){//有人申請任務通知
                        mNotifyList.add(mission)
                    }else if(mission.job_status==1 && mission.modify_apply!!>=0){
                        mNotifyList.add(mission)
                    }
                    if(mission.job_status==5 || mission.job_status==3){
                        mNotifyList.add(mission)
                    }
                }
                mOwnJobListPre.postValue(mListPre)
                mOwnJobListOnGoing.postValue(mListOngoing)
                list = Utils.getFakeJobList(jsonOwnFinJobList!!)
                for(mission in list){
                    mission.job_owner = client.userData.userId
                    mission.owner_picture = client.userData.userPicture
                    mission.owner_firstname = client.userData.userFirstname
                    mission.owner_lastname = client.userData.userLastname
                    mission.owner_nickname = client.userData.userNickname
                    if(mission.job_status==4 || mission.job_status==3){
                        mNotifyList.add(mission)
                    }
                }
                mOwnerNotiList.postValue(mNotifyList)
                mOwnJobFinList.postValue(list)
            }else{
                if (dataSnapshot.exists()) {
                    for (item in dataSnapshot.children) {
                        val job = item.getValue(Mission::class.java)
                        if(job!!.job_status==0){
                            mListPre.add(job)
                        }else if(job.job_status==2){//已完成
                            mFinList.add(job)
                        }else{
                            mListOngoing.add(job)
                        }
                    }
                }
                mOwnJobListPre.postValue(mListPre)
                mOwnJobListOnGoing.postValue(mListOngoing)
                mOwnJobFinList.postValue(mFinList)
            }


        }
    }

    private val mTakenJobListListener = object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {

        }

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val mListApply = ArrayList<Mission>()
            val mListOngoing = ArrayList<Mission>()
            val mFinList = ArrayList<Mission>()
            val mNotifyList = ArrayList<Mission>()
            if(Const.DEBUG){

                var list = Utils.getFakeJobList(jsonTakenJobList!!)
                for(mission in list){
                    Log.e(mission)
                    mission.job_taker = client.userData.userId
                    mission.taker_picture = client.userData.userPicture
                    mission.taker_firstname = client.userData.userFirstname
                    mission.taker_lastname = client.userData.userLastname
                    mission.taker_nickname = client.userData.userNickname

                    if(mission.job_status==0 && mission.modify_apply>=0)
                        mListApply.add(mission)
                    else if(mission.job_status==2 && mission.job_taker== client.userData.userId)
                        mFinList.add(mission)
                    else if(mission.job_status==1)
                        mListOngoing.add(mission)

                    if(mission.job_status==0 && mission.modify_apply!!>=0){//有人申請任務通知
                        mNotifyList.add(mission)
                    }else if(mission.job_status==1 && mission.modify_apply!!>=0){
                        mNotifyList.add(mission)
                    }
                    if(mission.job_status==4 || mission.job_status==3){
                        mNotifyList.add(mission)
                    }
                }
                mTakenJobListApply.postValue(mListApply)
                mTakenJobListOnGoing.postValue(mListOngoing)
                list = Utils.getFakeJobList(jsonTakenFinJobList!!)
                for(mission in list){
                    mission.job_taker = client.userData.userId
                    mission.taker_picture = client.userData.userPicture
                    mission.taker_firstname = client.userData.userFirstname
                    mission.taker_lastname = client.userData.userLastname
                    mission.taker_nickname = client.userData.userNickname
                    if(mission.job_status==4 || mission.job_status==3){
                        mNotifyList.add(mission)
                    }
                }
                mTakerNotiList.postValue(mNotifyList)
                mTakenJobFinList.postValue(list)
            }else{
                if (dataSnapshot.exists()) {
                    for (item in dataSnapshot.children) {
                        val job = item.getValue(Mission::class.java)
                        if(job!!.job_status==0)
                            mListApply.add(job)
                        else if(job!!.job_status==1 && job.job_taker == client.userData.userId)//以承接&&taker是自己
                            mListOngoing.add(job)
                        else if(job.job_status>=2 && job.job_taker== client.userData.userId)
                            mFinList.add(job)
                    }
                }
                mTakenJobListApply.postValue(mListApply)
                mTakenJobListOnGoing.postValue(mListOngoing)
                mTakenJobFinList.postValue(mFinList)
            }

        }
    }

    init {
        rootDatabaseRef.child(FIREBASE_JOBLIST_KEY).addValueEventListener(mJobListListener)
        rootDatabaseRef.child(FIREBASE_OWNED_JOBLIST_KEY).addValueEventListener(mOwnJobListListener)
        rootDatabaseRef.child(FIREBASE_TAKEN_JOBLIST_KEY).addValueEventListener(mTakenJobListListener)
    }

    fun refreshJobListsImmediately(){
        rootDatabaseRef.child(FIREBASE_JOBLIST_KEY).addListenerForSingleValueEvent(mJobListListener)
        rootDatabaseRef.child(FIREBASE_OWNED_JOBLIST_KEY).addListenerForSingleValueEvent(mOwnJobListListener)
        rootDatabaseRef.child(FIREBASE_TAKEN_JOBLIST_KEY).addListenerForSingleValueEvent(mTakenJobListListener)
    }

    companion object {
        lateinit var INSTANCE: JobListRepository
        private val initialized = AtomicBoolean()
        val instance: JobListRepository get() = INSTANCE
        fun getInstance(client: UserAccount): JobListRepository {

            if (!initialized.getAndSet(true) || (INSTANCE.userId!= client.userData.userId)) {
                INSTANCE = JobListRepository(client)
            }
            return INSTANCE
        }
    }

    fun serverRenewSubDB(param: QueryBody) {
        val call = apiService.renewSubDB(param)
        call.enqueue(mApiCallback)
    }

    fun serverRetrieveJobList(param: QueryBody){
        val call = apiService.retrieveJobList(param)
        call.enqueue(mApiCallback)
    }

    fun serverCreateNewStore(param: QueryBody){
        val call = apiService.createNewStore(param)
        call.enqueue(mApiCallback)
    }

    fun serverGetStoreList(param: QueryBody){
        val call = apiService.getStoreList(param)
        call.enqueue(mApiCallback)
    }

    fun serverCreateNewJob(param: QueryBody){
        val call = apiService.createNewJob(param)
        call.enqueue(mApiCallback)
    }

    fun serverTakeJob(param: QueryBody){
        val call = apiService.takeJob(param)
        call.enqueue(mApiCallback)
    }

    fun serverFinishJob(param: QueryBody){
        val call = apiService.finishJob(param)
        call.enqueue(mApiCallback)
    }

    fun serverCancelJob(param: QueryBody){
        val call = apiService.cancelJob(param)
        call.enqueue(mApiCallback)
    }

    fun getJobList(): MutableLiveData<List<Mission>> = mJobList
    fun getOwnJobListPre(): MutableLiveData<List<Mission>> = mOwnJobListPre
    fun getOwnJobListOnGoing(): MutableLiveData<List<Mission>> = mOwnJobListOnGoing
    fun getOwnJobFinList(): MutableLiveData<List<Mission>> = mOwnJobFinList
    fun getTakenJobListApply(): MutableLiveData<List<Mission>> = mTakenJobListApply
    fun getTakenJobListOnGoing(): MutableLiveData<List<Mission>> = mTakenJobListOnGoing
    fun getTakenJobFinList(): MutableLiveData<List<Mission>> = mTakenJobFinList
    fun getOwnerNotiList(): MutableLiveData<List<Mission>> = mOwnerNotiList
    fun getTakerNotiList(): MutableLiveData<List<Mission>> = mTakerNotiList
    fun getStoreList(): MutableLiveData<List<Store>> = mStoreList
    fun getStoreId(): MutableLiveData<Int> = mStoreId
    fun getResultCode(): MutableLiveData<Int> = result_code

    //------------callback------------//
    private var mApiCallback: Callback<JsonElement> = object : Callback<JsonElement> {
        override fun onFailure(call: Call<JsonElement>, t: Throwable) {
            Log.e("onFailure: " + t.message)
        }
        override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
            val func_name = (call.request() as Request).url.encodedPathSegments[1]
            Log.e("$func_name - response body: "  + response.body().toString())
            if(response.code()==404 || response.body().toString().isEmpty() || response.body().toString()=="null")//not found
                return
            val jsonObject = JSONObject(response.body().toString())
            mSysRepo.setSysTime(jsonObject.getLong("timestamp"))
            val res_code = jsonObject.getInt("result_code")
            if(res_code!=0){
                //error handle
                result_code.postValue(res_code)
            }else{
                when (func_name) {
                    "GetCountryData" -> Log.d("GetCountryData")
                    "RenewSubDB" -> {}
                    "RetrieveJobList" -> Log.d("RetrieveJobList")
                    "GetStoreList"->{
                        mStoreList.postValue(gson.fromJson(jsonObject.getJSONArray("store_list").toString(), Array<Store>::class.java).toMutableList())
                    }
                    "CreateNewStore"->{
                        if(jsonObject.getInt("result_code")==0){
                            mStoreId.postValue(jsonObject.getInt("store_id"))
                        }
                    }
                    "TakeJob"->{
                        //TODO 留言板變成一對一
                        mSysRepo.setNotiTimer(tmp_mission)
                    }
                    "CancelJob"->{
                        FirebaseDatabase.getInstance().reference.child(client?.userData?.userId.toString())
                            .child("OwnJob").child(tmp_mission.job_id.toString()).removeValue()
                    }
                }
            }
        }
    }

}