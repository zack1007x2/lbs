package com.app.lbs.models


import com.google.gson.annotations.SerializedName

/**
 * @file UserSetting
 * @brief
 *       UserAccount 的衍生結構
 *
 * @author Zack
 * */

data class UserSetting(
    @SerializedName(value ="creditInfo", alternate=["credit_info"])
    var creditInfo: Any?,
    @SerializedName(value ="missionDetectRange", alternate=["mission_detect_range"])
    var missionDetectRange: Double,
    @SerializedName(value ="priceIncreaseAlert", alternate=["price_increase_alert"])
    var priceIncreaseAlert: Boolean
) {
    constructor() : this(null, 5.0, false)
}