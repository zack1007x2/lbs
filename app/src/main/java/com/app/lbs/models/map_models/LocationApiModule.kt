package com.app.lbs.models.map_models

import com.app.lbs.BuildConfig
import com.app.lbs.interfaces.NearByLocationApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @file LocationApiModule
 * @brief
 *       管理retrofit
 *       用於call map api for web用
 *
 * @author Zack
 * */
@Module
class LocationApiModule {

    @Provides
    fun provideClient(): OkHttpClient {
        val clientBuilder = OkHttpClient().newBuilder()
        clientBuilder.addInterceptor {

            val original = it.request()
            val originalHttpUrl = original.url

            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("key", BuildConfig.API_KEY)
                .build()

            val requestBuilder = original.newBuilder()
                .url(url)

            val request = requestBuilder.build()
            it.proceed(request)
        }
        return clientBuilder.build()
    }

    @Provides
    fun provideRetroFit(baseUrl: String, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideApiService(): NearByLocationApi {
        return provideRetroFit(BASE_URL, provideClient()).create(NearByLocationApi::class.java)
    }

    companion object {
        const val BASE_URL = "https://maps.googleapis.com/"
    }
}