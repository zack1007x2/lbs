package com.app.lbs.models.map_models


import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

data class LocationModel(
    @SerializedName("place_id")
    var place_id: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("vicinity")
    var addr: String?,
    @SerializedName("lat")
    var lat: Double?,
    @SerializedName("lng")
    var lng: Double?
) {
    fun getPosition(): LatLng {
        return LatLng(lat!!, lng!!)
    }
}