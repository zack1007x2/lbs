package com.app.lbs.models


import com.google.gson.annotations.SerializedName

/**
 * @file UserData
 * @brief
 *       UserAccount 的衍生結構
 *
 * @author Zack
 * */
data class UserData(
    @SerializedName("business_district_index")
    var businessDistrictIndex: List<Any>,
    @SerializedName("own_jobs")
    var ownJobs: Any?,
    @SerializedName("take_jobs")
    var takeJobs: Any?,
    @SerializedName("user_age")
    var userAge: Any?,
    @SerializedName("user_email")
    var userEmail: String,
    @SerializedName("user_firstname")
    var userFirstname: String,
    @SerializedName("user_gender")
    var userGender: Any?,
    @SerializedName("user_id")
    var userId: Int,
    @SerializedName("user_lastname")
    var userLastname: String,
    @SerializedName("user_mobilephone")
    var userMobilephone: Any?,
    @SerializedName("user_nickname")
    var userNickname: String,
    @SerializedName("user_picture")
    var userPicture: String,
    @SerializedName("verified_by_email")
    var verifiedByEmail: Boolean,
    @SerializedName("verified_by_mobilephone")
    var verifiedByMobilephone: Boolean
){
    constructor():this(ArrayList(), null, null, null, "", "", null, 0,"", null, "","",false, false)
}