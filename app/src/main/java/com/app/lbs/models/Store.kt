package com.app.lbs.models


import com.app.lbs.utils.Utils
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import com.google.maps.android.clustering.ClusterItem


/**
 * @file Store
 * @brief
 *       店家model
 *
 * @author Zack
 * */

data class Store(
    @SerializedName("store_id")
    var store_id: Int,
    @SerializedName("store")
    var store: String,
    @SerializedName("addr")
    var addr: String,
    @SerializedName("tel")
    var tel: String,
    @SerializedName("coor2addr")
    var coor2addr: String,
    @SerializedName("dist")
    var dist: String
): ClusterItem {
    override fun getSnippet(): String? {
        return dist
    }

    override fun getTitle(): String? {
        return store
    }

    override fun getPosition(): LatLng {
        return LatLng(Utils.getLatLng(coor2addr).latitude, Utils.getLatLng(coor2addr).longitude)
    }
}