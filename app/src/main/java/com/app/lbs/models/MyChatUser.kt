package com.app.lbs.models

import com.github.bassaer.chatmessageview.model.IChatUser

/**
 * @file MyChatUser
 * @brief
 *       聊天室user model
 *
 * @author Zack
 * */

class MyChatUser(var mUser:UserAccount):IChatUser {
    override fun getId(): String {
        return mUser.userData.userId.toString()
    }

    override fun getName(): String? {
        return mUser.name_admin
    }

    override fun getIconPath(): String? {
        return mUser.picture
    }

}