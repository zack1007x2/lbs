package com.app.lbs.models

import com.app.lbs.utils.Utils
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import com.google.maps.android.clustering.ClusterItem

/**
 * @file Mission
 * @brief
 *       任務model, SerializedName中的屬性名用於對接API介面
 *
 * @author Zack
 * */

data class Mission(
    @SerializedName("addr2coor")
    var addr2coor: String?,
    @SerializedName("comment")
    var comment: String?,//暫時用於面交地點名稱
    @SerializedName("coor2addr")
    var coor2addr: String,
    @SerializedName("description")
    var description: String,
    @SerializedName("duration")
    var duration: Int,
    @SerializedName("finish_time")
    var finish_time: String?,
    @SerializedName("job_id")
    var job_id: Int,
    @SerializedName("job_owner")
    var job_owner: Int,
    @SerializedName("job_status")
    var job_status: Int,
    @SerializedName("job_taker")
    var job_taker: Any?,
    @SerializedName("job_type")
    var job_type: Int,
    @SerializedName("owner_firstname")
    var owner_firstname: String,
    @SerializedName("owner_lastname")
    var owner_lastname: String,
    @SerializedName("owner_nickname")
    var owner_nickname: String,
    @SerializedName("owner_picture")
    var owner_picture: String,
    @SerializedName("salary")
    var salary: Int,
    @SerializedName("start_time")
    var start_time: String,
    @SerializedName("store_addr")
    var store_addr: String,
    @SerializedName("store_coor2addr")
    var store_coor2addr: String,
    @SerializedName("store_id")
    var store_id: Int,
    @SerializedName("store_name")
    var store_name: String,
    @SerializedName("store_tel")
    var store_tel: String,
    @SerializedName("take_time")//承接任務時間
    var take_time: Any?,
    @SerializedName("taker_firstname")
    var taker_firstname: String?,
    @SerializedName("taker_lastname")
    var taker_lastname: String?,
    @SerializedName("taker_nickname")
    var taker_nickname: String?,
    @SerializedName("taker_picture")
    var taker_picture: String?,
    @SerializedName("title")
    private var title: String,
    @SerializedName("yelp_rank")
    var yelp_rank: String,
    @SerializedName("is_delivery_loc")
    var is_delivery_loc: Boolean,
    @SerializedName("modify_apply")
    var modify_apply: Int=-1,
    var devilery_time:Long,
    var application_type: Int//1:修改2:取消
) : ClusterItem, Comparable<Mission> {
    constructor() : this(
        "", "", "", "", -1, "", -1, -1, -1, -1, -1, "", "", "", ""
        , 0, "", "", "", 0, "", "", "", "", "", "", "", "", "", false, -1,0, -1
    )

    override fun getSnippet(): String? {
        return description
    }

    override fun getTitle(): String {
        return title
    }

    override fun getPosition(): LatLng {
        return LatLng(Utils.getLatLng(coor2addr).latitude, Utils.getLatLng(coor2addr).longitude)
    }

    fun setTitle(str:String){
        title = str
    }

    override fun compareTo(other: Mission): Int {
          return this.title.compareTo(other.title)
    }
}





