package com.app.lbs.models


import com.google.gson.annotations.SerializedName

/**
 * @file UserAccount
 * @brief
 *       根據收到結構衍伸另外兩個models
 *       UserAccount -------UserData
 *                      └---UserSetting(用於存放使用者設定資料)
 *
 * @author Zack
 * */


data class UserAccount(
    @SerializedName(value = "accessToken", alternate = ["access_token"])//可兼容"accessToken", "access_token"兩種properties
    var accessToken: String,//並賦值給accessToken
    @SerializedName(value = "apiToken", alternate = ["api_token"])
    var apiToken: String,
    @SerializedName("email")
    var email: String,
    @SerializedName(value = "familyName", alternate = ["family_name"])
    var familyName: String,
    @SerializedName(value = "givenName", alternate = ["given_name"])
    var givenName: String,
    @SerializedName("id")
    var id_admin: String,
    @SerializedName("locale")
    var locale: String,
    @SerializedName("name")
    var name_admin: String,
    @SerializedName("picture")
    var picture: String,
    @SerializedName(value = "refreshToken", alternate = ["refresh_token"])
    var refreshToken: String,
    @SerializedName(value = "resultCode", alternate = ["result_code"])
    var resultCode: Int,
    @SerializedName(value = "userData", alternate = ["user_data"])
    var userData: UserData,
    @SerializedName(value = "verifiedEmail", alternate = ["verified_email"])
    var verifiedEmail: Boolean,
    @SerializedName(value = "userSetting", alternate = ["user_setting"])
    var userSetting: UserSetting?=UserSetting(null, 5.0, false),
    var isOwner:Boolean?=false
) {
    constructor() : this("","","","","","","","","","",0,UserData(),false,UserSetting(null, 5.0, false))
}