package com.app.lbs.models

/**
 * @file ImageModel
 * @brief
 *       管理橫向image array的model
 *
 * @author Zack
 * */
class ImageModel(private var id: Int,private var imagePath: String?) {
    constructor() : this(0, null)

    fun getId(): Int {
        return id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getImagePath(): String? {
        return imagePath
    }

    fun setImagePath(imagePath: String) {
        this.imagePath = imagePath
    }
}



