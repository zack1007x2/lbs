package com.app.lbs.service

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.app.lbs.utils.Const


/**
 * @file SessionManager
 * @brief
 *      備用 : API29不支援以前check service running的方式
 *      未來有需要再實裝
 */
class SessionManager(var context: Context) {
    private val loginpreferences: SharedPreferences
    private val logineditor: SharedPreferences.Editor

    init {
        loginpreferences = context.getSharedPreferences(Const.APPNAME, MODE_PRIVATE)
        logineditor = loginpreferences.edit()
    }

    var isRunning: Boolean
        get() = loginpreferences.getBoolean(SERVICES, false)
        set(value) {
            logineditor.putBoolean(SERVICES, value)
            logineditor.commit()
        }

    companion object {
        private val SERVICES = "service"
    }

}