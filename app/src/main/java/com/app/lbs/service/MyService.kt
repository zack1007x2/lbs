package com.app.lbs.service

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.location.Location
import android.os.*
import androidx.core.app.NotificationCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.app.lbs.R
import com.app.lbs.models.UserAccount
import com.app.lbs.repository.JobListRepository
import com.app.lbs.repository.SysRepository
import com.app.lbs.ui.activities.MainActivity
import com.app.lbs.utils.Const
import com.app.lbs.utils.MyLog
import com.app.lbs.utils.Utils
import com.app.lbs.utils.gson
import com.google.android.gms.location.*

/**
 * @file MyService
 * @brief
 *       目前主要功能為監聽location資訊
 *       確保能在背景執行
 *
 * @author Zack
 * */

class MyService : LifecycleService() {
    private val mBinder: IBinder = LocalBinder()
    val Log = MyLog.log()

    /**
     * Used to check whether the bound activity has really gone away and not unbound as parrt of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private var mChangingConfiguration = false
    private var mNotificationManager: NotificationManager? = null

    private var mLocationRequest: LocationRequest? = null

    /**
     * Provides access to the Fused Location Provider API.
     */
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    /**
     * Callback for changes in location.
     */
    private var mLocationCallback: LocationCallback? = null
    private var mServiceHandler: Handler? = null
    lateinit var mSysRepo: SysRepository
    lateinit var mJobListRepository: JobListRepository

    /**
     * The current location.
     */
    private var mLocation: Location? = null

    @SuppressLint("MissingPermission")

    override fun onCreate() {
        super.onCreate()
        mSysRepo = SysRepository.getInstance()
        val userData: String? =
            application.getSharedPreferences(Const.APPNAME, Context.MODE_PRIVATE)
                .getString(Const.PREF_USER_KEY, null)
        val user = gson.fromJson(userData, UserAccount::class.java)
        mJobListRepository = JobListRepository.getInstance(user)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mSysRepo.getIsDEBUG().observe(this, Observer { DEBUG ->
            mFusedLocationClient!!.setMockMode(DEBUG)
            try {
                Log.d("DEBUG : $DEBUG")
                if (DEBUG) {
                    if(mSysRepo.getIsRequestLocation().value!!) {
                        removeLocationUpdates()
                    }
                    mFusedLocationClient!!.setMockLocation(Utils.getMockLocation())
                        .addOnSuccessListener { Log.d("location mocked") }
                        .addOnFailureListener { e->
                            Log.e("mock failed : $e")
                        }
                } else {

                    mFusedLocationClient!!.requestLocationUpdates(
                        mLocationRequest,
                        mLocationCallback, Looper.myLooper()
                    )
                }
            } catch (unlikely: SecurityException) {
                mSysRepo.getIsRequestLocation().postValue(false)
                Log.e("Lost location permission. Could not request updates. $unlikely")
            }

        })

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                if (Const.DEBUG) {
                    removeLocationUpdates()
                    mFusedLocationClient!!.setMockMode(Const.DEBUG)
                    mFusedLocationClient!!.setMockLocation(Utils.getMockLocation())
                        .addOnSuccessListener { Log.d("location mocked") }
                        .addOnFailureListener { e->
                            Log.e("mock failed : $e")
                        }
                }
                onNewLocation(locationResult.lastLocation)
                mSysRepo.getLocation().postValue(locationResult.lastLocation)
            }
        }
        createLocationRequest()
        getLastLocation()
        val handlerThread = HandlerThread(TAG)
        handlerThread.start()
        mServiceHandler = Handler(handlerThread.looper)
        mNotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = getString(R.string.app_name)
            // Create the channel for the notification
            val mChannel = NotificationChannel(
                CHANNEL_ID,
                name,
                NotificationManager.IMPORTANCE_DEFAULT
            )

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager!!.createNotificationChannel(mChannel)
        }
    }

    override fun getLifecycle(): Lifecycle {
        return super.getLifecycle()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        Log.i("Service started")
        val startedFromNotification = intent?.getBooleanExtra(
            EXTRA_STARTED_FROM_NOTIFICATION,
            false
        )

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification!!) {
            removeLocationUpdates()
            stopSelf()
        }
        mSysTimeUpdate.run()

        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mChangingConfiguration = true
    }

    override fun onBind(intent: Intent): IBinder? {
        super.onBind(intent)
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i("in onBind()")
        stopForeground(true)
        mChangingConfiguration = false
        return mBinder
    }

    override fun onRebind(intent: Intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i("in onRebind()")
        stopForeground(true)
        mChangingConfiguration = false
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent): Boolean {
        Log.i("Last client unbound from service")

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && mSysRepo.getIsRequestLocation().value!!) {
            Log.i("Starting foreground service")
            startForeground(NOTIFICATION_ID, getNotification())
        }
        return true // Ensures onRebind() is called when a client re-binds.
    }

    override fun onDestroy() {
        super.onDestroy()
        mServiceHandler!!.removeCallbacksAndMessages(null)
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun requestLocationUpdates() {
        Log.i("Requesting location updates")
        mSysRepo.getIsRequestLocation().postValue(true)
        startService(Intent(applicationContext, MyService::class.java))
        try {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest,
                mLocationCallback, Looper.myLooper()
            )
        } catch (unlikely: SecurityException) {
            mSysRepo.getIsRequestLocation().postValue(false)
            Log.e("Lost location permission. Could not request updates. $unlikely")
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun removeLocationUpdates() {
        Log.i("Removing location updates")
        try {
            mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
            mSysRepo.getIsRequestLocation().postValue(false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            mSysRepo.getIsRequestLocation().postValue(true)
            Log.e("Lost location permission. Could not remove updates. $unlikely")
        }
    }
    /**
     * Channel ID Extra to help us figure out if we arrived in onStartCommand via the notification or not.
    The PendingIntent that leads to a call to onStartCommand() in this service.
    The PendingIntent to launch activity.
    Set the Channel ID for Android O.
     */


    /**
     * Returns the [NotificationCompat] used as parrt of the foreground service.
     */
    private fun getNotification(): Notification {
        val intent = Intent(this, MyService::class.java)
        val text: CharSequence = Utils.getLocationText(mLocation)!!

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true)

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        val servicePendingIntent = PendingIntent.getService(
            this, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        // The PendingIntent to launch activity.
        val activityPendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, MainActivity::class.java), 0
        )
        val builder =
            NotificationCompat.Builder(this)
                .addAction(
                    R.drawable.ic_launch, getString(R.string.launch_activity),
                    activityPendingIntent
                )
                .addAction(
                    R.drawable.ic_cancel, getString(R.string.remove_location_updates),
                    servicePendingIntent
                )
                .setContentText(text)
                .setContentTitle("MyService")
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(android.R.drawable.arrow_up_float)
                .setTicker(text)
                .setOnlyAlertOnce(true)
                .setWhen(System.currentTimeMillis())
                .setNotificationSilent()
                .setVibrate(LongArray(0))

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID) // Channel ID
        }
        return builder.build()
    }


    private fun getLastLocation() {
        try {
            mFusedLocationClient!!.lastLocation
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        mLocation = task.result
                    } else {
                        Log.w(
                            "Failed to get location."
                        )
                    }
                }
        } catch (unlikely: SecurityException) {
            Log.e(
                "Lost location permission.$unlikely"
            )
        }
    }

    private fun onNewLocation(location: Location) {
        Log.i("New location: $location")
        mLocation = location

        // Notify anyone listening for broadcasts about the new location.
        val intent = Intent(ACTION_BROADCAST)
        intent.putExtra(EXTRA_LOCATION, location)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)

        // Update notification content if running as a foreground service.
        if (serviceIsRunningInForeground(this)) {
            mNotificationManager!!.notify(
                NOTIFICATION_ID,
                getNotification()
            )
        }
    }

    /**
     * Sets the location request parameters.
     */
    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest!!.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        val service: MyService
            get() = this@MyService
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The [Context].
     */
    fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(
            Context.ACTIVITY_SERVICE
        ) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (javaClass.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }

    companion object {
        private const val PACKAGE_NAME =
            "com.app.lbs"
        private val TAG = MyService::class.java.simpleName

        /**
         * The name of the channel for notifications.
         */
        private const val CHANNEL_ID = "channel_01"
        const val ACTION_BROADCAST =
            "$PACKAGE_NAME.broadcast"
        const val EXTRA_LOCATION =
            "$PACKAGE_NAME.location"
        private const val EXTRA_STARTED_FROM_NOTIFICATION =
            PACKAGE_NAME +
                    ".started_from_notification"

        /**
         * The desired interval for location updates. Inexact. Updates may be more or less frequent.
         */
        private const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000

        /**
         * The fastest rate for active location updates. Updates will never be more frequent
         * than this value.
         */
        private const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2

        /**
         * The identifier for the notification displayed for the foreground service.
         */
        private const val NOTIFICATION_ID = 12345678
    }

    private val mSysTimeUpdate = object : Runnable {
        override fun run() {
            mSysRepo.getSysTime().postValue(mSysRepo.getSysTime().value?.plus(1000L))
            mServiceHandler?.postDelayed(this, 1000)
        }
    }
}