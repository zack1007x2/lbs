package com.app.lbs.utils.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.*
import android.graphics.Paint.Align
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.view.View
import com.app.lbs.R
import com.app.lbs.utils.Utils


/**
 * @author Diogo Bernardino
 * @email mail@diogobernardino.com
 * @date 04/2014
 */
class CircularCounter @SuppressLint("Recycle") constructor(context: Context, attrs: AttributeSet?) :
    View(context, attrs) {
    /**
     * Default background
     */
    private var mBackgroundCenter = 0
    private var mBackgroundRadius = 0

    /**
     * Current degrees
     */
    private var mOneDegrees = 0
    private var mTwoDegrees = 0
    private var mThreeDegrees = 0

    /**
     * Current real value
     */
    private var mOneValue = 0

    /**
     * Range of view
     */
    private var mRange = 0

    /**
     * Thickness of flows
     */
    private var mOneWidth = 0f
    private var mTwoWidth = 0f
    private var mThreeWidth = 0f

    /**
     * Size of text
     */
    private var mTextSize = 0f
    private var mMetricSize = 0f

    /**
     * Color of bars
     */
    private var mOneColor = 0
    private var mTwoColor = 0
    private var mThreeColor = 0

    /**
     * Color of text
     */
    private var mTextColor = -1
    private var mBackgroundColor = 0

    /**
     * Paint objects
     */
    private var mOnePaint: Paint? = null
    private var mTwoPaint: Paint? = null
    private var mThreePaint: Paint? = null
    private var mBackgroundPaint: Paint? = null
    private var mTextPaint: Paint? = null
    private var mMetricPaint: Paint? = null

    /**
     * Bounds of each flow
     */
    private var mOneBounds: RectF? = null
    private var mTwoBounds: RectF? = null
    private var mThreeBounds: RectF? = null

    /**
     * Text position
     */
    private var mTextPosY = 0f
    private var mMetricPosY = 0f
    private var mMetricPaddingY = 0f

    /**
     * Metric in use
     */
    private var mMetricText: String? = null

    /**
     * Typeface of text
     */
    private var mTypeface: Typeface? = null

    /**
     * Handler to update the view
     */
    private var mSpinHandler: SpeedHandler? = null

    /**
     * Setting up variables on attach
     */
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        mSpinHandler = SpeedHandler(this)
        setupBounds()
        setupPaints()
        setupTextPosition()
    }

    /**
     * Free variables on detached
     */
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mSpinHandler = null
        mOnePaint = null
        mOneBounds = null
        mTwoPaint = null
        mTwoBounds = null
        mBackgroundPaint = null
        mTextPaint = null
        mMetricPaint = null
    }

    /**
     * Set up paint variables to be used in onDraw method
     */
    private fun setupPaints() {
        mOnePaint = Paint()
        mOnePaint!!.color = mOneColor
        mOnePaint!!.isAntiAlias = true
        mOnePaint!!.style = Paint.Style.STROKE
        mOnePaint!!.strokeWidth = mOneWidth
        mTwoPaint = Paint()
        mTwoPaint!!.color = mTwoColor
        mTwoPaint!!.isAntiAlias = true
        mTwoPaint!!.style = Paint.Style.STROKE
        mTwoPaint!!.strokeWidth = mTwoWidth
        mThreePaint = Paint()
        mThreePaint!!.color = mThreeColor
        mThreePaint!!.isAntiAlias = true
        mThreePaint!!.style = Paint.Style.STROKE
        mThreePaint!!.strokeWidth = mThreeWidth
        mBackgroundPaint = Paint()
        mBackgroundPaint!!.color = mBackgroundColor
        mBackgroundPaint!!.isAntiAlias = true
        mBackgroundPaint!!.style = Paint.Style.FILL
        mTextPaint = Paint()
        mTextPaint!!.color = mTextColor
        mTextPaint!!.style = Paint.Style.FILL
        mTextPaint!!.isAntiAlias = true
        mTextPaint!!.textSize = mTextSize
        mTextPaint!!.typeface = mTypeface
        mTextPaint!!.textAlign = Align.CENTER
        mMetricPaint = Paint()
        mMetricPaint!!.color = mTextColor
        mMetricPaint!!.style = Paint.Style.FILL
        mMetricPaint!!.isAntiAlias = true
        mMetricPaint!!.textSize = mMetricSize
        mMetricPaint!!.typeface = mTypeface
        mMetricPaint!!.textAlign = Align.CENTER
    }

    /**
     * Set the bounds of the bars.
     */
    private fun setupBounds() {
        mBackgroundCenter = this.layoutParams.width / 2
        mBackgroundRadius = mBackgroundCenter - this.paddingTop
        mOneBounds = RectF(
            this.paddingTop + mOneWidth / 2,
            this.paddingLeft + mOneWidth / 2,
            this.layoutParams.width - this.paddingRight
                    - mOneWidth / 2, (this.layoutParams.height
                    - this.paddingBottom - (mOneWidth / 2))
        )
        mTwoBounds = RectF(
            this.paddingTop + (mTwoWidth / 2) + mOneWidth,
            this.paddingLeft + (mTwoWidth / 2) + mOneWidth,
            (this.layoutParams.width - this.paddingRight
                    - (mTwoWidth / 2) - mOneWidth),
            (this.layoutParams.height - this.paddingBottom
                    - (mTwoWidth / 2) - mOneWidth)
        )
        mThreeBounds = RectF(
            this.paddingTop + (mThreeWidth / 2
                    ) + mTwoWidth + mOneWidth, this.paddingLeft + ((mThreeWidth
                    / 2)) + mTwoWidth + mOneWidth, (this.layoutParams.width
                    - this.paddingRight - (mThreeWidth / 2) - mTwoWidth
                    - mOneWidth), (this.layoutParams.height
                    - this.paddingBottom - (mThreeWidth / 2) - mTwoWidth
                    - mOneWidth)
        )
    }

    /**
     * Setting up text position
     */
    private fun setupTextPosition() {
        val textBounds = Rect()
        mTextPaint!!.getTextBounds("1", 0, 1, textBounds)
        mTextPosY = mOneBounds!!.centerY() + (textBounds.height() / 2f)
        mMetricPosY = mTextPosY + mMetricPaddingY
    }

    /**
     * Parse the attributes passed to the view and default values.
     */
    private fun init(a: TypedArray) {
        mTextSize = a.getDimension(
            R.styleable.CircularMeter_textSize,
            resources.getDimension(R.dimen.size_20)
        )
        mTextColor = a
            .getColor(R.styleable.CircularMeter_textColor, mTextColor)
        mMetricSize = a.getDimension(
            R.styleable.CircularMeter_metricSize,
            resources.getDimension(R.dimen.size_14)
        )
        mMetricText = a.getString(R.styleable.CircularMeter_metricText)
        mMetricPaddingY = resources.getDimension(R.dimen.size_10)
        mRange = a.getInt(R.styleable.CircularMeter_range, 100)
        mOneWidth = resources.getDimension(R.dimen.size_1)
        mTwoWidth = resources.getDimension(R.dimen.size_1)
        mThreeWidth = resources.getDimension(R.dimen.size_1)
        mOneColor = -1213350
        mTwoColor = -7747644
        mThreeColor = -1
        mOneDegrees = 0
        mTwoDegrees = 0
        mThreeDegrees = 0
        val aux = a.getString(R.styleable.CircularMeter_typeface)
        if (aux != null) mTypeface = Typeface.createFromAsset(
            this.resources
                .assets, aux
        )
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawCircle(
            mBackgroundCenter.toFloat(), mBackgroundCenter.toFloat(),
            mBackgroundRadius.toFloat(), (mBackgroundPaint)!!
        )
        canvas.drawArc(
            (mOneBounds)!!, START_DEGREES, -mOneDegrees.toFloat(), false,
            (mOnePaint)!!
        )
        canvas.drawArc(
            (mTwoBounds)!!, START_DEGREES, -mTwoDegrees.toFloat(), false,
            (mTwoPaint)!!
        )
        canvas.drawArc(
            (mThreeBounds)!!, START_DEGREES, -mThreeDegrees.toFloat(), false,
            (mThreePaint)!!
        )
        canvas.drawText(
            Utils.getSecondsToHMmSs(mOneValue.toLong()/1000)!!, mOneBounds!!.centerX(),
            mTextPosY, (mTextPaint)!!
        )
//        canvas.drawText(
//            (mMetricText)!!, mOneBounds!!.centerX(), mMetricPosY,
//            (mMetricPaint)!!
//        )
    }
    /*
	 * Setters
	 *
	 */
    /**
     * Set the next values to be drawn
     * @param v1
     * @param v2
     * @param v3
     */
    fun setValues(v1: Int, v2: Int, v3: Int) {
        if (v1 <= mRange) mOneDegrees = Math.round((v1.toFloat() * 360) / mRange) else mOneDegrees =
            360
        if (v2 <= mRange) mTwoDegrees = Math.round((v2.toFloat() * 360) / mRange) else mTwoDegrees =
            360
        if (v3 <= mRange) mThreeDegrees =
            Math.round((v3.toFloat() * 360) / mRange) else mThreeDegrees = 360
        mOneValue = v1
        mSpinHandler?.sendEmptyMessage(0)
    }

    fun setRange(range: Int): CircularCounter {
        mRange = range
        return this
    }
    fun setFirstWidth(width: Float): CircularCounter {
        mOneWidth = width
        return this
    }

    fun setSecondWidth(width: Float): CircularCounter {
        mTwoWidth = width
        return this
    }

    fun setThirdWidth(width: Float): CircularCounter {
        mThreeWidth = width
        return this
    }

    fun setTextSize(size: Float): CircularCounter {
        mTextSize = size
        return this
    }

    fun setMetricSize(size: Float): CircularCounter {
        mMetricSize = size
        return this
    }

    fun setFirstColor(color: Int): CircularCounter {
        mOneColor = color
        return this
    }

    fun setSecondColor(color: Int): CircularCounter {
        mTwoColor = color
        return this
    }

    fun setThirdColor(color: Int): CircularCounter {
        mThreeColor = color
        return this
    }

    fun setTextColor(color: Int): CircularCounter {
        mTextColor = color
        return this
    }

    fun setMetricText(text: String?): CircularCounter {
        mMetricText = text
        return this
    }

    override fun setBackgroundColor(color: Int) {
        mBackgroundColor = color
    }

    fun setTypeface(typeface: Typeface?): CircularCounter {
        mTypeface = typeface
        return this
    }

    /**
     * Handles display invalidates
     */
    private class SpeedHandler(private val act: CircularCounter) : Handler() {
        override fun handleMessage(msg: Message) {
            act.invalidate()
            super.handleMessage(msg)
        }
    }

    companion object {
        /**
         * View starts at 6 o'clock
         */
        private val START_DEGREES = 270f
    }

    init {
        init(context.obtainStyledAttributes(attrs, R.styleable.CircularMeter))
    }
}