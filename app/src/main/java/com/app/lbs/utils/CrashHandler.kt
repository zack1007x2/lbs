package com.app.lbs.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Log
import java.io.*
import java.lang.Thread.UncaughtExceptionHandler
import java.text.SimpleDateFormat
import java.util.*

/**
 * @file CrashHandler
 * @brief
 *       crash發生時紀錄 error log 並儲存
 *
 * @author Zack
 *
 * */
class CrashHandler private constructor()// 保證只產生一個實體
    : UncaughtExceptionHandler, Runnable {
    private var mContext: Context? = null
    private val info = HashMap<String, String>()// 用來存儲設備信息和異常信息
    /**系統預設的Handler，當自製Handler無法正常使用時，使用預設Handler */
    private var mDefaultHandler: UncaughtExceptionHandler? = null
    private val format = SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.getDefault())
    private var errThread: Thread? = null
    private var errorInfoToFile: Throwable? = null

    fun init(context: Context) {
        this.mContext = context
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler(this)
        //        scanner = new MediaScannerConnection(mContext, this);
        //        scanner.connect();
    }

    override fun uncaughtException(thread: Thread, ex: Throwable) {
        this.errThread = thread
        if (!handleException(ex) && mDefaultHandler != null)
            mDefaultHandler!!.uncaughtException(thread, ex)
    }

    /**
     * @hide
     * 收集設備參數信息
     * @param context
     */
    internal fun collectDeviceInfo(context: Context) {
        try {
            val pm = context.packageManager// 獲得包管理器
            val pi = pm.getPackageInfo(
                context.packageName,
                PackageManager.GET_ACTIVITIES
            )// 得到該應用的信息，即主Activity
            if (pi != null) {
                val versionName = if (pi.versionName == null)
                    "null"
                else
                    pi.versionName
                val versionCode = pi.versionCode.toString() + ""
                info["versionName"] = versionName
                info["versionCode"] = versionCode
            }
        } catch (e: NameNotFoundException) {
            e.printStackTrace()
        }

        val fields = Build::class.java.declaredFields// 反射機制
        for (field in fields) {
            try {
                field.isAccessible = true
                info[field.name] = field.get("")!!.toString()
                Log.d("TAG", field.name + ":" + field.get(""))
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            }

        }
    }

    /**
     *
     * @param ex
     * @return 是否成功處理錯誤訊息
     */
    private fun handleException(ex: Throwable?): Boolean {
        if (ex == null)
            return false

        this.errorInfoToFile = ex
        Thread(this).start()

        return true
    }

    /**
     * 保存訊息至檔案
     * @param ex
     */
    private fun saveCrashInfoToFile(thread: Thread?, ex: Throwable) {
        val sb = StringBuffer()
        val writer = StringWriter()
        val pw = PrintWriter(writer)
        ex.printStackTrace(pw)
        var cause: Throwable? = ex.cause
        // 循環著把所有的異常信息寫入writer中
        while (cause != null) {
            cause.printStackTrace(pw)
            cause = cause.cause
        }
        pw.close()
        val result = writer.toString()
        sb.append(result)
        // 保存文件
        val timetamp = System.currentTimeMillis()
        val time = format.format(Date())
        val fileName = "crash-$time.txt"
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            try {
                val dir = File(mContext!!.getExternalFilesDir(null), "crash")
                if (!dir.exists())
                    dir.mkdir()
                else
                    Log.d("TAG", "folder exist")

                val file = File(dir, fileName)

                if (!file.exists())
                    file.createNewFile()

                val fos = FileOutputStream(file)

                val str = sb.toString()

                fos.write(str.replace("\t".toRegex(), "\r\n").toByteArray())
                fos.close()

                if (file.exists() && file.isFile)
                // 送出廣播讓系統處理，千萬別用TMD MediaScannerClient，因為接下來程式就要當掉了
                    mContext!!.sendBroadcast(
                        Intent(
                            Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                            Uri.fromFile(file)
                        )
                    )
            } catch (e: FileNotFoundException) {
                Log.e("TAG", e.message)
            } catch (e: IOException) {
                Log.e("TAG", e.message)
            }

            mDefaultHandler!!.uncaughtException(thread!!, ex)
        }
    }

    override fun run() {
        saveCrashInfoToFile(this.errThread, this.errorInfoToFile!!)
    }

    companion object {
        private var INSTANCE: CrashHandler? = CrashHandler()

        /**
         * 取得[CrashHandler]實體
         * @return
         */
        val instance: CrashHandler
            get() {
                if (INSTANCE == null)
                    INSTANCE = CrashHandler()
                return INSTANCE as CrashHandler
            }
    }
}