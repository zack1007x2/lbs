package com.app.lbs.utils

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken



/** @file QueryBody
 *  @brief
 *        用於產生web api所需參數的model
 *        變數涵蓋所有api所需的parameters
 *        目的是以物件的形式來傳遞參數
 *        並提供轉換功能 object和hashmap可互相轉換
 *
 * @author Zack
 * @date 20200510 */


data class QueryBody (
    @SerializedName("timestamp")
    var timestamp: String?,
    @SerializedName("user_id")
    var userId: String?,
    @SerializedName("api_token")
    var apiToken: String?,
    @SerializedName("mac")
    var mac: String?,
    @SerializedName("access_token")
    var accessToken: String?,
    @SerializedName("country_id")
    var countryId: String?,
    @SerializedName("city_id")
    var cityId: String?,
    @SerializedName("dist_index")
    var distIndex: String?,
    @SerializedName("longi")
    var longi: String?,
    @SerializedName("lati")
    var lati: String?,
    @SerializedName("radius")
    var radius: String?,
    @SerializedName("title")
    var title: String?,
    @SerializedName("job_type")
    var jobType: Int?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("start_time")
    var startTime: String?,
    @SerializedName("duration")
    var duration: Int?,
    @SerializedName("salary")
    var salary: Int?,
    @SerializedName("store_id")
    var storeId: Int?,
    @SerializedName("job_id")
    var jobId: Int?,
    @SerializedName("limit")
    var limit: String?,
    @SerializedName("store")
    var store: String?,
    @SerializedName("addr")
    var addr: String?,
    @SerializedName("tel")
    var tel: String?,
    @SerializedName("comment")
    var comment: String?,
    @SerializedName("ignore")
    var ignore: String?,
    @SerializedName("mobile")
    var mobile: String?
) {
    constructor() : this(null,null,null,null,null,null,null,null,null,null,null,
        null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
}

val gson = Gson()

//convert a data class to a map
fun <QueryBody> QueryBody.serializeToMap(): HashMap<String, String> {
    return convert()
}

//convert a map to a data class
inline fun <reified QueryBody> HashMap<String, String>.toDataClass(): QueryBody {
    return convert()
}

//convert an object of type I to type O
inline fun <I, reified O> I.convert(): O {
    val json = gson.toJson(this)
    return gson.fromJson(json, object : TypeToken<O>() {}.type)
}


