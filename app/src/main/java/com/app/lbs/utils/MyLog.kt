package com.app.lbs.utils

import android.util.Log
import java.util.*

/**
 * 日誌工具
 * 使用前需先宣告
 * MyLog Log = MyLog.log();
 * 使用方式如下
 * Log.e(str);
 */
class MyLog private constructor(private val mClassName: String) {

    private var logFlag = Const.DEBUG
    /**
     * Get The Current Function Name
     *
     * @return
     */
    private val functionName: String?
        get() {
            val sts = Thread.currentThread().stackTrace ?: return null
            for (st in sts) {
                if (st.isNativeMethod) {
                    continue
                }
                if (st.className == Thread::class.java.name) {
                    continue
                }
                if (st.className == this.javaClass.name) {
                    continue
                }
                return (mClassName + "[ " + Thread.currentThread().name + ": "
                        + st.fileName + ":" + st.lineNumber + " "
                        + st.methodName + " ]")
            }
            return null
        }

    /**
     * The Log Level:i
     *
     * @param str
     */
    fun i(str: Any) {
        if (logFlag) {
            if (logLevel <= Log.INFO) {
                val name = functionName
                if (name != null) {
                    Log.i(tag, "$name - $str")
                } else {
                    Log.i(tag, str.toString())
                }
            }
        }

    }

    /**
     * The Log Level:d
     *
     * @param str
     */
    fun d(str: Any) {
        if (logFlag) {
            if (logLevel <= Log.DEBUG) {
                val name = functionName
                if (name != null) {
                    Log.d(tag, "$name - $str")
                } else {
                    Log.d(tag, str.toString())
                }
            }
        }
    }

    /**
     * The Log Level:V
     *
     * @param str
     */
    fun v(str: Any) {
        if (logFlag) {
            if (logLevel <= Log.VERBOSE) {
                val name = functionName
                if (name != null) {
                    Log.v(tag, "$name - $str")
                } else {
                    Log.v(tag, str.toString())
                }
            }
        }
    }

    /**
     * The Log Level:w
     *
     * @param str
     */
    fun w(str: Any) {
        if (logFlag) {
            if (logLevel <= Log.WARN) {
                val name = functionName
                if (name != null) {
                    Log.w(tag, "$name - $str")
                } else {
                    Log.w(tag, str.toString())
                }
            }
        }
    }

    /**
     * The Log Level:e
     *
     * @param str
     */
    fun e(str: Any) {
        if (logFlag) {
            if (logLevel <= Log.ERROR) {
                val name = functionName
                if (name != null) {
                    Log.e(tag, "$name - $str")
                } else {
                    Log.e(tag, str.toString())
                }
            }
        }
    }

    /**
     * The Log Level:e
     *
     * @param ex
     */
    fun e(ex: Exception) {
        if (logFlag) {
            if (logLevel <= Log.ERROR) {
                Log.e(tag, "error", ex)
            }
        }
    }

    /**
     * The Log Level:e
     *
     * @param log
     * @param tr
     */
    fun e(log: String, tr: Throwable) {
        if (logFlag) {
            val line = functionName
            Log.e(
                tag, "{Thread:" + Thread.currentThread().name + "}"
                        + "[" + mClassName + line + ":] " + log + "\n", tr
            )
        }
    }

    /**
     * The Log Level:d
     *
     * @param str
     */
    fun wtf(str: Any) {
        if (logFlag) {
            if (logLevel <= Log.ASSERT) {
                val name = functionName
                if (name != null) {
                    Log.wtf(tag, "$name - $str")
                } else {
                    Log.wtf(tag, str.toString())
                }
            }
        }
    }

    companion object {

        val tag = "AppName"
        private var logLevel = Log.VERBOSE

        private val sLoggerTable = Hashtable<String, MyLog>()
        private var log: MyLog? = null
        private val ZACK = "@Zack@ "

        /**
         *
         * @param className
         * @return
         */
        private fun getLogger(className: String): MyLog {
            var classLogger = sLoggerTable[className]
            if (classLogger == null) {
                classLogger = MyLog(className)
                sLoggerTable[className] = classLogger
            }
            return classLogger
        }

        /**
         * Purpose:Mark user one
         *
         * @return
         */
        @JvmStatic
        fun log(): MyLog {
            if (log == null) {
                log =
                    MyLog(ZACK)
            }
            return log as MyLog
        }

    }
}
