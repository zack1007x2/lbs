package com.app.lbs.utils

import android.Manifest
import com.google.android.gms.wallet.WalletConstants
import java.util.*
import kotlin.collections.HashMap


/**
 * @file Const
 * @brief
 *       存放固定值
 *
 * @author Zack
 *
 * */

object Const {

    var DEBUG: Boolean = true
    const val MAX_DETECT_RANGE: Double = 5.0
    const val STORE_INFO = 13826795
    const val FORE_SERVICE_ID = 742
    //FireBase
    const val FIREBASE_JOBLIST_KEY = "JobList"
    const val FIREBASE_TAKEN_JOBLIST_KEY = "TakenJob"
    const val FIREBASE_OWNED_JOBLIST_KEY = "OwnJob"
    const val FIREBASE_TAKEN_OWNED_JOBLIST_KEY = "TakenAndOwnJob"

    const val PARAM_API_TOKEN = "api_token"
    const val PARAM_MAC = "mac"
    const val PARAM_GET_MAC = "getmac"
    const val PARAM_IGNORE = "ignore"
    const val PARAM_TIME_STAMP = "timestamp"

    const val BROADCAST_PERMISSION_REQUEST = "permission_req"
    const val BROADCAST_PERMISSION_GRANTED = "permission_granted"
    const val MY_PERMISSION_ACCESS_COARSE_LOCATION = 101
    const val MY_PERMISSION_ACCESS_FINE_LOCATION = 102
    const val MY_PERMISSION_CAMERA_ETC = 103

    const val MY_PERMISSION_TAKE_PIC = 104
    const val MY_PERMISSION_GALLERY = 105
    const val REQUEST_IMAGE = 106
    const val MY_PERMISSIONS_REQUEST = 107
    const val REQUSET_CAMERA = 108

    val ERROR_NO_INTERNET = "error_no_internet"
    val APPNAME = "LBS"
    val PREF_USER_KEY = "USERACCOUNT"
    val PREF_TUTORIAL = "TUTORIAL"
    val PREF_GUIDE = "material_showcaseview_prefs";
    val IAMOWNER = "IMOWNER"
    val DATABASE_NAME = "base_db"
    val IGNORE = 1
    val ISSETTING="ISSETTING"
    val ISBANK="ISBANK"
    val TOHISTORY="TOHISTORY"

    val numArr: LongArray = longArrayOf(
        451062895347, 74268238105, 5035017064843, 45096362172,
        267317540680, 2425605388719, 84123049072, 61742754098,
        89084953367, 6435178916350, 169362491067, 385407389142,
        43370163236
    )


    val keyArray = arrayOf(
        "q3HKSFQ6YEtddw4s", "AyVpsPFbN2fMYmKP", "Bm45eaWKeC9BfdT8", "fPmaPAn7xdFxQrFV",
        "fe8KQTFH6223fgcd", "9VKKaZeA2DCQ7ash", "8krDdpe3MuVy2Kna", "xUfRDrcYuQxMDsMe",
        "MwQRuXnarrYWN74D", "SzeTcyx8dBHpVktq", "aXsm3s7Dub92h5sN", "aGFjHUB8UN4hM798",
        "DcwXeDrHWxkzhKZn", "sGFZz9chbqXnbBqd", "4wxrs3B97uDFsnkh", "c7QUxBkNkf36a2Bn",
        "MnMAmZtrxVYr9PbA", "8NTMqhEnAPAY6VTM", "ErqRhPcbEz8Tyvvz", "NQwtSzXbNBM9QPwD",
        "PgtpR7mMnyH4zzKs", "9Td5wC9EnTrkfm74", "eaApXBkf7xW4Nkmt"
    )

    var permissions = arrayOf(
        Manifest.permission.INTERNET,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )
    enum class USERSTATE{
        //0:default 1:owner 2:taker 3:decide by previous
        DEFAULT,
        OWNER,
        TAKER,
        DECIED_BY_PREVIOUS
    }

    /**
     * Changing this to ENVIRONMENT_PRODUCTION will make the API return chargeable card information.
     * Please refer to the documentation to read about the required steps needed to enable
     * ENVIRONMENT_PRODUCTION.
     *
     * @value #PAYMENTS_ENVIRONMENT
     */
    const val PAYMENTS_ENVIRONMENT = WalletConstants.ENVIRONMENT_PRODUCTION

    /**
     * The allowed networks to be requested from the API. If the user has cards from networks not
     * specified here in their account, these will not be offered for them to choose in the popup.
     *
     * @value #SUPPORTED_NETWORKS
     */
    val SUPPORTED_NETWORKS = listOf(
        "AMEX",
        "DISCOVER",
        "JCB",
        "MASTERCARD",
        "VISA")

    /**
     * The Google Pay API may return cards on file on Google.com (PAN_ONLY) and/or a device token on
     * an Android device authenticated with a 3-D Secure cryptogram (CRYPTOGRAM_3DS).
     *
     * @value #SUPPORTED_METHODS
     */
    val SUPPORTED_METHODS = listOf(
        "PAN_ONLY",
        "CRYPTOGRAM_3DS")

    /**
     * Required by the API, but not visible to the user.
     *
     * @value #CURRENCY_CODE Your local currency
     */
    const val CURRENCY_CODE = "USD"

    /**
     * Supported countries for shipping (use ISO 3166-1 alpha-2 country codes). Relevant only when
     * requesting a shipping address.
     *
     * @value #SHIPPING_SUPPORTED_COUNTRIES
     */
    val SHIPPING_SUPPORTED_COUNTRIES = listOf("US", "GB", "IN")

    /**
     * The name of your payment processor/gateway. Please refer to their documentation for more
     * information.
     *
     * @value #PAYMENT_GATEWAY_TOKENIZATION_NAME
     */
    const val PAYMENT_GATEWAY_TOKENIZATION_NAME = "example"

    /**
     * Custom parameters required by the processor/gateway.
     * In many cases, your processor / gateway will only require a gatewayMerchantId.
     * Please refer to your processor's documentation for more information. The number of parameters
     * required and their names vary depending on the processor.
     *
     * @value #PAYMENT_GATEWAY_TOKENIZATION_PARAMETERS
     */
    val PAYMENT_GATEWAY_TOKENIZATION_PARAMETERS = mapOf(
        "gateway" to PAYMENT_GATEWAY_TOKENIZATION_NAME,
        "gatewayMerchantId" to "exampleGatewayMerchantId"
    )

    /**
     * Only used for `DIRECT` tokenization. Can be removed when using `PAYMENT_GATEWAY`
     * tokenization.
     *
     * @value #DIRECT_TOKENIZATION_PUBLIC_KEY
     */
    const val DIRECT_TOKENIZATION_PUBLIC_KEY = "REPLACE_ME"

    /**
     * Parameters required for `DIRECT` tokenization.
     * Only used for `DIRECT` tokenization. Can be removed when using `PAYMENT_GATEWAY`
     * tokenization.
     *
     * @value #DIRECT_TOKENIZATION_PARAMETERS
     */
    val DIRECT_TOKENIZATION_PARAMETERS = mapOf(
        "protocolVersion" to "ECv1",
        "publicKey" to DIRECT_TOKENIZATION_PUBLIC_KEY
    )


}
