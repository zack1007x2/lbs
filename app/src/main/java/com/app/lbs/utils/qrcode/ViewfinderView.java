/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.app.lbs.utils.qrcode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.app.lbs.R;
import com.app.lbs.utils.qrcode.camera.CameraManager;
import com.google.zxing.ResultPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * This view is overlaid on top of the camera preview. It adds the viewfinder
 * rectangle and partial transparency outside it, as well as the laser scanner
 * animation and result points.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class ViewfinderView extends View {

	private static final int[] SCANNER_ALPHA = { 0, 64, 128, 192, 255, 192,
			128, 64 };
	private static final long ANIMATION_DELAY = 80L;
	private static final int CURRENT_POINT_OPACITY = 0xA0;
	private static final int MAX_RESULT_POINTS = 20;
	private static final int POINT_SIZE = 6;

	private CameraManager cameraManager;
	private final Paint paint;
	private Bitmap resultBitmap;
	private Drawable cameraframe;
	private final int maskColor;
	private final int resultColor;
	private final int laserColor;
	private final int resultPointColor;
	private int scannerAlpha;
	private List<ResultPoint> possibleResultPoints;
	private List<ResultPoint> lastPossibleResultPoints;
	
	private int Y_counter, ScanLineMode;
	private static final int KEEP_GOING_UP = 1;
	private static final int KEEP_GOING_DOWN = 2;
	private static final int NEEDCHANGE = 3;
	
	

	// This constructor is used when the class is built from an XML resource.
	public ViewfinderView(Context context, AttributeSet attrs) {
		super(context, attrs);

		// Initialize these once for performance rather than calling them every
		// time in onDraw().
		final int HeadervalueInPixels = (int) getResources().getDimension(
				R.dimen.size_41);
		paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		Resources resources = getResources();
		maskColor = resources.getColor(R.color.viewfinder_mask);
		resultColor = resources.getColor(R.color.result_view);
		laserColor = resources.getColor(R.color.viewfinder_laser);
		resultPointColor = resources.getColor(R.color.possible_result_points);
		scannerAlpha = 0;
		possibleResultPoints = new ArrayList<>(5);
		lastPossibleResultPoints = null;
		cameraframe = context.getResources().getDrawable(R.drawable.cameraframe);
		
	}

	public void setCameraManager(CameraManager cameraManager) {
		this.cameraManager = cameraManager;
	}

	@SuppressLint("DrawAllocation")
	@Override
	public void onDraw(Canvas canvas) {
		if (cameraManager == null) {
			return; // not ready yet, early draw before done configuring
		}
		Rect frame = cameraManager.getFramingRect();
		Rect previewFrame = cameraManager.getFramingRectInPreview();
		if (frame == null || previewFrame == null) {
			return;
		}
		int width = canvas.getWidth();
		int height = canvas.getHeight();
		final int HeadervalueInPixels = (int) getResources().getDimension(
				R.dimen.size_1);
		final int tunevalueInPixels = (int) getResources().getDimension(
				R.dimen.size_200);
		final int size50 = (int) getResources().getDimension(
				R.dimen.size_50);
		final int size160 = (int) getResources().getDimension(
				R.dimen.size_160);

//		int square_top = frame.top- HeadervalueInPixels;
//		int square_bottom = frame.bottom - HeadervalueInPixels * 2;
//		int square_left = frame.left + HeadervalueInPixels / 2;
//		int square_right = frame.right - HeadervalueInPixels / 2;

		int square_top = frame.top- tunevalueInPixels;
		int square_bottom = frame.bottom - size160;
		int square_left = frame.left - size50 / 2;
		int square_right = frame.right + size50 / 2;
		Log.d("Zack", square_top+" | "+square_bottom+" | "+square_left+" | "+square_right);
		// Draw the exterior (i.e. outside the framing rect) darkened
		paint.setColor(resultBitmap != null ? resultColor : maskColor);
		paint.setStyle(Paint.Style.STROKE);
		Paint PaintMask = new Paint();
		PaintMask.setColor(getResources().getColor(R.color.viewfinder_mask));
		canvas.drawRect(0, 0, width, square_top, PaintMask);
		canvas.drawRect(0, square_top , square_left, square_bottom, PaintMask);
		canvas.drawRect(square_right, square_top, width, square_bottom, PaintMask);
		canvas.drawRect(0, square_bottom, width, height, PaintMask);
		
		//detect area 偵測範圍畫面設定
//		canvas.drawRect(frame.left + HeadervalueInPixels / 2, frame.top
//				- HeadervalueInPixels, frame.right - HeadervalueInPixels / 2,
//				frame.bottom - HeadervalueInPixels * 2, paint);
		Rect Bound = new Rect(square_left, square_top, square_right,
				square_bottom);
		cameraframe.setBounds(Bound);
		cameraframe.draw(canvas);
		
		
		if (resultBitmap != null) {
			// Draw the opaque result bitmap over the scanning rectangle
			paint.setAlpha(CURRENT_POINT_OPACITY);
			canvas.drawBitmap(resultBitmap, null, frame, paint);
		} else {
			
			// Draw a red "laser scanner" line through the middle to show
			// decoding is active
			paint.setColor(getResources().getColor(R.color.taker_blue));
			paint.setStyle(Paint.Style.FILL);
			paint.setAlpha(SCANNER_ALPHA[scannerAlpha]);
			scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.length;
			int middle = frame.height() / 2 + frame.top;
			// canvas.drawRect(frame.left + 2, middle - 1, frame.right - 1,
			// middle + 2, paint);
			paint.setStrokeWidth(5);
			float speed = (square_bottom-square_top)/20;
			
			if(ScanLineMode== KEEP_GOING_DOWN){
				canvas.drawLine(square_left, square_top+Y_counter, square_right, square_top+Y_counter, paint);
				Y_counter +=speed;
			}else if(ScanLineMode == KEEP_GOING_UP){
				Y_counter -= speed;
				canvas.drawLine(square_left, square_top+Y_counter, square_right, square_top+Y_counter, paint);
			}
			if((square_top+Y_counter)>square_bottom+1){
				ScanLineMode = KEEP_GOING_UP;
			}else if((square_top+Y_counter)<=square_top){
				ScanLineMode = KEEP_GOING_DOWN;
			}
			
			float scaleX = frame.width() / (float) previewFrame.width();
			float scaleY = frame.height() / (float) previewFrame.height();

			List<ResultPoint> currentPossible = possibleResultPoints;
			List<ResultPoint> currentLast = lastPossibleResultPoints;
			int frameLeft = frame.left;
			int frameTop = frame.top;
			if (currentPossible.isEmpty()) {
				lastPossibleResultPoints = null;
			} else {
				possibleResultPoints = new ArrayList<>(5);
				lastPossibleResultPoints = currentPossible;
				paint.setAlpha(CURRENT_POINT_OPACITY);
				paint.setColor(resultPointColor);
//				synchronized (currentPossible) {
//					for (ResultPoint point : currentPossible) {
//					// �ϬM������QR����ڰ�����m
// 					canvas.drawRect(frameLeft+point.getX()-POINT_SIZE,frameTop+point.getY()+POINT_SIZE,frameLeft+point.getX()+POINT_SIZE,frameTop+point.getY()-POINT_SIZE,paint);
//					}
//				}
			}
//			if (currentLast != null) {
//				paint.setAlpha(CURRENT_POINT_OPACITY / 2);
//				paint.setColor(resultPointColor);
//				synchronized (currentLast) {
//					float radius = POINT_SIZE / 2.0f;
//					for (ResultPoint point : currentLast) {
//						canvas.drawCircle(frameLeft
//								+ (int) (point.getX() * scaleX), frameTop
//								+ (int) (point.getY() * scaleY), radius, paint);
//					}
//				}
//			}

			// Request another update at the animation interval, but only
			// repaint the laser line,
			// not the entire viewfinder mask.
			postInvalidateDelayed(ANIMATION_DELAY, frame.left - POINT_SIZE,
					frame.top - POINT_SIZE, frame.right + POINT_SIZE,
					frame.bottom + POINT_SIZE);
		}
	}

	public void drawViewfinder() {
		Bitmap resultBitmap = this.resultBitmap;
		this.resultBitmap = null;
		if (resultBitmap != null) {
			resultBitmap.recycle();
		}
		invalidate();
	}

	/**
	 * Draw a bitmap with the result points highlighted instead of the live
	 * scanning display.
	 *
	 * @param barcode
	 *            An image of the decoded barcode.
	 */
	public void drawResultBitmap(Bitmap barcode) {
		resultBitmap = barcode;
		invalidate();
	}

	public void addPossibleResultPoint(ResultPoint point) {
		List<ResultPoint> points = possibleResultPoints;
		synchronized (points) {
			points.add(point);
			int size = points.size();
			if (size > MAX_RESULT_POINTS) {
				// trim it
				points.subList(0, size - MAX_RESULT_POINTS / 2).clear();
			}
		}
	}

}
