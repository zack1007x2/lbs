package com.app.lbs.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.core.content.ContextCompat
import com.app.lbs.R
import com.app.lbs.models.Mission
import com.app.lbs.models.Store
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer

/** @file CustomClusterRenderer
 *  @brief
 *        用於繪製map上的圖標（包含cluster和marker）
 *
 * @author Zack
 * @date 20200510 */

class CustomClusterRenderer(
    context: Context,
    map: GoogleMap,
    clusterManager: ClusterManager<ClusterItem>,
    state:Const.USERSTATE
) :
    DefaultClusterRenderer<ClusterItem>(context, map, clusterManager) {
    private val mContext = context
    var markerIcon: BitmapDescriptor? = null
    var markerIconbelow29: BitmapDescriptor? = null
    var markerIconbelow59: BitmapDescriptor? = null
    var markerIconbelow99: BitmapDescriptor? = null
    var markerIcon100up: BitmapDescriptor? = null
    var mState = state

    /** @brief
     *      func: onBeforeClusterItemRendered, onClusterItemUpdated
     *      用於繪製marker圖案, 圖標在init時預載到markerIcon
     *      本函式在UiThread上執行 禁止長時間繪製
     * */
    override fun onBeforeClusterItemRendered(
        item: ClusterItem,
        markerOptions: MarkerOptions
    ) {
        if(item is Mission){
            markerOptions
                .icon(when(item.yelp_rank.toIntOrNull() ?:-1){
                    in 0..29->{
                        markerIconbelow29
                    }
                    in 30..59->{
                        markerIconbelow59
                    }
                    in 59..99->{
                        markerIconbelow99
                    }
                    else->{
                        markerIcon100up
                    }
                })
        }
        if(item is Store)
            markerOptions.icon(markerIcon)

    }

    override fun onClusterItemUpdated(item: ClusterItem, marker: Marker) {
        if(item is Mission){
            marker
                .setIcon(when(item.yelp_rank.toIntOrNull() ?:-1){
                    in 0..29->{
                        markerIconbelow29
                    }
                    in 30..59->{
                        markerIconbelow59
                    }
                    in 59..99->{
                        markerIconbelow99
                    }
                    else->{
                        markerIcon100up
                    }
                })
        }

        if(item is Store)
            marker.setIcon(markerIcon)

    }

    /** @brief
     *      關閉點擊marker時出現的資訊欄
     * */
    override fun setOnClusterItemInfoWindowClickListener(listener: ClusterManager.OnClusterItemInfoWindowClickListener<ClusterItem>?) {
//        super.setOnClusterItemInfoWindowClickListener(listener)
    }

    /** @brief
     *      設定多少個marker聚集時就該合併（目前設定2個以上即合併）
     * */
    override fun shouldRenderAsCluster(cluster: Cluster<ClusterItem>?): Boolean {
        return cluster!!.size > 1
    }

    /** @brief
     *      設定cluster的背景顏色
     * */
    override fun getColor(clusterSize: Int): Int {
        var col: Int
        if(mState==Const.USERSTATE.OWNER)
            col= R.color.owner_red
        else
            col= R.color.taker_blue

        return ContextCompat.getColor(mContext, col)
    }

    /** @brief
     *
     *      預載marker圖標, 把drawable res 轉為 BitmapDescriptor
     *
     *      func:getMarkerIconFromDrawable
     *         透過繪製到canvas轉為bitmap的方式
     *         可將圖片或xml轉為 BitmapDescriptor
     * */

    init {
        var circleDrawable: Drawable =
            mContext.resources.getDrawable(R.drawable.map_dot_below29, null)
        markerIconbelow29 = Utils.getMarkerIconFromDrawable(circleDrawable)
        circleDrawable = mContext.resources.getDrawable(R.drawable.map_dot_below59, null)
        markerIconbelow59 = Utils.getMarkerIconFromDrawable(circleDrawable)
        circleDrawable = mContext.resources.getDrawable(R.drawable.map_dot_below99, null)
        markerIconbelow99 = Utils.getMarkerIconFromDrawable(circleDrawable)
        circleDrawable = mContext.resources.getDrawable(R.drawable.map_dot_100up, null)
        markerIcon100up = Utils.getMarkerIconFromDrawable(circleDrawable)
        circleDrawable = mContext.resources.getDrawable(R.drawable.map_dot_normal, null)
        markerIcon = Utils.getMarkerIconFromDrawable(circleDrawable)
    }
}






