package com.app.lbs.utils

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Pattern


/** @file DecimalInputFilter
 *  @brief
 *        用於限制數字輸出範圍
 *        根據不同constructor有不同限制方式
 *
 *  constructor(digitsBeforeDecimal: Int, digitsAfterDecimal: Int)
 *  @param digitsBeforeDecimal: 顯示小數點前多少位數
 *  @param digitsAfterDecimal: 顯示小數點後多少位數
 *
 *  constructor(max: Double, min: Double, digitsBeforeDecimal: Int, digitsAfterDecimal: Int)
 *  除了位數還可同時設定最大小值範圍
 *  @param max: 最大值
 *  @param min: 最小值
 *
 * @author Zack */

open class DecimalInputFilter : InputFilter {

    private var mPattern: Pattern? = null
    private var maxLimit: Double=0.0
    private var minLimit: Double=0.0
    private var hasLimit: Boolean = false

    constructor(digitsBeforeDecimal: Int, digitsAfterDecimal: Int) {
        mPattern = Pattern.compile(
            "^-?\\d{0," + digitsBeforeDecimal + "}"
                    + "([\\.](\\d{0," + digitsAfterDecimal + "})?)?$"
        )
        hasLimit = false
    }

    constructor(max: Double, min: Double, digitsBeforeDecimal: Int, digitsAfterDecimal: Int) {
        mPattern = Pattern.compile(
            ("^-?\\d{0," + digitsBeforeDecimal + "}"
                    + "([\\.](\\d{0," + digitsAfterDecimal + "})?)?$")
        )
        maxLimit = max
        minLimit = min
        hasLimit = true
    }

    override fun filter(
        source: CharSequence, start: Int, end: Int, dest: Spanned,
        dstart: Int, dend: Int
    ): CharSequence? {

        val newString =
            (dest.toString().substring(0, dstart) + source.toString().substring(start, end)
                    + dest.toString().substring(dend, dest.toString().length))

        val matcher = mPattern!!.matcher(newString)
        if (hasLimit) {
            try {
                val input = java.lang.Double.parseDouble(dest.toString() + source.toString())
                if (isInRange(minLimit, maxLimit, input) && matcher.matches())
                    return null
            } catch (nfe: NumberFormatException) {
            }

            return ""
        } else {
            if (!matcher.matches()) {
                return ""
            }
        }
        return null
    }

    private fun isInRange(min: Double, max: Double, input: Double): Boolean {
        return if (max > min)
            input in min..max
        else
            input in max..min
    }
}