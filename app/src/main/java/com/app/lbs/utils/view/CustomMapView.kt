package com.app.lbs.utils.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.MapView


/**
 * @file CustomMapView
 * @brief
 *       為解決mapview inside scrollview 滑動手勢互相影響問題
 *
 * @author Zack
 * */

class CustomMapView : MapView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet): super(context, attrs)

    constructor(context:Context,  var2: GoogleMapOptions) : super(context, var2)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr)



    override fun onTouchEvent(ev: MotionEvent): Boolean {
        val action = ev.action
        when (action) {
            MotionEvent.ACTION_DOWN ->         // Disallow ScrollView to intercept touch events.
                this.getParent().requestDisallowInterceptTouchEvent(true)
            MotionEvent.ACTION_UP ->         // Allow ScrollView to intercept touch events.
                this.getParent().requestDisallowInterceptTouchEvent(false)
        }

        // Handle MapView's touch events.
        super.onTouchEvent(ev)
        return true
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        val action = ev!!.action
        when (action) {
            MotionEvent.ACTION_DOWN ->         // Disallow ScrollView to intercept touch events.
                this.getParent().requestDisallowInterceptTouchEvent(true)
            MotionEvent.ACTION_UP ->         // Allow ScrollView to intercept touch events.
                this.getParent().requestDisallowInterceptTouchEvent(false)
        }
        return super.dispatchTouchEvent(ev)
    }

}

