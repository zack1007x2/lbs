package com.app.lbs.utils.drawroutemap;

import android.content.Context;

import com.app.lbs.utils.MyLog;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;


public class DrawRouteMaps {

    private static DrawRouteMaps instance;
    private Context context;
    private MyLog Log = MyLog.log();

    public static DrawRouteMaps getInstance(Context context) {
        instance = new DrawRouteMaps();
        instance.context = context;
        return instance;
    }

    public DrawRouteMaps draw(LatLng origin, LatLng destination, GoogleMap googleMap){
        String url_route = FetchUrl.getUrl(origin, destination);
        DrawRoute drawRoute = new DrawRoute(googleMap);
        Log.d("DrawRoute exec2: "+url_route);
        drawRoute.execute(url_route);
        return instance;
    }

    public static Context getContext() {
        return instance.context;
    }
}
