package com.app.lbs.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.*
import android.view.View.OnClickListener
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.app.lbs.R
import com.app.lbs.interfaces.IDialogValueListener
import com.app.lbs.interfaces.IFilterResults
import com.app.lbs.utils.view.SwipeDetector
import com.app.lbs.utils.view.SwipeDetector.SwipeTypeEnum
import com.app.lbs.utils.view.SwipeDetector.onSwipeEvent
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker
import com.google.android.material.tabs.TabLayout
import java.text.SimpleDateFormat
import java.util.*


/** @file CustomDialogBuilder
 *  @brief
 *        統一管理對話視窗
 *        以Factory pattern設計
 *
 * @author Zack */

class CustomDialogBuilder(private val mContext: Context, private val mLayoutId: Int) {
    private var mPositiveOnClickListener: OnClickListener? = null
    private var mCameraListener: OnClickListener? = null
    private var mGalleryListener: OnClickListener? = null
    private var mValueListener: IDialogValueListener<String>? = null
    private val saveDialogEdittext: EditText? = null
    private var mItemClickListener: OnItemClickListener? = null
    private var mCustomTitle: String? = null
    private var mCustomMessage: String? = null
    private var cancelable: Boolean? = null
    private val mAutoRunnable: Runnable? = null
    private val mVersionInfoRunnable: Runnable? = null
    private val Log = MyLog.log()
    private var mUri: String? = null
    private var webViewClient: WebViewClient? = null
    private var mTabSelectedListener: TabLayout.OnTabSelectedListener? = null
    private var mBtnAgreeListener: OnClickListener? = null
    private var mBtnRecoverListener: OnClickListener? = null
    private var mBtnSelLocListener: OnClickListener? = null
    private var mBtnProposeListener: OnClickListener? = null
    private var mCheckChangeListener: CompoundButton.OnCheckedChangeListener? = null
    private var filter_type_array:Array<Boolean>?=null
    private var mIFilterResults:IFilterResults?=null

    fun setCheckChangeListener(listener: CompoundButton.OnCheckedChangeListener): CustomDialogBuilder {
        mCheckChangeListener = listener
        return this
    }
    fun setOnClickListener(listener: OnClickListener): CustomDialogBuilder {
        mPositiveOnClickListener = listener
        return this
    }

    fun setFilterState(arr: Array<Boolean>): CustomDialogBuilder{
        filter_type_array = arr
        return this
    }

    fun setIFilterResults(listener: IFilterResults): CustomDialogBuilder{
        mIFilterResults = listener
        return this
    }

    fun setCameraListener(listener: OnClickListener): CustomDialogBuilder {
        mCameraListener = listener
        return this
    }

    fun setGalleryListener(listener: OnClickListener): CustomDialogBuilder {
        mGalleryListener = listener
        return this
    }

    fun setValueRetListener(valueListener: IDialogValueListener<String>): CustomDialogBuilder {
        mValueListener = valueListener
        return this
    }

    fun setOnItemClickListener(listener: OnItemClickListener): CustomDialogBuilder {
        mItemClickListener = listener
        return this
    }

    fun setCustomMessage(message: String): CustomDialogBuilder {
        this.mCustomMessage = message
        return this
    }

    fun setCustomMessage(strId: Int): CustomDialogBuilder {
        this.mCustomMessage = mContext.resources.getString(strId)
        return this
    }

    fun setCustomTitle(title: String): CustomDialogBuilder {
        this.mCustomTitle = title
        return this
    }

    fun setCustomTitle(titleStrId: Int): CustomDialogBuilder {
        this.mCustomTitle = mContext.resources.getString(titleStrId)
        return this
    }

    fun setCancelable(cancelable: Boolean): CustomDialogBuilder {
        this.cancelable = cancelable
        return this
    }

    fun setUri(uri: String): CustomDialogBuilder {
        mUri = uri
        return this
    }

    fun setWebViewClient(wvc: WebViewClient): CustomDialogBuilder {
        this.webViewClient = wvc
        return this
    }

    fun setOnTabSelectedListener(tabSelectedListener: TabLayout.OnTabSelectedListener): CustomDialogBuilder {
        mTabSelectedListener = tabSelectedListener
        return this
    }

    fun setBtnAgreeListener(listener: OnClickListener): CustomDialogBuilder {
        mBtnAgreeListener = listener
        return this
    }

    fun setBtnRecoverListener(listener: OnClickListener): CustomDialogBuilder {
        mBtnRecoverListener = listener
        return this
    }

    fun setBtnSelLocListener(listener: OnClickListener): CustomDialogBuilder {
        mBtnSelLocListener = listener
        return this
    }

    fun setBtnProposeListener(listener: OnClickListener): CustomDialogBuilder {
        mBtnProposeListener = listener
        return this
    }


    /**
     * @brief
     *      根據不同layout選擇所需的功能來顯示
     */
    @SuppressLint("SetJavaScriptEnabled", "JavascriptInterface")
    fun build(): Dialog {
        var dialog = Dialog(mContext)
        if(mLayoutId==R.layout.custom_dialog_bottom_sheet_filter){
            dialog = Dialog(ContextThemeWrapper(mContext, R.style.DialogSlideAnim))
            dialog.window?.setGravity(Gravity.BOTTOM)
            dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        if (mPositiveOnClickListener == null) {
            mPositiveOnClickListener = OnClickListener { dialog.cancel() }
        }
        dialog.setContentView(mLayoutId)

        val negativeListener = OnClickListener { dialog.dismiss() }

        when (mLayoutId) {
            R.layout.custom_dialog_loading -> {
                dialog.setCancelable(cancelable ?: false)
                dialog.setCanceledOnTouchOutside(cancelable ?: false)
                if (mCustomMessage != null && !TextUtils.isEmpty(mCustomMessage))
                    (dialog.findViewById(R.id.tv_loading_dialog_message) as TextView).text =
                        mCustomMessage
                else
                    (dialog.findViewById(R.id.tv_loading_dialog_message) as TextView).visibility =
                        View.GONE
            }

            R.layout.custom_dialog_web -> {
                val webView = dialog.findViewById<WebView>(R.id.webv)
                val webSettings = webView.settings
                webSettings.javaScriptEnabled = true
                webSettings.loadWithOverviewMode = true
                webSettings.useWideViewPort = true
                webView.setInitialScale(30)
                webView.settings.userAgentString = "here"
                webView.webViewClient = webViewClient
                webView.addJavascriptInterface(this, "Android")
                webView.loadUrl(mUri)
            }

            R.layout.custom_dialog_message_only -> {
                dialog.setCancelable(cancelable ?: true)
                dialog.setCanceledOnTouchOutside(cancelable ?: true)
                dialog.findViewById<TextView>(R.id.tvContent).text = mCustomMessage
            }
            R.layout.custom_dialog_message_1_btn -> {
                dialog.setCancelable(cancelable ?: false)
                dialog.setCanceledOnTouchOutside(cancelable ?: false)
                dialog.findViewById<TextView>(R.id.tvContent).text = mCustomMessage
                dialog.findViewById<TextView>(R.id.tvTitle).text = mCustomTitle
                dialog.findViewById<Button>(R.id.btConfirm)
                    .setOnClickListener(mPositiveOnClickListener)
            }
            R.layout.custom_dialog_message_2_btn -> {
                dialog.setCancelable(cancelable ?: false)
                dialog.setCanceledOnTouchOutside(cancelable ?: false)
                dialog.findViewById<TextView>(R.id.tvContent).text = mCustomMessage
                dialog.findViewById<TextView>(R.id.tvTitle).text = mCustomTitle
                dialog.findViewById<Button>(R.id.btConfirm)
                    .setOnClickListener(mPositiveOnClickListener)
                dialog.findViewById<Button>(R.id.btLater).setOnClickListener(negativeListener)
            }
            R.layout.custom_dialog_edittext_2_btn -> {
                mPositiveOnClickListener = OnClickListener {
                    mValueListener?.onReturnValue(dialog.findViewById<EditText>(R.id.etContent).text.toString())
                }
                dialog.setCancelable(cancelable ?: false)
                dialog.setCanceledOnTouchOutside(cancelable ?: false)
                dialog.findViewById<TextView>(R.id.tvTitle).text = mCustomTitle
                dialog.findViewById<Button>(R.id.btConfirm)
                    .setOnClickListener(mPositiveOnClickListener)
                dialog.findViewById<Button>(R.id.btLater).setOnClickListener(negativeListener)
            }
            R.layout.custom_dialog_camera_or_gallery -> {
                dialog.setCancelable(cancelable ?: true)
                dialog.setCanceledOnTouchOutside(cancelable ?: true)
                dialog.findViewById<TextView>(R.id.tvTitle).text = mCustomTitle
                dialog.findViewById<Button>(R.id.btCamera).setOnClickListener(mCameraListener)
                dialog.findViewById<Button>(R.id.btGallery).setOnClickListener(mGalleryListener)
            }
            R.layout.custom_bottom_sheet_add_custom_loc->{
                val et_name_bar = (dialog.findViewById(R.id.et_name_bar) as EditText)
                val et_address_bar = (dialog.findViewById(R.id.et_address_bar) as EditText)
                val btn_confirm_add_new_loc = (dialog.findViewById(R.id.btn_confirm_add_new_loc) as Button)
                val cb_location_save = (dialog.findViewById(R.id.cb_location_save) as AppCompatCheckBox)
                val onClearTextListener = OnClickListener { v->
                    when(v.id){
                        R.id.btn_text_name_clear->{
                            et_name_bar.setText("")
                        }
                        R.id.btn_text_addr_clear->{
                            et_address_bar.setText("")
                        }
                    }
                }
                (dialog.findViewById(R.id.btn_text_name_clear) as Button).setOnClickListener(onClearTextListener)
                (dialog.findViewById(R.id.btn_text_addr_clear) as Button).setOnClickListener(onClearTextListener)

                if (mCustomMessage != null && !TextUtils.isEmpty(mCustomMessage))
                    et_address_bar.setText(mCustomMessage)

                et_address_bar.addTextChangedListener(object: TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable?) {
                        btn_confirm_add_new_loc.isEnabled = s!!.isNotEmpty() && et_name_bar.text.isNotEmpty()
                    }

                })

                et_name_bar.addTextChangedListener(object: TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable?) {
                        btn_confirm_add_new_loc.isEnabled = s!!.isNotEmpty() && et_address_bar.text.isNotEmpty()
                    }

                })

                btn_confirm_add_new_loc.setOnClickListener {
                    var ret = arrayListOf<String>()
                    ret.add(dialog.findViewById<EditText>(R.id.et_name_bar).text.toString())
                    ret.add(dialog.findViewById<EditText>(R.id.et_address_bar).text.toString())
                    ret.add(cb_location_save.isChecked.toString())
                    mIFilterResults?.ResultPublished(ret)
                }
            }
            R.layout.custom_dialog_bottom_sheet_filter->{
                val isOwner = filter_type_array!![0]
                dialog.setCancelable(cancelable ?: true)
                dialog.setCanceledOnTouchOutside(cancelable ?: true)
                dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
                val tv_filter_cancel = dialog.findViewById<TextView>(R.id.tv_filter_cancel)
                tv_filter_cancel.setOnClickListener(negativeListener)
                val cb_sort_distance = dialog.findViewById<CheckBox>(R.id.cb_sort_distance)
                cb_sort_distance.setOnCheckedChangeListener(mCheckChangeListener)
                val cb_sort_time = dialog.findViewById<CheckBox>(R.id.cb_sort_time)
                cb_sort_time.setOnCheckedChangeListener(mCheckChangeListener)
                val cb_sort_wage = dialog.findViewById<CheckBox>(R.id.cb_sort_wage)
                cb_sort_wage.setOnCheckedChangeListener(mCheckChangeListener)
                val rb_mission_type_meal = dialog.findViewById<CheckBox>(R.id.rb_mission_type_meal)
                rb_mission_type_meal.setOnCheckedChangeListener(mCheckChangeListener)
                val rb_mission_type_100m = dialog.findViewById<CheckBox>(R.id.rb_mission_type_100m)
                rb_mission_type_100m.setOnCheckedChangeListener(mCheckChangeListener)
                val rb_mission_type_booking = dialog.findViewById<CheckBox>(R.id.rb_mission_type_booking)
                rb_mission_type_booking.setOnCheckedChangeListener(mCheckChangeListener)
                val rb_mission_type_inline = dialog.findViewById<CheckBox>(R.id.rb_mission_type_inline)
                rb_mission_type_inline.setOnCheckedChangeListener(mCheckChangeListener)
                val rb_mission_type_ticket = dialog.findViewById<CheckBox>(R.id.rb_mission_type_ticket)
                rb_mission_type_ticket.setOnCheckedChangeListener(mCheckChangeListener)
                val tv_filter_reset = dialog.findViewById<TextView>(R.id.tv_filter_reset)
                tv_filter_reset.setOnClickListener {
                    rb_mission_type_meal.isChecked= false
                    rb_mission_type_100m.isChecked= false
                    rb_mission_type_inline.isChecked= false
                    rb_mission_type_ticket.isChecked= false
                    rb_mission_type_booking.isChecked= false
                    cb_sort_distance.isChecked= true
                    cb_sort_time.isChecked= false
                    cb_sort_wage.isChecked= false
                }

                val btn_filter_agree = dialog.findViewById<Button>(R.id.btn_filter_agree)
                btn_filter_agree.setOnClickListener{
                    mIFilterResults?.ResultPublished(arrayOf(isOwner,
                        rb_mission_type_meal.isChecked,
                        rb_mission_type_100m.isChecked,
                        rb_mission_type_inline.isChecked,
                        rb_mission_type_ticket.isChecked,
                        rb_mission_type_booking.isChecked,
                        cb_sort_distance.isChecked,
                        cb_sort_time.isChecked,
                        cb_sort_wage.isChecked))
                }

                SwipeDetector(dialog.findViewById<LinearLayout>(R.id.bottom_sheet_filter))
                    .setOnSwipeListener(object : onSwipeEvent {
                    override fun SwipeEventDetected(v: View?, SwipeType: SwipeTypeEnum?) {
                        if (SwipeType === SwipeTypeEnum.TOP_TO_BOTTOM)
                            dialog.dismiss()
                    }
                })

                rb_mission_type_meal.isChecked= filter_type_array!![1]
                rb_mission_type_100m.isChecked= filter_type_array!![2]
                rb_mission_type_inline.isChecked= filter_type_array!![3]
                rb_mission_type_ticket.isChecked= filter_type_array!![4]
                rb_mission_type_booking.isChecked= filter_type_array!![5]
                cb_sort_distance.isChecked= filter_type_array!![6]
                cb_sort_time.isChecked= filter_type_array!![7]
                cb_sort_wage.isChecked= filter_type_array!![8]

                if (isOwner) {
                    tv_filter_cancel.setTextColor(ContextCompat.getColor(mContext, R.color.owner_red))
                    tv_filter_reset.setTextColor(ContextCompat.getColor(mContext, R.color.owner_red))
                    rb_mission_type_meal.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_owner_text_black_selector))
                    rb_mission_type_100m.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_owner_text_black_selector))
                    rb_mission_type_booking.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_owner_text_black_selector))
                    rb_mission_type_inline.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_owner_text_black_selector))
                    rb_mission_type_ticket.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_owner_text_black_selector))
                    btn_filter_agree.setBackgroundResource(R.drawable.btn_owner_rect)
                    cb_sort_distance.setButtonDrawable(R.drawable.checkbox_selector_owner)
                    cb_sort_time.setButtonDrawable(R.drawable.checkbox_selector_owner)
                    cb_sort_wage.setButtonDrawable(R.drawable.checkbox_selector_owner)
                }else{
                    tv_filter_cancel.setTextColor(ContextCompat.getColor(mContext, R.color.taker_blue))
                    tv_filter_reset.setTextColor(ContextCompat.getColor(mContext, R.color.taker_blue))
                    rb_mission_type_meal.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_taker_text_black_selector))
                    rb_mission_type_100m.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_taker_text_black_selector))
                    rb_mission_type_booking.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_taker_text_black_selector))
                    rb_mission_type_inline.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_taker_text_black_selector))
                    rb_mission_type_ticket.setTextColor(ContextCompat.getColorStateList(mContext, R.color.custom_taker_text_black_selector))
                    btn_filter_agree.setBackgroundResource(R.drawable.btn_taker_rect)
                    cb_sort_distance.setButtonDrawable(R.drawable.checkbox_selector_taker)
                    cb_sort_time.setButtonDrawable(R.drawable.checkbox_selector_taker)
                    cb_sort_wage.setButtonDrawable(R.drawable.checkbox_selector_taker)
                }


            }
        }
        return dialog
    }
}
