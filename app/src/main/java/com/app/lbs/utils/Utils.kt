package com.app.lbs.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.SystemClock
import android.provider.MediaStore
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.FileProvider
import com.app.lbs.BuildConfig
import com.app.lbs.R
import com.app.lbs.models.Mission
import com.app.lbs.repository.SysRepository
import com.app.lbs.utils.Const.PARAM_API_TOKEN
import com.app.lbs.utils.Const.PARAM_GET_MAC
import com.app.lbs.utils.Const.PARAM_IGNORE
import com.app.lbs.utils.Const.PARAM_MAC
import com.app.lbs.utils.Const.PARAM_TIME_STAMP
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.gson.reflect.TypeToken
import com.google.maps.DistanceMatrixApi
import com.google.maps.GeoApiContext
import com.google.maps.model.TravelMode
import uk.co.deanwild.materialshowcaseview.PrefsManager
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.lang.ref.WeakReference
import java.lang.reflect.Type
import java.math.BigDecimal
import java.security.MessageDigest
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


/**
 * @file Utils
 * @brief
 *       通用functions
 *
 * @author Zack
 * @date 2016/7/23.
 * */
object Utils {
    var Log = MyLog.log()

    /** @brief
     *      func: getMac
     *          會利用到map的排序功能因此需先將object轉為map形式
     *          在傳遞給此function
     * */
    fun getMac(func: String, map: HashMap<String, String>): String {
        var timeStamp: String = ""

        if (map.containsKey(PARAM_TIME_STAMP))
            timeStamp = map[PARAM_TIME_STAMP].toString()

        val key = getKey(timeStamp)
        val api_token = map[PARAM_API_TOKEN]
        val sMap = getSortedMap(HashMap(map))

        var params: String = ""
        for ((k, v) in sMap) {
            params += v
        }

        val ret = key.substring(0, key.length / 2) +
                api_token?.substring(api_token.length / 2) +
                func + params +
                api_token?.substring(0, api_token.length / 2) +
                key.substring(key.length / 2)
        return sha256(ret)
    }

    fun getSortedMap(map: HashMap<String, String>): SortedMap<String, String> {
        map.remove(PARAM_API_TOKEN)
        map.remove(PARAM_MAC)
        map.remove(PARAM_GET_MAC)
        map.remove(PARAM_IGNORE)
        return map.toSortedMap()
    }

    fun getKey(timeStamp: String): String {
        var numKey: Long?=0
        try{
            numKey = Const.numArr[(timeStamp.toLong() % 13).toInt()]
        }catch (e: Exception){
            Log.e(Const.numArr.size.toString() + " | " + timeStamp)
            Log.e(e)
        }


        var compareSize: Int = numKey.toString().length
        var timeStampSize = timeStamp.lastIndex
        if (timeStamp.length < compareSize) {
            compareSize = timeStamp.length
        }

        var code = 0
        var round = 0
        while (compareSize > 0) {
            code += Integer.parseInt(timeStamp[timeStampSize].toString()) * Integer.parseInt(numKey.toString()[round].toString())
            compareSize--
            timeStampSize--
            round++
        }
        return Const.keyArray[code % 23]
    }

    val timeStamp: String
        get() = System.currentTimeMillis().toString()

    fun toHex(byteArray: ByteArray): String {
        val result = with(StringBuilder()) {
            byteArray.forEach {
                val hex = it.toInt() and (0xFF)
                val hexStr = Integer.toHexString(hex)
                if (hexStr.length == 1) {
                    this.append("0").append(hexStr)
                } else {
                    this.append(hexStr)
                }
            }
            this.toString()
        }
        return result
    }

    fun sha256(str: String): String {
        val digest = MessageDigest.getInstance("SHA-256")
        val result = digest.digest(str.toByteArray())
        return toHex(result)
    }

    /**
     * @brief
     *      透過遞迴設定整個view group的enable
     *
     */
    fun recursiveEnableView(view: ViewGroup, b: Boolean) {
        view.isEnabled = b

        for (i in 0 until view.childCount) {
            val child = view.getChildAt(i)
            if (child is ViewGroup) {
                recursiveEnableView(child, b)
            } else {
                child.isEnabled = b
            }
        }
    }

    /**
     * @brief
     *      確認網路連線
     *
     */
    fun hasInternet(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!.isConnected
    }


    /**
     * @brief
     *      查看service是否運行中
     *
     */
    fun isMyServiceRunning(mContext: Context, serviceClass: Class<*>): Boolean {
        val manager = mContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        for (service in manager!!.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    /** @brief
     *      func: getLatLng
     *          將 "(lat, long)":String 轉為 object:LatLng()
     * */
    @JvmStatic
    fun getLatLng(coor2addr: String): LatLng {
        try{
            val latlon: List<String> = coor2addr.substring(1, coor2addr.length - 1).split(",")
            return LatLng(latlon[1].toDouble(), latlon[0].toDouble())
        }catch (e: Exception){
            Log.e("getLatLng Error: $e")
            return LatLng(0.0, 0.0)
        }
    }

    /** @brief
     *      func: getCoor2Addr
     *          將 object:LatLng() 轉為 "(lonh, lati)":String
     * */
    fun getCoor2Addr(latlng: LatLng): String {
        return "(" + latlng.longitude + ", " + latlng.latitude + ")"
    }

    /** @brief
     *      將圖片轉為byte code進行傳輸
     *  @param Bitmap
     *
     *  @return ByteArray
     * */
    fun getBytesFromBitmap(bitmap: Bitmap): ByteArray {
        var stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray()
    }

    /** @brief
     *      取得實際檔案路徑
     *  @param Context
     *  @param Uri
     *
     *  @return String
     * */
    fun getRealPathFromUri(context: Context, contentUri: Uri): String {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(column_index)
        } finally {
            cursor?.close()
        }
    }

    /**
     * Only clear data inside ../PATH_TO_CACHE/camera
     */
    fun clearCropCache(context: Context) {
        val path = File(context.getExternalFilesDir("images"), "camera")
        if (path.exists() && path.isDirectory) {
            for (child in path.listFiles()) {
                child.delete()
            }
        }
    }

    fun clearCrashCache(context: Context){
        val path = File(context.getExternalFilesDir(null), "crash")
        if (path.exists() && path.isDirectory) {
            for (child in path.listFiles()) {
                child.delete()
            }
        }
    }

    /**
     * Calling this will delete the images from cache directory
     * useful to clear some memory
     * Mode= 0:crash, 1:Crop Cache 2:ALL
     */
    fun clearCache(context: Context, mode: Int) {
        CacheClearAsyncTask(context, mode).execute()
    }

    /**
     * use weak ref to prevent memory leak
     * clearDiskCache 需在背景執行
     * clearMemory 需在 main thread 執行
     * https://stackoverflow.com/a/43909446
     */
    internal class CacheClearAsyncTask(context: Context, mode: Int) :
        AsyncTask<Void?, Void?, Void?>() {
        private val contextRef = WeakReference(context)
        private val modeRef = WeakReference(mode)

        override fun doInBackground(vararg params: Void?): Void? {
            when(modeRef.get()!!){
                0 -> {
                    clearCrashCache(contextRef.get()!!)
                }
                1 -> {
                    clearCropCache(contextRef.get()!!)
                }
                2 -> {
                    clearCrashCache(contextRef.get()!!)
                    clearCropCache(contextRef.get()!!)
                    Glide.get(contextRef.get()!!).clearDiskCache()
                    contextRef.get()!!.getSharedPreferences(Const.PREF_GUIDE, Context.MODE_PRIVATE).edit().clear().apply()
                }
            }

            return null
        }

        override fun onPostExecute(result: Void?) {
            Log.e("CacheClearAsyncTask")
            Glide.get(contextRef.get()!!).clearMemory()
            SysRepository.getInstance().getCurMemorySize().postValue(getCacheSize(contextRef.get()!!))
            Toast.makeText(contextRef.get(), "已清除快取", Toast.LENGTH_SHORT).show()
        }

    }

    //fake data for test
    fun getFakeJobList(jsonData: String): ArrayList<Mission> {
        val type: Type = object : TypeToken<List<Mission?>?>() {}.type
        val mList: ArrayList<Mission> = gson.fromJson(jsonData, type)
        return mList
    }

    fun loadJSONFromAsset(context: Context, fileName: String): String? {
        var json: String? = null
        try {
            val inst: InputStream = context.assets.open(fileName)
            val size: Int = inst.available()
            val buffer = ByteArray(size)
            inst.read(buffer)
            inst.close()
            json = String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    /**
     * @brief
     *      將UTC Time轉換成 Date Time
     */

    @SuppressLint("SimpleDateFormat")
    fun getDate(time: Long, format:String?="yyyy-MM-dd hh:mm:ss"): String {
        val sdf = SimpleDateFormat(format)
        val resultdate = sdf.format(Date(time))
        return resultdate
    }
    @SuppressLint("SimpleDateFormat", "NewApi")
    fun getDateForServer(time: Long): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ssX")
        val resultdate = sdf.format(Date(time))
        return resultdate
    }

    fun getDateToTimeStamp(str_date: String): Long {
        Log.d("str_date: " + str_date)
        if(str_date.isNullOrEmpty())
            return 0L
        var sdf: SimpleDateFormat?

        val date=
        try{
            sdf=SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            sdf.parse(str_date) as Date
        }catch (ex: ParseException){
            sdf=SimpleDateFormat("E MM dd yyyy hh:mm:ss")
            try {
                sdf.parse(str_date) as Date
            }catch (exx:ParseException){
                return BigDecimal(str_date).toLong()
            }

        }

        return date.time
    }

    fun getSecondsToHMmSs(seconds: Long): String? {
        val s = seconds % 60
        val m = seconds / 60 % 60
        val h = seconds / (60 * 60)
        return String.format("%d:%02d:%02d", h, m, s)
    }
    /**
     * @brief
     *      取得快取大小
     */

    fun getCacheSize(context: Context): String {
        var size: Long = 0
        size += getDirSize(File(context.getExternalFilesDir("images"), "camera"))
        size += getDirSize(File(context.getExternalFilesDir(null), "crash"))
        size += getDirSize(context.cacheDir)
        return readableFileSize(size)
    }

    /**
     * @ref https://stackoverflow.com/a/35488027/4935427
     */
    private fun getDirSize(dir: File): Long {
        var size: Long = 0
        if (dir != null && dir.isDirectory) {
            for (file in dir.listFiles()) {
                if (file != null && file.isDirectory) {
                    size += getDirSize(file)
                } else if (file != null && file.isFile) {
                    size += file.length()
                }
            }
        }
        return size
    }

    /**
     * @brief
     *      根據bytes大小轉單位
     *
     */

    private fun readableFileSize(size: Long): String {
        if (size <= 0) return "0 Bytes"
        val units =
            arrayOf("Bytes", "kB", "MB", "GB", "TB")
        val digitGroups =
            (Math.log10(size.toDouble()) / Math.log10(1024.0)).toInt()
        return DecimalFormat("#,##0.#").format(
            size / Math.pow(
                1024.0,
                digitGroups.toDouble()
            )
        ).toString() + " " + units[digitGroups]
    }

    /**
     * @brief
     *      設定mock gps
     *
     */

    fun setMock(locMgr: LocationManager) {
        //避免重複添加provider
        try {
            locMgr.addTestProvider(
                LocationManager.GPS_PROVIDER, false, false,
                false, false, true, true, true, 0, 5
            )
            locMgr.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true)
            locMgr.setTestProviderLocation(LocationManager.GPS_PROVIDER, getMockLocation())
        } catch (e: IllegalArgumentException) {
            Log.e(e)
        }catch (er: RuntimeException) {
            Log.e(er)
        }
    }

    /**
     * @brief
     *      取得假GPS位置
     *      經緯度直接在此設定
     */
    fun getMockLocation(): Location {
        Log.w("getMockLocation")
        val newLocation = Location(LocationManager.GPS_PROVIDER)
        newLocation.setLatitude(25.0574)
        newLocation.setLongitude(121.3600)
        newLocation.setAltitude(0.0)
        newLocation.setAccuracy(500F)
        newLocation.setTime(System.currentTimeMillis())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            newLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos())
        }
        return newLocation
    }

    /**
     * @brief
     *      取得resource的本地位址
     */
    fun getURLForResource(resourceId: Int): String? {
        //use BuildConfig.APPLICATION_ID instead of R.class.getPackage().getName() if both are not same
        return Uri.parse(
            "android.resource://" + R::class.java.getPackage()!!.getName() + "/" + resourceId
        ).toString()
    }

    /**
     * @brief
     *      將Drawable轉換成 marker所需的icon格式（BitmapDescriptor）
     *
     */
    fun getMarkerIconFromDrawable(drawable: Drawable): BitmapDescriptor? {
        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        canvas.setBitmap(bitmap)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    /**
     * @brief
     *      去掉數字前後不合理的0
     * @param string 數字字串
     * @return 數字字串
     */
    fun trimLeadingZeros(string: String): String? {
        var string = string
        var startsWithZero = true
        while (startsWithZero) {
            if (string.startsWith("0") && string.length >= 2 && !string.substring(1, 2)
                    .equals(".", ignoreCase = true)
            ) {
                string = string.substring(1)
            } else {
                startsWithZero = false
            }
        }
        return string
    }

    /**
     * @brief
     *      關閉虛擬鍵盤
     */
    fun hideKeyboardInAndroidFragment(view: View) {
        val imm: InputMethodManager = view.getContext()
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
    }


    const val KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates"


    /**
     * Returns the `location` object as a human readable string.
     * @param location  The [Location].
     */
    fun getLocationText(location: Location?): String? {
        return if (location == null) "Unknown location" else "(" + location.latitude + ", " + location.longitude + ")"
    }

    /**
     * @brief
     *      在debug模式中可用此方式寄送error到開發者信箱進行分析
     *
     */
    fun mailCrashLogIntent(mCon: Context):Intent{

        var mPath = File(mCon.getExternalFilesDir(null), "crash")
        val userSelectedImageUriList = ArrayList<Uri>()

        if (mPath.exists() && mPath.isDirectory()) {
            for (child in mPath.listFiles()) {
                child.setReadable(true, false)
                val crashURI: Uri = FileProvider.getUriForFile(
                    mCon,
                    mCon.packageName + ".provider",
                    child
                )
                userSelectedImageUriList.add(crashURI)
                Log.d(crashURI)
            }
        }

        if(userSelectedImageUriList.isEmpty())
            return Intent()

        val emailIntent = Intent(Intent.ACTION_SEND_MULTIPLE)
        // set the type to 'email'
        emailIntent.setType("text/plain")
        val to = arrayOf("SET_YOUR_MAIL_HERE")//此處設定要寄給的開發者
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
        // the attachment
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, userSelectedImageUriList)
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getDeviceName() + " ERR LOG")
        return Intent.createChooser(emailIntent, "Send email...")
    }

    /**
     * @brief
     *      抓取手機型號 用於寄送error report
     *
     */
    fun getDeviceName(): String? {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else capitalize(manufacturer) + " " + model
    }

    /**
     * @brief
     *       String轉換成大寫
     *
     */
    private fun capitalize(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true
        val phrase = java.lang.StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }
        return phrase.toString()
    }

    /**
     * @brief
     *       自動產生自定義開頭的假卡號
     *
     */
    fun getRandomAcc(title:String):String{
        val rand = Random()
        var card = title
        var sb =StringBuilder()
        for (i in 0..13) {
            val n = rand.nextInt(10) + 0
            card += Integer.toString(n)
        }
        for (i in 0..15) {
            if (i % 4 == 0) sb.append(" ")
            sb.append(card[i])
        }

        return sb.toString()
    }

    /**
     * @brief
     *       用於比較日期大小
     *
     */
    val MissionDateComparator =  Comparator<Mission> { a, b ->
        when {
            (a.finish_time?.let { getDateToTimeStamp(it) } == b.finish_time?.let { getDateToTimeStamp(it) }) -> 0
            (a.finish_time?.let { getDateToTimeStamp(it) }!! < b.finish_time?.let { getDateToTimeStamp(it) }!!) -> -1
            else -> 1
        }
    }

    fun getScreenHeight(activity: Activity): Int {
        val displayMetrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

    /**
     * @brief
     *       計算地圖上兩點"以走路方式"抵達需要的時間
     *
     */
    fun getMapTravelTime(from:LatLng, to:LatLng):String {
        var ret=""
        try{
            val context = GeoApiContext.Builder()
                .apiKey(BuildConfig.API_KEY)
                .build()

            val req = DistanceMatrixApi.newRequest(context)
            val origin = com.google.maps.model.LatLng(from.latitude, from.longitude)
            val destination = com.google.maps.model.LatLng(to.latitude, to.longitude)

            val trix = req.origins(origin)
                .destinations(destination)
                .mode(TravelMode.WALKING)
                .await()

            ret = trix.rows[0].elements[0].duration.humanReadable
            Log.d(ret)
        }catch (ex:Exception){
            Log.e(ex)
            return ret
        }
        return ret
    }

}
