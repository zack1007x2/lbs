package com.app.lbs.utils.drawroutemap;


import com.app.lbs.BuildConfig;
import com.google.android.gms.maps.model.LatLng;

public class FetchUrl {
    public static String getUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String key="key="+ BuildConfig.API_KEY;
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&"+key;
        String output = "json";
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }
}
