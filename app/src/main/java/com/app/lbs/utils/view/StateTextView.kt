package com.app.lbs.utils.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.TextView
import com.app.lbs.R
import com.app.lbs.utils.MyLog


/**
 * @file StateTextView
 * @brief
 *       客製化Textview 可多紀錄一種state
 *       用於記錄是否選擇過地點了
 *
 * @author Zack
 * */

class StateTextView(context: Context, attrs: AttributeSet) : androidx.appcompat.widget.AppCompatTextView(context, attrs),
    View.OnClickListener {
    val Log = MyLog.log()
    override fun onClick(v: View?) {
    }

    private val STATE_LOCATION_SELECTED = intArrayOf(R.attr.state_chose)

    private var chose: Boolean = false

    fun setLocationSelect(choose: Boolean) {
        Log.d("NewMission - setLocationSelect : $choose")
        this.chose = choose
        refreshDrawableState()
    }

    fun getLocationSelect(): Boolean {
        return this.chose
    }
}