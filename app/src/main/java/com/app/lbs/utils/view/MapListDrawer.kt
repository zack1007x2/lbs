package com.app.lbs.utils.view

import android.animation.Animator
import android.animation.Animator.AnimatorListener
import android.app.Activity
import android.os.Build
import android.os.Handler
import android.view.View
import android.view.ViewTreeObserver
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.adapters.AItemListAdapter
import com.app.lbs.utils.Utils.Log


/**
 * @file MapListDrawer
 * @brief
 *       底部可自動隱藏選單
 *       本專案中點擊cluster或marker時會出現的底部選單
 *
 * @author Zack
 * */

class MapListDrawer constructor(view: View?, mainActivity: FragmentActivity?) {

    private var handlCountDown: Handler? = null
    private var bottomDrawer: View? = null
    private var activity: Activity
    private var mView: View
    private var direct: Int = 0
    private var color: Int = 0
    private var mainlayoutHeight: Int = 0
    private var currentDrawer = -1
    private var isSwitched = false
    private lateinit var drawerListView: RecyclerView
    private var mainLayout: ConstraintLayout? = null


    companion object {
        const val waitMS = 30000
        const val DRAWER_UP = 1
        const val DRAWER_DOWN = 0
    }
    private enum class S {
        OPEN_NOW, OPEN, CLOSE_NOW, CLOSE, CANCELED_NOW, CANCEL, TIME_OFF
    }     // States of animation

    private var animState =
        S.CLOSE                                                        // Set state

   init {
        this@MapListDrawer.mView = view!!
        this@MapListDrawer.activity = mainActivity!!
        initialize()
        getLayoutHeight()
    }

    private fun initialize() {
        // Bottom Drawer
        bottomDrawer = mView.findViewById(R.id.bottom_drawer)
//        drawerTxt = bottomDrawer!!.findViewById(R.id.drawer_txt)


        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        drawerListView = mView.findViewById(R.id.drawer_list) as RecyclerView
        drawerListView.layoutManager = layoutManager

        // Handler for timing for automatically closing the drawer
        handlCountDown = Handler()
    }

    private fun drawerMovement(movement: Int) {
        when (movement) {
            DRAWER_UP
            -> {
                //此處調整drawer高度
                val heightStatusMenu = if(mView.tag == R.layout.fragment_find_mission_by_map)
                    activity.resources.getDimension(R.dimen.size_350)else activity.resources.getDimension(R.dimen.size_320)
                bottomDrawer!!.animate().translationY(mainlayoutHeight - heightStatusMenu)
                    .setListener(animationListener())
                direct = DRAWER_UP
            }

            DRAWER_DOWN
            -> {
                bottomDrawer!!.animate().translationY(mainlayoutHeight.toFloat())
                    .setListener(animationListener())
                direct = DRAWER_DOWN
            }
        }
    }

    private inner class animationListener : AnimatorListener {

        override fun onAnimationStart(animation: Animator) {
            if (direct == DRAWER_UP && animState != S.CANCELED_NOW && animState != S.CANCEL && animState != S.TIME_OFF) animState =
                S.OPEN_NOW
            if (direct == DRAWER_DOWN && animState != S.CANCELED_NOW && animState != S.CANCEL && animState != S.TIME_OFF) animState =
                S.CLOSE_NOW
            Log.d( "Start Animation: $animState")

            // Turning off the automatic timer closing drawer
            handlCountDown!!.removeCallbacks(closeDrawerTimer)
        }

        override fun onAnimationEnd(animation: Animator) {
            if (direct == DRAWER_UP && animState != S.CANCELED_NOW && animState != S.CANCEL) {
                animState = S.OPEN

                // Turning on the automatic timer closing drawer
                handlCountDown!!.postDelayed(closeDrawerTimer, waitMS.toLong())
            }
            if (direct == DRAWER_DOWN && animState != S.CANCELED_NOW && animState != S.CANCEL) animState =
                S.CLOSE
            Log.d( "End Animation: $animState")

            // Animation Cancel
            if (animState == S.CANCELED_NOW) {
                if (direct == DRAWER_UP) {
                    Log.d( "Animation Cancel - DIRECT UP: $animState")
                    drawerMovement(DRAWER_DOWN)
                    animState = S.CANCEL
                } else { // DIRECT DOWN
                    Log.d( "Animation Cancel - DIRECT DOWN: $animState")
                    animState = S.CANCEL
                }
            }

            if (animState != S.CANCELED_NOW && animState != S.CANCEL && animState != S.TIME_OFF)
                switchToNewDrawer(currentDrawer)

            // Close Drawer after animation cancel
            if (animState == S.CANCEL) {
                if (animState == S.CLOSE) refreshData(currentDrawer)
                animState = S.OPEN_NOW
                drawerMovement(DRAWER_UP)
                Log.d( "Animation Cancel")
            }
        }

        override fun onAnimationCancel(animation: Animator) {
            animState = S.CANCELED_NOW
            isSwitched = true
        }

        override fun onAnimationRepeat(animation: Animator) {}
    }

    fun switchDrawer(selectedDrawer: Int) {
        when (animState) {
            S.CLOSE // ------------------------------------------------------------------------- DRAWER UP
            -> {
                refreshData(selectedDrawer)
                drawerMovement(DRAWER_UP)
            }

            S.OPEN // -------------------------------------------------------------------------- DRAWER DOWN
            -> if (selectedDrawer != currentDrawer) {
                drawerMovement(DRAWER_DOWN)
                this.isSwitched = true
            }

            S.OPEN_NOW // ---------------------------------------------------------------------- DRAWER is OPENING NOW
            -> if (selectedDrawer != currentDrawer) {
                drawerMovement(DRAWER_DOWN)
            }

            S.CLOSE_NOW // --------------------------------------------------------------------- DRAWER is CLOSING NOW
            -> if (selectedDrawer != currentDrawer) {
                drawerMovement(DRAWER_UP)
            }

            S.TIME_OFF // ---------------------------------------------------------------------- Closing the drawer because time is over
            -> drawerMovement(DRAWER_DOWN)
        }
        currentDrawer = selectedDrawer
    }

    private fun switchToNewDrawer(currentDrawer: Int) {
        if (this.isSwitched) {
            Log.d("Switch Drawer $currentDrawer")
            refreshData(currentDrawer)
            switchDrawer(currentDrawer)
            this.isSwitched = false
        }
    }

    private fun refreshData(currentDrawer: Int) {
//        bottomDrawer!!.setBackgroundColor(ContextCompat.getColor(activity, color))
//        val drawerDescr = "Drawer " + Integer.toString(currentDrawer)
//        drawerTxt!!.text = drawerDescr
    }

    private fun closeDrawer() {
        animState = S.TIME_OFF
        this.isSwitched = false
        drawerMovement(DRAWER_DOWN)

        // Turning on the automatic timer closing drawer
        handlCountDown!!.postDelayed(closeDrawerTimer, waitMS.toLong())
    }

    fun closeDrawerNow() {
        animState = S.TIME_OFF
        this.isSwitched = false
        drawerMovement(DRAWER_DOWN)
    }

    private fun getLayoutHeight() {
        mainLayout = activity.findViewById(R.id.container) as ConstraintLayout
        val vto = mainLayout!!.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                mainlayoutHeight = mainLayout!!.measuredHeight
                if(bottomDrawer==null)
                    bottomDrawer = activity.findViewById(R.id.bottom_drawer)
                bottomDrawer!!.y = mainlayoutHeight.toFloat()
                Log.d("Layout Height: $mainlayoutHeight")
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    mainLayout!!.viewTreeObserver.removeGlobalOnLayoutListener(this)
                } else {
                    mainLayout!!.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            }
        })
    }

    // Automatically closes drawer after a set time
    private val closeDrawerTimer = Runnable { closeDrawer() }
    fun setAdapter(mAdapter:AItemListAdapter<*>){
        drawerListView.adapter = mAdapter
    }
}