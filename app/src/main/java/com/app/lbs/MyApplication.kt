package com.app.lbs

import android.app.Application
import com.app.lbs.utils.CrashHandler

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        val crashHandler = CrashHandler.instance
        crashHandler.init(this)
    }


}