package com.app.lbs.interfaces

import com.google.gson.JsonElement
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * @file NearByLocationApi
 * @property Interface
 * @brief
 *       用於Call google map web api
 *       由於 map api for android 限制較 web 多
 *       因此使用web api取代
 *
 * @author Zack
 **/
interface NearByLocationApi {

    @GET("/maps/api/place/nearbysearch/json")
    fun getLocations(
        @Query("keyword")query: String?,
        @Query("language")language:String,
        @Query("location") location: String,
        @Query("radius") radius: Int
    ): Call<JsonElement>
}