package com.app.lbs.interfaces

import android.view.View

/** @file IOnItemClickListener
 *  @property Interface
 *  @brief
 *        callback可二選一 return model或原本onclick的parameters
 *
 * @author Zack
 * @date 20200519 */

interface IOnItemClickListener<E> {

    fun onItemClick(item_model: E, position: Int)
    fun onItemClick(view: View, position: Int)
}