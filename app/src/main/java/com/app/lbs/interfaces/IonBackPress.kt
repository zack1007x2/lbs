package com.app.lbs.interfaces


/**
 * @file IonBackPress
 * @property Interface
 * @brief
 *       用於監聽activity onBack活動
 *
 * @author Zack
 **/


interface IonBackPress {
    //return true if allow backpress, false to block backpress
    fun onBackPressed():Boolean
}