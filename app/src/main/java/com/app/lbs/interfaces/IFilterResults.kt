package com.app.lbs.interfaces

/**
 * @file IFilterResults
 * @property Interface
 * @brief
 *       搜尋欄的結果callback listener
 *
 * @author Zack
 * */

interface IFilterResults {
    fun ResultPublished(results: ArrayList<*>)
    fun ResultPublished(results: Array<*>)
}