package com.app.lbs.interfaces

/**
 * @file IDialogValueListener
 * @property Interface
 * @brief
 *       萬用 callback
 *
 * @author Zack
 * */
interface IDialogValueListener<E>{
    fun onReturnValue(ret: E)
}
