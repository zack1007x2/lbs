package com.app.lbs.interfaces
/**
 * @file OnStateChangedListener
 * @property Interface
 * @brief
 *       取得permission result code
 *
 * @author Zack
 * */

interface OnStateChangedListener {
    fun onStateChanged(state:Int)
}