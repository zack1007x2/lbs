package com.app.lbs.interfaces

import com.app.lbs.utils.QueryBody
import com.google.gson.JsonElement
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


/**
 * @file ApiInterface
 * @property Interface
 * @brief
 *       call server API 介面
 *
 * @author Zack
 * */

interface ApiInterface {

    companion object {
        const val BASE = "https://ie53124.ie.nthu.edu.tw/lbs-server/"
        const val BASE_SECURE = "https://ie53124.ie.nthu.edu.tw/lbs-server/"
        const val TYPE_FB = "facebook"
        const val TYPE_GOO = "google"
    }

    @GET(BASE_SECURE+"login-url/{type}")
    fun getSecureOAuth2LoginUrl(@Path("type") type: String): Call<JsonElement>

    @POST("GetOAuth2LoginUrl/{type}")
    fun getOAuth2LoginUrl(@Path("type") type: String, @Body param: QueryBody): Call<JsonElement>

    @POST("LoginByOAuth2Toekn/{type}")
    fun loginByOAuth2Token(@Path("type") type: String, @Body param: QueryBody): Call<JsonElement>

    @POST("sms_validation")
    fun getSMSValidation(@Body param: QueryBody): Call<JsonElement>

    @POST("GetCountryData")
    fun getCountryData(@Body param: QueryBody): Call<JsonElement>

    @POST("GetCityData")
    fun getCityData(@Body param: QueryBody): Call<JsonElement>

    @POST("GetAllBusinessDistrict")
    fun getAllBusinessDistrict(@Body param: QueryBody): Call<JsonElement>

    @POST("UpdateBusinessDistrict")
    fun updateBusinessDistrict(@Body param: QueryBody): Call<JsonElement>

    @POST("RetrieveJobList")
    fun retrieveJobList(@Body param: QueryBody): Call<JsonElement>

    @POST("RenewSubDB")
    fun renewSubDB(@Body param: QueryBody): Call<JsonElement>

    @POST("CreateNewJob")
    fun createNewJob(@Body param: QueryBody): Call<JsonElement>

    @POST("TakeJob")
    fun takeJob(@Body param: QueryBody): Call<JsonElement>

    @POST("FinishJob")
    fun finishJob(@Body param: QueryBody): Call<JsonElement>

    @POST("CancelJob")
    fun cancelJob(@Body param: QueryBody): Call<JsonElement>

    @POST("GetStoreList")
    fun getStoreList(@Body param: QueryBody): Call<JsonElement>


    @POST("CreateNewStore")
    fun createNewStore(@Body param: QueryBody): Call<JsonElement>


}