package com.app.lbs.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.Nullable
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.ImageModel
import com.app.lbs.utils.MyLog
import com.app.lbs.utils.Utils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

/**
 * @file HorizontalRecyclerViewAdapter
 * @brief
 *       用於新建任務時的橫向圖片序列
 * @param showAdd: 是否顯示第一張"＋"用於新增圖片的item
 * @author Zack
 * */

class HorizontalRecyclerViewAdapter(
    private val context: Context?,
    @Nullable val showAdd: Boolean = true
) :
    RecyclerView.Adapter<HorizontalRecyclerViewAdapter.ImageViewHolder>() {

    private var Log = MyLog.log()
    private var imageModelArrayList: ArrayList<ImageModel> = ArrayList()
    private var mOnItemClickListener: IOnItemClickListener<ImageModel>? = null


    override fun getItemCount(): Int {
        return imageModelArrayList.size
    }

    fun getList(): ArrayList<ImageModel> {
        return imageModelArrayList
    }

    fun setList(list: ArrayList<ImageModel>) {
        imageModelArrayList = list
    }

    fun setOnItemClickListener(listener: IOnItemClickListener<ImageModel>) {
        mOnItemClickListener = listener
    }

    init {
        if(showAdd)
            imageModelArrayList.add(ImageModel(0, Utils.getURLForResource(R.drawable.fab_add)))
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val model = imageModelArrayList[position]

        if(model.getImagePath()==Utils.getURLForResource(R.drawable.fab_add)){
            holder.imageView.background=null
            Glide.with(holder.itemView.context)
                .load(model.getImagePath())
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView)
        }else{
            Glide.with(holder.itemView.context)
                .load(model.getImagePath())
                .transform(RoundedCorners(context!!.resources.getDimensionPixelSize(R.dimen.size_10)))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(object : CustomTarget<Drawable>() {
                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        holder.imageView.setImageResource(android.R.color.transparent)
                        holder.imageView.background=resource
                    }

                    override fun onLoadCleared(@Nullable placeholder: Drawable?) {}
                })
        }

        if (mOnItemClickListener != null) {
            holder.imageView.setOnClickListener {
                mOnItemClickListener?.onItemClick(model, position)
            }
        }

    }


    inner class ImageViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {

        internal var imageView: ImageView

        init {
            imageView = view.findViewById(R.id.imageView) as ImageView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_image_slide, parent, false)
        return ImageViewHolder(itemView)
    }
}