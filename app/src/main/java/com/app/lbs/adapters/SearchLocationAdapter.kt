package com.app.lbs.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.map_models.LocationModel
import com.app.lbs.utils.MyLog
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.PlaceLikelihood

/**
 * @file SearchLocationAdapter
 * @brief
 *       地圖搜尋列表適配器
 * @param
 *          mLocationList:要顯示的附近地點的搜尋結果列表
 *          isOwner:根據使用者狀態決定背景顏色
 *          mFavMap:我的最愛列表，call by copy ref 所以在此修改map即可
 *
 * @author Zack
 * */


class SearchLocationAdapter(private val context: Context?, private var mLocationList: ArrayList<LocationModel>? = arrayListOf(), private var isResult: Boolean?=false
                            , private var isOwner: Boolean=false, var mFavMap:HashMap<String, LocationModel>): RecyclerView.Adapter<SearchLocationAdapter.SearchResultViewHolder>() {

    private var Log = MyLog.log()
    private var mListener:IOnItemClickListener<LocationModel>?=null
    private var mOnCheckListener : CompoundButton.OnCheckedChangeListener?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchLocationAdapter.SearchResultViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_search_location, parent, false)
        return SearchResultViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        val item = mLocationList!![position]
        holder.itemView.setOnClickListener {
            mListener?.onItemClick(item, position)
        }
        holder.tv_location_item!!.text = item.name
        holder.tv_location_item_addr!!.text = item.addr
        holder.cb_location_save!!.setOnCheckedChangeListener(mOnCheckListener)
        holder.cb_location_save!!.tag = item
        holder.cb_location_save!!.isChecked = mFavMap.containsKey(item.place_id)
    }

    override fun getItemCount(): Int {
        return if(mLocationList==null) 0 else mLocationList!!.size
    }


    inner class SearchResultViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {

        var tv_location_item: TextView? = null
        var cb_location_save: CheckBox? = null
        var tv_location_item_addr:TextView?=null

        init {
            tv_location_item = view.findViewById(R.id.tv_location_item)
            cb_location_save = view.findViewById(R.id.cb_location_save)
            tv_location_item_addr = view.findViewById(R.id.tv_location_item_addr)
            if(isOwner)
                cb_location_save!!.buttonDrawable=context!!.resources.getDrawable(R.drawable.star_checkbox_selector_owner, null)
            else
                cb_location_save!!.buttonDrawable=context!!.resources.getDrawable(R.drawable.star_checkbox_selector_taker, null)
        }
    }

    fun getItem(id: Int): LocationModel {
        return mLocationList!![id]
    }

    fun addPredicList(mList: MutableList<AutocompletePrediction>){
        mList.forEach outer@ { i->
            Log.e("NewMissionSelectLocation: "+i.getPrimaryText(null).toString())
            val tmp = LocationModel(i.placeId, i.getPrimaryText(null).toString(), i.getSecondaryText(null).toString(), null, null)
            mLocationList?.forEach{ j->
                if(j.place_id==i.placeId)
                    return@outer
            }
            mLocationList?.add(tmp)
        }
        notifyDataSetChanged()
    }

    fun setNearList(mList: List<PlaceLikelihood>){
        val clist = arrayListOf<LocationModel>()
        for(i in mList){
            val tmp = LocationModel(i.place.id, i.place.name, i.place.address, i.place.latLng?.latitude, i.place.latLng?.longitude)
            clist.add(tmp)
        }
        mLocationList = clist
        notifyDataSetChanged()
    }


    fun setList(mList: ArrayList<LocationModel>){
        mLocationList = mList
        notifyDataSetChanged()
    }

    fun setFavoriteList(map:HashMap<String, LocationModel>){
        mLocationList?.clear()
        val clist = arrayListOf<LocationModel>()
        for (value in map.values) {
            clist.add(value)
        }

        mLocationList = clist
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(listener: IOnItemClickListener<LocationModel>){
        mListener =listener
    }

    fun setOnCheckChangeListener(checkListener : CompoundButton.OnCheckedChangeListener){
        mOnCheckListener = checkListener
    }
}