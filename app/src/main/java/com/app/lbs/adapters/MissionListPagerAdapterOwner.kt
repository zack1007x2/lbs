package com.app.lbs.adapters

import android.content.Context
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.app.lbs.R
import com.app.lbs.ui.fragments.MissionUnstartFragment
import com.app.lbs.ui.fragments.MissionOnGoingFragment
import com.app.lbs.ui.fragments.MissionHistoryFragment
import com.app.lbs.ui.fragments.NewMission
import com.app.lbs.utils.MyLog


/**
 * @file MissionListPagerAdapterOwner
 * @brief
 *      Mission Management頁面的分頁選項
 *
 * @author Zack
 * */

private val TAB_TITLES_OWNER = arrayOf(
    R.string.title_add_mission,
    R.string.title_mission_prepare,
    R.string.title_mission_ongoing,
    R.string.title_mission_history
)

class MissionListPagerAdapterOwner(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){
//TODO 通知小紅點    , PagerSlidingTabStrip.CustomTabProvider {
    private var mNewMissionFragment: NewMission = NewMission()
    private var mMissionHistoryFragment: MissionHistoryFragment = MissionHistoryFragment()
    private var mMissionOnGoingFragment: MissionOnGoingFragment = MissionOnGoingFragment()
    private var mMissionUnstartFragment: MissionUnstartFragment = MissionUnstartFragment()
    private var Log = MyLog.log()

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Log.d("getItem : " + position)
        return when (position) {
            0 -> mNewMissionFragment
            1 -> mMissionUnstartFragment
            2 -> mMissionOnGoingFragment
            3 -> mMissionHistoryFragment
            else -> mNewMissionFragment
        }


    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES_OWNER[position])
    }

    override fun getCount(): Int {
        return TAB_TITLES_OWNER.size
    }

    override fun getItemPosition(ob: Any): Int {
        return POSITION_NONE
    }
    override fun saveState(): Parcelable? {
        return null
    }
}