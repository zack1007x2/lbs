package com.app.lbs.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.map_models.LocationModel
import com.app.lbs.utils.MyLog
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.PlaceLikelihood

/**
 * @file NearByLocationAdapter
 * @brief
 *      用於列出附近地點列表
 *
 * @param
 *          mLocationList:要顯示的附近地點的搜尋結果列表
 *          isOwner:根據使用者狀態決定背景顏色
 *          mFavMap:我的最愛列表，call by copy ref 所以在此修改map即可
 *
 * @author Zack
 **/

class NearByLocationAdapter(private val context: Context?, mLocationList: ArrayList<LocationModel>?,
                            private var isOwner: Boolean=false, var mFavMap:HashMap<String, LocationModel>):
    RecyclerView.Adapter<NearByLocationAdapter.NearByResultViewHolder>() {

    private var Log = MyLog.log()
    private var mListener:IOnItemClickListener<LocationModel>?=null
    var mList: ArrayList<LocationModel>? = mLocationList
    private var mOnCheckListener : CompoundButton.OnCheckedChangeListener?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NearByLocationAdapter.NearByResultViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_search_location, parent, false)
        return NearByResultViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: NearByResultViewHolder, position: Int) {
        val item = mList!![position]
        holder.itemView.setOnClickListener {
            mListener?.onItemClick(item, position)
        }
        holder.tv_location_item!!.text = item.name
        holder.tv_location_item_addr!!.text = item.addr
        holder.cb_location_save!!.setOnCheckedChangeListener(mOnCheckListener)
        holder.cb_location_save!!.tag = item
        holder.cb_location_save!!.isChecked = mFavMap.containsKey(item.place_id)
    }

    override fun getItemCount(): Int {
        return if(mList==null) 0 else mList!!.size
    }


    inner class NearByResultViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {

        var tv_location_item: TextView? = null
        var cb_location_save: CheckBox? = null
        var tv_location_item_addr:TextView?=null

        init {
            tv_location_item = view.findViewById(R.id.tv_location_item)
            cb_location_save = view.findViewById(R.id.cb_location_save)
            tv_location_item_addr = view.findViewById(R.id.tv_location_item_addr)
            if(isOwner)
                cb_location_save!!.buttonDrawable=context!!.resources.getDrawable(R.drawable.star_checkbox_selector_owner, null)
            else
                cb_location_save!!.buttonDrawable=context!!.resources.getDrawable(R.drawable.star_checkbox_selector_taker, null)
        }
    }

    fun getItem(id: Int): LocationModel {
        return mList!![id]
    }

    fun addPredicList(list: MutableList<AutocompletePrediction>){
        val tmp_list = arrayListOf<LocationModel>()
        for(i in list){
            Log.e("NewMissionSelectLocation: "+i.getPrimaryText(null).toString())
            val tmp = LocationModel(i.placeId, i.getPrimaryText(null).toString(), i.getSecondaryText(null).toString(), null, null)
            tmp_list.add(tmp)
        }
        mList?.clear()
        mList?.addAll(tmp_list)
        notifyDataSetChanged()
    }

    fun setNearList(list: List<PlaceLikelihood>){
        val clist = arrayListOf<LocationModel>()
        for(i in list){
            val tmp = LocationModel(i.place.id, i.place.name, i.place.address, i.place.latLng?.latitude, i.place.latLng?.longitude)
            clist.add(tmp)
        }
        mList?.clear()
        mList?.addAll(clist)
        notifyDataSetChanged()
    }


    fun setList(list: ArrayList<LocationModel>){
        mList?.clear()
        mList?.addAll(list)
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(listener: IOnItemClickListener<LocationModel>){
        mListener =listener
    }

    fun setOnCheckChangeListener(checkListener : CompoundButton.OnCheckedChangeListener){
        mOnCheckListener = checkListener
    }



}