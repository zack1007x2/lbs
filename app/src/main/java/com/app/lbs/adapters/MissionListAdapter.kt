package com.app.lbs.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.CountDownTimer
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.interfaces.IOnItemClickListener
import com.app.lbs.models.Mission
import com.app.lbs.models.UserData
import com.app.lbs.utils.Const
import com.app.lbs.utils.MyLog
import com.app.lbs.utils.Utils
import com.app.lbs.utils.view.CircularCounter
import com.google.android.material.imageview.ShapeableImageView


/**
 * @file MissionListAdapter
 * @brief
 *       管理各種以Mission為model的各類missionList項目顯示
 *       list_type ------- TYPE_MAIN_LIST: 顯示於FindMissionByMap底部菜單的 mission item
 *                     |
 *                     |-- TYPE_POP_CLUSTER_LIST:顯示於FindMissionByMap中點擊地圖出現的橫向列表的 mission item
 *                     |
 *                     |-- TYPE_MISSION_LIST:顯示於任務管理中"進行中","申請中","未開始"等狀態的 mission item
 *                     |
 *                     |-- TYPE_NOTIFICATION:顯示於通知中的 mission item
 *                     |
 *                     |-- TYPE_FIN_LIST:顯示於任務管理中"歷史任務"狀態的 mission item
 * @param
 *          missionList:要顯示的任務列表
 *          list_type:顯示的列表類型，預設為任務管理列表
 *          usetData:根據列表類型決定是否要帶入userdata資訊，目前只有通知列表會用到
 *
 * @author Zack
 * */

class MissionListAdapter(
    private var mContext: Context,
    private var missionList: List<Mission>?,
    @Nullable val list_type: Int = 7648,
    @Nullable val usetData:UserData?=null
) :
    AItemListAdapter<RecyclerView.ViewHolder>(), View.OnClickListener {

    companion object {
        const val TYPE_MAIN_LIST = 4562
        const val TYPE_POP_CLUSTER_LIST = 3463
        const val TYPE_MISSION_LIST = 7648
        const val TYPE_NOTIFICATION = 2783
        const val TYPE_FIN_LIST = 4753
    }

    private var mState: Const.USERSTATE = Const.USERSTATE.OWNER
    val handler = Handler()


    var mPopularityVisable: Boolean = false
        set(value) {
            field = value
        }

    override fun onClick(v: View) {
        try {
            if (v.tag != null)
                mOnItemClickListener?.onItemClick(v, v.tag as Int)
        } catch (e: TypeCastException) {
            Log.e("Tag: " + v.tag)
            Log.e(e)
        }

    }

    val Log = MyLog.log()
    private var mOnItemClickListener: IOnItemClickListener<Mission>? = null

    inner class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvItemMissionTitle: TextView =
            itemView.findViewById(R.id.tv_item_mission_title) as TextView
        var counter: CircularCounter = itemView.findViewById(R.id.counter_timer)
        var tvItemTime: TextView = itemView.findViewById(R.id.tv_item_time) as TextView
        var tvContentMissionLoc: TextView =
            itemView.findViewById(R.id.tv_content_mission_loc) as TextView
        var tvContentAbortLoc: TextView =
            itemView.findViewById(R.id.tv_content_abort_loc) as TextView
        var tvPrice: TextView = itemView.findViewById(R.id.tv_price) as TextView
        var tv_popularity: TextView = itemView.findViewById(R.id.tv_popularity) as TextView
        var img00: ShapeableImageView =itemView.findViewById(R.id.img_5_00)
        var img01: ShapeableImageView =itemView.findViewById(R.id.img_5_01)
        var img02: ShapeableImageView =itemView.findViewById(R.id.img_5_02)
        var img03: ShapeableImageView =itemView.findViewById(R.id.img_5_03)
        var img04: ShapeableImageView =itemView.findViewById(R.id.img_5_04)
        var timer: CountDownTimer?=null

    }

    inner class MapViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvItemMissionTitle: TextView =
            itemView.findViewById(R.id.tv_item_mission_title) as TextView
        var tvContentMissionLoc: TextView =
            itemView.findViewById(R.id.tv_content_mission_loc) as TextView
        var tvContentAbortLoc: TextView =
            itemView.findViewById(R.id.tv_content_abort_loc) as TextView
        var tvPrice: TextView = itemView.findViewById(R.id.tv_price) as TextView
        var tv_popularity: TextView = itemView.findViewById(R.id.tv_popularity) as TextView
    }

    inner class MissionListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvItemMissionTitle: TextView =
            itemView.findViewById(R.id.tv_item_mission_title) as TextView
        var counter: CircularCounter = itemView.findViewById(R.id.counter_timer)
        var timer: CountDownTimer?=null
        var tvItemTime: TextView = itemView.findViewById(R.id.tv_item_time) as TextView
        var tv_mission_status:TextView=itemView.findViewById(R.id.tv_mission_status)
        var rl_reason:LinearLayout=itemView.findViewById(R.id.rl_reason)
    }
    inner class MissionFinListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvItemMissionTitle: TextView =
            itemView.findViewById(R.id.tv_item_mission_title) as TextView
        var tv_item_taker_content: TextView = itemView.findViewById(R.id.tv_item_taker_content) as TextView
        var tv_mission_status:TextView=itemView.findViewById(R.id.tv_mission_status)
    }

    inner class NotificationListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_noti_type: TextView = itemView.findViewById(R.id.tv_noti_type) as TextView
        var tv_item_mission_title: TextView = itemView.findViewById(R.id.tv_item_mission_title)
        var tv_status_subtitle: TextView =
            itemView.findViewById(R.id.tv_status_subtitle) as TextView
        var tv_status_subcontent: TextView =
            itemView.findViewById(R.id.tv_status_subcontent) as TextView
        var tv_more_info_title: TextView =
            itemView.findViewById(R.id.tv_more_info_title) as TextView
        var img_item_mission_type: ImageView =
            itemView.findViewById(R.id.img_item_mission_type) as ImageView
        var tv_more_info_content: TextView =
            itemView.findViewById(R.id.tv_more_info_content) as TextView
        var rl_modify_bottons: RelativeLayout =
            itemView.findViewById(R.id.rl_modify_bottons) as RelativeLayout
        var msg_modify_agree: Button = itemView.findViewById(R.id.msg_modify_agree) as Button
        var msg_modify_disagree: Button = itemView.findViewById(R.id.msg_modify_disagree) as Button
        var ll_more_info: LinearLayout = itemView.findViewById(R.id.ll_more_info)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (list_type) {
            TYPE_MAIN_LIST -> {
                return MainViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_mission, parent, false)
                )
            }
            TYPE_POP_CLUSTER_LIST -> {
                return MapViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_mission_hori, parent, false)
                )
            }
            TYPE_MISSION_LIST -> {
                return MissionListViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_mission_manage, parent, false)
                )
            }
            TYPE_NOTIFICATION -> {
                return NotificationListViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_notification, parent, false)
                )
            }
            else->{
                return MissionFinListViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_mission_manage_fin, parent, false)
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return missionList?.size ?: return 0
    }

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val curMission = this.missionList!![position]
        //fake data

        if (curMission.finish_time ==""
            || curMission.finish_time!!.toBigDecimalOrNull()==null
            || curMission.finish_time!!.toBigDecimalOrNull()!!.toLong() <= 0L) {
            try {
                //      curMission.finish_time=(System.currentTimeMillis()+86400000+Math.random()*43200000).toString()
                curMission.start_time = System.currentTimeMillis().toString()
                curMission.finish_time =
                    (System.currentTimeMillis() + Math.random() * 320000).toString()
                curMission.duration = (curMission.finish_time!!.toBigDecimal()
                    .toLong() - curMission.start_time.toLong()).toInt()
            } catch (e: Exception) {
//          curMission.finish_time=(System.currentTimeMillis()+86400000+Math.random()*43200000).toString()
                curMission.start_time = System.currentTimeMillis().toString()
                curMission.finish_time =
                    (System.currentTimeMillis() + Math.random() * 320000).toString()
                curMission.duration = (curMission.finish_time!!.toBigDecimal()
                    .toLong() - curMission.start_time.toLong()).toInt()
            }
        }


        when (list_type) {
            TYPE_MAIN_LIST -> {
                holder as MainViewHolder
                holder.counter.setFirstWidth(mContext.resources.getDimension(R.dimen.size_2))

                holder.counter.setTextColor(if(mState==Const.USERSTATE.OWNER){
                    ContextCompat.getColor(mContext, R.color.owner_red)
                }else {
                    ContextCompat.getColor(mContext, R.color.taker_blue)
                })
                holder.counter.setFirstColor(if(mState==Const.USERSTATE.OWNER){
                    ContextCompat.getColor(mContext, R.color.owner_red)
                }else {
                    ContextCompat.getColor(mContext, R.color.taker_blue)
                })
                holder.counter.setBackgroundColor(
                    ContextCompat.getColor(mContext, R.color.colorMainBrightWhite)
                )
                try {
                    var last = (curMission.finish_time!!.toBigDecimal()
                        .toLong() - System.currentTimeMillis())
                    holder.counter.setRange(curMission.duration*20)
                    if (holder.timer != null) {
                        holder.timer!!.cancel()
                    }
                    holder.timer = object:CountDownTimer(last, 1000){
                        override fun onFinish() {
                            holder.counter.setValues(0, 0, 0)
                            curMission.finish_time=""
                        }

                        override fun onTick(millisUntilFinished: Long) {
                            holder.counter.setValues(millisUntilFinished.toInt(), 0, 0)
                        }

                    }.start()

                } catch (e: NumberFormatException) {
                    Log.e("TIMMER: "+e)
                    //時間格式不統一 造成資料轉換錯誤 且server資料無法更改
                }
                holder.tvItemMissionTitle.text = curMission.title
                holder.tvItemTime.text = Utils.getDate(curMission.start_time.toLong())
                holder.tvContentMissionLoc.text = curMission.store_addr
                holder.tvContentAbortLoc.text = curMission.addr2coor
                holder.tvPrice.text = curMission.salary.toString()

                val pu: Int = curMission.yelp_rank.toIntOrNull() ?: -1
                when {
                    pu < 0 -> holder.tv_popularity.text = "0"
                    pu in 0..100 -> holder.tv_popularity.text = pu.toString()
                    else -> holder.tv_popularity.text = "100+"
                }


                //TODO imagelist
                when(position){
                    0->{
                        holder.img00.visibility=View.GONE
                        holder.img01.visibility=View.VISIBLE
                        holder.img02.visibility=View.GONE
                        holder.img03.visibility=View.GONE
                        holder.img04.visibility=View.GONE
                    }
                    1->{
                        holder.img00.visibility=View.GONE
                        holder.img01.visibility=View.VISIBLE
                        holder.img02.visibility=View.VISIBLE
                        holder.img03.visibility=View.GONE
                        holder.img04.visibility=View.GONE
                    }
                    2->{
                        holder.img00.visibility=View.GONE
                        holder.img01.visibility=View.VISIBLE
                        holder.img02.visibility=View.VISIBLE
                        holder.img03.visibility=View.GONE
                        holder.img04.visibility=View.VISIBLE
                    }
                    3->{
                        holder.img00.visibility=View.GONE
                        holder.img01.visibility=View.VISIBLE
                        holder.img02.visibility=View.VISIBLE
                        holder.img03.visibility=View.VISIBLE
                        holder.img04.visibility=View.VISIBLE
                    }
                    else->{
                        holder.img00.visibility=View.VISIBLE
                        holder.img01.visibility=View.VISIBLE
                        holder.img02.visibility=View.VISIBLE
                        holder.img03.visibility=View.VISIBLE
                        holder.img04.visibility=View.VISIBLE
                    }

                }

            }


            TYPE_POP_CLUSTER_LIST -> {
                holder as MapViewHolder
                holder.tvItemMissionTitle.text = curMission.title
                holder.tvContentMissionLoc.text = curMission.store_addr
                holder.tvContentAbortLoc.text = curMission.addr2coor
                holder.tvPrice.text = curMission.salary.toString()
                val pu: Int = curMission.yelp_rank.toIntOrNull() ?: -1
                when {
                    pu < 0 -> holder.tv_popularity.text = "0"
                    pu in 0..100 -> holder.tv_popularity.text = pu.toString()
                    else -> holder.tv_popularity.text = "100+"
                }
            }

            TYPE_MISSION_LIST -> {
                holder as MissionListViewHolder
                holder.tvItemMissionTitle.text = curMission.title
                Log.e("curMission.start_time : " + curMission.start_time)
                holder.tvItemTime.text = Utils.getDate(curMission.start_time.toLong())
                holder.tv_mission_status.background

                holder.counter.setFirstWidth(mContext.resources.getDimension(R.dimen.size_2))
                holder.counter.setTextColor(if(mState==Const.USERSTATE.OWNER){
                    ContextCompat.getColor(mContext, R.color.owner_red)
                }else {
                    ContextCompat.getColor(mContext, R.color.taker_blue)
                })
                holder.counter.setFirstColor(if(mState==Const.USERSTATE.OWNER){
                    ContextCompat.getColor(mContext, R.color.owner_red)
                }else {
                    ContextCompat.getColor(mContext, R.color.taker_blue)
                })
                holder.counter.setBackgroundColor(
                    ContextCompat.getColor(mContext, R.color.colorMainBrightWhite)
                )
                try {
                    var last = (curMission.finish_time!!.toBigDecimal()
                        .toLong() - System.currentTimeMillis())
                    holder.counter.setRange(curMission.duration*20)
                    if (holder.timer != null) {
                        holder.timer!!.cancel()
                    }
                    holder.timer = object:CountDownTimer(last, 1000){
                        override fun onFinish() {
                            holder.counter.setValues(0, 0, 0)
                            curMission.finish_time=""
                        }

                        override fun onTick(millisUntilFinished: Long) {
                            holder.counter.setValues(millisUntilFinished.toInt(), 0, 0)
                        }

                    }.start()

                } catch (e: NumberFormatException) {
                    Log.e("TIMMER: "+e)
                    //時間格式不統一 造成資料轉換錯誤 且server資料無法更改
                }


                //status
                Log.d("job_status:"+curMission.job_status)
                when(curMission.job_status){
                    0->{
                        if(curMission.modify_apply>=0){
                            holder.tv_mission_status.text ="尚未同意"
                            holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_init)
                        }else{
                            holder.tv_mission_status.text ="未有人接"
                            holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_pending)
                        }

                    }
                    1->{
                        holder.tv_mission_status.text ="進行中"
                        holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_ongoing)
                        //TODO 有人提出修改或撤銷要在此調整
//                        if(curMission.modify_apply>=0)
//                            holder.rl_reason.visibility=View.VISIBLE
                    }
                    2->{//已完成
                        holder.tv_mission_status.text ="已完成"
                        holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_ongoing)
                    }
                    3->{
                        holder.tv_mission_status.text ="失敗"
                        holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_fail)
                    }
                    4,5->{
                        holder.tv_mission_status.text ="已取消"
                        holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_cancel)
                    }
                }
            }

            TYPE_NOTIFICATION -> {
                holder as NotificationListViewHolder
//                holder.tv_noti_type.text =
//                    mContext!!.resources.getStringArray(R.array.mission_status)[curMission.job_status]
                holder.tv_item_mission_title.text = curMission.title
                if(curMission.job_owner== usetData?.userId) {
                    holder.tv_noti_type.setTextColor(ContextCompat.getColor(mContext, R.color.owner_red))
                }else{
                    holder.tv_noti_type.setTextColor(ContextCompat.getColor(mContext, R.color.taker_blue))
                }

                when (curMission.job_status) {
                    //任務建立
                    0 -> {
                        if (curMission.modify_apply >= 0) {
                            holder.tv_noti_type.text = "任務申請"
                            if (curMission.modify_apply != usetData?.userId) {//他人提出申請
                                holder.tv_status_subtitle.text = "申請人："
                                holder.tv_status_subcontent.text = curMission.taker_firstname+" "+curMission.taker_lastname
                                holder.ll_more_info.visibility = View.VISIBLE
                                holder.rl_modify_bottons.visibility = View.VISIBLE
                                holder.tv_more_info_content.visibility = View.GONE
                            } else {//自己向別人提出申請
                                holder.tv_status_subtitle.text = "受理狀態："
                                holder.tv_status_subcontent.text = "等待同意中"
                                holder.rl_modify_bottons.visibility = View.GONE
                                holder.tv_more_info_content.visibility = View.GONE
                            }
                        }
                    }
                    //任務已承接
                    1 -> {
                        if (curMission.modify_apply >= 0) {
                            holder.tv_status_subtitle.visibility = View.VISIBLE
                            holder.tv_status_subcontent.visibility = View.VISIBLE
                            holder.tv_noti_type.text="任務修改申請"
                            if (curMission.modify_apply != usetData?.userId) {//他人提出申請
                                holder.tv_status_subtitle.text = "申請人："
                                holder.tv_status_subcontent.text = curMission.taker_firstname+" "+curMission.taker_lastname
                                holder.ll_more_info.visibility=View.VISIBLE
                                holder.tv_more_info_title.visibility=View.VISIBLE
                                holder.tv_more_info_content.visibility = View.VISIBLE
                                holder.tv_more_info_content.text="酬勞金額由20元修改至30元"
                                holder.rl_modify_bottons.visibility = View.VISIBLE
                            } else {//自己提出申請 等待同意
                                holder.tv_status_subtitle.text = "受理狀態："
                                holder.tv_status_subcontent.text = "等待同意中"
                                holder.rl_modify_bottons.visibility = View.GONE
                                holder.tv_more_info_content.visibility = View.GONE
                            }
                        }
                    }
                    //任務完成
                    2 -> {

                    }
                    //任務逾期
                    3 -> {
                        holder.tv_noti_type.text="任務失敗通知"
                        holder.tv_status_subtitle.visibility = View.GONE
                        holder.tv_status_subcontent.visibility = View.GONE
                        holder.ll_more_info.visibility=View.VISIBLE
			            holder.rl_modify_bottons.visibility = View.GONE
                        holder.tv_more_info_title.visibility = View.GONE
                        holder.tv_more_info_content.visibility = View.VISIBLE
                        holder.tv_more_info_content.text="請前往任務填寫任務失敗表單"
                    }
                    //取消通知
                    4 -> {
                        holder.tv_noti_type.text="任務失敗通知"
                        holder.tv_status_subtitle.visibility = View.GONE
                        holder.tv_status_subcontent.visibility = View.GONE
                        holder.ll_more_info.visibility=View.VISIBLE
			            holder.rl_modify_bottons.visibility = View.GONE
                        holder.tv_more_info_title.visibility = View.GONE
                        holder.tv_more_info_content.visibility = View.VISIBLE
                        holder.tv_more_info_content.text="請前往任務填寫任務失敗表單"
                    }
                    //取消通知
                    5 -> {
                        holder.tv_noti_type.text="任務失敗通知"
                        holder.tv_status_subtitle.visibility = View.GONE
                        holder.tv_status_subcontent.visibility = View.GONE
                        holder.ll_more_info.visibility=View.VISIBLE
		                holder.rl_modify_bottons.visibility = View.GONE
                        holder.tv_more_info_title.visibility = View.GONE
                        holder.tv_more_info_content.visibility = View.VISIBLE
                        holder.tv_more_info_content.text="請前往任務填寫任務失敗表單"
                    }
                    //任務修改申請
                    6 -> {
                        Log.d(curMission.title + " | modify_apply: "+curMission.modify_apply +" | job_owner: "+curMission.job_owner + " | user: "+usetData?.userId)
                        holder.tv_more_info_title.visibility = View.VISIBLE
                        holder.tv_status_subtitle.visibility = View.VISIBLE
                        holder.tv_status_subcontent.visibility = View.VISIBLE
                        holder.rl_modify_bottons.visibility = View.VISIBLE
                        holder.tv_more_info_content.visibility = View.GONE
                    }
                    //soon to expire
                    7 -> {
                    }
                    //wait for accept
                    8 -> {
                        holder.rl_modify_bottons.visibility = View.VISIBLE
                    }
                }


            }

            TYPE_FIN_LIST->{
                holder as MissionFinListViewHolder
                holder.tvItemMissionTitle.text = curMission.title
                holder.tv_item_taker_content.text = curMission.taker_firstname+" "+curMission.taker_lastname
                Log.e("job_status:"+curMission.job_status)
                when(curMission.job_status){
                    2->{//已完成
                        holder.tv_mission_status.text ="已完成"
                        holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_ongoing)
                    }
                    3->{
                        holder.tv_mission_status.text ="失敗"
                        holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_fail)
                    }
                    4,5->{
                        holder.tv_mission_status.text ="已取消"
                        holder.tv_mission_status.background = ContextCompat.getDrawable(mContext, R.drawable.mission_status_bg_cancel)
                    }
                }

            }

        }


        holder.itemView.setOnClickListener(this)
        holder.itemView.tag = position
    }

    @Suppress("UNCHECKED_CAST")
    override fun setData(clusterList: List<*>) {
        missionList = clusterList as List<Mission>
        notifyDataSetChanged()
    }


    fun getSelectedMission(position: Int): Mission {
        return missionList!![position]
    }

    @Suppress("UNCHECKED_CAST")
    override fun setOnItemClickListener(listener: IOnItemClickListener<*>) {
        this.mOnItemClickListener = listener as IOnItemClickListener<Mission>
    }

    fun updateUserState(state:Const.USERSTATE) {
        mState = state
        notifyDataSetChanged()
    }


}