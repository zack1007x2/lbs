package com.app.lbs.adapters

import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.interfaces.IOnItemClickListener

/**
 * @file AItemListAdapter
 * @brief
 *       抽象化List Adapter 統一接口
 *       使得Mission 和 Store List可以在clusterRenderer中共用
 *
 * @author Zack
 * */
abstract class AItemListAdapter<T: RecyclerView.ViewHolder> :  RecyclerView.Adapter<T> (){

    abstract fun setOnItemClickListener(listener: IOnItemClickListener<*>)
    abstract fun setData(clusterList: List<*>)
}