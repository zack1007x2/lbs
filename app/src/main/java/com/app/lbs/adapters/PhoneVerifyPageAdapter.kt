package com.app.lbs.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

/**
 * @file PhoneVerifyPageAdapter
 * @brief
 *       電話驗證分頁管理
 *
 * @author Zack
 * */

class PhoneVerifyPageAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    private var fragments: ArrayList<Fragment>?=null


    fun setFragmentsList(frag: ArrayList<Fragment>){
        fragments= frag
    }

    override fun getItemCount(): Int {
         return fragments!!.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments!![position]
    }
}