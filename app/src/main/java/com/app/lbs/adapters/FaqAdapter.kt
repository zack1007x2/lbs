package com.app.lbs.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.lbs.R
import com.app.lbs.utils.MyLog


/**
 * @file FaqAdapter
 * @brief
 *       FAQ問題項目 將對應的問題和解答分發到item中顯示
 * @param
 *       mQuestList:問題list
 *       mAnsList:解答list
 *       會依序顯示問題與答案 eg.Q[0], A[0]在Q&A中會是一組
 *
 * @author Zack
 * */
class FaqAdapter(private val context: Context?, private val mQuestList: List<String>?, private val mAnsList: List<String>? ): RecyclerView.Adapter<FaqAdapter.faqViewHolder>() {

    private var Log = MyLog.log()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqAdapter.faqViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_faq, parent, false)
        return faqViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: faqViewHolder, position: Int) {
        val que = mQuestList!![position]
        val ans = mAnsList!![position]
        holder.mQuest!!.text = que
        holder.mAns!!.text = ans
    }

    override fun getItemCount(): Int {
        return mQuestList!!.size
    }


    inner class faqViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {

        var mQuest: TextView? = null
        var mAns: TextView? = null

        init {
            mQuest = view.findViewById(R.id.tv_faq_question)
            mAns = view.findViewById(R.id.tv_faq_ans)
        }
    }

    fun getItem(id: Int): String {
        return mQuestList!![id]
    }
}