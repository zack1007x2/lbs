package com.app.lbs.adapters

import android.content.Context
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.app.lbs.R
import com.app.lbs.ui.fragments.MissionApplyingFragment
import com.app.lbs.ui.fragments.MissionHistoryFragment
import com.app.lbs.ui.fragments.MissionOnGoingFragment
import com.app.lbs.utils.MyLog

/**
 * @file MissionListPagerAdapterTaker
 * @brief
 *      Mission Management頁面的分頁選項
 *
 * @author Zack
 * */

private val TAB_TITLES_TAKER = arrayOf(
    R.string.title_mission_ongoing,
    R.string.title_mission_apply,
    R.string.title_mission_history
)

class MissionListPagerAdapterTaker(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var mfm = fm
    private var mMissionApplyingFragment: MissionApplyingFragment = MissionApplyingFragment()
    private var mMissionOnGoingFragment: MissionOnGoingFragment = MissionOnGoingFragment()
    private var mMissionHistoryFragment: MissionHistoryFragment = MissionHistoryFragment()
    private var Log = MyLog.log()

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Log.d("getItem : " + position)
        return when (position) {
            0 -> mMissionOnGoingFragment
            1 -> mMissionApplyingFragment
            2 -> mMissionHistoryFragment
            else -> mMissionApplyingFragment
        }


    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES_TAKER[position])
    }

    override fun getCount(): Int {
        return TAB_TITLES_TAKER.size
    }

    override fun getItemPosition(ob: Any): Int {
        return POSITION_NONE
    }
    override fun saveState(): Parcelable? {
        return null
    }

}