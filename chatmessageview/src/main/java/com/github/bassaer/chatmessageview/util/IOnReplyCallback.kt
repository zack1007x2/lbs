package com.github.bassaer.chatmessageview.util

import com.github.bassaer.chatmessageview.model.Message

interface IOnReplyCallback {
    fun onReply(msg: Message)
}