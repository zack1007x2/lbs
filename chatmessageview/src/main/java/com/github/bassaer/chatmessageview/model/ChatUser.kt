package com.github.bassaer.chatmessageview.model

class ChatUser(internal var id: Int?, internal var name: String, private var iconPath: String) : IChatUser {

    override fun getId(): String {
        return this.id!!.toString()
    }

    override fun getName(): String? {
        return this.name
    }

    override fun getIconPath(): String? {
        return this.iconPath
    }

}
