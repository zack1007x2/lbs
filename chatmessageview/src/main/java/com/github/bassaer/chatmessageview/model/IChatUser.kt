package com.github.bassaer.chatmessageview.model

interface IChatUser {
    fun getId(): String
    fun getName(): String?
    fun getIconPath(): String?
}
