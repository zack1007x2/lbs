package com.github.jjobes.slidedatetimepicker;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * <p>The {@code DialogFragment} that contains the {@link SlidingTabLayout}
 * and {@link CustomViewPager}.</p>
 *
 * <p>The {@code CustomViewPager} contains the {@link TimerFragment}.</p>
 *
 * <p>This {@code DialogFragment} is managed by {@link SlideTimerPicker}.</p>
 *
 * @author jjobes
 */
public class SlideTimerDialogFragment extends DialogFragment implements TimerFragment.TimeChangedListener {
    public static final String TAG_SLIDE_TIMER_DIALOG_FRAGMENT = "tagSlideTimerDialogFragment";

    private static SlideTimerSetListener mListener;

    private Context mContext;
    private CustomViewPager mViewPager;
    private TimerViewPagerAdapter mViewPagerAdapter;
    private SlidingTabLayout mSlidingTabLayout;
    private View mButtonHorizontalDivider;
    private View mButtonVerticalDivider;
    private Button mOkButton;
    private Button mCancelButton;
    private int mTheme;
    private int mIndicatorColor;
    private boolean mIsClientSpecified24HourTime;
    private Long mCurTimeDura=0L;
    private TimerFragment mTimeFragment;

    public SlideTimerDialogFragment() {
        // Required empty public constructor
    }

    /**
     * <p>Return a new instance of {@code SlideTimerDialogFragment} with its bundle
     * filled with the incoming arguments.</p>
     *
     * <p>Called by {@link SlideTimerPicker#show()}.</p>
     *
     * @param listener
     * @param isClientSpecified24HourTime
     * @param is24HourTime
     * @param theme
     * @param indicatorColor
     * @return
     */
    public static SlideTimerDialogFragment newInstance(SlideTimerSetListener listener,
                                                       boolean isClientSpecified24HourTime,
                                                       boolean is24HourTime, int theme, int indicatorColor) {
        mListener = listener;

        // Create a new instance of SlideTimerDialogFragment
        SlideTimerDialogFragment dialogFragment = new SlideTimerDialogFragment();

        // Store the arguments and attach the bundle to the fragment
        Bundle bundle = new Bundle();
        bundle.putBoolean("isClientSpecified24HourTime", isClientSpecified24HourTime);
        bundle.putBoolean("is24HourTime", is24HourTime);
        bundle.putInt("theme", theme);
        bundle.putInt("indicatorColor", indicatorColor);
        dialogFragment.setArguments(bundle);

        // Return the fragment with its bundle
        return dialogFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        unpackBundle();

        switch (mTheme) {
            case SlideTimerPicker.HOLO_DARK:
                setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Dialog_NoActionBar);
                break;
            case SlideTimerPicker.HOLO_LIGHT:
                setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                break;
            default:  // if no theme was specified, default to holo light
                setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.slide_date_time_picker, container);

        setupViews(view);
        customizeViews();
        initViewPager();
        initTabs();
        initButtons();

        return view;
    }

    @Override
    public void onDestroyView() {
        // Workaround for a bug in the compatibility library where calling
        // setRetainInstance(true) does not retain the instance across
        // orientation changes.
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }

        super.onDestroyView();
    }

    private void unpackBundle() {
        Bundle args = getArguments();

        mIsClientSpecified24HourTime = args.getBoolean("isClientSpecified24HourTime");
        mTheme = args.getInt("theme");
        mIndicatorColor = args.getInt("indicatorColor");
    }

    private void setupViews(View v) {
        mViewPager = v.findViewById(R.id.viewPager);
        mSlidingTabLayout = v.findViewById(R.id.slidingTabLayout);
        mButtonHorizontalDivider = v.findViewById(R.id.buttonHorizontalDivider);
        mButtonVerticalDivider = v.findViewById(R.id.buttonVerticalDivider);
        mOkButton = v.findViewById(R.id.okButton);
        mCancelButton = v.findViewById(R.id.cancelButton);
    }

    private void customizeViews() {
        int lineColor = mTheme == SlideTimerPicker.HOLO_DARK ?
                getResources().getColor(R.color.gray_holo_dark) :
                getResources().getColor(R.color.gray_holo_light);

        // Set the colors of the horizontal and vertical lines for the
        // bottom buttons depending on the theme.
        switch (mTheme) {
            case SlideTimerPicker.HOLO_LIGHT:
            case SlideTimerPicker.HOLO_DARK:
                mButtonHorizontalDivider.setBackgroundColor(lineColor);
                mButtonVerticalDivider.setBackgroundColor(lineColor);
                break;

            default:  // if no theme was specified, default to holo light
                mButtonHorizontalDivider.setBackgroundColor(getResources().getColor(R.color.gray_holo_light));
                mButtonVerticalDivider.setBackgroundColor(getResources().getColor(R.color.gray_holo_light));
        }

        // Set the color of the selected tab underline if one was specified.
        if (mIndicatorColor != 0)
            mSlidingTabLayout.setSelectedIndicatorColors(mIndicatorColor);
    }

    private void initViewPager() {
        mViewPagerAdapter = new TimerViewPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.setIsOnePage(true);

        mSlidingTabLayout.setCustomTabView(R.layout.custom_tab, R.id.tabText);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private void initTabs() {
        // Set initial time on time tab
        updateTimeTab();
    }

    private void initButtons() {
        mOkButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mListener == null) {
                    throw new NullPointerException(
                            "Listener no longer exists for mOkButton");
                }

                mListener.onTimerSet(mCurTimeDura);

                dismiss();
            }
        });

        mCancelButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mListener == null) {
                    throw new NullPointerException(
                            "Listener no longer exists for mCancelButton");
                }

                mListener.onTimerCancel();

                dismiss();
            }
        });
    }

    /**
     * <p>The callback used by the TimePicker to update {@code mCalendar} as
     * the user changes the time. Each time this is called, we also update
     * the text on the time tab to reflect the time the user has currenly
     * selected.</p>
     *
     * <p>Implements the {@link TimerFragment.TimeChangedListener}
     * interface.</p>
     */
    private int cur_hour = 0;
    private int cur_minute = 0;

    @Override
    public void onTimeChanged(int hour, int minute) {
        cur_hour = hour;
        cur_minute = minute;
        mCurTimeDura = (long) (cur_hour * 60 * 60 + cur_minute * 60);
        updateTimeTab();
    }

    private void updateTimeTab() {
        if (mIsClientSpecified24HourTime) {
            //mCurTimeDura.toString()
            mSlidingTabLayout.setTabText(0, cur_hour+" 小時   :   "+cur_minute+" 分鐘");

        } else  // display time using the device's default 12/24 hour format preference
        {
            mSlidingTabLayout.setTabText(0, DateFormat.getTimeFormat(
                    mContext).format(mCurTimeDura));
        }
    }

    /**
     * <p>Called when the user clicks outside the dialog or presses the <b>Back</b>
     * button.</p>
     *
     * <p><b>Note:</b> Actual <b>Cancel</b> button clicks are handled by {@code mCancelButton}'s
     * event handler.</p>
     */
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        if (mListener == null) {
            throw new NullPointerException(
                    "Listener no longer exists in onCancel()");
        }

        mListener.onTimerCancel();
    }
    private class TimerViewPagerAdapter extends FragmentPagerAdapter {
        public TimerViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                mTimeFragment = TimerFragment.newInstance(
                        mTheme,
                        0,
                        0,
                        mIsClientSpecified24HourTime,
                        true);
                /**
                 * bug:
                 * timeFragment.setTargetFragment(SlideDateTimeDialogFragment.this, 100);
                 **/
                return mTimeFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 1;
        }
    }
}
